/*
 * This file is part of Cleanflight and Betaflight.
 *
 * Cleanflight and Betaflight are free software. You can redistribute
 * this software and/or modify this software under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Cleanflight and Betaflight are distributed in the hope that they
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.
 *
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <math.h>

#include "platform.h"

#include "axis.h"
#include "maths.h"

#if defined(FAST_MATH) || defined(VERY_FAST_MATH)
#if defined(VERY_FAST_MATH)

// http://lolengine.net/blog/2011/12/21/better-function-approximations
// Chebyshev http://stackoverflow.com/questions/345085/how-do-trigonometric-functions-work/345117#345117
// Thanks for ledvinap for making such accuracy possible! See: https://github.com/cleanflight/cleanflight/issues/940#issuecomment-110323384
// https://github.com/Crashpilot1000/HarakiriWebstore1/blob/master/src/mw.c#L1235
// sin_approx maximum absolute error = 2.305023e-06
// cos_approx maximum absolute error = 2.857298e-06
#define sinPolyCoef3 -1.666568107e-1f
#define sinPolyCoef5  8.312366210e-3f
#define sinPolyCoef7 -1.849218155e-4f
#define sinPolyCoef9  0
#else
#define sinPolyCoef3 -1.666665710e-1f                                          // Double: -1.666665709650470145824129400050267289858e-1
#define sinPolyCoef5  8.333017292e-3f                                          // Double:  8.333017291562218127986291618761571373087e-3
#define sinPolyCoef7 -1.980661520e-4f                                          // Double: -1.980661520135080504411629636078917643846e-4
#define sinPolyCoef9  2.600054768e-6f                                          // Double:  2.600054767890361277123254766503271638682e-6
#endif
FAST_CODE FAST_CODE_NOINLINE float sin_approx(register float x) {
    register float
    x2 = (
        fabsf(x = fmodf(x, M_PI * 2e0)) > M_PI_2f ?
            x = copysignf(M_PI, x) - x : x
    );
    x2*= x2;

    return x + x * x2 * (sinPolyCoef3 + x2 * (sinPolyCoef5 + x2 * (sinPolyCoef7 + x2 * sinPolyCoef9)));
}

// Initial implementation by Crashpilot1000 (https://github.com/Crashpilot1000/HarakiriWebstore1/blob/396715f73c6fcf859e0db0f34e12fe44bace6483/src/mw.c#L1292)
// Polynomial coefficients by Andor (http://www.dsprelated.com/showthread/comp.dsp/21872-1.php) optimized by Ledvinap to save one multiplication
// Max absolute error 0,000027 degree
// atan2_approx maximum absolute error = 7.152557e-07 rads (4.098114e-05 degree)
FAST_CODE FAST_CODE_NOINLINE float atan2_approx(
    register float const y,
    register float const x
) {
    #define atanPolyCoef1  3.14551665884836e-07f
    #define atanPolyCoef2  0.99997356613987f
    #define atanPolyCoef3  0.14744007058297684f
    #define atanPolyCoef4  0.3099814292351353f
    #define atanPolyCoef5  0.05030176425872175f
    #define atanPolyCoef6  0.1471039133652469f
    #define atanPolyCoef7  0.6444640676891548f

    register float res;

{   register float const
    absY = fabsf(y),
    absX = fabsf(x);

    if (isnormal(res = fmaxf(absY, absX)))
        res = fminf(absY, absX) / res;

    res =
        (   (   (   (res *-atanPolyCoef5 + atanPolyCoef4) * res
                        + atanPolyCoef3
                ) * res + atanPolyCoef2
            ) * res + atanPolyCoef1
        ) / ((res * atanPolyCoef7 + atanPolyCoef6) * res + 1.f);

    if (absY > absX)
        res = M_PI_2 - res;
}
    #undef atanPolyCoef1
    #undef atanPolyCoef2
    #undef atanPolyCoef3
    #undef atanPolyCoef4
    #undef atanPolyCoef5
    #undef atanPolyCoef6
    #undef atanPolyCoef7

    if (signbit(x))
        res = M_PI - res;
    if (signbit(y))
        return-res;

    return res;
}

FAST_CODE_NOINVOKE float asin_approx(register float const x) {
    // See equation 15 of <https://mathworld.wolfram.com/InverseSine.html>
    return atan2_approx(x, opposf(true, x * x));
}

FAST_CODE_NOINVOKE float acos_approx(register float const x) {
    // See equation 16 of <https://mathworld.wolfram.com/InverseCosine.html>
    return atan2_approx(opposf(true, x * x), x);
}
#endif

FAST_CODE_NOINVOKE int32_t applyDeadband(
    register int32_t const value,
    register int32_t const deadband
) { return
      value > deadband ?
        value - deadband
    : value <-deadband ?
        value + deadband
    :
        0l;
}

FAST_CODE_NOINVOKE float fapplyDeadband(
    register float const value,
    register float const deadband
) {
    return copysignf(fdimf(fabsf(value), deadband), value);
}

void devPush(register stdev_t dev[restrict const], register float const x) {
    if (++dev->m_n== 1.f)
        dev->m_newM = x,
        dev->m_newS = 0.;
    else
        dev->m_newS+= ISRVALUE({
            register float const
            DELTA = x - dev->m_newM;

            (x - (dev->m_newM+= DELTA / dev->m_n)) * DELTA;
        });
}

FAST_CODE_NOINVOKE float devVariance(
    register stdev_t const dev[restrict const]
) {
    return dev->m_n > 1.f ? dev->m_newS / (dev->m_n - 1.f) : 0.f;
}

int scaleRange(int x, int srcFrom, int srcTo, int destFrom, int destTo) {
    long int a = ((long int) destTo - (long int) destFrom) * ((long int) x - (long int) srcFrom);
    long int b = (long int) srcTo - (long int) srcFrom;
    return (a / b) + destFrom;
}

float scaleRangef(float x, float srcFrom, float srcTo, float destFrom, float destTo) {
    float a = (destTo - destFrom) * (x - srcFrom);
    float b = srcTo - srcFrom;
    return (a / b) + destFrom;
}

// Quick median filter implementation
// (c) N. Devillard - 1998
// http://ndevilla.free.fr/median/median.pdf
#define QMF_SORT(a,b) { if ((a)>(b)) QMF_SWAP((a),(b)); }
#define QMF_SWAP(a,b) { int32_t temp=(a);(a)=(b);(b)=temp; }
#define QMF_COPY(p,v,n) { int32_t i; for (i=0; i<n; i++) p[i]=v[i]; }
#define QMF_SORTF(a,b) { if ((a)>(b)) QMF_SWAPF((a),(b)); }
#define QMF_SWAPF(a,b) { float temp=(a);(a)=(b);(b)=temp; }

int32_t quickMedianFilter3(int32_t * v) {
    int32_t p[3];
    QMF_COPY(p, v, 3);
    QMF_SORT(p[0], p[1]);
    QMF_SORT(p[1], p[2]);
    QMF_SORT(p[0], p[1]) ;
    return p[1];
}

int32_t quickMedianFilter5(int32_t * v) {
    int32_t p[5];
    QMF_COPY(p, v, 5);
    QMF_SORT(p[0], p[1]);
    QMF_SORT(p[3], p[4]);
    QMF_SORT(p[0], p[3]);
    QMF_SORT(p[1], p[4]);
    QMF_SORT(p[1], p[2]);
    QMF_SORT(p[2], p[3]);
    QMF_SORT(p[1], p[2]);
    return p[2];
}

int32_t quickMedianFilter7(int32_t * v) {
    int32_t p[7];
    QMF_COPY(p, v, 7);
    QMF_SORT(p[0], p[5]);
    QMF_SORT(p[0], p[3]);
    QMF_SORT(p[1], p[6]);
    QMF_SORT(p[2], p[4]);
    QMF_SORT(p[0], p[1]);
    QMF_SORT(p[3], p[5]);
    QMF_SORT(p[2], p[6]);
    QMF_SORT(p[2], p[3]);
    QMF_SORT(p[3], p[6]);
    QMF_SORT(p[4], p[5]);
    QMF_SORT(p[1], p[4]);
    QMF_SORT(p[1], p[3]);
    QMF_SORT(p[3], p[4]);
    return p[3];
}

int32_t quickMedianFilter9(int32_t * v) {
    int32_t p[9];
    QMF_COPY(p, v, 9);
    QMF_SORT(p[1], p[2]);
    QMF_SORT(p[4], p[5]);
    QMF_SORT(p[7], p[8]);
    QMF_SORT(p[0], p[1]);
    QMF_SORT(p[3], p[4]);
    QMF_SORT(p[6], p[7]);
    QMF_SORT(p[1], p[2]);
    QMF_SORT(p[4], p[5]);
    QMF_SORT(p[7], p[8]);
    QMF_SORT(p[0], p[3]);
    QMF_SORT(p[5], p[8]);
    QMF_SORT(p[4], p[7]);
    QMF_SORT(p[3], p[6]);
    QMF_SORT(p[1], p[4]);
    QMF_SORT(p[2], p[5]);
    QMF_SORT(p[4], p[7]);
    QMF_SORT(p[4], p[2]);
    QMF_SORT(p[6], p[4]);
    QMF_SORT(p[4], p[2]);
    return p[4];
}

float quickMedianFilter3f(float * v) {
    float p[3];
    QMF_COPY(p, v, 3);
    QMF_SORTF(p[0], p[1]);
    QMF_SORTF(p[1], p[2]);
    QMF_SORTF(p[0], p[1]) ;
    return p[1];
}

float quickMedianFilter5f(float * v) {
    float p[5];
    QMF_COPY(p, v, 5);
    QMF_SORTF(p[0], p[1]);
    QMF_SORTF(p[3], p[4]);
    QMF_SORTF(p[0], p[3]);
    QMF_SORTF(p[1], p[4]);
    QMF_SORTF(p[1], p[2]);
    QMF_SORTF(p[2], p[3]);
    QMF_SORTF(p[1], p[2]);
    return p[2];
}

float quickMedianFilter7f(float * v) {
    float p[7];
    QMF_COPY(p, v, 7);
    QMF_SORTF(p[0], p[5]);
    QMF_SORTF(p[0], p[3]);
    QMF_SORTF(p[1], p[6]);
    QMF_SORTF(p[2], p[4]);
    QMF_SORTF(p[0], p[1]);
    QMF_SORTF(p[3], p[5]);
    QMF_SORTF(p[2], p[6]);
    QMF_SORTF(p[2], p[3]);
    QMF_SORTF(p[3], p[6]);
    QMF_SORTF(p[4], p[5]);
    QMF_SORTF(p[1], p[4]);
    QMF_SORTF(p[1], p[3]);
    QMF_SORTF(p[3], p[4]);
    return p[3];
}

float quickMedianFilter9f(float * v) {
    float p[9];
    QMF_COPY(p, v, 9);
    QMF_SORTF(p[1], p[2]);
    QMF_SORTF(p[4], p[5]);
    QMF_SORTF(p[7], p[8]);
    QMF_SORTF(p[0], p[1]);
    QMF_SORTF(p[3], p[4]);
    QMF_SORTF(p[6], p[7]);
    QMF_SORTF(p[1], p[2]);
    QMF_SORTF(p[4], p[5]);
    QMF_SORTF(p[7], p[8]);
    QMF_SORTF(p[0], p[3]);
    QMF_SORTF(p[5], p[8]);
    QMF_SORTF(p[4], p[7]);
    QMF_SORTF(p[3], p[6]);
    QMF_SORTF(p[1], p[4]);
    QMF_SORTF(p[2], p[5]);
    QMF_SORTF(p[4], p[7]);
    QMF_SORTF(p[4], p[2]);
    QMF_SORTF(p[6], p[4]);
    QMF_SORTF(p[4], p[2]);
    return p[4];
}

void arraySubInt32(int32_t *dest, int32_t *array1, int32_t *array2, int count) {
    for (int i = 0; i < count; i++) {
        dest[i] = array1[i] - array2[i];
    }
}

int16_t qPercent(fix12_t q) {
    return (100 * q) >> 12;
}

int16_t qMultiply(fix12_t q, int16_t input) {
    return (input *  q) >> 12;
}

fix12_t  qConstruct(int16_t num, int16_t den) {
    return (num << 12) / den;
}

// quaternions
FAST_CODE_NOINVOKE void quaternionTransformVectorBodyToEarth(
    register quaternion       * const qVector,
    register quaternion const * const qReference
) {
    quaternionMultiply(
        qReference
    , (
        qVector->w = 0.,
        qVector
    ),
        qVector
    );
    quaternionMultiply(
        qVector
    ,
        (quaternion const[1]){ {
           -qReference->x,-qReference->y,-qReference->z, qReference->w
        } }
    ,
        qVector
    );
}

FAST_CODE_NOINVOKE void quaternionTransformVectorEarthToBody(
    register quaternion       * const qVector,
    register quaternion const * const qReference
) {
    quaternionMultiply((
        qVector->w = 0.,
        qVector
    ),
        (quaternion const[1]){ {
           -qReference->x,-qReference->y,-qReference->z, qReference->w
        } }
    ,
        qVector
    );
    quaternionMultiply(qVector, qReference, qVector);
}

FAST_CODE FAST_CODE_NOINLINE void quaternionMultiply(
    register quaternion const * const l,
    register quaternion const * const r,
    register quaternion       * const o
) {
   *o = (quaternion){
        l->w * r->x + l->x * r->w + l->y * r->z - l->z * r->y
    ,
        l->w * r->y - l->x * r->z + l->y * r->w + l->z * r->x
    ,
        l->w * r->z + l->x * r->y - l->y * r->x + l->z * r->w
    ,
        l->w * r->w - l->x * r->x - l->y * r->y - l->z * r->z
    };
}

FAST_CODE FAST_CODE_NOINLINE void quaternionNormalize(
    register quaternion * restrict const q
) {
    register float const
    NORM = quaternionModulus(q);

    if (isnormal(NORM))
        q->x/= NORM,
        q->y/= NORM,
        q->z/= NORM,
        q->w/= NORM;
}

FAST_CODE_NOINVOKE void quaternionAdd(
    register quaternion const * const l,
    register quaternion const * const r,
    register quaternion       * const o
) {
    o->x = l->x + r->x;
    o->y = l->y + r->y;
    o->z = l->z + r->z;
    o->w = l->w + r->w;
}

FAST_CODE_NOINVOKE void quaternionConjugate(
    register quaternion const * const i,
    register quaternion       * const o
) {
    o->w = + i->w;
    o->x = - i->x;
    o->y = - i->y;
    o->z = - i->z;
}

FAST_CODE_NOINVOKE float quaternionDotProduct(
    register quaternion const * const l,
    register quaternion const * const r
) {
    return (l->w * r->w + l->x * r->x + l->y * r->y + l->z * r->z);
}

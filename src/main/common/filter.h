/*
 * This file is part of Cleanflight and Betaflight.
 *
 * Cleanflight and Betaflight are free software. You can redistribute
 * this software and/or modify this software under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Cleanflight and Betaflight are distributed in the hope that they
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.
 *
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <float.h>

#include "common/maths.h"

#define BIQUAD_Q ((float)M_SQRT1_2)

typedef struct pt1FilterState_s {
    float input, level;
} pt1FilterState_t;

typedef struct pt1Filter_s {
    float            k;
    pt1FilterState_t state;
} pt1Filter_t;

/* this holds the data required to update samples thru a filter */
typedef struct biquadFilter_s {
    float b0, b1, b2, a1, a2;
    float x1, x2, y1, y2;
} biquadFilter_t;

#ifdef USE_SMITH_PREDICTOR
typedef struct alphaBetaGammaFilter_s {
    float a, b, g, e;
    float dT, dT2, dT3;
    float halfLife, boost;
    struct {
        float r, v, a, j, x;
    } k;
} alphaBetaGammaFilter_t;
#endif

typedef struct ptnFilter_s {
    float state[5];
    float k;
    uint8_t order;
} ptnFilter_t;

typedef enum {
    FILTER_PT1 = 0,
    FILTER_BIQUAD,
    FILTER_PT2,
    FILTER_PT3,
    FILTER_PT4,
} lowpassFilterType_e;

typedef enum {
    FILTER_LPF,    // 2nd order Butterworth section
    FILTER_NOTCH,
    FILTER_BPF,
} biquadFilterType_e;


typedef float (* filterApplyFnPtr)(void *, float);

float pt1FilterMaxK(float);
float pt1FilterGain(float, float);
float pt1FilterFreq(pt1Filter_t const[], float);
#define pt1FilterInit(x, y) (*(pt1Filter_t *)(x) = (pt1Filter_t){ (y) })
#define pt1FilterUpdateCutoff(x, y) (((pt1Filter_t *)(x))->k = (y))
float pt1FilterReuse(pt1Filter_t[], float);
float pt1FilterApply(pt1Filter_t[], float);
float pt1FilterLevel(pt1Filter_t const[]);
float pt1FilterTrend(pt1Filter_t const[]);

void biquadFilterUpdate(
    biquadFilter_t[], float, float, float, biquadFilterType_e
);
#define biquadFilterUpdateLPF(p, x, y) (\
    biquadFilterUpdate((p), (x), (y), BIQUAD_Q, FILTER_LPF)\
)
#define biquadFilterInit(p, x, y, z, k) (\
    biquadFilterUpdate(\
        memset((p), false, sizeof(biquadFilter_t)), (x), (y), (z), (k)\
    )\
)
#define biquadFilterInitLPF(p, x, y) (\
    biquadFilterInit((p), (x), (y), BIQUAD_Q, FILTER_LPF)\
)

float biquadFilterApplyDF1(biquadFilter_t[], float);
float biquadFilterApply(biquadFilter_t[], float);
float filterGetNotchQ(float centerFreq, float cutoffFreq);

#ifdef USE_SMITH_PREDICTOR
void ABGInit(alphaBetaGammaFilter_t[], float, uint16_t, uint16_t, float);
float alphaBetaGammaApply(alphaBetaGammaFilter_t[], float);
#endif

void ptnFilterInit(ptnFilter_t[], uint8_t, float, float);
void ptnFilterUpdate(ptnFilter_t[], float, float, float);
float ptnFilterApply(ptnFilter_t[], float);

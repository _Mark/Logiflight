/*
 * This file is part of Cleanflight and Betaflight.
 *
 * Cleanflight and Betaflight are free software. You can redistribute
 * this software and/or modify this software under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Cleanflight and Betaflight are distributed in the hope that they
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.
 *
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include <string.h>
#include <stdint.h>

#include "streambuf.h"

void sbufFill(sbuf_t *dst, uint8_t data, int len) {
    memset(dst->ptr, data, len);
    dst->ptr += len;
}

void sbufWriteData(sbuf_t *dst, const void *data, int len) {
    memcpy(dst->ptr, data, len);
    dst->ptr += len;
}

void sbufWriteString(sbuf_t *dst, const char *string) {
    sbufWriteData(dst, string, strlen(string));
}

void sbufWriteStringWithZeroTerminator(sbuf_t *dst, const char *string) {
    sbufWriteData(dst, string, strlen(string) + 1);
}

// reader - return bytes remaining in buffer
// writer - return available space
ptrdiff_t sbufBytesRemaining(
    register sbuf_t const * restrict const buf
) {
    return buf->end - buf->ptr;
}

// advance buffer pointer
// reader - skip data
// writer - commit written data

// modifies streambuf so that written data are prepared for reading
void sbufSwitchToReader(sbuf_t *buf, uint8_t *base) {
    buf->end = buf->ptr;
    buf->ptr = base;
}

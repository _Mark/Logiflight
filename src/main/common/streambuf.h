/*
 * This file is part of Cleanflight and Betaflight.
 *
 * Cleanflight and Betaflight are free software. You can redistribute
 * this software and/or modify this software under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Cleanflight and Betaflight are distributed in the hope that they
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.
 *
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <stdint.h>
#include <string.h>

// simple buffer-based serializer/deserializer without implicit size check

typedef struct sbuf_s {
    uint8_t *ptr;          // data pointer must be first (sbuf_t* is equivalent to uint8_t **)
    uint8_t *end;
} sbuf_t;

#define sbufInit(x, y, z) ISRVALUE({\
    register void * restrict const\
   _x = (x);\
\
   *(sbuf_t *)_x = (sbuf_t){ (y), (z) };\
\
   _x;\
})

#define sbufReadU8(x) ((++(x)->ptr)[-1])
#define sbufReadU16(x) (((uint16_t *)((x)->ptr+= 2))[-1])
#define sbufReadU32(x) (((uint32_t *)((x)->ptr+= 4))[-1])
#define sbufWriteU8(x, y) (sbufReadU8(x) = (y))
#define sbufWriteU16(x, y) (sbufReadU16(x) = (y))
#define sbufWriteU32(x, y) (sbufReadU32(x) = (y))
#define sbufWriteU16BigEndian(x, y) (sbufWriteU16((x),__builtin_bswap16((y))))
#define sbufWriteU32BigEndian(x, y) (sbufWriteU32((x),__builtin_bswap32((y))))
void sbufFill(sbuf_t *dst, uint8_t data, int len);
void sbufWriteData(sbuf_t *dst, const void *data, int len);
void sbufWriteString(sbuf_t *dst, const char *string);
void sbufWriteStringWithZeroTerminator(sbuf_t *dst, const char *string);

#define sbufReadData(x, y, n) (memcpy((y), (x)->ptr, (n)))

ptrdiff_t sbufBytesRemaining(sbuf_t const *);
#define sbufPtr(x) ((x)->ptr)
#define sbufAdvance(x, n) ((x)->ptr+= (n))

void sbufSwitchToReader(sbuf_t *buf, uint8_t * base);

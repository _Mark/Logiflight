/*
 * This file is part of Cleanflight and Betaflight.
 *
 * Cleanflight and Betaflight are free software. You can redistribute
 * this software and/or modify this software under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Cleanflight and Betaflight are distributed in the hope that they
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.
 *
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <stdint.h>
#include <stdlib.h>
#include <limits.h>
#include <math.h>

#include "platform.h"

#ifdef USE_ARM_MATH
#include "arm_math.h"
#endif

#define CMPLX(x, y) ISRVALUE({\
    register float _Complex\
   _x = (x);\
  __imag__\
   _x = (y);\
\
   _x;\
})

#define oppos(r, x) (sqrt(fdim((r), (x))))
#define opposf(r, x) (sqrtf(fdimf((r), (x))))

#define SQ_MOV(x, y) ISRVALUE({\
    register __auto_type const\
   _y = (y);\
\
    ((x) =_y) * _y;\
})

#define DIV(x, y)\
   _Generic(\
        (x) % (y)\
    ,\
        long long: lldiv, long long unsigned: lldiv,\
        unsigned : lldiv, long      unsigned: lldiv,\
        long     :  ldiv, short     unsigned:  ldiv,\
        default  :   div\
    )((x), (y))

#define abserr(x, y) ISRVALUE({\
    register struct {\
        typeof((x) + (y)) _x,_y;\
    } const _z = { (x), (y) };\
\
   _z._x < _z._y ? _z._y - _z._x : _z._x - _z._y;\
})

#define SGN2(x) ((x)>> sizeof (x) * CHAR_BIT - 1 | true)

#define uquot(x, y) ISRVALUE({\
    typedef typeof((x) % (y)) _y_type;\
\
    register struct {\
       _y_type       _x,_y;\
       _y_type const _r;\
    }_z = {._r = (_z._x = (x)) % (_z._y = (y)) };\
\
   _z._x/=_z._y;\
\
    ((_z._y-=_z._r)<=_z._r && (_z._y!=_z._r ||_z._x & true)) + _z._x;\
})

#define nquot(x, y) ISRVALUE({\
    typedef typeof((x) % (y)) _y_type;\
\
    register struct {\
       _y_type       _x,_y;\
       _y_type const _r,_s;\
    }_z = {._r = (_z._x = (x)) % (_z._y = (y)), SGN2(_z._x/=_z._y) };\
\
   _z._r / (_z._y =_z._s * _z._y - _z._r) && (_z._y!=_z._r ||_z._x & true) ?\
       _z._x + _z._s :_z._x;\
})

#define rquot(x, y) \
   _Generic(\
        (x) % (y)\
    ,\
        unsigned long long: uquot(x, y),\
        unsigned long     : uquot(x, y),\
        unsigned          : uquot(x, y),\
        unsigned short    : uquot(x, y),\
        unsigned char     : uquot(x, y),\
        default           : nquot(x, y)\
    )

#define fmult(x, y) ISRVALUE({\
    register typeof((x) / (y)) const\
   _y = (y);\
\
   _Generic(_y, float: truncf, default: trunc)((x) /_y) * _y;\
})

#define POWI(x, i) \
   _Generic(\
        (x),\
        double long:__builtin_powil,\
        double     :__builtin_powi ,\
        default    :__builtin_powif\
    )((x), (i))
#ifndef sq
#define sq(x) ((x)*(x))
#endif
#ifndef power3
#define power3(x) POWI(x, 3)
#endif

#define SIGN(x) ISRVALUE({\
    register __auto_type const\
   _x = (x);\
\
    (_x > false) - (_x < false);\
})

// Undefine this for use libc sinf/cosf. Keep this defined to use fast sin/cos approximations
/*
#define FAST_MATH             // order 9 approximation
#define VERY_FAST_MATH      // order 7 approximation
*/

// Use floating point M_PI instead explicitly.
#define M_PIf       ((float)M_PI)
#define M_PI_2f     ((float)M_PI_2)
#define M_PI_4f     ((float)M_PI_4)
#define M_PI_HALFf  M_PI_2f
#define M_EULERf    ((float)M_E)
#define M_SQRT2f    ((float)M_SQRT2)
#define M_LN2f      ((float)M_LN2)

#define M_PIl    ((double)M_PI)
#define M_PI_2l  ((double)M_PI_2)
#define M_PI_4l  ((double)M_PI_4)

#define RAD                        ((float)(M_PI_4 / 45e0))
#define     RADIANS_TO_DEGREES(x)  (((x) * u'\x2D') / M_PI_4f)
#define RADIANS_TO_CENTIDEGREES(x) (((x) * 4500l) / M_PI_4f)
#define RADIANS_TO_DECIDEGREES(x)  (((x) * 450l) / M_PI_4f)
#define RADS_CW_TO_DECIDEGREES(x)  (((x) *-450l) / M_PI_4f)
#define DEGREES_TO_DECIDEGREES(x)  ((x) * u'\xA')
#define DECIDEGREES_TO_DEGREES(x)  ((x) / u'\xA')
#define DECIDEGREES_TO_RADIANS(x)  ((x) * M_PI_4f / 45e1f)
#define DECIDEGS_CW_TO_RADIANS(x)  ((x) * M_PI_4f /-45e1f)
#define     DEGREES_TO_RADIANS(x)  ((x) * M_PI_4f / 45e0f)

#define CM_S_TO_KM_H(centimetersPerSecond) ((centimetersPerSecond) * 36 / 1000)
#define CM_S_TO_MPH(centimetersPerSecond) ((centimetersPerSecond) * 10000 / 5080 / 88)

#define MIN(a,b) \
   _Generic(\
        (a) + (b)\
    ,\
        double : fmin((a), (b)),\
        float  : fminf((a), (b)),\
        default: ISRVALUE({\
            register struct {\
                typeof((a) + (b)) _a,_b;\
            } const _x = { (a), (b) };\
\
           _x._b >_x._a ?_x._a :_x._b;\
        })\
    )
#define MAX(a,b) \
   _Generic(\
        (a) + (b)\
    ,\
        double : fmax((a), (b)),\
        float  : fmaxf((a), (b)),\
        default: ISRVALUE({\
            register struct {\
                typeof((a) + (b)) _a,_b;\
            } const _x = { (a), (b) };\
\
           _x._b <_x._a ?_x._a :_x._b;\
        })\
    )
#define ABS(x) \
   _Generic(\
        (x)\
    ,\
        unsigned long long: (x),\
        unsigned long     : (x),\
        unsigned          : (x),\
        unsigned short    : (x),\
        unsigned char     : (x),\
        default:\
           _Generic(\
                (x)\
            ,\
                double   :  fabs,\
                float    :  fabsf,\
                long long: llabs,\
                long     :  labs,\
                default  :   abs\
            )(x)\
    )
#define SCALE_UNITARY_RANGE(x, from, to) ((1.0f - (x)) * (from) + (x) * (to))

#ifdef USE_ESC_SENSOR
#define RAD_S_TO_RPM(x) (\
    (x) *_Generic(\
        (x)\
    ,\
        double : 19.098593171027440292266051604702,\
        float  : 19.098593171027440292266051604702f,\
        default: u'\x13'\
    )\
)
#endif

#define HZ_TO_INTERVAL_US(x) (999999 / (x) + true)

typedef int32_t fix12_t;

typedef struct stdev_s {
    float m_n, m_newM, m_newS;
} stdev_t;

// Floating point Euler angles.
// Be carefull, could be either of degrees or radians.
typedef struct fp_angles {
    float roll;
    float pitch;
    float yaw;
} fp_angles_def;

typedef union {
    float raw[3];
    fp_angles_def angles;
} fp_angles_t;

#if defined(USE_ARM_MATH) &&__FPU_USED== 1
 #ifdef __CC_ARM
#define fast_fsqrtf __sqrtf
 #elif defined(__GNUC__) || (\
  defined(__ARMCC_VERSION) &&__ARMCC_VERSION >= 6010050\
 )
#define fast_fsqrtf __builtin_sqrtf
 #elif defined(__ICCARM__) &&__VER__>= 6040000
#define fast_fsqrtf(x) ISRVALUE({\
    register float ret;\
\
  __ASM("VSQRT.F32 %0,%1" : "=t"(ret) : "t"(x));\
\
    ret;\
})
 #else
#define fast_fsqrtf sqrtf
 #endif
#else
#define fast_fsqrtf sqrtf
#endif
int32_t applyDeadband(int32_t value, int32_t deadband);
float fapplyDeadband(float value, float deadband);

#define devClear(x) ((x)->m_n = false)
void devPush(stdev_t[], float);
float devVariance(stdev_t const[]);
#define devStandardDeviation(x) (fast_fsqrtf(devVariance((x))))
#define degreesToRadians(x) DEGREES_TO_RADIANS(x)

int scaleRange(int x, int srcFrom, int srcTo, int destFrom, int destTo);
float scaleRangef(float x, float srcFrom, float srcTo, float destFrom, float destTo);

int32_t quickMedianFilter3(int32_t * v);
int32_t quickMedianFilter5(int32_t * v);
int32_t quickMedianFilter7(int32_t * v);
int32_t quickMedianFilter9(int32_t * v);

float quickMedianFilter3f(float * v);
float quickMedianFilter5f(float * v);
float quickMedianFilter7f(float * v);
float quickMedianFilter9f(float * v);

#if defined(FAST_MATH) || defined(VERY_FAST_MATH)
float sin_approx(float x);
#define cos_approx(x) (sin_approx((x) + M_PI_2))
float atan2_approx(float y, float x);
float asin_approx(float x);
float acos_approx(float x);
#define tan_approx(x)       (sin_approx((x)) / cos_approx((x)))
float exp_approx(float val);
float log_approx(float val);
float pow_approx(float a, float b);
#else
#define sin_approx(x)   sinf(x)
#define cos_approx(x)   cosf(x)
#define atan2_approx(y,x)   atan2f(y,x)
#define asin_approx(x)      asinf(x)
#define acos_approx(x)      acosf(x)
#define tan_approx(x)       tanf(x)
#define exp_approx(x)       expf(x)
#define log_approx(x)       logf(x)
#define pow_approx(a, b)    powf(b, a)
#endif
#define sq_approx(x) POWI(x, 2)

#define debugRescale(x) ISRVALUE({\
    register float const\
    RATIO = (x);\
\
    pow_approx(\
        1e4f\
    ,\
       -truncf(log_approx(RATIO) * .10857362047581295691278222972915f)\
    ) * RATIO;\
})

void arraySubInt32(int32_t *dest, int32_t *array1, int32_t *array2, int count);

int16_t qPercent(fix12_t q);
int16_t qMultiply(fix12_t q, int16_t input);
fix12_t qConstruct(int16_t num, int16_t den);

#define constrain(x, y, z) MIN(MAX(x, y), z)
#define constrainf(x, y, z) constrain(x, y, z)

// tensors
typedef union {
    int16_t raw[2];
    struct {
        int16_t x, y;
    };
} s16Tensor2_t;

typedef union {
    int16_t raw[3];
    struct {
        int16_t x, y, z;
    };
} s16Tensor3_t;

typedef union {
   uint16_t raw[2];
    struct {
       uint16_t x, y;
    };
} u16Tensor2_t;

typedef union {
   uint16_t raw[3];
    struct {
       uint16_t x, y, z;
    };
} u16Tensor3_t;

typedef union {
    int32_t raw[2];
    struct {
        int32_t x, y;
    };
} s32Tensor2_t;

typedef union {
    int32_t raw[3];
    struct {
        int32_t x, y, z;
    };
} s32Tensor3_t;

typedef union {
   uint32_t raw[2];
    struct {
       uint32_t x, y;
    };
} u32Tensor2_t;

typedef union {
   uint32_t raw[3];
    struct {
       uint32_t x, y, z;
    };
} u32Tensor3_t;

typedef union {
    int64_t raw[2];
    struct {
        int64_t x, y;
    };
} s64Tensor2_t;

typedef union {
    int64_t raw[3];
    struct {
        int64_t x, y, z;
    };
} s64Tensor3_t;

typedef union {
   uint64_t raw[2];
    struct {
       uint64_t x, y;
    };
} u64Tensor2_t;

typedef union {
   uint64_t raw[3];
    struct {
       uint64_t x, y, z;
    };
} u64Tensor3_t;

// vectors
typedef union {
    float raw[2];
    struct {
        float x, y;
    };
} fpVector2_t;

typedef union {
    float raw[3];
    struct {
        float x, y, z;
    };
} fpVector3_t;

typedef union {
    double raw[2];

  __PACKED_STRUCT {
        double x, y;
    };
} flVector2_t;

typedef union {
    double raw[3];

  __PACKED_STRUCT {
        double x, y, z;
    };
} flVector3_t;

// quaternions
typedef union {
  __PACKED_STRUCT {
        float x, y, z, w;
    };

    float raw[4];
} quaternion;
#define QUATERNION_INITIALIZE { 0., 0., 0., 1. }
#define VECTOR_INITIALIZE     { 0., 0., 0., 0. }

typedef struct {
    float xx, xy, xz, xw, yy, yz, yw, zz, zw, ww;
} quaternionProducts;

void quaternionTransformVectorBodyToEarth(quaternion *, quaternion const *);
void quaternionTransformVectorEarthToBody(quaternion *, quaternion const *);
void quaternionMultiply(quaternion const *, quaternion const *, quaternion *);
void quaternionNormalize(quaternion *q);
void quaternionAdd(quaternion const *, quaternion const *, quaternion *);
#define quaternionCopy(x, y) (*(y) =*(x))
void quaternionConjugate(quaternion const *, quaternion *);
float quaternionDotProduct(quaternion const *, quaternion const *);
#define quaternionNorm(x) ISRVALUE({\
    register __auto_type const _x = (x);\
\
    quaternionDotProduct(_x,_x);\
})
#define quaternionModulus(x) (fast_fsqrtf(quaternionNorm(x)))

/*
 * This file is part of Cleanflight and Betaflight.
 *
 * Cleanflight and Betaflight are free software. You can redistribute
 * this software and/or modify this software under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Cleanflight and Betaflight are distributed in the hope that they
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.
 *
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <strings.h>

#include "common/filter.h"

// Brown's linear exponential smoothing
// See <https://faculty.fuqua.duke.edu/~rnau/Decision411_2007/411avg.htm>

float pt1FilterMaxK(register float deltaT) {
    return deltaT / ((deltaT*= M_PIf) + true);
}

float pt1FilterGain(
    register float const f_cut,
    register float       deltaT
) {
    return fmaxf(
        deltaT / ((deltaT*= M_PIf) + fmaxf(5e5f / f_cut, FLT_EPSILON * 5e5))
    ,
        // See <https://www.uml.edu/docs/First-Cutoff_tcm18-190085.pdf>
        FLT_MIN
    );
}

float pt1FilterFreq(
    register pt1Filter_t const filter[restrict const],
    register float             deltaT                  // µs
) {
    return constrainf(
        5e5f / ((deltaT*= M_PIf) / filter->k - deltaT)
    ,
        // See <https://www.uml.edu/docs/First-Cutoff_tcm18-190085.pdf>
        FLT_MIN
    ,
        FLT_MAX
    );
}

FAST_CODE FAST_CODE_NOINLINE float pt1FilterApply(
    register pt1Filter_t filter[restrict const],
    register float       input
) {
    input = filter->state.input+=
        (input - filter->state.input) * filter->k;

    return ldexpf(input, true) - (
        filter->state.level+= (input - filter->state.level) * filter->k
    );
}

FAST_CODE FAST_CODE_NOINLINE float pt1FilterReuse(
    register pt1Filter_t filter[restrict const],
    register float       input
) {
    return
    isnormal(filter->k) ?
        (   input-= filter->state.level+=
                (   (   input = filter->state.input+=
                            (input - filter->state.input) * filter->k
                    ) - filter->state.level
                ) * filter->k
        ) / (true / filter->k - true)
    :
        false;
}

FAST_CODE FAST_CODE_NOINLINE float pt1FilterLevel(
    register pt1Filter_t const filter[restrict const]
) {
    return ldexpf(filter->state.input, true) - filter->state.level;
}

FAST_CODE FAST_CODE_NOINLINE float pt1FilterTrend(
    register pt1Filter_t const filter[restrict const]
) {
    return
    isnormal(filter->k) ?
        (filter->state.input - filter->state.level) / (true / filter->k - true)
    :
        false;
}


// Biquad filters

// get notch filter Q given center frequency (f0) and lower cutoff frequency (f1)
// Q = f0 / (f2 - f1) ; f2 = f0^2 / f1
float filterGetNotchQ(
    register float const centerFreq,
    register float const cutoffFreq
) {
    return centerFreq * cutoffFreq / (centerFreq * centerFreq - cutoffFreq * cutoffFreq);
}

FAST_CODE FAST_CODE_NOINLINE void biquadFilterUpdate(
    register biquadFilter_t           filter[restrict const],
             float                    filterFreq,
             float                    refreshRate,
    register float              const Q,
    register biquadFilterType_e const filterType
) {
    // setup variables
    sincosf(
        ldexpf(filterFreq * refreshRate * M_PIf / 15625.f,-5)
    ,
       &filterFreq
    ,
       &refreshRate
    );

    filter->a2 =
        fdimf(true, filterFreq = ldexpf(filterFreq / Q,-true)) /++filterFreq;
    filter->a1 =-ldexpf(refreshRate, true) / filterFreq;

    switch (filterType) {
    case FILTER_LPF:
        // 2nd order Butterworth (with Q=1/sqrt(2)) / Butterworth biquad section with Q
        // described in http://www.ti.com/lit/an/slaa447/slaa447.pdf
        filter->b2 =
        filter->b0 = ldexpf(filter->b1 = filter->a2,-true);
        break;
    case FILTER_NOTCH:
        filter->b1 = filter->a1;
        filter->b2 =
        filter->b0 = true / filterFreq;
        break;
    default:
        filter->b2 =-(filter->b0 = refreshRate / filterFreq);
        break;
    }
}

/* Computes a biquadFilter_t filter on a sample (slightly less precise than df2 but works in dynamic mode) */
#ifdef USE_RC_SMOOTHING_FILTER
FAST_CODE FAST_CODE_NOINLINE float biquadFilterApplyDF1(
#else
float biquadFilterApplyDF1(
#endif
    register biquadFilter_t       filter[restrict const],
    register float          const input
) {
    /* compute result */
    register
    const float result = filter->b0 * input + filter->b1 * filter->x1 + filter->b2 * filter->x2 - filter->a1 * filter->y1 - filter->a2 * filter->y2;
    /* shift x1 to x2, input to x1 */
    filter->x2 = filter->x1;
    filter->x1 = input;
    /* shift y1 to y2, result to y1 */
    filter->y2 = filter->y1;
    return
    filter->y1 = isnormal(result) ? result : false;
}

/* Computes a biquadFilter_t filter in direct form 2 on a sample (higher precision but can't handle changes in coefficients */
FAST_CODE FAST_CODE_NOINLINE float biquadFilterApply(
    register biquadFilter_t       filter[restrict const],
    register float          const input
) {
    register
    const float result = filter->b0 * input + filter->x1;
    filter->x1 = filter->b1 * input - filter->a1 * result + filter->x2;
    filter->x2 = filter->b2 * input - filter->a2 * result;
    return result;
}

#ifdef USE_SMITH_PREDICTOR
// Robert Bouwens AlphaBetaGamma

void ABGInit(
    register alphaBetaGammaFilter_t       filter[restrict const],
    register float                  const alpha,
    register uint16_t               const boostGain,
    register uint16_t               const halfLife,
    register float                  const dT
) {
  // beta, gamma, and eta gains all derived from
  // http://yadda.icm.edu.pl/yadda/element/bwmeta1.element.baztech-922ff6cb-e991-417f-93f0-77448f1ef4ec/c/A_Study_Jeong_1_2017.pdf

    filter->b =
        ldexpf(
            powf(
                filter->e = fdimf(
                    true
                ,
                    filter->g = powf(
                        fdimf(true, filter->a = ldexpf(alpha / 125.f,-3)), .25
                    )
                )
            ,
                2
            ) * ((filter->g * 11.f + 14.f) * filter->g + 11.f) / 3.f
        ,
           -true
        );
    filter->g+=
    filter->g*= ldexpf(__builtin_powif(filter->e, 3)      , true);
    filter->e = ldexpf(__builtin_powif(filter->e, 4) / 3.f,-true);

    filter->dT3 = (filter->dT2 = (filter->dT = dT) * dT) * 3.f * dT;
    filter->halfLife = (
        halfLife ? powf(.5, ldexpf(dT / (halfLife * 25ul),-2)) : true
    );

    filter->boost = ldexpf(
        (sq_approx((uint32_t)boostGain) * 3ul) / 1953125.l,-9
    );

    BZERO(filter->k);
} // ABGInit

float alphaBetaGammaApply(
  register alphaBetaGammaFilter_t filter[restrict const],
  register float                  rk
) {
	// float xk;   // current system state (ie: position)
	// float vk;   // derivative of system state (ie: velocity)
  // float ak;   // derivative of system velociy (ie: acceleration)
  // float jk;   // derivative of system acceleration (ie: jerk)

  filter->k.v+=
    (   filter->k.r = rk+=
          fabsf(
              rk-= filter->k.x =
                ( ldexpf(
                    ( (filter->k.j*= filter->halfLife) * filter->dT / 3.f +
                      (filter->k.a*= filter->halfLife)
                    ) * filter->dT
                  ,
                   -true
                  ) + (filter->k.v*= filter->halfLife)
                ) * filter->dT + (filter->k.x*= filter->halfLife)
          ) * rk * filter->boost
    ) * filter->b / filter->dT;
  filter->k.v+= ldexpf(filter->k.j * filter->dT2,-true) + filter->k.a * filter->dT;
  filter->k.a+=
    ldexpf(filter->g * rk / filter->dT2,-true) + filter->k.j * filter->dT;
  filter->k.j+= ldexpf(filter->e * rk / filter->dT3,-true);
  return
  filter->k.x+= filter->a * rk;
} // ABGUpdate
#endif

void ptnFilterInit(
    register ptnFilter_t       filter[restrict const],
    register uint8_t     const order,
    register float       const f_cut,
    register float       const dT
) {
    ptnFilterUpdate(
        filter
    ,
        f_cut
    ,
	  // AdjCutHz = CutHz /(sqrtf(powf(2, 1/Order) -true))
        (filter->order = constrain(order, 1, 4))[
             (float const[]){ [1] =
                 6.283185307, 9.762649804, 12.32421148, 14.44478681
             }
        ]
    ,
        dT
    );
    bzero(filter->state, sizeof filter->state);
} // ptnFilterInit

FAST_CODE FAST_CODE_NOINLINE void ptnFilterUpdate(
    register ptnFilter_t       filter[restrict const],
    register float       const f_cut,
    register float       const ScaleF,
    register float       const dT
) {
    filter->k/= (filter->k = f_cut * ScaleF) + true / dT;
}

FAST_CODE FAST_CODE_NOINLINE float ptnFilterApply(
    register ptnFilter_t       filter[restrict const],
    register float       const input
) {
   *(float *)filter = input;

    for (
        register uint8_t
        n = 0;
        n < filter->order;
        n[filter->state]+= (n++[filter->state] - n[filter->state]) * filter->k
    ) NOOP

	  return filter->state[filter->order];
} // ptnFilterApply

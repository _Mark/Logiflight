/*
 * This file is part of Cleanflight and Betaflight.
 *
 * Cleanflight and Betaflight are free software. You can redistribute
 * this software and/or modify this software under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Cleanflight and Betaflight are distributed in the hope that they
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.
 *
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <signal.h>

#include "common/axis.h"
#include "common/maths.h"
#include "common/time.h"

#include "config/feature.h"

#include "pg/pg.h"

#define LAT 0
#define LON 1

#define GPS_DEGREES_DIVIDER 10000000L
#define GPS_X 1
#define GPS_Y 0

#define isGpsUsable() (STATE(GPS_USABLE_FIX) && feature(FEATURE_GPS))
#define isGpsCourseUsable() (STATE(GPS_FIX_AHEAD) && feature(FEATURE_GPS))

typedef enum {
    GPS_NMEA = 0,
    GPS_UBLOX
} gpsProvider_e;

typedef enum {
    SBAS_AUTO = 0,
    SBAS_EGNOS,
    SBAS_WAAS,
    SBAS_MSAS,
    SBAS_GAGAN
} sbasMode_e;

#define SBAS_MODE_MAX SBAS_GAGAN

typedef enum {
    GPS_BAUDRATE_115200 = 0,
    GPS_BAUDRATE_57600,
    GPS_BAUDRATE_38400,
    GPS_BAUDRATE_19200,
    GPS_BAUDRATE_9600
} gpsBaudRate_e;

typedef enum {
    GPS_AUTOCONFIG_OFF = 0,
    GPS_AUTOCONFIG_ON
} gpsAutoConfig_e;

typedef enum {
    GPS_AUTOBAUD_OFF = 0,
    GPS_AUTOBAUD_ON
} gpsAutoBaud_e;

#define GPS_BAUDRATE_MAX GPS_BAUDRATE_9600

typedef struct gpsConfig_s {
    gpsProvider_e provider;
    sbasMode_e sbasMode;
    gpsAutoConfig_e autoConfig;
    gpsAutoBaud_e autoBaud;
    uint8_t gps_ublox_use_galileo;
} gpsConfig_t;

PG_DECLARE(gpsConfig_t, gpsConfig);

typedef struct gpsCoordinateDDDMMmmmm_s {
    sig_atomic_t dddmm, mmmm;
} gpsCoordinateDDDMMmmmm_t;

/* LLH Location in NEU axis system */
typedef struct gpsLocation_s {
    sig_atomic_t
    lat,                            // latitude * 1e+7
    lon,                            // longitude * 1e+7
    vario,                          // climb rate in cm/s
    alt,                            // altitude in cm
    altSeaAGL;                      // sea level relative to home in cm
} gpsLocation_t;

typedef struct gpsSolutionData_s {
    gpsLocation_t llh;

    sig_atomic_t
    groundSpeed :16,                // ground speed in cm/s
    groundCourse:16,                // ground course in ddeg
    groundRadius,                   // turning radius in cm CW
    hdop  :16,                      // generic HDOP value (*100)
    numSat:16;

    float gndAccNorm;

    struct {
#ifdef USE_ACC
        float _Complex gnd, acc;
#else
        float _Complex gnd;
#endif
    } cis;
} gpsSolutionData_t;

typedef enum {
    GPS_MESSAGE_STATE_IDLE = 0,
    GPS_MESSAGE_STATE_INIT,
    GPS_MESSAGE_STATE_SBAS,
    GPS_MESSAGE_STATE_GALILEO,
    GPS_MESSAGE_STATE_ENTRY_COUNT
} gpsMessageState_e;

typedef struct gpsData_s {
    sig_atomic_t
    errors,                         // gps error counter - crc error/lost of data/sync etc..
    timeouts,
    lastMessage,                    // last time valid GPS data was received (millis)
    lastLastMessage;                // last-last valid GPS message. Used to calculate delta.

    uint32_t state_position;        // incremental variable for loops
    uint32_t state_ts;              // timestamp for last state_position increment
    uint8_t state;                  // GPS thread state. Used for detecting cable disconnects and configuring attached devices
    uint8_t baudrateIndex;          // index into auto-detecting or current baudrate
    gpsMessageState_e messageState;
} gpsData_t;

#define GPS_PACKET_LOG_ENTRY_COUNT 21 // To make this useful we should log as many packets as we can fit characters a single line of a OLED display.
extern char gpsPacketLog[GPS_PACKET_LOG_ENTRY_COUNT];

extern sig_atomic_t
GPS_home[2],
GPS_distanceToHome,                        // m
GPS_directionToHome;                       // deg

extern gpsData_t gpsData;
extern gpsSolutionData_t gpsSol;

extern uint8_t GPS_update;                 // it's a binary toogle to distinct a GPS position update
extern uint32_t GPS_packetCount;
extern uint32_t GPS_svInfoReceivedCount;
extern uint8_t GPS_numCh;                  // Number of channels
extern uint8_t GPS_svinfo_chn[16];         // Channel number
extern uint8_t GPS_svinfo_svid[16];        // Satellite ID
extern uint8_t GPS_svinfo_quality[16];     // Bitfield Qualtity
extern uint8_t GPS_svinfo_cno[16];         // Carrier to Noise Ratio (Signal Strength)

#define GPS_DBHZ_MIN 0
#define GPS_DBHZ_MAX 55

void gpsInit(void);
void gpsUpdate(timeUs_t currentTimeUs);
struct serialPort_s;
void gpsEnablePassthrough(struct serialPort_s *gpsPassthroughPort);
void onGpsNewData(void);
void GPS_reset_home_position(void);
void GPS_calc_longitude_scaling(int32_t lat);
void GPS_distance_cm_bearing(int32_t *currentLat1, int32_t *currentLon1, int32_t *destinationLat2, int32_t *destinationLon2, uint32_t *dist, int32_t *bearing);

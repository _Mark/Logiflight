/*
 * This file is part of Cleanflight and Betaflight.
 *
 * Cleanflight and Betaflight are free software. You can redistribute
 * this software and/or modify this software under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Cleanflight and Betaflight are distributed in the hope that they
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.
 *
 * If not, see <http://www.gnu.org/licenses/>.
 */

static FAST_CODE FAST_CODE_NOINLINE void GYRO_FILTER_FUNCTION_NAME(
    register gyroSensor_t gyroSensor[restrict const]
) {
    for (register ptrdiff_t axis = XYZ_AXIS_COUNT; axis--;) {
        float scaleFn(register float const input) {
            return ldexpf(input * gyroSensor->gyroDev.scale,-11);
        }

#ifdef GYRO_FILTER_DEBUG_SET
        switch (gyroDebugMode) {
            case DEBUG_GYRO_SCALED:
            axis[debug] =
 #ifdef USE_GYRO_IMUF9001
                axis[gyroSensor->gyroDev.gyroADCRaw];
 #else
                lrintf(scaleFn(axis[gyroSensor->gyroDev.gyroADCRaw]));
 #endif
 #ifndef USE_DUAL_GYRO
            break;

            case DEBUG_GYRO_RAW:
            axis[debug] = axis[gyroSensor->gyroDev.gyroADCRaw];
 #endif
            FALLTHROUGH;

            default:
            break;
        }
#endif // GYRO_FILTER_DEBUG_SET

        // scale gyro output to degrees per second
        axis[gyroSensor->gyroDev.gyroADCf] =
#ifdef USE_GYRO_IMUF9001
            axis[gyroSensor->gyroDev.gyroADCRaw];
#else
 #ifdef USE_GYRO_SLEW_LIMITER
            scaleFn(
                gyroSlewLimiter(gyroSensor, axis) * gyroSensor->gyroDev.scale
            );
 #elif defined(USE_GYRO_OVERFLOW_CHECK)
            ISRVALUE({
                float scaleFn(register int16_t const input) {
                    return ldexpf(input * gyroSensor->gyroDev.scale,-11);
                }

                gyroSensor->overflowDetected && ({
                    register uint8_t const
                    overflowAxisMask = gyroConfig()->checkOverflow;

                    overflowAxisMask && (
                        overflowAxisMask== GYRO_OVERFLOW_CHECK_ALL_AXES ||
                                axis    == YAW
                    );
                }) ?
                    scaleFn(axis[gyroSensor->gyroDev.gyroADCRawPrevious])
                        + axis[gyro.gyroAccf]
                : scaleFn(
                    axis[gyroSensor->gyroDev.gyroADCRawPrevious] =
                    axis[gyroSensor->gyroDev.gyroADCRaw]
                );
            });
 #else
            ldexpf(
                axis[gyroSensor->gyroDev.gyroADCRaw] * gyroSensor->gyroDev.scale
            ,
               -11
            );
 #endif
#endif
#ifdef USE_ACC
        axis[gyroSensor->gyroDev.accADCf] =
            (axis[gyroSensor->gyroDev.accADCRaw] * 196133ll)
                / gyroSensor->gyroDev.gyro_200G;
#endif

#ifdef GYRO_FILTER_DEBUG_SET
        if (gyroDebugMode== DEBUG_GYRO_FILTERED)
            axis[debug] = lrintf(axis[gyroSensor->gyroDev.gyroADCf]); // deg/s
#endif // GYRO_FILTER_DEBUG_SET
    }

#ifdef GYRO_FILTER_DEBUG_SET
    if (gyroDebugMode== DEBUG_GYRO_SCALED)
        debug[DEBUG16_VALUE_COUNT - 1] = gyroSensor->overflowDetected;
#endif // GYRO_FILTER_DEBUG_SET

    gyroSensor->gyroDev.tempAbs =
        fdimf(gyroSensor->gyroDev.tempRel, gyroSensor->gyroDev.gyro_27315cK)
            / gyroSensor->gyroDev.gyro_1K;
}

/*
 * This file is part of Cleanflight and Betaflight.
 *
 * Cleanflight and Betaflight are free software. You can redistribute
 * this software and/or modify this software under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Cleanflight and Betaflight are distributed in the hope that they
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.
 *
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

#include "platform.h"

#include "build/debug.h"

#include "common/axis.h"
#include "common/maths.h"
#include "common/filter.h"

#include "config/feature.h"

#include "pg/pg.h"
#include "pg/pg_ids.h"

#include "drivers/accgyro/accgyro.h"
#include "drivers/accgyro/accgyro_fake.h"
#include "drivers/accgyro/accgyro_mpu.h"
#include "drivers/accgyro/accgyro_mpu3050.h"
#include "drivers/accgyro/accgyro_mpu6050.h"
#include "drivers/accgyro/accgyro_mpu6500.h"
#include "drivers/accgyro/accgyro_spi_bmi160.h"
#include "drivers/accgyro/accgyro_spi_icm20649.h"
#include "drivers/accgyro/accgyro_spi_icm20689.h"
#include "drivers/accgyro/accgyro_spi_mpu6000.h"
#include "drivers/accgyro/accgyro_spi_mpu6500.h"
#include "drivers/accgyro/accgyro_spi_mpu9250.h"

#ifdef USE_GYRO_L3G4200D
#include "drivers/accgyro_legacy/accgyro_l3g4200d.h"
#endif

#ifdef USE_GYRO_L3GD20
#include "drivers/accgyro_legacy/accgyro_l3gd20.h"
#endif

#ifdef USE_GYRO_IMUF9001
#include "drivers/accgyro/accgyro_imuf9001.h"
#endif //USE_GYRO_IMUF9001
#include "drivers/accgyro/gyro_sync.h"
#include "drivers/bus_spi.h"
#include "drivers/dma_spi.h"
#include "drivers/io.h"
#include "drivers/system.h"
#include "drivers/time.h"
#include "drivers/accgyro/gyro_sync.h"
#include "drivers/bus_spi.h"
#include "drivers/io.h"

#include "fc/config.h"
#include "fc/runtime_config.h"

#include "flight/imu.h"
#include "flight/mixer.h"

#include "io/beeper.h"
#include "io/statusindicator.h"

#include "scheduler/scheduler.h"

#include "fc/fc_rc.h"
#include "fc/rc_controls.h"
#include "rx/rx.h"

#ifdef USE_ACC_SENSOR
#include "sensors/acceleration.h"
#endif
#include "sensors/boardalignment.h"
#ifdef USE_BARO
#include "sensors/barometer.h"
#endif
#include "sensors/gyro.h"
#include "sensors/sensors.h"

#ifdef USE_HARDWARE_REVISION_DETECTION
#include "hardware_revision.h"
#endif

#define ARMING_DISABLED_ACCGYRO (\
    ARMING_DISABLED_CALIBRATING | ARMING_DISABLED_BST\
)

#if ((FLASH_SIZE > 128) && (defined(USE_GYRO_SPI_ICM20601) || defined(USE_GYRO_SPI_ICM20689) || defined(USE_GYRO_SPI_MPU6500)))
#define USE_GYRO_SLEW_LIMITER
#endif

FAST_RAM_ZERO_INIT gyro_t gyro;

static FAST_RAM_ZERO_INIT uint8_t gyroDebugMode;
static FAST_RAM_ZERO_INIT uint8_t gyroToUse;
#ifdef USE_GYRO_OVERFLOW_CHECK
static FAST_RAM_ZERO_INIT uint8_t overflowAxisMask;
#endif

static FAST_RAM_ZERO_INIT bool overflowDetected;
#ifdef USE_YAW_SPIN_RECOVERY
static FAST_RAM_ZERO_INIT bool yawSpinDetected;
#endif
static FAST_RAM_ZERO_INIT bool gyroHasOverflowProtection;
static FAST_RAM_ZERO_INIT bool firstArmingCalibrationWasStarted;

typedef struct gyroCalibration_s {
    stdev_t
#ifdef USE_ACC
    gyro[XYZ_AXIS_COUNT],
    acc [XYZ_AXIS_COUNT];
#else
    gyro[XYZ_AXIS_COUNT];
#endif

    float sum[XYZ_AXIS_COUNT];

    timeUs_t cyclesRemaining;
} gyroCalibration_t;

static FAST_RAM_ZERO_INIT struct {
  __PACKED_STRUCT {
        pt1Filter_t
        gyroLPF[XYZ_AXIS_COUNT],
        tempLPF[1];
    };

    gyroCalibration_t calibration;

    double period, frequency;
} gyroAnemo;

typedef struct gyroSensor_s {
    gyroDev_t gyroDev;

    // overflow and recovery
    timeUs_t overflowTimeUs;
    bool overflowDetected;
#ifdef USE_YAW_SPIN_RECOVERY
    timeUs_t yawSpinTimeUs;
    bool yawSpinDetected;
#endif // USE_YAW_SPIN_RECOVERY
} gyroSensor_t;

static FAST_RAM_ZERO_INIT union {
#ifdef USE_DUAL_GYRO
  __PACKED_STRUCT {
        gyroSensor_t first, second;
    };

    gyroSensor_t raw[2];
#else
    gyroSensor_t first, raw[1];
#endif
} gyroPair;
#define gyroSensor1 (gyroPair.first)
#ifdef USE_DUAL_GYRO
#define gyroSensor2 (gyroPair.second)
#endif

#ifdef UNIT_TEST
#define gyroSensorPtr (gyroPair.raw)
#define gyroDevPtr    gyroSensorPtr
#endif

#define DEBUG_GYRO_CALIBRATION 3

#define GYRO_OVERFLOW_TRIGGER_THRESHOLD 31980  // 97.5% full scale (1950dps for 2000dps gyro)
#define GYRO_OVERFLOW_RESET_THRESHOLD 30340    // 92.5% full scale (1850dps for 2000dps gyro)

PG_REGISTER_WITH_RESET_TEMPLATE(gyroConfig_t, gyroConfig, PG_GYRO_CONFIG, 6);

#ifndef GYRO_CONFIG_USE_GYRO_DEFAULT
#define GYRO_CONFIG_USE_GYRO_DEFAULT GYRO_CONFIG_USE_GYRO_1
#endif

#ifdef USE_GYRO_IMUF9001
PG_RESET_TEMPLATE(gyroConfig_t, gyroConfig,
                  .gyro_align = ALIGN_DEFAULT,
                  .gyroCalibrationDuration = 125,        // 1.25 seconds
                  .gyroMovementCalibrationThreshold = 48,
                  .gyro_sync_denom = 1,

                  // See <https://youtu.be/bn5JsFYz8TE?t=282>
                  .gyro_hardware_lpf = GYRO_LPF_98HZ,

                  .gyro_32khz_hardware_lpf = GYRO_32KHZ_HARDWARE_LPF_NORMAL,
                  .gyro_lowpass_type = FILTER_PT1,
                  .gyro_lowpass_hz[ROLL] = 0,
                  .gyro_lowpass_hz[PITCH] = 0,
                  .gyro_lowpass_hz[YAW] = 0,
                  .gyro_lowpass2_type = FILTER_PT1,
                  .gyro_lowpass2_hz[ROLL] = 0,
                  .gyro_lowpass2_hz[PITCH] = 0,
                  .gyro_lowpass2_hz[YAW] = 0,
                  .gyro_high_fsr = false,
                  .gyro_use_32khz = true,
                  .gyro_to_use = GYRO_CONFIG_USE_GYRO_DEFAULT,
                  .gyro_soft_notch_hz_1 = 0,
                  .gyro_soft_notch_cutoff_1 = 0,
                  .gyro_soft_notch_hz_2 = 0,
                  .gyro_soft_notch_cutoff_2 = 0,
                  .checkOverflow = GYRO_OVERFLOW_CHECK_ALL_AXES,
                  .yaw_spin_recovery = true,
                  .yaw_spin_threshold = 1950,
#ifdef USE_GYRO_DATA_ANALYSE
                  .dyn_notch_q_factor = u'\x1F4',        // ‰ of sample rate
                  // See <https://youtu.be/W1losp4DMoo?t=667>
                  .dyn_notch_min_hz = u'\xA',
#endif
                  .imuf_mode = GTBCM_GYRO_ACC_FILTER_F,
                  .imuf_rate = IMUF_RATE_16K,
                  .imuf_roll_q = 6000,
                  .imuf_pitch_q = 6000,
                  .imuf_yaw_q = 6000,
                  .imuf_w = u'\x19',                     // Hz
                  .imuf_sharpness   = u'\xFA',           // ks
                  .imuf_roll_lpf_cutoff_hz = IMUF_DEFAULT_LPF_HZ,
                  .imuf_pitch_lpf_cutoff_hz = IMUF_DEFAULT_LPF_HZ,
                  .imuf_yaw_lpf_cutoff_hz = IMUF_DEFAULT_LPF_HZ,
                  .imuf_acc_lpf_cutoff_hz = IMUF_DEFAULT_ACC_LPF_HZ,
                  .gyro_offset_yaw = 0,
#ifdef USE_SMITH_PREDICTOR
                  .gyro_ABG_alpha = 0,
                  .gyro_ABG_boost = 275,
                  .gyro_ABG_half_life = 50,
#endif
                 );
#else //USE_GYRO_IMUF9001
PG_RESET_TEMPLATE(gyroConfig_t, gyroConfig,
                  .gyro_align = ALIGN_DEFAULT,
                  .gyroCalibrationDuration = u'\xFA',    // i.e., 2.5 seconds
                  .gyroMovementCalibrationThreshold = 48,
                  .gyro_sync_denom = GYRO_SYNC_DENOM_DEFAULT,

                  // See <https://youtu.be/bn5JsFYz8TE?t=282>
                  .gyro_hardware_lpf = GYRO_LPF_98HZ,

                  .gyro_32khz_hardware_lpf = GYRO_32KHZ_HARDWARE_LPF_NORMAL,
                  .gyro_lowpass_type = FILTER_PT1,
#ifdef USE_GYRO_DATA_ANALYSE
                  // See <https://youtu.be/WiWr0gP3AT0?t=552>
                  .gyro_lowpass_hz[ROLL ] = u'\0',
                  .gyro_lowpass_hz[PITCH] = u'\0',
                  .gyro_lowpass_hz[YAW  ] = u'\0',
#else
                  .gyro_lowpass_hz[ROLL] = 115,
                  .gyro_lowpass_hz[PITCH] = 115,
                  .gyro_lowpass_hz[YAW] = 105,
#endif
                  .gyro_lowpass2_type = FILTER_PT1,
                  .gyro_lowpass2_hz[ROLL] = 0,
                  .gyro_lowpass2_hz[PITCH] = 0,
                  .gyro_lowpass2_hz[YAW] = 0,
                  .gyro_high_fsr = false,
                  .gyro_use_32khz = false,
                  .gyro_to_use = GYRO_CONFIG_USE_GYRO_DEFAULT,
                  .gyro_soft_notch_hz_1 = 0,
                  .gyro_soft_notch_cutoff_1 = 0,
                  .gyro_soft_notch_hz_2 = 0,
                  .gyro_soft_notch_cutoff_2 = 0,
                  .checkOverflow = GYRO_OVERFLOW_CHECK_ALL_AXES,
                  .imuf_roll_q = 6000,
                  .imuf_pitch_q = 6000,
                  .imuf_yaw_q = 6000,
                  .imuf_w = u'\x64',                     // Hz
                  .imuf_sharpness = u'\x3E8',            // ds
                  .gyro_offset_yaw = 0,
                  .yaw_spin_recovery = true,
                  .yaw_spin_threshold = 1950,
#ifdef USE_GYRO_DATA_ANALYSE
                  .dyn_notch_q_factor = u'\x1F4',        // ‰ of sample rate
                  // See <https://youtu.be/W1losp4DMoo?t=667>
                  .dyn_notch_min_hz = u'\xA',
#endif
#ifdef USE_SMITH_PREDICTOR
                  .gyro_ABG_alpha = 0,
                  .gyro_ABG_boost = 275,
                  .gyro_ABG_half_life = 50,
#endif
                 );
#endif //USE_GYRO_IMUF9001


#ifdef USE_DUAL_GYRO
#define gyroSensorBusByDevice(x) ((busDevice_t const *)(\
    (x)== GYRO_CONFIG_USE_GYRO_2 ?\
       &gyroSensor2.gyroDev.bus :&gyroSensor1.gyroDev.bus\
))
#else
#define gyroSensorBusByDevice(x) ((busDevice_t const *)(\
   &gyroSensor1.gyroDev.bus\
))
#endif // USE_DUAL_GYRO

static void gyroReboot(void) {
    writeEEPROM();
    stopPwmAllMotors();
    systemReset();
}

static int16_t gyroGet100Fold(register float const input) {
    return lrintf(ldexpf(input, 2) * 25.f);
}

FAST_CODE_NOINVOKE busDevice_t const *gyroSensorBus(void) {
    return gyroSensorBusByDevice(gyroToUse);
}

const mpuConfiguration_t *gyroMpuConfiguration(void) {
#ifdef USE_DUAL_GYRO
    if (gyroToUse == GYRO_CONFIG_USE_GYRO_2) {
        return &gyroSensor2.gyroDev.mpuConfiguration;
    } else {
        return &gyroSensor1.gyroDev.mpuConfiguration;
    }
#else
    return &gyroSensor1.gyroDev.mpuConfiguration;
#endif
}

const mpuDetectionResult_t *gyroMpuDetectionResult(void) {
#ifdef USE_DUAL_GYRO
    if (gyroToUse == GYRO_CONFIG_USE_GYRO_2) {
        return &gyroSensor2.gyroDev.mpuDetectionResult;
    } else {
        return &gyroSensor1.gyroDev.mpuDetectionResult;
    }
#else
    return &gyroSensor1.gyroDev.mpuDetectionResult;
#endif
}

STATIC_UNIT_TESTED gyroSensor_e gyroDetect(gyroDev_t *dev) {
    gyroSensor_e gyroHardware = GYRO_DEFAULT;
    switch (gyroHardware) {
    case GYRO_DEFAULT:
        FALLTHROUGH;
#ifdef USE_GYRO_MPU6050
    case GYRO_MPU6050:
        if (mpu6050GyroDetect(dev)) {
            gyroHardware = GYRO_MPU6050;
#ifdef GYRO_MPU6050_ALIGN
            dev->gyroAlign = GYRO_MPU6050_ALIGN;
#endif
            break;
        }
        FALLTHROUGH;
#endif
#ifdef USE_GYRO_L3G4200D
    case GYRO_L3G4200D:
        if (l3g4200dDetect(dev)) {
            gyroHardware = GYRO_L3G4200D;
#ifdef GYRO_L3G4200D_ALIGN
            dev->gyroAlign = GYRO_L3G4200D_ALIGN;
#endif
            break;
        }
        FALLTHROUGH;
#endif
#ifdef USE_GYRO_MPU3050
    case GYRO_MPU3050:
        if (mpu3050Detect(dev)) {
            gyroHardware = GYRO_MPU3050;
#ifdef GYRO_MPU3050_ALIGN
            dev->gyroAlign = GYRO_MPU3050_ALIGN;
#endif
            break;
        }
        FALLTHROUGH;
#endif
#ifdef USE_GYRO_L3GD20
    case GYRO_L3GD20:
        if (l3gd20Detect(dev)) {
            gyroHardware = GYRO_L3GD20;
#ifdef GYRO_L3GD20_ALIGN
            dev->gyroAlign = GYRO_L3GD20_ALIGN;
#endif
            break;
        }
        FALLTHROUGH;
#endif
#ifdef USE_GYRO_SPI_MPU6000
    case GYRO_MPU6000:
        if (mpu6000SpiGyroDetect(dev)) {
            gyroHardware = GYRO_MPU6000;
#ifdef GYRO_MPU6000_ALIGN
            dev->gyroAlign = GYRO_MPU6000_ALIGN;
#endif
            break;
        }
        FALLTHROUGH;
#endif
#if defined(USE_GYRO_MPU6500) || defined(USE_GYRO_SPI_MPU6500)
    case GYRO_MPU6500:
    case GYRO_ICM20601:
    case GYRO_ICM20602:
    case GYRO_ICM20608G:
#ifdef USE_GYRO_SPI_MPU6500
        if (mpu6500GyroDetect(dev) || mpu6500SpiGyroDetect(dev)) {
#else
        if (mpu6500GyroDetect(dev)) {
#endif
            switch (dev->mpuDetectionResult.sensor) {
            case MPU_9250_SPI:
                gyroHardware = GYRO_MPU9250;
                break;
            case ICM_20601_SPI:
                gyroHardware = GYRO_ICM20601;
                break;
            case ICM_20602_SPI:
                gyroHardware = GYRO_ICM20602;
                break;
            case ICM_20608_SPI:
                gyroHardware = GYRO_ICM20608G;
                break;
            default:
                gyroHardware = GYRO_MPU6500;
            }
#ifdef GYRO_MPU6500_ALIGN
            dev->gyroAlign = GYRO_MPU6500_ALIGN;
#endif
            break;
        }
        FALLTHROUGH;
#endif
#ifdef USE_GYRO_SPI_MPU9250
    case GYRO_MPU9250:
        if (mpu9250SpiGyroDetect(dev)) {
            gyroHardware = GYRO_MPU9250;
#ifdef GYRO_MPU9250_ALIGN
            dev->gyroAlign = GYRO_MPU9250_ALIGN;
#endif
            break;
        }
        FALLTHROUGH;
#endif
#ifdef USE_GYRO_SPI_ICM20649
    case GYRO_ICM20649:
        if (icm20649SpiGyroDetect(dev)) {
            gyroHardware = GYRO_ICM20649;
#ifdef GYRO_ICM20649_ALIGN
            dev->gyroAlign = GYRO_ICM20649_ALIGN;
#endif
            break;
        }
        FALLTHROUGH;
#endif
#ifdef USE_GYRO_SPI_ICM20689
    case GYRO_ICM20689:
        if (icm20689SpiGyroDetect(dev)) {
            gyroHardware = GYRO_ICM20689;
#ifdef GYRO_ICM20689_ALIGN
            dev->gyroAlign = GYRO_ICM20689_ALIGN;
#endif
            break;
        }
        FALLTHROUGH;
#endif
#ifdef USE_ACCGYRO_BMI160
    case GYRO_BMI160:
        if (bmi160SpiGyroDetect(dev)) {
            gyroHardware = GYRO_BMI160;
#ifdef GYRO_BMI160_ALIGN
            dev->gyroAlign = GYRO_BMI160_ALIGN;
#endif
            break;
        }
        FALLTHROUGH;
#endif
#ifdef USE_GYRO_IMUF9001
    case GYRO_IMUF9001:
        if (imufSpiGyroDetect(dev)) {
            gyroHardware = GYRO_IMUF9001;
            break;
        }
        FALLTHROUGH;
#endif //USE_GYRO_IMUF9001
#ifdef USE_FAKE_GYRO
    case GYRO_FAKE:
        if (fakeGyroDetect(dev)) {
            gyroHardware = GYRO_FAKE;
            break;
        }
        FALLTHROUGH;
#endif
    default:
        gyroHardware = GYRO_NONE;
    }
    if (gyroHardware != GYRO_NONE) {
        detectedSensors[SENSOR_INDEX_GYRO] = gyroHardware;
        sensorsSet(SENSOR_GYRO);
    }
    return gyroHardware;
}

static FAST_CODE_NOINVOKE bool gyroInitSensor(
    register gyroSensor_t gyroSensor[restrict const]
) {
    gyroSensor->gyroDev.gyro_high_fsr = gyroConfig()->gyro_high_fsr;
#if defined(USE_GYRO_MPU6050) || defined(USE_GYRO_MPU3050) || defined(USE_GYRO_MPU6500) || defined(USE_GYRO_SPI_MPU6500) || defined(USE_GYRO_SPI_MPU6000) \
 || defined(USE_ACC_MPU6050) || defined(USE_GYRO_SPI_MPU9250) || defined(USE_GYRO_SPI_ICM20601) || defined(USE_GYRO_SPI_ICM20649) || defined(USE_GYRO_SPI_ICM20689) || defined(USE_GYRO_IMUF9001) || defined(USE_ACCGYRO_BMI160)
    mpuDetect((void *)gyroSensor);
    mpuResetFn = gyroSensor->gyroDev.mpuConfiguration.resetFn; // must be set after mpuDetect
#endif
    register gyroSensor_e const
                        gyroHardware =
    gyroSensor->gyroDev.gyroHardware = gyroDetect((void *)gyroSensor);
#ifdef USE_GYRO_SAMPLING_32KHZ
    switch (gyroHardware) {
    default:
        gyroConfigMutable()->gyro_use_32khz = false;
        FALLTHROUGH;
    case GYRO_MPU6500:
    case GYRO_MPU9250:
    case GYRO_ICM20601:
    case GYRO_ICM20602:
    case GYRO_ICM20608G:
    case GYRO_ICM20689:
    case GYRO_IMUF9001:
        // do nothing, as gyro supports 32kHz
        break;
    case GYRO_NONE:
        // gyro does not support 32kHz
        return false;
    }
#else
    gyroConfigMutable()->gyro_use_32khz = false;
#endif
    // Must set gyro targetLooptime before gyroDev.init and initialisation of filters
    gyro.targetFrequency = ldexpf(
        15625.f / (
            gyro.targetLooptimef =
            gyro.targetLooptime  =
                gyroSetSampleRate((void *)
                    gyroSensor
                ,
                    gyroSensor->gyroDev.hardware_lpf =
                        gyroConfig()->gyro_hardware_lpf
                ,
                    gyroConfig()->gyro_sync_denom
                ,
                    gyroConfig()->gyro_use_32khz
                )
        )
    ,
        6
    );
    gyroSensor->gyroDev.hardware_32khz_lpf = gyroConfig()->gyro_32khz_hardware_lpf;
    gyroSensor->gyroDev.initFn((void *)gyroSensor);
#ifndef USE_GYRO_IMUF9001
    if (gyroConfig()->gyro_align != ALIGN_DEFAULT) {
        gyroSensor->gyroDev.gyroAlign = gyroConfig()->gyro_align;
    }
#endif //!USE_GYRO_IMUF9001
    // As new gyros are supported, be sure to add them below based on whether they are subject to the overflow/inversion bug
    // Any gyro not explicitly defined will default to not having built-in overflow protection as a safe alternative.
    switch (gyroHardware) {
    case GYRO_DEFAULT:
    case GYRO_FAKE:
    case GYRO_MPU6050:
    case GYRO_L3G4200D:
    case GYRO_MPU3050:
    case GYRO_L3GD20:
    case GYRO_BMI160:
    case GYRO_MPU6000:
    case GYRO_MPU6500:
    case GYRO_MPU9250:
        gyroSensor->gyroDev.gyroHasOverflowProtection = true;
        break;
    default:
        gyroSensor->gyroDev.gyroHasOverflowProtection = false;  // default catch for newly added gyros until proven to be unaffected
        break;
    }
#ifdef USE_GYRO_SLEW_LIMITER
    gyroInitSensorFilters(gyroSensor);
#endif
#ifdef USE_ACC
{   register accelerometerConfig_t * restrict const
    config = accelerometerConfigMutable();
    if ((
        config->acc_hardware = accDetect(
            (void *)gyroSensor, config->acc_hardware
        )
    )) {
        gyroSensor->gyroDev.isAccDataUsed = XYZ_AXIS_COUNT;
        gyroSensor->gyroDev.gyro_200G = gyroSensor->gyroDev.gyro_1G * 200ul;
#ifndef USE_ACC_IMUF9001
        if (config->acc_align)
            gyroSensor->gyroDev.accAlign = config->acc_align;
#endif
        gyroSensor->gyroDev.acc_high_fsr = config->acc_high_fsr;
    }
}
#endif
    return true;
}

bool gyroInit(void) {
    bzero(&gyro, sizeof gyro);
#ifdef USE_GYRO_OVERFLOW_CHECK
    overflowAxisMask = gyroConfig()->checkOverflow;
#endif //USE_GYRO_OVERFLOW_CHECK
    switch (debugMode) {
    case DEBUG_GYRO_RAW:
    case DEBUG_GYRO_SCALED:
    case DEBUG_GYRO_FILTERED:
    case DEBUG_GYRO_TREND:
 #ifdef USE_ACC
    case DEBUG_ACCELEROMETER:
 #endif
    case DEBUG_AIRSPEED:
        gyroDebugMode = debugMode;
        break;
    default:
        // debugMode is not gyro-related
        gyroDebugMode = DEBUG_NONE;
        break;
    }
    firstArmingCalibrationWasStarted = false;
    gyroHasOverflowProtection = true;
    bool ret = false;
    gyroToUse = gyroConfig()->gyro_to_use;
#if defined(USE_DUAL_GYRO) && defined(GYRO_1_CS_PIN)
    if (gyroToUse == GYRO_CONFIG_USE_GYRO_1 || gyroToUse == GYRO_CONFIG_USE_GYRO_BOTH) {
        gyroSensor1.gyroDev.bus.busdev_u.spi.csnPin = IOGetByTag(IO_TAG(GYRO_1_CS_PIN));
        IOInit(gyroSensor1.gyroDev.bus.busdev_u.spi.csnPin, OWNER_MPU_CS, RESOURCE_INDEX(0));
        IOHi(gyroSensor1.gyroDev.bus.busdev_u.spi.csnPin); // Ensure device is disabled, important when two devices are on the same bus.
        IOConfigGPIO(gyroSensor1.gyroDev.bus.busdev_u.spi.csnPin, SPI_IO_CS_CFG);
    }
#endif
#if defined(USE_DUAL_GYRO) && defined(GYRO_2_CS_PIN)
    if (gyroToUse == GYRO_CONFIG_USE_GYRO_2 || gyroToUse == GYRO_CONFIG_USE_GYRO_BOTH) {
        gyroSensor2.gyroDev.bus.busdev_u.spi.csnPin = IOGetByTag(IO_TAG(GYRO_2_CS_PIN));
        IOInit(gyroSensor2.gyroDev.bus.busdev_u.spi.csnPin, OWNER_MPU_CS, RESOURCE_INDEX(1));
        IOHi(gyroSensor2.gyroDev.bus.busdev_u.spi.csnPin); // Ensure device is disabled, important when two devices are on the same bus.
        IOConfigGPIO(gyroSensor2.gyroDev.bus.busdev_u.spi.csnPin, SPI_IO_CS_CFG);
    }
#endif
    gyroSensor1.gyroDev.gyroAlign = ALIGN_DEFAULT;
#if defined(GYRO_1_EXTI_PIN)
    gyroSensor1.gyroDev.mpuIntExtiTag =  IO_TAG(GYRO_1_EXTI_PIN);
#elif defined(MPU_INT_EXTI)
    gyroSensor1.gyroDev.mpuIntExtiTag =  IO_TAG(MPU_INT_EXTI);
#elif defined(USE_HARDWARE_REVISION_DETECTION)
    gyroSensor1.gyroDev.mpuIntExtiTag =  selectMPUIntExtiConfigByHardwareRevision();
#else
    gyroSensor1.gyroDev.mpuIntExtiTag =  IO_TAG_NONE;
#endif // GYRO_1_EXTI_PIN
#ifdef USE_DUAL_GYRO
#ifdef GYRO_1_ALIGN
    gyroSensor1.gyroDev.gyroAlign = GYRO_1_ALIGN;
#endif
    gyroSensor1.gyroDev.bus.bustype = BUSTYPE_SPI;
    spiBusSetInstance(&gyroSensor1.gyroDev.bus, GYRO_1_SPI_INSTANCE);
    if (gyroToUse!= GYRO_CONFIG_USE_GYRO_2) {
        if ((ret = gyroInitSensor(gyroPair.raw)))
        gyroHasOverflowProtection =  gyroHasOverflowProtection && gyroSensor1.gyroDev.gyroHasOverflowProtection;
        else
            gyroConfigMutable()->gyro_to_use = GYRO_CONFIG_USE_GYRO_2,
            gyroReboot();
    }
#else // USE_DUAL_GYRO
    ret = gyroInitSensor(gyroPair.raw);
    gyroHasOverflowProtection =  gyroHasOverflowProtection && gyroSensor1.gyroDev.gyroHasOverflowProtection;
#endif // USE_DUAL_GYRO
#ifdef USE_DUAL_GYRO
    gyroSensor2.gyroDev.gyroAlign = ALIGN_DEFAULT;
#if defined(GYRO_2_EXTI_PIN)
    gyroSensor2.gyroDev.mpuIntExtiTag =  IO_TAG(GYRO_2_EXTI_PIN);
#elif defined(USE_HARDWARE_REVISION_DETECTION)
    gyroSensor2.gyroDev.mpuIntExtiTag =  selectMPUIntExtiConfigByHardwareRevision();
#else
    gyroSensor2.gyroDev.mpuIntExtiTag =  IO_TAG_NONE;
#endif // GYRO_2_EXTI_PIN
#ifdef GYRO_2_ALIGN
    gyroSensor2.gyroDev.gyroAlign = GYRO_2_ALIGN;
#endif
    gyroSensor2.gyroDev.bus.bustype = BUSTYPE_SPI;
    spiBusSetInstance(&gyroSensor2.gyroDev.bus, GYRO_2_SPI_INSTANCE);
    if (gyroToUse!= GYRO_CONFIG_USE_GYRO_1) {
        if ((ret = gyroInitSensor(gyroPair.raw + 1)))
        gyroHasOverflowProtection =  gyroHasOverflowProtection && gyroSensor2.gyroDev.gyroHasOverflowProtection;
        else
            gyroConfigMutable()->gyro_to_use = GYRO_CONFIG_USE_GYRO_1,
            gyroReboot();
    }
#endif // USE_DUAL_GYRO

    gyro.revisionCode =
#ifdef USE_DUAL_GYRO
        (   gyroSensor1.gyroDev.mpuDetectionResult.resolution ||
            gyroSensor2.gyroDev.mpuDetectionResult.resolution
        )["no"];
#else
        gyroSensor1.gyroDev.mpuDetectionResult.resolution["no"];
#endif // USE_DUAL_GYRO

    gyro.temp.home  = true;
    gyro.temp.ratio = true;

    gyroAnemo.frequency =
    gyroAnemo.period    = true;

#ifdef USE_DUAL_GYRO
{   register bool
    i = true;

    do
        i[gyroPair.raw].gyroDev.gyro_27315cK =
            ldexpf(i[gyroPair.raw].gyroDev.gyro_1K * 5463.f /-5.f,-2);
    while (i--);
}
#else
    gyroSensor1.gyroDev.gyro_27315cK =
        ldexpf(gyroSensor1.gyroDev.gyro_1K * 5463.f /-5.f,-2);
#endif // USE_DUAL_GYRO

    return ret;
}

#if defined(USE_GYRO_SLEW_LIMITER)
FAST_CODE_NOINVOKE void gyroInitFilters(void) {
#ifdef USE_DUAL_GYRO
{   register bool
    i = true;

    do
        bzero(
            i[gyroPair.raw].gyroDev.gyroADCRawPrevious
        ,
            sizeof gyroSensor1.gyroDev.gyroADCRawPrevious
        );
    while (i--);
}
#else
    bzero(
        gyroSensor1.gyroDev.gyroADCRawPrevious
    ,
        sizeof gyroSensor1.gyroDev.gyroADCRawPrevious
    );
#endif
}
#endif

bool isGyroCalibrating(void) {
    return gyroAnemo.calibration.cyclesRemaining;
}

#define isOnFinalGyroCalibrationCycle(x) ((x).cyclesRemaining== true)

static timeUs_t gyroGetCalibrationPeriodUs(void) {
    return gyroConfig()->gyroCalibrationDuration /* cs */ * 10000ul;
}

static timeUs_t gyroGetCalibrationCycles(void) {
    return (gyroGetCalibrationPeriodUs() - true) / gyro.targetLooptime + true;
}

#define isOnFirstGyroCalibrationCycle(x) (\
    gyroGetCalibrationCycles()== (x).cyclesRemaining\
)

void gyroStartCalibration(register bool const isFirstArmingCalibration) {
    if (isFirstArmingCalibration && firstArmingCalibrationWasStarted++)
        NOOP
    else
#ifdef USE_GYRO_IMUF9001
        imufStartCalibration(),
#endif
        gyroAnemo.calibration.cyclesRemaining = gyroGetCalibrationCycles();
}

bool isFirstArmingGyroCalibrationRunning(void) {
    return
    gyroAnemo.calibration.cyclesRemaining && firstArmingCalibrationWasStarted;
}

static FAST_CODE_NOINLINE void performGyroCalibration(void) {
#ifdef USE_ACC
    union {
      __PACKED_STRUCT {
            float raw[XYZ_AXIS_COUNT], acc[XYZ_AXIS_COUNT];
        };
      __PACKED_STRUCT {
            float x, y, z, u, v, w;
        };
    } variance;
#else
    fpVector3_t variance;
#endif

    if (isOnFirstGyroCalibrationCycle(gyroAnemo.calibration))
        // Reset g[axis] at start of calibration
        bzero(
            gyroAnemo.calibration.gyro
        ,
            (void const *)gyroAnemo.calibration.sum -
            (void const *)gyroAnemo.calibration.gyro
        );

    for (register ptrdiff_t axis = XYZ_AXIS_COUNT; axis--;) {
#ifndef USE_ACC
        register var_t * restrict const
        var = axis + gyroAnemo.calibration.gyro;
#else
        register struct {
            stdev_t * restrict gyro, * restrict acc;
        } const var = {
            axis + gyroAnemo.calibration.acc
        ,
            axis + gyroAnemo.calibration.gyro
        };
#endif

#ifdef USE_ACC
        devPush(var.acc, axis[acc.accADC]);
#endif
        devPush(
#ifdef USE_ACC
            var.gyro
#else
            var
#endif
        , ISRVALUE({
            register float const
            VEL = axis[gyro.gyroADCf];

            axis[gyroAnemo.calibration.sum]+= VEL;

            VEL;
        }));

        if (isOnFinalGyroCalibrationCycle(gyroAnemo.calibration)) {
            register float const
            spinStdDev = sqrtf(
#ifdef USE_ACC
            (   axis[variance.acc] = devVariance(var.acc ),
                axis[variance.raw] = devVariance(var.gyro)
            )
#else
                axis[variance.raw] = devVariance(var)
#endif
            );

            // DEBUG_GYRO_CALIBRATION records the standard deviation of roll
            // into the spare field - debug[3], in DEBUG_GYRO_RAW
            if (axis || gyroDebugMode!= DEBUG_GYRO_SCALED) NOOP else
                debug[DEBUG_GYRO_CALIBRATION] = lrintf(
                    ldexpf(spinStdDev, 3) * 125.f
                );

            // check deviation and startover in case the model was moved
            if (ISRVALUE({
                register uint8_t const
                MAX = gyroConfig()->gyroMovementCalibrationThreshold;

                MAX && MAX < spinStdDev;
            })) {
                gyroAnemo.calibration.cyclesRemaining =
                    gyroGetCalibrationCycles();
                return;
            }
        }
    }

    if (isOnFinalGyroCalibrationCycle(gyroAnemo.calibration)) {
        schedulerResetTaskStatistics(TASK_SELF); // so calibration cycles do not pollute tasks statistics

        if (!firstArmingCalibrationWasStarted ||
            getArmingDisableFlags() & ARMING_DISABLED_CALIBRATING
        ) {
#ifdef USE_ACC
            acc.accStdDev = sqrtf(
                acc.accVariance =
                    variance.u + variance.v + variance.w
            );
#endif

            // calculate gyro noise standard deviation modulus
            imuRuntimeConfig.gyro_deadband =
                (   gyro.gyroStdDev = sqrt(
                        gyro.gyroVariance =
                            variance.x + variance.y + variance.z
                    )
                ) * imuConfig()->small_angle;

            for (
                register struct {
                    float     const k;
                    ptrdiff_t       axis;
                } config = {
                    pt1FilterMaxK(gyro.targetLooptimef), XYZ_AXIS_COUNT
                };
                config.axis--;
#ifndef USE_GYRO_IMUF9001
                config.axis[gyro.gyroZero]-=
                    config.axis[gyroAnemo.calibration.sum ] /
                    config.axis[gyroAnemo.calibration.gyro].m_n,
#endif
                config.axis[gyroAnemo.gyroLPF] = (pt1Filter_t){ config.k }
            ) NOOP

           *gyroAnemo.tempLPF = (pt1Filter_t){
                pt1FilterGain(true, gyro.targetLooptimef)
            ,
                gyro.temp.here
            ,
                gyro.temp.here
            };

            gyroAnemo.period    = gyroConfig()->imuf_sharpness;
            gyroAnemo.frequency = gyroConfig()->imuf_w;

            beeper(BEEPER_GYRO_CALIBRATED);
        }
    }

  --gyroAnemo.calibration.cyclesRemaining;
}

#if defined(USE_GYRO_SLEW_LIMITER)
static FAST_CODE int32_t gyroSlewLimiter(
    register gyroSensor_t const gyroSensor[restrict const],
    register ptrdiff_t    const axis
) {
    register int32_t const
    VEL = gyroSensor->gyroDev.gyroADCRaw[axis];

    return
    gyroHasOverflowProtection ||!gyroConfig()->checkOverflow ?
        // don't use the slew limiter if overflow checking is on or gyro is not subject to overflow bug
        VEL
    : labs(gyroSensor->gyroDev.gyroADCRawPrevious[axis] - VEL) < 16385l ?
        gyroSensor->gyroDev.gyroADCRawPrevious[axis] = VEL
    :
        // there has been a large change in value, so assume overflow has occurred and return the previous value
        gyroSensor->gyroDev.gyroADCRawPrevious[axis];
}
#endif

#ifdef USE_GYRO_OVERFLOW_CHECK
static FAST_CODE void handleOverflow(
    register gyroSensor_t       gyroSensor[restrict const],
    register timeUs_t     const currentTimeUs
) {
    for (register ptrdiff_t i = XYZ_AXIS_COUNT; i--;)
        if (fabsf(gyroSensor->gyroDev.gyroADCf[i])
                > GYRO_OVERFLOW_RESET_THRESHOLD
        ) {
            gyroSensor->overflowTimeUs = currentTimeUs;
            return;
        }
        // if we have 50ms of consecutive OK gyro vales, then assume yaw readings are OK again and reset overflowDetected
        // reset requires good OK values on all axes
        if (cmpTimeUs(currentTimeUs, gyroSensor->overflowTimeUs) > 50000) {
            gyroSensor->overflowDetected = false;
        }
}

static FAST_CODE void checkForOverflow(
    register gyroSensor_t       gyroSensor[restrict const],
    register timeUs_t     const currentTimeUs
) {
    // check for overflow to handle Yaw Spin To The Moon (YSTTM)
    // ICM gyros are specified to +/- 2000 deg/sec, in a crash they can go out of spec.
    // This can cause an overflow and sign reversal in the output.
    // Overflow and sign reversal seems to result in a gyro value of +1996 or -1996.
    if (gyroSensor->overflowDetected)
        handleOverflow(gyroSensor, currentTimeUs);
#ifndef SIMULATOR_BUILD
    else for (
        register struct {
            float const * restrict const BEGIN;
            float       * restrict       CURR;
        } PTR = {
            gyroSensor->gyroDev.gyroADCf, (void *)(PTR.BEGIN + XYZ_AXIS_COUNT)
        };
      --PTR.CURR>= PTR.BEGIN;
    )
        // check for overflow in the axes set in overflowAxisMask
        if (fabsf(PTR.CURR[0]) > GYRO_OVERFLOW_TRIGGER_THRESHOLD) {
            gyroSensor->overflowTimeUs = currentTimeUs;
            gyroSensor->overflowDetected = true;
#ifdef USE_YAW_SPIN_RECOVERY
            gyroSensor->yawSpinDetected = false;
#endif // USE_YAW_SPIN_RECOVERY
            break;
        }
#endif // SIMULATOR_BUILD
}
#endif // USE_GYRO_OVERFLOW_CHECK

#ifdef USE_YAW_SPIN_RECOVERY
static FAST_CODE_NOINVOKE void handleYawSpin(
    register gyroSensor_t       gyroSensor[restrict const],
    register timeUs_t     const currentTimeUs
) {
    const float yawSpinResetRate = gyroConfig()->yaw_spin_threshold - 100.0f;
    if (fabsf(gyroSensor->gyroDev.gyroADCf[Z]) < yawSpinResetRate) {
        // testing whether 20ms of consecutive OK gyro yaw values is enough
        if (cmpTimeUs(currentTimeUs, gyroSensor->yawSpinTimeUs) > 20000) {
            gyroSensor->yawSpinDetected = false;
        }
    } else {
        // reset the yaw spin time
        gyroSensor->yawSpinTimeUs = currentTimeUs;
    }
}

static FAST_CODE_NOINVOKE void checkForYawSpin(
    register gyroSensor_t       gyroSensor[restrict const],
    register timeUs_t     const currentTimeUs
) {
    // if not in overflow mode, handle yaw spins above threshold
#ifdef USE_GYRO_OVERFLOW_CHECK
    if (gyroSensor->overflowDetected) {
        gyroSensor->yawSpinDetected = false;
        return;
    }
#endif // USE_GYRO_OVERFLOW_CHECK
    if (gyroSensor->yawSpinDetected) {
        handleYawSpin(gyroSensor, currentTimeUs);
    } else {
#ifndef SIMULATOR_BUILD
        // check for spin on yaw axis only
        if (fabsf(gyroSensor->gyroDev.gyroADCf[Z]) > gyroConfig()->yaw_spin_threshold) {
            gyroSensor->yawSpinDetected = true;
            gyroSensor->yawSpinTimeUs = currentTimeUs;
        }
#endif // SIMULATOR_BUILD
    }
}
#endif // USE_YAW_SPIN_RECOVERY

#define GYRO_FILTER_FUNCTION_NAME filterGyro
#include "gyro_filter_impl.h"
#undef GYRO_FILTER_FUNCTION_NAME

#define GYRO_FILTER_FUNCTION_NAME filterGyroDebug
#define GYRO_FILTER_DEBUG_SET DEBUG_SET
#include "gyro_filter_impl.h"
#undef GYRO_FILTER_FUNCTION_NAME
#undef GYRO_FILTER_DEBUG_SET

FAST_CODE_NOINVOKE bool isGyroDataNew(void) {
    return
#ifdef USE_DUAL_GYRO
    gyroToUse ?
        (   gyroToUse== GYRO_CONFIG_USE_GYRO_2 ||
            gyroSensor1.gyroDev.dataReady
        )&& gyroSensor2.gyroDev.dataReady
    :
#endif
    gyroSensor1.gyroDev.dataReady;
}

#ifdef USE_DUAL_GYRO
static FAST_CODE FAST_CODE_NOINLINE void gyroUpdateSensor(
#else
static FAST_CODE_NOINVOKE void gyroUpdateSensor(
#endif
    register gyroSensor_t       gyroSensor[restrict const],
    register timeUs_t     const currentTimeUs
) {
#ifdef USE_DMA_SPI_DEVICE
    gyroSensor->gyroDev.dataReady = true;
#else
    if ((
        gyroSensor->gyroDev.dataReady =
            gyroSensor->gyroDev.readFn((void *)gyroSensor)
    )) NOOP else
        return;
#endif
    (gyroDebugMode ? filterGyroDebug : filterGyro)(gyroSensor);
#ifdef USE_GYRO_OVERFLOW_CHECK
    if (gyroConfig()->checkOverflow && !gyroHasOverflowProtection) {
        checkForOverflow(gyroSensor, currentTimeUs);
    }
#endif
#ifdef USE_YAW_SPIN_RECOVERY
    if (gyroConfig()->yaw_spin_recovery) {
        checkForYawSpin(gyroSensor, currentTimeUs);
    }
#endif
#if (!defined(USE_GYRO_OVERFLOW_CHECK) && !defined(USE_YAW_SPIN_RECOVERY))
    UNUSED(currentTimeUs);
#endif
#ifndef USE_GYRO_IMUF9001
    alignSensors(gyroSensor->gyroDev.gyroADCf, gyroSensor->gyroDev.gyroAlign);
#endif
}

#ifdef USE_ACC
FAST_CODE_NOINVOKE void gyroReadAcceleration(void) {
 #ifndef USE_GYRO_IMUF9001
  #ifdef USE_DUAL_GYRO
    for (
        register bool
        i = true;
        alignSensors(
            i[gyroPair.raw].gyroDev.accADCf, i[gyroPair.raw].gyroDev.accAlign
        ),
        i--;
    ) NOOP
  #else
    alignSensors(gyroSensor1.gyroDev.accADCf, gyroSensor1.gyroDev.accAlign);
  #endif
 #endif

    for (
        register struct {
            void      (* const fn)(float);
            ptrdiff_t curr;
        } axis = { ISRVALUE({
            void setFn0(register float __attribute__((unused)) const accel) NOOP

            void setFn1(register float const accel) {
                axis.curr[debug] = lrintf(accel);
            }

            gyroDebugMode== DEBUG_ACCELEROMETER ? setFn1 : setFn0;
        }), (
 #ifdef USE_DUAL_GYRO
            gyroSensor2.gyroDev.isAccDataUsed =
 #endif
            gyroSensor1.gyroDev.isAccDataUsed = XYZ_AXIS_COUNT,
            XYZ_AXIS_COUNT
        ) };
        axis.curr--;
        axis.fn(
        axis.curr[acc.accADC] =
 #ifdef USE_DUAL_GYRO
            gyroToUse ?
                gyroToUse== GYRO_CONFIG_USE_GYRO_BOTH ?
                    ldexpf(
                        axis.curr[gyroSensor2.gyroDev.accADCf] +
                        axis.curr[gyroSensor1.gyroDev.accADCf]
                    ,
                       -true
                    )
                :
                    axis.curr[gyroSensor2.gyroDev.accADCf]
            :
 #endif
            axis.curr[gyroSensor1.gyroDev.accADCf]
        )
    ) NOOP
}
#endif // USE_ACC

#ifdef USE_DMA_SPI_DEVICE
FAST_CODE_NOINVOKE void gyroDmaSpiFinishRead(void) {
    //called by dma callback
    mpuGyroDmaSpiReadFinish(&gyroSensor1.gyroDev);
}

FAST_CODE_NOINVOKE void gyroDmaSpiStartRead(void) {
    //called by exti
    mpuGyroDmaSpiReadStart(&gyroSensor1.gyroDev);
}
#endif

FAST_CODE_NOINVOKE void gyroUpdate(register timeUs_t const currentTimeUs) {
#ifdef USE_DUAL_GYRO
{   register gyroSensor_t * restrict const
    gyroSensor = (
        gyroToUse ?
            gyroToUse== GYRO_CONFIG_USE_GYRO_2 ?
                gyroPair.raw + true
            :
                NULL
        :
            gyroPair.raw
    );

    if (gyroSensor) {
        gyroUpdateSensor(gyroSensor, currentTimeUs);

        for (
            register ptrdiff_t
            axis = XYZ_AXIS_COUNT;
            axis--;
            axis[gyro.gyroADCf] = axis[gyroSensor->gyroDev.gyroADCf],
            axis[gyro.gyroAccf] = axis[gyroSensor->gyroDev.gyroAccf]
        ) NOOP

 #ifdef USE_GYRO_OVERFLOW_CHECK
        overflowDetected = gyroSensor->overflowDetected;
 #endif
 #ifdef USE_YAW_SPIN_RECOVERY
        yawSpinDetected = gyroSensor->yawSpinDetected;
 #endif
    } else {
    {   register bool
        i = true;

        do
            gyroUpdateSensor(i[gyroPair.raw], currentTimeUs);
        while (i--);
    }
        for (
            register ptrdiff_t
            axis = XYZ_AXIS_COUNT;
            axis--;
            axis[gyro.gyroADCf] =
                ldexpf(
                    axis[gyroSensor1.gyroDev.gyroADCf] +
                    axis[gyroSensor2.gyroDev.gyroADCf]
                ,
                   -true
                ),
            axis[gyro.gyroAccf] =
                ldexpf(
                    axis[gyroSensor1.gyroDev.gyroAccf] +
                    axis[gyroSensor2.gyroDev.gyroAccf]
                ,
                   -true
                )
        ) NOOP

 #ifdef USE_GYRO_OVERFLOW_CHECK
        overflowDetected =
            gyroSensor1.overflowDetected || gyroSensor2.overflowDetected;
 #endif
 #ifdef USE_YAW_SPIN_RECOVERY
        yawSpinDetected =
            gyroSensor1.yawSpinDetected || gyroSensor2.yawSpinDetected;
 #endif
    }
}

    switch (gyroDebugMode) {
        case DEBUG_DUAL_GYRO_RAW:
        for (
            register ptrdiff_t
            i = DEBUG16_VALUE_COUNT;
            i--;
            i[debug] = (i & true)[(i>> true)[gyroPair.raw].gyroDev.gyroADCRaw]
        ) NOOP
        break;

        case DEBUG_DUAL_GYRO:
        for (
            register ptrdiff_t
            i = DEBUG16_VALUE_COUNT;
            i--;
            i[debug] = lrintf(
                (i & true)[(i>> true)[gyroPair.raw].gyroDev.gyroADCf]
            )
        ) NOOP
        break;

        case DEBUG_GYRO_TREND:
        for (
            register ptrdiff_t
            i = DEBUG16_VALUE_COUNT;
            i--;
            i[debug] = lrintf(
                (i & true)[(i>> true)[gyroPair.raw].gyroDev.gyroAccf]
            )
        ) NOOP
        break;

 #ifdef USE_ACC
        case DEBUG_ACCELEROMETER:
        for (
            register ptrdiff_t
            i = DEBUG16_VALUE_COUNT;
            i--;
            i[debug] = lrintf(
                (i & true)[(i>> true)[gyroPair.raw].gyroDev. accADCf]
            )
        ) NOOP
        break;
 #endif

        case DEBUG_DUAL_GYRO_COMBINE:
        for (
            register ptrdiff_t
            axis = XYZ_AXIS_COUNT;
            axis--;
            axis[debug] = lrintf(axis[gyro.gyroADCf])
        ) NOOP
        break;

        case DEBUG_DUAL_GYRO_DIFF:
        for (
            register ptrdiff_t
            axis = XYZ_AXIS_COUNT;
            axis--;
            axis[debug] = lrintf(
                axis[gyroSensor1.gyroDev.gyroADCf] -
                axis[gyroSensor2.gyroDev.gyroADCf]
            )
        ) NOOP
        FALLTHROUGH;

        default:
        break;
    }
#else
    gyroUpdateSensor(gyroPair.raw, currentTimeUs);

    for (
        register ptrdiff_t
        axis = XYZ_AXIS_COUNT;
        axis--;
        axis[gyro.gyroAccf] =
            pt1FilterReuse(
                axis + gyroAnemo.gyroLPF
            ,
                axis[gyro.gyroADCf] =
                    axis[gyroSensor1.gyroDev.gyroADCf] + axis[gyro.gyroZero]
            ) * gyro.targetFrequency
    ) NOOP

    gyro.temp.here = pt1FilterApply(
        gyroAnemo.tempLPF, gyroSensor1.gyroDev.tempAbs
    );
    gyro.temp.rise = pt1FilterTrend(gyroAnemo.tempLPF) * gyro.targetFrequency;

#ifdef USE_GYRO_OVERFLOW_CHECK
    overflowDetected = gyroSensor1.overflowDetected;
#endif
#ifdef USE_YAW_SPIN_RECOVERY
    yawSpinDetected = gyroSensor1.yawSpinDetected;
#endif
#endif

    switch (gyroDebugMode) {
        case DEBUG_GYRO_SCALED:
        for (
            register ptrdiff_t
            axis = (
                XYZ_AXIS_COUNT[debug] = gyroGet100Fold(gyro.temp.here),
                XYZ_AXIS_COUNT
            );
            axis--;
            axis[debug] = lrintf(axis[gyro.gyroADCf])
        ) NOOP
        break;

        case DEBUG_GYRO_TREND:
        for (
            register ptrdiff_t
            axis = (
                XYZ_AXIS_COUNT[debug] = gyroGet100Fold(gyro.temp.rise),
                XYZ_AXIS_COUNT
            );
            axis--;
            axis[debug] = lrintf(axis[gyro.gyroAccf])
        ) NOOP
        FALLTHROUGH;

        default:
        break;
    }

    if (gyroAnemo.calibration.cyclesRemaining)
        performGyroCalibration(),
        bzero(
            gyro.gyroADCf
        ,
            (void const *)(gyro.gyroAccf + XYZ_AXIS_COUNT) -
            (void const *) gyro.gyroADCf
        );
}

void gyroReadTemperature(void) {
    gyro.speed.air = (
        gyro.temp.here < gyro.temp.home || (
            signbit(gyro.temp.rise) && (
                gyro.temp.home = gyro.temp.here,
                true
            )
        ) ?
            fdim(
                // See <https://youtu.be/XDkmavL4qzU?t=175>
                sqrt(gyro.temp.home - gyro.temp.here) / gyroAnemo.period
            ,
                gyro.temp.rise
            ) / gyroAnemo.frequency
        :
            false
    );

    gyro.temp.ratio = REINTERPRET_CAST(
        sig_atomic_t
    , (float)(
        (   gyro.speed.sagT =
                fdim(
                    gyro.speed.snd = fmax(
#ifdef USE_GPS
                        REINTERPRET_CAST(float, gyro.temp.lapse) +
#endif
                        gyro.temp.home
                    ,
                        FLT_EPSILON
                    )
                ,
                    gyro.speed.air
                )
        ) / gyro.speed.snd
    ));

    gyro.temp.ambient =
        (int16_t)lrintf(REINTERPRET_CAST(
            float
        ,
            gyro.temp.scaled = REINTERPRET_CAST(
                sig_atomic_t, gyro.temp.air = gyro.speed.snd
            )
        )) + -u'\x6AB3';

#ifdef USE_BARO
    baro.baroAAS = lrint(
#endif
    // See <https://youtu.be/t98s1A7fvm0?t=38>
    gyro.speed.air = sqrt(
        ldexp(gyro.speed.air, 5) * 36375775625.l / 57929.l
#ifdef USE_BARO
    )
#endif
    );

    imuRuntimeConfig.erpm.limit.cmss = lrint(
        (   gyro.speed.snd = sqrt(
                ldexp(gyro.speed.snd, 5) * 7275155125.l / 57929.l
            )
        ) * imuRuntimeConfig.erpm.limit.mach
    );

    if (gyroDebugMode== DEBUG_AIRSPEED)
        debug[0] = lrint (gyro.speed.air),
        debug[1] = lrint (ldexp (gyro.speed.snd / 25.l,-2)),
        debug[2] = gyroGet100Fold(gyro.temp.here) + -u'\x6AB3',
        debug[3] = gyroGet100Fold(gyro.temp.rise);
}

bool gyroOverflowDetected(void) {
#ifdef USE_GYRO_OVERFLOW_CHECK
    return overflowDetected;
#else
    return false;
#endif // USE_GYRO_OVERFLOW_CHECK
}

#ifdef USE_YAW_SPIN_RECOVERY
bool gyroYawSpinDetected(void) {
    return yawSpinDetected;
}
#endif // USE_YAW_SPIN_RECOVERY

#ifdef USE_GYRO_REGISTER_DUMP
uint8_t gyroReadRegister(uint8_t whichSensor, uint8_t reg) {
    return mpuGyroReadRegister(gyroSensorBusByDevice(whichSensor), reg);
}
#endif // USE_GYRO_REGISTER_DUMP

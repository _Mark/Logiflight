/*
 * This file is part of Cleanflight and Betaflight.
 *
 * Cleanflight and Betaflight are free software. You can redistribute
 * this software and/or modify this software under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Cleanflight and Betaflight are distributed in the hope that they
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.
 *
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <signal.h>

#include "common/time.h"
#include "pg/pg.h"
#include "drivers/barometer/barometer.h"

typedef enum {
    BARO_DEFAULT = 0,
    BARO_NONE = 1,
    BARO_BMP085 = 2,
    BARO_MS5611 = 3,
    BARO_BMP280 = 4,
    BARO_LPS = 5,
    BARO_QMP6988 = 6
} baroSensor_e;

#define TASK_BARO_DESIRED_PERIOD   23256

typedef struct barometerConfig_s {
    uint8_t baro_bustype;
    uint8_t baro_spi_device;
    ioTag_t baro_spi_csn;                   // Also used as XCLR (positive logic) for BMP085
    uint8_t baro_i2c_device;
    uint8_t baro_i2c_address;
    uint8_t baro_hardware;                  // Barometer hardware to use
} barometerConfig_t;

PG_DECLARE(barometerConfig_t, barometerConfig);

typedef struct baro_s {
    union {
        baroDev_t dev, pdev[1];
    };

   _Atomic float
    baroPressureScaled;
    sig_atomic_t
    baroPressure,                           // Pa
    baroAltitude,                           // cm
    baroVario,                              // cm/s
    baroAAS, baroTAS,                       // cm/s
    baroAccZ;                               // cm/s/s
} baro_t;

#define BaroAlt baroAltitude

extern baro_t baro;

bool baroDetect(baroDev_t *dev, baroSensor_e baroHardwareToUse);
bool isBaroCalibrationComplete(void);
void baroSetCalibrationCycles(void);
timeUs_t baroUpdate(timeUs_t);
#define isBaroReady isBaroCalibrationComplete
#define baroCalculateAltitude() (baro.baroAltitude)
void performBaroCalibrationCycle(void);

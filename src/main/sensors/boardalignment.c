/*
 * This file is part of Cleanflight and Betaflight.
 *
 * Cleanflight and Betaflight are free software. You can redistribute
 * this software and/or modify this software under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Cleanflight and Betaflight are distributed in the hope that they
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.
 *
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stdint.h>
#include <math.h>
#include <string.h>

#include "platform.h"

#include "common/maths.h"
#include "common/axis.h"

#include "pg/pg.h"
#include "pg/pg_ids.h"

#include "drivers/sensor.h"

#include "boardalignment.h"

static FAST_RAM_ZERO_INIT bool  isBoardAlignmentNonStd;
static FAST_RAM_ZERO_INIT float boardRotation[3][XYZ_AXIS_COUNT];

// no template required since defaults are zero
PG_REGISTER(boardAlignment_t, boardAlignment, PG_BOARD_ALIGNMENT, 0);

bool isBoardAlignmentStandard(const boardAlignment_t *boardAlignment) {
    return !boardAlignment->rollDegrees && !boardAlignment->pitchDegrees && !boardAlignment->yawDegrees;
}

void initBoardAlignment(
    register boardAlignment_t const * restrict const boardAlignment
) {
    if ((isBoardAlignmentNonStd =!isBoardAlignmentStandard(boardAlignment))) {
        sincosf(DEGREES_TO_RADIANS(
            boardAlignment-> rollDegrees
        ),
            boardRotation[1] + Y
        ,
            boardRotation[1] + X
        );
        sincosf(DEGREES_TO_RADIANS(
            boardAlignment->  yawDegrees
        ),
            boardRotation[2] + Y
        ,
            boardRotation[2] + X
        );
        sincosf(DEGREES_TO_RADIANS(
            boardAlignment->pitchDegrees
        ),
            boardRotation[0] + Z
        ,
            boardRotation[2] + Z
        );

        boardRotation[0][X] = boardRotation[2][Z] * boardRotation[2][X];
        boardRotation[0][Y] =-boardRotation[2][Z] * boardRotation[2][Y];
        boardRotation[1][Z] =-boardRotation[2][Z] * boardRotation[1][Y];
        boardRotation[2][Z]*= boardRotation[1][X];

        register float
        RATIO = boardRotation[1][X] * boardRotation[2][X];

        boardRotation[1][Y] =
            RATIO - (
                boardRotation[2][X] = boardRotation[1][X] * boardRotation[2][Y]
            ) * boardRotation[0][Z];
        boardRotation[2][X]-= boardRotation[0][Z] * RATIO;
        boardRotation[1][X] =
            (boardRotation[2][Y] = boardRotation[1][Y] * boardRotation[2][X])
                * boardRotation[0][Z] +
            (RATIO = boardRotation[1][Y] * boardRotation[2][Y]);
        boardRotation[2][Y]+= boardRotation[0][Z] * RATIO;
    }
}

static FAST_CODE_NOINVOKE void alignBoard(register float vec[restrict const]) {
    float const
    ARR[] = { vec[0], vec[1], vec[2] };

    for (register ptrdiff_t j = XYZ_AXIS_COUNT; j--;)
        vec[j] = ISRVALUE({
            register float
            SUM = 0.;

            for (register ptrdiff_t i = XYZ_AXIS_COUNT; i--;)
                SUM+= boardRotation[i][j] * ARR[i];

            SUM;
        });
}

FAST_CODE FAST_CODE_NOINLINE void alignSensors(
    register float         dest[restrict const],
    register uint8_t const rotation
) {
    if (rotation > CW0_DEG) {
        register fpVector2_t const
        VEC = 0[(fpVector2_t const *)dest];

        switch (rotation) {
            case CW90_DEG:
            dest[X] = VEC.y;
            dest[Y] =-VEC.x;
            break;

            case CW180_DEG:
            dest[X] =-VEC.x;
            dest[Y] =-VEC.y;
            break;

            case CW270_DEG:
            dest[X] =-VEC.y;
            dest[Y] = VEC.x;
            break;

            case CW0_DEG_FLIP:
            dest[X] =-VEC.x;
            goto DO_FINISH_FLIP;

            case CW90_DEG_FLIP:
            dest[X] = VEC.y;
            dest[Y] = VEC.x;
            goto DO_FINISH_FLIP;

            case CW180_DEG_FLIP:
            dest[Y] =-VEC.y;
            goto DO_FINISH_FLIP;

            default:
            dest[X] =-VEC.y;
            dest[Y] =-VEC.x;
DO_FINISH_FLIP:
            dest[Z] =-dest[Z];
            break;
        }
    }

    if (isBoardAlignmentNonStd)
        alignBoard(dest);
}

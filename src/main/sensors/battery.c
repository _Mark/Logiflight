/*
 * This file is part of Cleanflight and Betaflight.
 *
 * Cleanflight and Betaflight are free software. You can redistribute
 * this software and/or modify this software under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Cleanflight and Betaflight are distributed in the hope that they
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.
 *
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stdint.h>

#include "platform.h"

#include "build/debug.h"

#include "common/filter.h"
#include "common/maths.h"
#include "common/utils.h"

#include "config/feature.h"
#include "pg/pg.h"
#include "pg/pg_ids.h"

#include "drivers/adc.h"

#include "fc/runtime_config.h"
#include "fc/config.h"
#include "fc/controlrate_profile.h"
#include "fc/rc_controls.h"

#include "io/beeper.h"

#include "sensors/sensors.h"
#include "sensors/battery.h"
#include "sensors/gyro.h"

#include "scheduler/scheduler.h"

#include "flight/redline.h"
#include "flight/mixer.h"

/**
 * terminology: meter vs sensors
 *
 * voltage and current sensors are used to collect data.
 * - e.g. voltage at an MCU ADC input pin, value from an ESC sensor.
 *   sensors require very specific configuration, such as resistor values.
 * voltage and current meters are used to process and expose data collected from sensors to the rest of the system.
 * - e.g. a meter exposes normalized, and often filtered, values from a sensor.
 *   meters require different or little configuration.
 *   meters also have different precision concerns, and may use different units to the sensors.
 *
 */

#define VBAT_STABLE_MAX_DELTA 20
#define LVC_AFFECT_TIME 10000000 //10 secs for the LVC to slowly kick in

static FAST_RAM_ZERO_INIT timeUs_t       ibatLastServiced, deadlineTimeUs;
static FAST_RAM_ZERO_INIT currentMeter_t currentMeter;
static FAST_RAM_ZERO_INIT voltageMeter_t voltageMeter;

static FAST_RAM_ZERO_INIT long long         // cA
batteryLatestAmperage;

static FAST_RAM_ZERO_INIT sig_atomic_t      // cV
// Battery monitoring stuff
batteryAppliedVoltage,
batteryRestingVoltage,
batteryTakeoffVoltage,
batteryCellCount,
batteryMaximumVoltage,
batteryWarningVoltage,
batteryMinimumVoltage,
batteryRedlineVoltage,
batteryNominalVoltage,
batteryMeasErrVoltage;

       FAST_RAM_ZERO_INIT batteryVoltage_t   batteryVoltage;
       FAST_RAM_ZERO_INIT sig_atomic_t       consumptionState, voltageState,
                                             batteryState;
static FAST_RAM_ZERO_INIT stdev_t            voltageStats[1];
static FAST_RAM_ZERO_INIT lowVoltageCutoff_t lowVoltageCutoff;
static FAST_RAM_ZERO_INIT float              motorRedlineAmperage;
//

#ifndef DEFAULT_CURRENT_METER_SOURCE
#ifdef USE_VIRTUAL_CURRENT_METER
#define DEFAULT_CURRENT_METER_SOURCE CURRENT_METER_VIRTUAL
#else
#ifdef USE_MSP_CURRENT_METER
#define DEFAULT_CURRENT_METER_SOURCE CURRENT_METER_MSP
#else
#define DEFAULT_CURRENT_METER_SOURCE CURRENT_METER_NONE
#endif
#endif
#endif

#ifndef DEFAULT_VOLTAGE_METER_SOURCE
#define DEFAULT_VOLTAGE_METER_SOURCE VOLTAGE_METER_NONE
#endif

PG_REGISTER_WITH_RESET_TEMPLATE(batteryConfig_t, batteryConfig, PG_BATTERY_CONFIG, 3);

// See <https://blog.ampow.com/lipo-voltage-chart/>
PG_RESET_TEMPLATE(batteryConfig_t, batteryConfig,
                  // voltage
                  .vbatmaxcellvoltage     = 435, // ca. 100% capacity of LiHv
                  .vbatfullcellvoltage    = 415, // ca.  95% capacity of LiPo
                  .vbatmincellvoltage     = 373, // ca.  20% capacity of LiPo
                  .vbatwarningcellvoltage = 384, // ca.  50% capacity of LiPo
                  .voltageMeterSource = DEFAULT_VOLTAGE_METER_SOURCE,
                  .lvcPercentage = 100, //Off by default at 100%

                  // current
                  .batteryCapacity = 0,
                  .currentMeterSource = DEFAULT_CURRENT_METER_SOURCE,

                  // cells
                  .forceBatteryCellCount = 0, //0 will be ignored

                  // warnings / alerts
                  .useVBatAlerts = true,
                  .useConsumptionAlerts = false,
                  .consumptionWarningPercentage = 10,
                  .vbathysteresis = 95,     // percent

                  .vbatLpfPeriod = 35,
                  .ibatLpfPeriod = 10,
                  .vbatDurationForWarning = 0,
                  .vbatDurationForCritical = 0,
                 );

static FAST_CODE_NOINVOKE void batteryUpdateVoltage(
    register timeUs_t currentTimeUs
) {
#ifndef USE_ESC_SENSOR
    if (batteryConfig()->voltageMeterSource== VOLTAGE_METER_ADC) {
#else
    switch (batteryConfig()->voltageMeterSource) {
    case VOLTAGE_METER_ESC:
        if (feature(FEATURE_ESC_SENSOR)) {
            voltageMeterESCRefresh();
            voltageMeterESCReadCombined(&voltageMeter);
        }
        break;
    case VOLTAGE_METER_ADC:
#endif
        voltageMeterADCRefresh();
        voltageMeterADCRead(VOLTAGE_SENSOR_ADC_VBAT, &voltageMeter);
#ifdef USE_ESC_SENSOR
        break;
    default:
#else
    } else {
#endif
        isRedline.voltage = false;
        return;
    }

    if (ISRVALUE({
            register struct {
                int64_t latest, change;
            } const amperage = {
                currentMeter.amperageLatest
            ,
                amperage.latest - batteryLatestAmperage
            };

            amperage.change ?
// See <https://faculty.math.illinois.edu/~dpv/ECE110Notes/content/course_notes/course_notes_files/resistanceAndOhmsLaw.html#RES-OHM>
                batteryRestingVoltage = rquot(({
                    register struct {
                        int64_t voltage, power;
                    } const working = {
                        batteryAppliedVoltage
                    ,
                        (   batteryAppliedVoltage =
                                voltageMeter.unfiltered * 10
                        ) * batteryLatestAmperage
                    };

                    (batteryLatestAmperage = amperage.latest)
                        * working.voltage
                    - working.power;
                }),
                    amperage.change
                )
            :
                batteryRestingVoltage;
        }) > batteryTakeoffVoltage
    )
        batteryVoltage.rpmK = REINTERPRET_CAST(sig_atomic_t, fdimf(sqrtf(
            ((batteryTakeoffVoltage = batteryRestingVoltage)<< '\2') + true
        ),
            true
        )),
        batteryVoltage.peak =
            REINTERPRET_CAST(sig_atomic_t, (float)batteryRestingVoltage);

    if (loggingThrottle && batteryRestingVoltage > batteryAppliedVoltage)
        batteryVoltage.thrK = REINTERPRET_CAST(sig_atomic_t, constrainf(
            REINTERPRET_CAST(float, loggingThrottle)
                * (batteryRestingVoltage - batteryNominalVoltage)
                / (batteryRestingVoltage - batteryAppliedVoltage)
        ,
            1e-3
        ,
            1e3
        ));

    isRedline.voltage =
        (   isRedline.voltage ?
                batteryRedlineVoltage + batteryMeasErrVoltage
            :
                batteryRedlineVoltage
        )>= voltageMeter.unfiltered;

    register batteryConfig_t const * restrict const
    config = batteryConfig();

{   register sig_atomic_t const
    voltageRest = batteryRestingVoltage - batteryMeasErrVoltage;

    switch (voltageState) {
        case BATTERY_OK:
        if (batteryWarningVoltage < voltageRest)
            NOOP
        else if (deadlineTimeUs)
            if (currentTimeUs < deadlineTimeUs)
                NOOP
            else
                deadlineTimeUs = false,
                voltageState   = BATTERY_WARNING;
        else
            deadlineTimeUs =
                config->vbatDurationForWarning * (timeUs_t)100000
                    + currentTimeUs;
        break;

        case BATTERY_WARNING:
        if (batteryWarningVoltage < voltageRest)
            voltageState = BATTERY_OK;
        else if (batteryMinimumVoltage < voltageRest)
            NOOP
        else if (deadlineTimeUs)
            if (currentTimeUs < deadlineTimeUs)
                NOOP
            else
                deadlineTimeUs = false,
                voltageState   = BATTERY_CRITICAL;
        else
            deadlineTimeUs =
                config->vbatDurationForCritical * (timeUs_t)100000
                    + currentTimeUs;
        break;

        case BATTERY_CRITICAL:
        if (batteryMinimumVoltage < voltageRest)
            voltageState = BATTERY_WARNING;
        break;

        case BATTERY_INIT:
        if (devPush(voltageStats, voltageRest),
            voltageStats->m_n > CALIBRATING_BARO_CYCLES
        )
            if (batteryMeasErrVoltage = lrintf(
                    devStandardDeviation(voltageStats)
                ),
                debugMode== DEBUG_BATTERY
            )
                debug[3] = batteryMeasErrVoltage;
        else
            break;
        FALLTHROUGH;

        default:
        consumptionState = voltageState = (
            batteryRestingVoltage > VBAT_CELL_VOTAGE_RANGE_MIN * 2 - 1 || (
                batteryRestingVoltage > VBAT_CELL_VOTAGE_RANGE_MIN - 1 &&
                batteryRestingVoltage < VBAT_CELL_VOTAGE_RANGE_MAX + 1
            ) ? (
                lowVoltageCutoff.percentage = 100,
                lowVoltageCutoff.startTime  = currentTimeUs,
                BATTERY_OK
            ) : (
                batteryRedlineVoltage =
                batteryMinimumVoltage =
                batteryWarningVoltage = batteryCellCount = false,
                BATTERY_NOT_PRESENT
            )
        );
        break;
    }
}
    batteryState = MAX(
        config->useConsumptionAlerts && config->batteryCapacity ?
            consumptionState = ISRVALUE({
                register uint8_t const
                capacityPercentage = calculateBatteryPercentageRemaining();

                capacityPercentage ?
                    capacityPercentage<= config->consumptionWarningPercentage
                :
                    BATTERY_CRITICAL;
            })
        :
            consumptionState
    ,
        voltageState
    );

    if (config->lvcPercentage < (uint8_t)'\x64') {
        if (lowVoltageCutoff.enabled)
            lowVoltageCutoff.percentage = (
                (currentTimeUs-= lowVoltageCutoff.startTime) < LVC_AFFECT_TIME ?
                    (uint8_t)'\x64' - (uint8_t)rquot(
                        ((uint8_t)'\x64' - config->lvcPercentage)
                            * currentTimeUs
                    ,
                        LVC_AFFECT_TIME
                    )
                :
                    config->lvcPercentage
            );
        else if (voltageState== BATTERY_CRITICAL)
            lowVoltageCutoff.enabled = true,
            lowVoltageCutoff.percentage = '\x64',
            lowVoltageCutoff.startTime = currentTimeUs;
    }

    if (config->useVBatAlerts &&
        voltageState &&
        voltageState < BATTERY_NOT_PRESENT
    )
        beeper(
            voltageState== BATTERY_CRITICAL ?
                BEEPER_BAT_CRIT_LOW : BEEPER_BAT_LOW
        );
}

const lowVoltageCutoff_t *getLowVoltageCutoff(void) {
    return &lowVoltageCutoff;
}

const char * getBatteryStateString(void) {
    return
    getBatteryState()[
        (char const *[]){ "OK", "WARNING", "CRITICAL", "UNPLUGGED", "INIT" }
    ];
}

void batteryInit(void) {
    batteryVoltage.thrK = REINTERPRET_CAST(sig_atomic_t, 1.f);
    //
    // presence
    //
    batteryState =
    //
    // voltage
    //
    voltageState = BATTERY_INIT;
    lowVoltageCutoff.percentage = 100;
    voltageMeterReset(&voltageMeter);
    switch (batteryConfig()->voltageMeterSource) {
    case VOLTAGE_METER_ESC:
#ifdef USE_ESC_SENSOR
        voltageMeterESCInit();
#endif
        break;
    case VOLTAGE_METER_ADC:
        voltageMeterADCInit();
        break;
    default:
        break;
    }
    //
    // current
    //
    consumptionState = BATTERY_OK;
    currentMeterReset(&currentMeter);
    switch (batteryConfig()->currentMeterSource) {
    case CURRENT_METER_ADC:
        currentMeterADCInit();
        break;
    case CURRENT_METER_VIRTUAL:
#ifdef USE_VIRTUAL_CURRENT_METER
        currentMeterVirtualInit();
#endif
        break;
    case CURRENT_METER_ESC:
#ifdef ESC_SENSOR
        currentMeterESCInit();
#endif
        break;
    case CURRENT_METER_MSP:
#ifdef USE_MSP_CURRENT_METER
        currentMeterMSPInit();
#endif
        break;
    default:
        break;
    }
}

void batteryReInit(void) {
{   register batteryConfig_t const * restrict const
    config = batteryConfig();

    batteryMaximumVoltage =
        (   batteryCellCount = (
                config->forceBatteryCellCount ?
                    config->forceBatteryCellCount
                :
                    ((batteryTakeoffVoltage = batteryRestingVoltage) - true)
                        / config->vbatmaxcellvoltage
                    + true
            )
        ) * config->vbatmaxcellvoltage;
    batteryWarningVoltage = config->vbatwarningcellvoltage * batteryCellCount;
    batteryMinimumVoltage = config->vbatmincellvoltage     * batteryCellCount;
}
{   register redlineConfig_t const * restrict const
    config = redlineConfig();

    batteryRedlineVoltage = config->vbatRedlineCellVoltage * batteryCellCount;
      motorRedlineAmperage= config->motorRedlineAmperage;
}
    if (debugMode== DEBUG_BATTERY)
        debug[2] = batteryCellCount;

    changePidProfileFromCellCount(batteryCellCount);

    batteryNominalVoltage =
        currentControlRateProfile->vbat_comp_ref * batteryCellCount;
}

static FAST_CODE_NOINVOKE void batteryUpdateCurrentMeter(
    register timeUs_t lastUpdateAt
) {
    if (batteryCellCount) NOOP else {
DO_RESET_CUR_METER:
        currentMeterReset(&currentMeter);
        return;
    }

    lastUpdateAt = ISRVALUE({
        register timeUs_t const
        lastTimeUs = ibatLastServiced;

        (ibatLastServiced = lastUpdateAt) - lastTimeUs;
    });

    switch (batteryConfig()->currentMeterSource) {
    case CURRENT_METER_ADC:
        currentMeterADCRefresh(lastUpdateAt);
        currentMeterADCRead(&currentMeter);
        break;
    case CURRENT_METER_VIRTUAL: {
#ifdef USE_VIRTUAL_CURRENT_METER
        throttleStatus_e throttleStatus = calculateThrottleStatus();
        bool throttleLowAndMotorStop = (throttleStatus == THROTTLE_LOW && feature(FEATURE_MOTOR_STOP));
        int32_t throttleOffset = (int32_t)rcCommand[THROTTLE] - 1000;
        currentMeterVirtualRefresh(lastUpdateAt, ARMING_FLAG(ARMED), throttleLowAndMotorStop, throttleOffset);
        currentMeterVirtualRead(&currentMeter);
#endif
        break;
    }
    case CURRENT_METER_ESC:
#ifdef USE_ESC_SENSOR
        if (feature(FEATURE_ESC_SENSOR)) {
            currentMeterESCRefresh(lastUpdateAt);
            currentMeterESCReadCombined(&currentMeter);
        }
#endif
        break;
    case CURRENT_METER_MSP:
#ifdef USE_MSP_CURRENT_METER
        currentMeterMSPRefresh(currentTimeUs);
        currentMeterMSPRead(&currentMeter);
#endif
        break;
    default:
        goto DO_RESET_CUR_METER;
    }

    isRedline.amperage =
        (sig_atomic_t)ISRVALUE({
            register float const
            ibatMax =
                REINTERPRET_CAST(float, throttleGain) * motorRedlineAmperage;

            isRedline.amperage ?
                ldexpf(batteryConfig()->vbathysteresis * ibatMax / 25.f,-2)
            :
                ibatMax;
        }) < currentMeter.amperageLatest;
}

FAST_CODE_NOINVOKE void batteryUpdateStates(
    register timeUs_t const currentTimeUs
) {
    batteryUpdateCurrentMeter(currentTimeUs);
    batteryUpdateVoltage     (currentTimeUs);

    if (debugMode== DEBUG_BATTERY)
        debug[0] = batteryRestingVoltage,
        debug[1] = voltageMeter.unfiltered;
}

uint8_t calculateBatteryPercentageRemaining(void) {
    return
    batteryCellCount ? ISRVALUE({
        register struct {
            sig_atomic_t mAh, volts;
        } batt = {.volts =
            (batt.mAh = batteryConfig()->batteryCapacity) ?
                batt.mAh - currentMeter.mAhDrawn
            : (
                batt.mAh =
                batteryMaximumVoltage - batteryMinimumVoltage,
                batteryRestingVoltage - batteryMinimumVoltage
            )
        };

           batt.volts                                      < true ?
            false
        : (batt.volts = rquot(batt.volts * 100, batt.mAh)) < 101  ?
            (uint8_t)batt.volts
        :  batt.volts                                      < 149  ?
            (uint8_t)'\x64'
        :
            false;
    }) :
        (uint8_t)'\x64';
}

uint16_t getBatteryVoltage(void) {
    return rquot(batteryRestingVoltage, 10);
}

uint16_t getBatteryVoltageLatest(void) {
    return voltageMeter.unfiltered;
}

uint8_t getBatteryCellCount(void) {
    return batteryCellCount;
}

uint16_t getBatteryAverageCellVoltage(void) {
    return rquot(batteryRestingVoltage, batteryCellCount * 10);
}

int32_t getAmperage(void) {
    return currentMeter.amperage;
}

int32_t getAmperageLatest(void) {
    return currentMeter.amperageLatest;
}

int32_t getMAhDrawn(void) {
    return currentMeter.mAhDrawn;
}

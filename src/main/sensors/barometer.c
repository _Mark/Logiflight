/*
 * This file is part of Cleanflight and Betaflight.
 *
 * Cleanflight and Betaflight are free software. You can redistribute
 * this software and/or modify this software under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Cleanflight and Betaflight are distributed in the hope that they
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.
 *
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stdint.h>
#include <math.h>
#include <strings.h>

#include "platform.h"

#include "pg/pg.h"
#include "pg/pg_ids.h"

#include "drivers/bus.h"
#include "drivers/bus_spi.h"
#include "drivers/io.h"

#include "drivers/barometer/barometer.h"
#include "drivers/barometer/barometer_bmp085.h"
#include "drivers/barometer/barometer_bmp280.h"
#include "drivers/barometer/barometer_qmp6988.h"
#include "drivers/barometer/barometer_fake.h"
#include "drivers/barometer/barometer_ms5611.h"
#include "drivers/barometer/barometer_lps.h"

#include "fc/runtime_config.h"

#ifdef USE_GPS
#include "io/gps.h"
#endif

#include "sensors/barometer.h"
#include "sensors/gyro.h"
#include "sensors/sensors.h"

#include "scheduler/scheduler.h"

#ifdef USE_HARDWARE_REVISION_DETECTION
#include "hardware_revision.h"
#endif

#define BARO_SAMPLE_COUNT_MAX (\
    ((TEMP_SAMPLE_COUNT_MAX - 1) * TASK_GYROPID_DESIRED_PERIOD)\
        / TASK_BARO_DESIRED_PERIOD\
    + 2\
)

static FAST_RAM_ZERO_INIT bool isBaroDataNew;

// See <https://terpconnect.umd.edu/~toh/spectrum/Smoothing.html>
static FAST_RAM_ZERO_INIT  struct {
    struct {
        ptrdiff_t back, mid1, mid2, mid0;
    } index;

    struct {
        ptrdiff_t free, max_4, max_2, max;
    } size;

    timeUs_t time[BARO_SAMPLE_COUNT_MAX];

    struct {
       uint32_t
        raw[BARO_SAMPLE_COUNT_MAX];

        struct {
           uint64_t here;
            double  heref, home;
        } sum;

        double temp, ratio;
    } pressure;

    struct {
        int32_t raw[BARO_SAMPLE_COUNT_MAX];
        int64_t sum;
    } altitude;

    struct {
        ptrdiff_t index;

        struct {
            timeUs_t time[(BARO_SAMPLE_COUNT_MAX - 1) / 2 + 1];
            int32_t  vec [(BARO_SAMPLE_COUNT_MAX - 1) / 2 + 1];
        } delta;
        struct {
            int64_t  time, vec;
        } sum;
    } vario1;
    struct {
        ptrdiff_t index;

        struct {
            timeUs_t time[(BARO_SAMPLE_COUNT_MAX - 1) / 4 + 1];
            int32_t  vec [(BARO_SAMPLE_COUNT_MAX - 1) / 4 + 1];
        } delta;
        struct {
            int64_t  time, vec;
        } sum;
    } vario2, vario0;
} baroStats;

FAST_RAM_ZERO_INIT baro_t baro;

PG_REGISTER_WITH_RESET_FN(barometerConfig_t, barometerConfig, PG_BAROMETER_CONFIG, 0);

void pgResetFn_barometerConfig(barometerConfig_t *barometerConfig) {
    barometerConfig->baro_hardware = BARO_DEFAULT;
    // For backward compatibility; ceate a valid default value for bus parameters
    //
    // 1. If DEFAULT_BARO_xxx is defined, use it.
    // 2. Determine default based on USE_BARO_xxx
    //   a. Precedence is in the order of popularity; BMP280, MS5611 then BMP085, then
    //   b. If SPI variant is specified, it is likely onboard, so take it.
#if !(defined(DEFAULT_BARO_SPI_BMP280) || defined(DEFAULT_BARO_BMP280) || defined(DEFAULT_BARO_SPI_MS5611) || defined(DEFAULT_BARO_MS5611) || defined(DEFAULT_BARO_BMP085) || defined(DEFAULT_BARO_SPI_LPS) || defined(DEFAULT_BARO_SPI_QMP6988) || defined(DEFAULT_BARO_QMP6988))
#if defined(USE_BARO_BMP280) || defined(USE_BARO_SPI_BMP280)
#if defined(USE_BARO_SPI_BMP280)
#define DEFAULT_BARO_SPI_BMP280
#else
#define DEFAULT_BARO_BMP280
#endif
#elif defined(USE_BARO_MS5611) || defined(USE_BARO_SPI_MS5611)
#if defined(USE_BARO_SPI_MS5611)
#define DEFAULT_BARO_SPI_MS5611
#else
#define DEFAULT_BARO_MS5611
#endif
#elif defined(USE_BARO_QMP6988) || defined(USE_BARO_SPI_QMP6988)
#if defined(USE_BARO_SPI_QMP6988)
#define DEFAULT_BARO_SPI_QMP6988
#else
#define DEFAULT_BARO_QMP6988
#endif
#elif defined(USE_BARO_SPI_LPS)
#define DEFAULT_BARO_SPI_LPS
#elif defined(DEFAULT_BARO_BMP085)
#define DEFAULT_BARO_BMP085
#endif
#endif
#if defined(DEFAULT_BARO_SPI_BMP280)
    barometerConfig->baro_bustype = BUSTYPE_SPI;
    barometerConfig->baro_spi_device = SPI_DEV_TO_CFG(spiDeviceByInstance(BMP280_SPI_INSTANCE));
    barometerConfig->baro_spi_csn = IO_TAG(BMP280_CS_PIN);
    barometerConfig->baro_i2c_device = I2C_DEV_TO_CFG(I2CINVALID);
    barometerConfig->baro_i2c_address = 0;
#elif defined(DEFAULT_BARO_SPI_MS5611)
    barometerConfig->baro_bustype = BUSTYPE_SPI;
    barometerConfig->baro_spi_device = SPI_DEV_TO_CFG(spiDeviceByInstance(MS5611_SPI_INSTANCE));
    barometerConfig->baro_spi_csn = IO_TAG(MS5611_CS_PIN);
    barometerConfig->baro_i2c_device = I2C_DEV_TO_CFG(I2CINVALID);
    barometerConfig->baro_i2c_address = 0;
#elif defined(DEFAULT_BARO_SPI_QMP6988)
    barometerConfig->baro_bustype = BUSTYPE_SPI;
    barometerConfig->baro_spi_device = SPI_DEV_TO_CFG(spiDeviceByInstance(QMP6988_SPI_INSTANCE));
    barometerConfig->baro_spi_csn = IO_TAG(QMP6988_CS_PIN);
    barometerConfig->baro_i2c_device = I2C_DEV_TO_CFG(I2CINVALID);
    barometerConfig->baro_i2c_address = 0;
#elif defined(DEFAULT_BARO_SPI_LPS)
    barometerConfig->baro_bustype = BUSTYPE_SPI;
    barometerConfig->baro_spi_device = SPI_DEV_TO_CFG(spiDeviceByInstance(LPS_SPI_INSTANCE));
    barometerConfig->baro_spi_csn = IO_TAG(LPS_CS_PIN);
    barometerConfig->baro_i2c_device = I2C_DEV_TO_CFG(I2CINVALID);
    barometerConfig->baro_i2c_address = 0;
#elif defined(DEFAULT_BARO_MS5611) || defined(DEFAULT_BARO_BMP280) || defined(DEFAULT_BARO_BMP085)||defined(DEFAULT_BARO_QMP6988)
    // All I2C devices shares a default config with address = 0 (per device default)
    barometerConfig->baro_bustype = BUSTYPE_I2C;
    barometerConfig->baro_i2c_device = I2C_DEV_TO_CFG(BARO_I2C_INSTANCE);
    barometerConfig->baro_i2c_address = 0;
    barometerConfig->baro_spi_device = SPI_DEV_TO_CFG(SPIINVALID);
    barometerConfig->baro_spi_csn = IO_TAG_NONE;
#else
    barometerConfig->baro_hardware = BARO_NONE;
    barometerConfig->baro_bustype = BUSTYPE_NONE;
    barometerConfig->baro_i2c_device = I2C_DEV_TO_CFG(I2CINVALID);
    barometerConfig->baro_i2c_address = 0;
    barometerConfig->baro_spi_device = SPI_DEV_TO_CFG(SPIINVALID);
    barometerConfig->baro_spi_csn = IO_TAG_NONE;
#endif
}

#ifdef USE_BARO

bool baroDetect(baroDev_t *dev, baroSensor_e baroHardwareToUse) {
    // Detect what pressure sensors are available. baro->update() is set to sensor-specific update function
    #define baroHardware baroHardwareToUse
#if !defined(USE_BARO_BMP085) && !defined(USE_BARO_MS5611) && !defined(USE_BARO_SPI_MS5611) && !defined(USE_BARO_BMP280) && !defined(USE_BARO_SPI_BMP280)&& !defined(USE_BARO_QMP6988) && !defined(USE_BARO_SPI_QMP6988)
    UNUSED(dev);
#endif
    switch (barometerConfig()->baro_bustype) {
    case BUSTYPE_I2C:
#ifdef USE_I2C
        dev->busdev.bustype = BUSTYPE_I2C;
        dev->busdev.busdev_u.i2c.device = I2C_CFG_TO_DEV(barometerConfig()->baro_i2c_device);
        dev->busdev.busdev_u.i2c.address = barometerConfig()->baro_i2c_address;
#endif
        break;
    case BUSTYPE_SPI:
#ifdef USE_SPI
        dev->busdev.bustype = BUSTYPE_SPI;
        spiBusSetInstance(&dev->busdev, spiInstanceByDevice(SPI_CFG_TO_DEV(barometerConfig()->baro_spi_device)));
        dev->busdev.busdev_u.spi.csnPin = IOGetByTag(barometerConfig()->baro_spi_csn);
#endif
        break;
    default:
        return false;
    }
    switch (baroHardware) {
    case BARO_DEFAULT:
        FALLTHROUGH;
    case BARO_BMP085:
#ifdef USE_BARO_BMP085
    {
        const bmp085Config_t *bmp085Config = NULL;
#if defined(BARO_XCLR_GPIO) && defined(BARO_EOC_GPIO)
        static const bmp085Config_t defaultBMP085Config = {
            .xclrIO = IO_TAG(BARO_XCLR_PIN),
            .eocIO = IO_TAG(BARO_EOC_PIN),
        };
        bmp085Config = &defaultBMP085Config;
#endif
        if (bmp085Detect(bmp085Config, dev)) {
            baroHardware = BARO_BMP085;
            break;
        }
    }
#endif
    FALLTHROUGH;
    case BARO_MS5611:
#if defined(USE_BARO_MS5611) || defined(USE_BARO_SPI_MS5611)
        if (ms5611Detect(dev)) {
            baroHardware = BARO_MS5611;
            break;
        }
#endif
        FALLTHROUGH;
    case BARO_LPS:
#if defined(USE_BARO_SPI_LPS)
        if (lpsDetect(dev)) {
            baroHardware = BARO_LPS;
            break;
        }
#endif
        FALLTHROUGH;
    case BARO_BMP280:
#if defined(USE_BARO_BMP280) || defined(USE_BARO_SPI_BMP280)
        if (bmp280Detect(dev)) {
            baroHardware = BARO_BMP280;
            break;
        }
#endif
        FALLTHROUGH;
    case BARO_QMP6988:
#if defined(USE_BARO_QMP6988) || defined(USE_BARO_SPI_QMP6988)
        if (qmp6988Detect(dev)) {
            baroHardware = BARO_QMP6988;
            break;
        }
#endif
        FALLTHROUGH;
    case BARO_NONE:
        baroHardware = BARO_NONE;
        break;
    }
    if (baroHardware == BARO_NONE) {
        return false;
    }
    detectedSensors[SENSOR_INDEX_BARO] = baroHardware;
    #undef baroHardware
    sensorsSet(SENSOR_BARO);
    return true;
}

FAST_CODE_NOINVOKE bool isBaroCalibrationComplete(void) {
    return!baroStats.size.free;
}

void baroSetCalibrationCycles(void) {
    baroStats.pressure.ratio =
        ISRVALUE({
            register u32Tensor2_t const
            TIME = { {
                //gyroConfig()->temp_phase_delay * 500ul
                0xFA * 500
            ,
                baro.dev.ut_delay + baro.dev.up_delay
            } };

            baroStats.size.max_4 = ( TIME.x         - true) / TIME.y + true;
            baroStats.size.max_2 = ((TIME.x<< true) - true) / TIME.y + true;
            baroStats.size.max   = ((TIME.x<< '\2') - true) / TIME.y + true;
        }) * baro.dev.baro_4053Pa;
}

FAST_CODE_NOINVOKE timeUs_t baroUpdate(register timeUs_t const currentTimeUs) {
    void updateFn(void) {
        baro.dev.get_up(baro.pdev);

        (   baroStats.index.back =
                (baroStats.index.back + true) % baroStats.size.max
        )[baroStats.time] = currentTimeUs;

        baroStats.pressure.sum.here-=
            baroStats.index.back[baroStats.pressure.raw];

        baro.dev.calculate(baroStats.index.back + baroStats.pressure.raw, NULL);

        baro.baroPressureScaled =
            ldexp(
                baroStats.pressure.sum.heref=
                baroStats.pressure.sum.here+=
                    baroStats.index.back[baroStats.pressure.raw]
            ,
                6
            ) * 625.l / baroStats.pressure.ratio;
        baro.baroPressure =
            rquot(
                baroStats.pressure.sum.here
            ,
                baro.dev.baro_1Pa * baroStats.size.max
            );
        baro.baroAltitude =
            rquot((
                baroStats.altitude.sum-=
                    baroStats.index.back[baroStats.altitude.raw],
                baroStats.altitude.sum+=
// See <http://www.theochem.ru.nl/~pwormer/Knowino/knowino.org/wiki/Barometric_formula.html>
                    baroStats.index.back[baroStats.altitude.raw] = lrint(ldexp(
                        (   pow(baroStats.pressure.sum.home / (
                                    baroStats.size.free &&
                                 !--baroStats.size.free ? ({
                                        if (baroStats.pressure.sum.here)
                                            baroStats.pressure.sum.home =
                                            baroStats.pressure.sum.here,
                                            baroStats.pressure.temp =
                                                REINTERPRET_CAST(
                                                    float, gyro.temp.scaled
                                                ) + 27315e5f;
                                        else
                                            baroStats.size.free =
                                            baroStats.size.max;

                                        baroStats.pressure.sum.heref;
                                    }) :
                                        baroStats.pressure.sum.heref
/*
                        // See <https://www.grc.nasa.gov/WWW/BGH/isentrop.html>
                                        pow(REINTERPRET_CAST(
                                            float, gyro.temp.ratio
                                        ),
                                            3.5l
                                        ) * baroStats.pressure.sum.heref
*/
                                )
                            ,
                                .18997371807040302945777442074731l
                            ) * baroStats.pressure.temp
                              - baroStats.pressure.temp
                        ) / 649.l
                    ,
                       -true
                    ))
            ),
                baroStats.size.max
            );

        baroStats.vario1.sum.time-=
            (   baroStats.vario1.index =
                    (baroStats.vario1.index + 1) % baroStats.size.max_2
            )[baroStats.vario1.delta.time];
        baroStats.vario1.sum.time+=
            baroStats.vario1.index[baroStats.vario1.delta.time] =
                (   baroStats.index.mid2 =
                        (   (   baroStats.index.mid1 =
                                    (   baroStats.index.back +
                                        baroStats.size.max_2
                                    ) % baroStats.size.max
                            ) + baroStats.size.max_4
                        ) % baroStats.size.max
                )[baroStats.time] -
                (   baroStats.index.mid0 =
                        (baroStats.index.back + baroStats.size.max_4)
                            % baroStats.size.max
                )[baroStats.time];

        baro.baroTAS = ({
            register s64Tensor2_t
            SQ_NORM = { {
                (   SQ_NORM.x = baro.baroVario =
                        rquot(
                            (   baroStats.vario1.sum.vec-=
                                    baroStats.vario1.index[
                                        baroStats.vario1.delta.vec
                                    ],
                                baroStats.vario1.sum.vec+=
                                    baroStats.vario1.index[
                                        baroStats.vario1.delta.vec
                                    ] =
                                    baroStats.altitude.raw[
                                        baroStats.index.mid2
                                    ] -
                                    baroStats.altitude.raw[
                                        baroStats.index.mid0
                                    ]
                            ) * 1000000ll
                        ,
                            baroStats.vario1.sum.time
                        )
                ) * SQ_NORM.x
            ,
                (SQ_NORM.y = baro.baroAAS) * SQ_NORM.y
            } };

            SQ_NORM.y > SQ_NORM.x ? lrint(sqrt(SQ_NORM.y - SQ_NORM.x)) : 0l;
        });
        baro.baroAccZ = ISRVALUE({
            register int64_t const
            RATIO = baroStats.size.max_2 * 1000000000000ll;

            rquot(
                (   baroStats.vario2.sum.vec-=
                        (   baroStats.vario2.index =
                                (baroStats.vario2.index + 1)
                                    % baroStats.size.max_4
                        )[baroStats.vario2.delta.vec],
                    baroStats.vario2.sum.vec+=
                        baroStats.vario2.index[baroStats.vario2.delta.vec] =
                            baroStats.altitude.raw[baroStats.index.back] -
                            baroStats.altitude.raw[baroStats.index.mid2]
                ) * RATIO
            ,
                (   baroStats.vario2.sum.time-=
                        baroStats.vario2.index[baroStats.vario2.delta.time],
                    baroStats.vario2.sum.time+=
                        baroStats.vario2.index[baroStats.vario2.delta.time] =
                            baroStats.time[baroStats.index.back] -
                            baroStats.time[baroStats.index.mid2]
                ) * baroStats.vario1.sum.time
            ) -
            rquot(
                (   baroStats.vario0.sum.vec-=
                        (   baroStats.vario0.index =
                                (baroStats.vario0.index + 1)
                                    % baroStats.size.max_4
                        )[baroStats.vario0.delta.vec],
                    baroStats.vario0.sum.vec+=
                        baroStats.vario0.index[baroStats.vario0.delta.vec] =
                            baroStats.altitude.raw[baroStats.index.mid1] -
                            baroStats.altitude.raw[baroStats.index.mid0]
                ) * RATIO
            ,
                (   baroStats.vario0.sum.time-=
                        (   baroStats.vario0.index =
                                (baroStats.vario0.index + 1)
                                    % baroStats.size.max_4
                        )[baroStats.vario0.delta.time],
                    baroStats.vario0.sum.time+=
                        baroStats.vario0.index[baroStats.vario0.delta.time] =
                            baroStats.time[baroStats.index.mid1] -
                            baroStats.time[baroStats.index.mid0]
                ) * baroStats.vario1.sum.time
            );
        });
    }

    return
    baro.dev.get_ut ?
        (isBaroDataNew =!isBaroDataNew) ?
            baro.dev.get_ut(baro.pdev),
            baro.dev.up_delay
        : (
            updateFn(),
            baro.dev.ut_delay
        )
    : (
        updateFn(),
        baro.dev.up_delay
    );
}

void performBaroCalibrationCycle(void) {
    if (baroStats.size.free) NOOP else {
        baroStats.size.free = baroStats.size.max;

        if (isnormal(baroStats.pressure.ratio)) NOOP else
            baroStats.pressure.ratio = true;

        if (isnormal(baroStats.pressure.sum.home)) NOOP else
            baroStats.pressure.sum.home =
                baroStats.size.max * baro.dev.baro_1Pa * 101325ull /* Pa */;
   }
}

#endif /* BARO */

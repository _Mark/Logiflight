/*
 * This file is part of Cleanflight and Betaflight.
 *
 * Cleanflight and Betaflight are free software. You can redistribute
 * this software and/or modify this software under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Cleanflight and Betaflight are distributed in the hope that they
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.
 *
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <signal.h>

#include "common/time.h"

#include "fc/runtime_config.h"

#include "flight/imu.h"

#if defined(USE_BARO) || defined(USE_GPS)
extern sig_atomic_t
(* getEstimatedAltitude)(void),             // cm above "home level"
(* getEstimatedVario   )(void);             // cm/s upwards
#else
#define getEstimatedAltitude() (false)
 #ifdef USE_ACC_SENSOR
#define getEstimatedVario() (imuASI.Vario)
 #endif
#endif
#define isAltitudeOffset() (ARMING_FLAG(WAS_EVER_ARMED))
void calculateEstimatedAltitude(timeUs_t currentTimeUs);

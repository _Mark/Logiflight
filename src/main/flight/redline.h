/*
 * This file is part of Logiflight.
 *
 * Logiflight is free software. You can redistribute
 * this software and/or modify this software under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Logiflight is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.
 *
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <signal.h>

#include "platform.h"

#ifdef USE_ESC_SENSOR
#include "flight/imu.h"
#endif

#include "pg/pg.h"

#if defined(USE_ACC_SENSOR) || defined(USE_GPS)
#define USE_MOVE_SENSOR
#endif
#if defined(USE_ALT_HOLD) || defined(USE_GPS_RESCUE)
#define USE_ALT
#endif

typedef union {
    sig_atomic_t const active;

  __PACKED_STRUCT {
        sig_atomic_t
#ifdef USE_ESC_SENSOR
        temperature:1,
#endif
        amperage:1, voltage:1;

#if defined(USE_MOVE_SENSOR) || defined(USE_ESC_SENSOR)
      __PACKED_STRUCT {
            sig_atomic_t
 #ifdef USE_ESC_SENSOR
  #ifdef USE_ACC_SENSOR
            air:1,
  #endif
  #ifdef USE_GPS
            gnd:1,
  #endif
            tip:1;
 #else
  #ifdef USE_GPS
   #ifdef USE_ACC_SENSOR
            air:1,
   #endif
            gnd:1;
  #else
            air:1;
  #endif
 #endif
        } speed;
#endif
    };
} redlineFlag_t;

typedef struct {
   uint16_t
    imuRedlineEAS, imuHighestTAS,           // cm/s
    motorRedlineAmperage,                   // cA
    vbatRedlineCellVoltage;                 // cV

#ifdef USE_GPS
   uint16_t gpsRedlineGndSpeed, gpsHighestGndSpeed; // cm/s
#endif

#ifdef USE_ALT
   uint16_t varioRedlineAltitude;           // m AGL
#endif
#ifdef USE_ALT_HOLD
   uint16_t varioSetpointLimit[2];          // cm/s down, cm/s up
#endif

#ifdef USE_ESC_SENSOR
    uint16_t propDiameter;                  // mm
    uint8_t  propRedlineTipSpeed;           // units of MACH 0.01

     int16_t escRedlineTemperature;         // °C
#endif
} redlineConfig_t;

extern redlineFlag_t isRedline;

PG_DECLARE(redlineConfig_t, redlineConfig);

#define redlineIsActive() ((_Bool)isRedline.active)

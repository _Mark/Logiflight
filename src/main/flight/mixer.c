/*
 * This file is part of Cleanflight and Betaflight and Emuflight.
 *
 * Cleanflight and Betaflight are free software. You can redistribute
 * this software and/or modify this software under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Cleanflight and Betaflight are distributed in the hope that they
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.
 *
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <math.h>

#include "platform.h"

#include "build/build_config.h"
#include "build/debug.h"

#include "common/axis.h"
#include "common/filter.h"
#include "common/maths.h"

#include "config/feature.h"
#include "pg/pg.h"
#include "pg/pg_ids.h"
#include "pg/rx.h"
#include "pg/pinio.h"
#include "pg/piniobox.h"

#include "interface/msp_box.h"

#include "drivers/pwm_output.h"
#include "drivers/pwm_esc_detect.h"
#include "drivers/time.h"
#include "drivers/io.h"

#include "io/gps.h"
#include "io/motors.h"

#include "fc/config.h"
#include "fc/controlrate_profile.h"
#include "fc/rc_controls.h"
#include "fc/rc_modes.h"
#include "fc/runtime_config.h"
#include "fc/fc_core.h"
#include "fc/fc_rc.h"

#include "flight/redline.h"
#include "flight/failsafe.h"
#include "flight/imu.h"
#include "flight/gps_rescue.h"
#include "flight/mixer.h"
#include "flight/mixer_tricopter.h"
#include "flight/pid.h"

#include "rx/rx.h"

#ifdef USE_ACC
#include "sensors/acceleration.h"
#endif
#include "sensors/battery.h"
#include "sensors/gyro.h"

#ifdef USE_GYRO_DATA_ANALYSE
#include "scheduler/scheduler.h"
#endif

#include <complex.h>

#undef  isAirmodeActive
#define isAirmodeActive isAirmodeActivated

PG_REGISTER_WITH_RESET_TEMPLATE(mixerConfig_t, mixerConfig, PG_MIXER_CONFIG, 0);

#ifndef TARGET_DEFAULT_MIXER
#define TARGET_DEFAULT_MIXER    MIXER_QUADX
#endif
PG_RESET_TEMPLATE(mixerConfig_t, mixerConfig,
                  .mixerMode = TARGET_DEFAULT_MIXER,
                  .yaw_motors_reversed = false,
                  .crashflip_motor_percent = 0,
                  .crashflip_power_percent = 70,
                 );

PG_REGISTER_WITH_RESET_FN(motorConfig_t, motorConfig, PG_MOTOR_CONFIG, 1);

void pgResetFn_motorConfig(motorConfig_t *motorConfig) {
#if defined(BRUSHED_MOTORS) || defined(USE_BRUSHED_ESC_AUTODETECT)
#ifdef USE_BRUSHED_ESC_AUTODETECT
    if (hardwareMotorType== MOTOR_BRUSHED) {
#endif
    motorConfig->minthrottle = 1000;
    motorConfig->dev.motorPwmRate = BRUSHED_MOTORS_PWM_RATE;
    motorConfig->dev.motorPwmProtocol = PWM_TYPE_BRUSHED;
#ifdef USE_BRUSHED_ESC_AUTODETECT
    } else {
#endif
#endif
#ifndef BRUSHED_MOTORS
    motorConfig->minthrottle = 1020;
    motorConfig->dev.motorPwmRate = TASK_PERIOD_HZ(
        TASK_GYROPID_DESIRED_PERIOD
            *   GYRO_SYNC_DENOM_DEFAULT
            * PID_PROCESS_DENOM_DEFAULT
    );

// See <https://github.com/bitdump/BLHeli/blob/master/BLHeli_32%20ARM/BLHeli_32%20manual%20ARM%20Rev32.x.pdf>
    motorConfig->dev.motorPwmProtocol = PWM_TYPE_DSHOT150;
#ifdef USE_BRUSHED_ESC_AUTODETECT
    }
#endif
#endif
    motorConfig->dev.useUnsyncedPwm = true;
    motorConfig->maxthrottle = 2000;
    motorConfig->mincommand = 1000;
    motorConfig->digitalIdleOffsetValue = u'\xE1'; // i.e., 2.5%
#ifdef USE_DSHOT_DMAR
    motorConfig->dev.useBurstDshot = ENABLE_DSHOT_DMAR;
#endif
    for (int motorIndex = 0; motorIndex < MAX_SUPPORTED_MOTORS; motorIndex++) {
        motorConfig->dev.ioTags[motorIndex] = timerioTagGetByUsage(TIM_USE_MOTOR, motorIndex);
    }
    motorConfig->motorPoleCount = 14;   // Most brushes motors that we use are 14 poles
}

PG_REGISTER_ARRAY(motorMixer_t, MAX_SUPPORTED_MOTORS, customMotorMixer, PG_MOTOR_MIXER, 0);

#define PWM_RANGE_MID PWM_RANGE_MIDDLE

static FAST_RAM_ZERO_INIT bool         isThrustReversed;
       FAST_RAM_ZERO_INIT sig_atomic_t loggingThrottle, throttleGain,
                                       currentMixerMode,
                                       motorCount;
static FAST_RAM_ZERO_INIT float        motorCountf;

FAST_RAM_ZERO_INIT float
motor         [MAX_SUPPORTED_MOTORS],
motor_disarmed[MAX_SUPPORTED_MOTORS];

static FAST_RAM_ZERO_INIT motorMixer_t currentMixer[MAX_SUPPORTED_MOTORS];

static const motorMixer_t mixerQuadX[] = {
    { 1.0f, -1.0f,  1.0f, -1.0f },          // REAR_R
    { 1.0f, -1.0f, -1.0f,  1.0f },          // FRONT_R
    { 1.0f,  1.0f,  1.0f,  1.0f },          // REAR_L
    { 1.0f,  1.0f, -1.0f, -1.0f },          // FRONT_L
};
#ifndef USE_QUAD_MIXER_ONLY
static const motorMixer_t mixerTricopter[] = {
    { 1.0f,  0.0f,  1.333333f,  0.0f },     // REAR
    { 1.0f, -1.0f, -0.666667f,  0.0f },     // RIGHT
    { 1.0f,  1.0f, -0.666667f,  0.0f },     // LEFT
};

static const motorMixer_t mixerQuadP[] = {
    { 1.0f,  0.0f,  1.0f, -1.0f },          // REAR
    { 1.0f, -1.0f,  0.0f,  1.0f },          // RIGHT
    { 1.0f,  1.0f,  0.0f,  1.0f },          // LEFT
    { 1.0f,  0.0f, -1.0f, -1.0f },          // FRONT
};

#if defined(USE_UNCOMMON_MIXERS)
static const motorMixer_t mixerBicopter[] = {
    { 1.0f,  1.0f,  0.0f,  0.0f },          // LEFT
    { 1.0f, -1.0f,  0.0f,  0.0f },          // RIGHT
};
#else
#define mixerBicopter NULL
#endif

static const motorMixer_t mixerY4[] = {
    { 1.0f,  0.0f,  1.0f, -1.0f },          // REAR_TOP CW
    { 1.0f, -1.0f, -1.0f,  0.0f },          // FRONT_R CCW
    { 1.0f,  0.0f,  1.0f,  1.0f },          // REAR_BOTTOM CCW
    { 1.0f,  1.0f, -1.0f,  0.0f },          // FRONT_L CW
};


#if (MAX_SUPPORTED_MOTORS >= 6)
static const motorMixer_t mixerHex6X[] = {
    { 1.0f, -0.5f,  0.866025f,  1.0f },     // REAR_R
    { 1.0f, -0.5f, -0.866025f,  1.0f },     // FRONT_R
    { 1.0f,  0.5f,  0.866025f, -1.0f },     // REAR_L
    { 1.0f,  0.5f, -0.866025f, -1.0f },     // FRONT_L
    { 1.0f, -1.0f,  0.0f,      -1.0f },     // RIGHT
    { 1.0f,  1.0f,  0.0f,       1.0f },     // LEFT
};

#if defined(USE_UNCOMMON_MIXERS)
static const motorMixer_t mixerHex6H[] = {
    { 1.0f, -1.0f,  1.0f, -1.0f },     // REAR_R
    { 1.0f, -1.0f, -1.0f,  1.0f },     // FRONT_R
    { 1.0f,  1.0f,  1.0f,  1.0f },     // REAR_L
    { 1.0f,  1.0f, -1.0f, -1.0f },     // FRONT_L
    { 1.0f,  0.0f,  0.0f,  0.0f },     // RIGHT
    { 1.0f,  0.0f,  0.0f,  0.0f },     // LEFT
};

static const motorMixer_t mixerHex6P[] = {
    { 1.0f, -0.866025f,  0.5f,  1.0f },     // REAR_R
    { 1.0f, -0.866025f, -0.5f, -1.0f },     // FRONT_R
    { 1.0f,  0.866025f,  0.5f,  1.0f },     // REAR_L
    { 1.0f,  0.866025f, -0.5f, -1.0f },     // FRONT_L
    { 1.0f,  0.0f,      -1.0f,  1.0f },     // FRONT
    { 1.0f,  0.0f,       1.0f, -1.0f },     // REAR
};
static const motorMixer_t mixerY6[] = {
    { 1.0f,  0.0f,  1.333333f,  1.0f },     // REAR
    { 1.0f, -1.0f, -0.666667f, -1.0f },     // RIGHT
    { 1.0f,  1.0f, -0.666667f, -1.0f },     // LEFT
    { 1.0f,  0.0f,  1.333333f, -1.0f },     // UNDER_REAR
    { 1.0f, -1.0f, -0.666667f,  1.0f },     // UNDER_RIGHT
    { 1.0f,  1.0f, -0.666667f,  1.0f },     // UNDER_LEFT
};
#else
#define mixerHex6H NULL
#define mixerHex6P NULL
#define mixerY6 NULL
#endif // USE_UNCOMMON_MIXERS
#else
#define mixerHex6X NULL
#endif // MAX_SUPPORTED_MOTORS >= 6

#if defined(USE_UNCOMMON_MIXERS) && (MAX_SUPPORTED_MOTORS >= 8)
static const motorMixer_t mixerOctoX8[] = {
    { 1.0f, -1.0f,  1.0f, -1.0f },          // REAR_R
    { 1.0f, -1.0f, -1.0f,  1.0f },          // FRONT_R
    { 1.0f,  1.0f,  1.0f,  1.0f },          // REAR_L
    { 1.0f,  1.0f, -1.0f, -1.0f },          // FRONT_L
    { 1.0f, -1.0f,  1.0f,  1.0f },          // UNDER_REAR_R
    { 1.0f, -1.0f, -1.0f, -1.0f },          // UNDER_FRONT_R
    { 1.0f,  1.0f,  1.0f, -1.0f },          // UNDER_REAR_L
    { 1.0f,  1.0f, -1.0f,  1.0f },          // UNDER_FRONT_L
};

static const motorMixer_t mixerOctoFlatP[] = {
    { 1.0f,  0.707107f, -0.707107f,  1.0f },    // FRONT_L
    { 1.0f, -0.707107f, -0.707107f,  1.0f },    // FRONT_R
    { 1.0f, -0.707107f,  0.707107f,  1.0f },    // REAR_R
    { 1.0f,  0.707107f,  0.707107f,  1.0f },    // REAR_L
    { 1.0f,  0.0f, -1.0f, -1.0f },              // FRONT
    { 1.0f, -1.0f,  0.0f, -1.0f },              // RIGHT
    { 1.0f,  0.0f,  1.0f, -1.0f },              // REAR
    { 1.0f,  1.0f,  0.0f, -1.0f },              // LEFT
};

static const motorMixer_t mixerOctoFlatX[] = {
    { 1.0f,  1.0f, -0.414178f,  1.0f },      // MIDFRONT_L
    { 1.0f, -0.414178f, -1.0f,  1.0f },      // FRONT_R
    { 1.0f, -1.0f,  0.414178f,  1.0f },      // MIDREAR_R
    { 1.0f,  0.414178f,  1.0f,  1.0f },      // REAR_L
    { 1.0f,  0.414178f, -1.0f, -1.0f },      // FRONT_L
    { 1.0f, -1.0f, -0.414178f, -1.0f },      // MIDFRONT_R
    { 1.0f, -0.414178f,  1.0f, -1.0f },      // REAR_R
    { 1.0f,  1.0f,  0.414178f, -1.0f },      // MIDREAR_L
};
#else
#define mixerOctoX8 NULL
#define mixerOctoFlatP NULL
#define mixerOctoFlatX NULL
#endif

static const motorMixer_t mixerVtail4[] = {
    { 1.0f,  -0.58f,  0.58f, 1.0f },        // REAR_R
    { 1.0f,  -0.46f, -0.39f, -0.5f },       // FRONT_R
    { 1.0f,  0.58f,  0.58f, -1.0f },        // REAR_L
    { 1.0f,  0.46f, -0.39f, 0.5f },         // FRONT_L
};

static const motorMixer_t mixerAtail4[] = {
    { 1.0f, -0.58f,  0.58f, -1.0f },          // REAR_R
    { 1.0f, -0.46f, -0.39f,  0.5f },          // FRONT_R
    { 1.0f,  0.58f,  0.58f,  1.0f },          // REAR_L
    { 1.0f,  0.46f, -0.39f, -0.5f },          // FRONT_L
};

#if defined(USE_UNCOMMON_MIXERS)
static const motorMixer_t mixerDualcopter[] = {
    { 1.0f,  0.0f,  0.0f, -1.0f },          // LEFT
    { 1.0f,  0.0f,  0.0f,  1.0f },          // RIGHT
};
#else
#define mixerDualcopter NULL
#endif

static const motorMixer_t mixerSingleProp[] = {
    { 1.0f,  0.0f,  0.0f, 0.0f },
};

static const motorMixer_t mixerQuadX1234[] = {
    { 1.0f,  1.0f, -1.0f, -1.0f },          // FRONT_L
    { 1.0f, -1.0f, -1.0f,  1.0f },          // FRONT_R
    { 1.0f, -1.0f,  1.0f, -1.0f },          // REAR_R
    { 1.0f,  1.0f,  1.0f,  1.0f },          // REAR_L
};

// Keep synced with mixerMode_e
const mixer_t mixers[] = {
    // motors, use servo, motor mixer
    { 0, false, NULL },                // entry 0
    { 3, true,  mixerTricopter },      // MIXER_TRI
    { 4, false, mixerQuadP },          // MIXER_QUADP
    { 4, false, mixerQuadX },          // MIXER_QUADX
    { 2, true,  mixerBicopter },       // MIXER_BICOPTER
    { 0, true,  NULL },                // * MIXER_GIMBAL
    { 6, false, mixerY6 },             // MIXER_Y6
    { 6, false, mixerHex6P },          // MIXER_HEX6
    { 1, true,  mixerSingleProp },     // * MIXER_FLYING_WING
    { 4, false, mixerY4 },             // MIXER_Y4
    { 6, false, mixerHex6X },          // MIXER_HEX6X
    { 8, false, mixerOctoX8 },         // MIXER_OCTOX8
    { 8, false, mixerOctoFlatP },      // MIXER_OCTOFLATP
    { 8, false, mixerOctoFlatX },      // MIXER_OCTOFLATX
    { 1, true,  mixerSingleProp },     // * MIXER_AIRPLANE
    { 1, true,  mixerSingleProp },     // * MIXER_HELI_120_CCPM
    { 0, true,  NULL },                // * MIXER_HELI_90_DEG
    { 4, false, mixerVtail4 },         // MIXER_VTAIL4
    { 6, false, mixerHex6H },          // MIXER_HEX6H
    { 0, true,  NULL },                // * MIXER_PPM_TO_SERVO
    { 2, true,  mixerDualcopter },     // MIXER_DUALCOPTER
    { 1, true,  NULL },                // MIXER_SINGLECOPTER
    { 4, false, mixerAtail4 },         // MIXER_ATAIL4
    { 0, false, NULL },                // MIXER_CUSTOM
    { 2, true,  NULL },                // MIXER_CUSTOM_AIRPLANE
    { 3, true,  NULL },                // MIXER_CUSTOM_TRI
    { 4, false, mixerQuadX1234 },
};
#endif // !USE_QUAD_MIXER_ONLY

FAST_RAM_ZERO_INIT float motorOutputHigh, motorOutputLow, controllerMixRange;

#ifdef USE_GYRO_DATA_ANALYSE
static FAST_RAM_ZERO_INIT pt1Filter_t pidSumFilter[XYZ_AXIS_COUNT];
static FAST_RAM_ZERO_INIT float targetMixerLooptimef;
#endif
static FAST_RAM_ZERO_INIT float disarmMotorOutput, deadbandMotor3dHigh, deadbandMotor3dLow;
static FAST_RAM_ZERO_INIT float rcCommandThrottleRange;
static FAST_RAM_ZERO_INIT float rcThrottlePrevious;
static FAST_RAM_ZERO_INIT timeUs_t reversalTimeUs;

bool areMotorsRunning(void) {
    for (register ptrdiff_t i = motorCount; i;)
        if ((--i)[motor_disarmed]!= disarmMotorOutput)
            return true;

    return ARMING_FLAG(ARMED);
}

#ifdef USE_SERVOS
bool mixerIsTricopter(void) {
    switch (currentMixerMode) {
        case MIXER_TRI:
        case MIXER_CUSTOM_TRI:
        return true;

        default:
        return false;
    }
}
#endif

// All PWM motor scaling is done to standard PWM range of 1000-2000 for easier tick conversion with legacy code / configurator
// DSHOT scaling is done to the actual dshot range
void initEscEndpoints(void) {
    float motorOutputLimit = 1.0f;
    if (currentPidProfile->motor_output_limit < 100) {
        motorOutputLimit = currentPidProfile->motor_output_limit / 100.0f;
    }
    // Can't use 'isMotorProtocolDshot()' here since motors haven't been initialised yet
    switch (motorConfig()->dev.motorPwmProtocol) {
#ifdef USE_DSHOT
    case PWM_TYPE_PROSHOT1000:
    case PWM_TYPE_DSHOT4800:
    case PWM_TYPE_DSHOT2400:
    case PWM_TYPE_DSHOT1200:
    case PWM_TYPE_DSHOT600:
    case PWM_TYPE_DSHOT300:
    case PWM_TYPE_DSHOT150: {
        float outputLimitOffset = (DSHOT_MAX_THROTTLE - DSHOT_MIN_THROTTLE) * (1 - motorOutputLimit);
        disarmMotorOutput = DSHOT_CMD_MOTOR_STOP;
        if (feature(FEATURE_3D)) {
            motorOutputLow = DSHOT_MIN_THROTTLE + ((DSHOT_3D_FORWARD_MIN_THROTTLE - 1 - DSHOT_MIN_THROTTLE) / 100.0f) * CONVERT_PARAMETER_TO_PERCENT(motorConfig()->digitalIdleOffsetValue);
            motorOutputHigh = DSHOT_MAX_THROTTLE - outputLimitOffset / 2;
            deadbandMotor3dHigh = DSHOT_3D_FORWARD_MIN_THROTTLE + ((DSHOT_MAX_THROTTLE - DSHOT_3D_FORWARD_MIN_THROTTLE) / 100.0f) * CONVERT_PARAMETER_TO_PERCENT(motorConfig()->digitalIdleOffsetValue);
            deadbandMotor3dLow = DSHOT_3D_FORWARD_MIN_THROTTLE - 1 - outputLimitOffset / 2;
        } else {
            motorOutputLow = DSHOT_MIN_THROTTLE + ((DSHOT_MAX_THROTTLE - DSHOT_MIN_THROTTLE) / 100.0f) * CONVERT_PARAMETER_TO_PERCENT(motorConfig()->digitalIdleOffsetValue);
            motorOutputHigh = DSHOT_MAX_THROTTLE - outputLimitOffset;
        }
    }
    break;
#endif
    default:
        if (feature(FEATURE_3D)) {
            float outputLimitOffset = (flight3DConfig()->limit3d_high - flight3DConfig()->limit3d_low) * (1 - motorOutputLimit) / 2;
            disarmMotorOutput = flight3DConfig()->neutral3d;
            motorOutputLow = flight3DConfig()->limit3d_low + outputLimitOffset;
            motorOutputHigh = flight3DConfig()->limit3d_high - outputLimitOffset;
            deadbandMotor3dHigh = flight3DConfig()->deadband3d_high;
            deadbandMotor3dLow = flight3DConfig()->deadband3d_low;
        } else {
            disarmMotorOutput = motorConfig()->mincommand;
            motorOutputLow = motorConfig()->minthrottle;
            motorOutputHigh = motorConfig()->maxthrottle - ((motorConfig()->maxthrottle - motorConfig()->minthrottle) * (1 - motorOutputLimit));
        }
        break;
    }
    rcCommandThrottleRange = PWM_RANGE_MAX - PWM_RANGE_MIN;
}

FAST_CODE_NOINVOKE void mixerInit(register sig_atomic_t const mixerMode) {
    currentMixerMode = mixerMode;
    initEscEndpoints();
    if (mixerIsTricopter()) {
        mixerTricopterInit();
    }
    mixerInitProfile();
#ifdef USE_GYRO_DATA_ANALYSE
    for (
        register struct {
            float     const k;
            ptrdiff_t       axis;
        } config = {
            pt1FilterGain(
                ldexpf(
                    gyroConfig()->dyn_notch_q_factor // ‰ of sample rate
                        * 125ul
                ,
                    3
                ) / targetMixerLooptimef
            ,
                targetMixerLooptimef
            )
        ,
            ARRAYLEN(pidSumFilter) - 1
        };
        config.axis[pidSumFilter].k = config.k,
        config.axis--;
    ) NOOP
#endif
}

#ifndef USE_QUAD_MIXER_ONLY

void mixerConfigureOutput(void) {
    motorCount = 0;
    switch (currentMixerMode) {
        case MIXER_CUSTOM:
        case MIXER_CUSTOM_AIRPLANE:
        case MIXER_CUSTOM_TRI:
        // load custom mixer into currentMixer
        while (
            // check if done
            customMotorMixer(motorCount)->throttle && (
                motorCount[currentMixer] =*customMotorMixer(motorCount),
              ++motorCount < MAX_SUPPORTED_MOTORS
            )
        ) NOOP
        break;

        default:
        if ((motorCount = mixers[currentMixerMode].motorCount)
                > MAX_SUPPORTED_MOTORS
        )
            motorCount = MAX_SUPPORTED_MOTORS;
        // copy motor-based mixers
        if (mixers[currentMixerMode].motor)
            for (register ptrdiff_t i = motorCount; i--;)
                currentMixer[i] = mixers[currentMixerMode].motor[i];
        break;
    }
    motorCountf = motorCount;
    mixerResetDisarmedMotors();
}

void mixerLoadMix(int index, motorMixer_t *customMixers) {
    // we're 1-based
    index++;
    // clear existing
    for (int i = 0; i < MAX_SUPPORTED_MOTORS; i++) {
        customMixers[i].throttle = 0.0f;
    }
    // do we have anything here to begin with?
    if (mixers[index].motor != NULL) {
        for (int i = 0; i < mixers[index].motorCount; i++) {
            customMixers[i] = mixers[index].motor[i];
        }
    }
}
#else
void mixerConfigureOutput(void) {
    motorCountf=
    motorCount = QUAD_MOTOR_COUNT;
    for (int i = 0; i < motorCount; i++) {
        currentMixer[i] = mixerQuadX[i];
    }
    mixerResetDisarmedMotors();
}
#endif // USE_QUAD_MIXER_ONLY

void mixerResetDisarmedMotors(void) {
    // set disarmed motor values
    for (
        register ptrdiff_t
        i = MAX_SUPPORTED_MOTORS;
        i;
        (--i)[motor_disarmed] = disarmMotorOutput
    ) NOOP
}

void stopMotors(void) {
    // Sends commands to all motors
    {   register ptrdiff_t
        entry = motorCount;

        if (pwmAreMotorsEnabled()) {
            while (entry--)
                pwmWriteMotor(entry, motor[entry] = disarmMotorOutput);
            pwmCompleteMotorUpdate(motorCount);
        } else while (entry--)
            motor[entry] = disarmMotorOutput;
    }
    delay(50); // give the timers and ESCs a chance to react.
}

void stopPwmAllMotors(void) {
    pwmShutdownPulsesForAllMotors(motorCount);
    delayMicroseconds(1500);
}

static FAST_RAM_ZERO_INIT float throttleMin, throttleMax;
static FAST_RAM_ZERO_INIT float throttle = 0;
static FAST_RAM_ZERO_INIT float motorOutputMin;
static FAST_RAM_ZERO_INIT float motorRangeMin;
static FAST_RAM_ZERO_INIT float motorRangeMax;
static FAST_RAM_ZERO_INIT float motorOutputRange;
static FAST_RAM_ZERO_INIT bool  controllerMix3DModeSign; // F -> normal, T -> reversed

void writeMotors(void) {
    register float const * restrict const
    mix = areMotorsActivated() ? motor : motor_disarmed;

    switch (debugMode) {
        case DEBUG_MOTOR_SIGNALS:
        for (
            register ptrdiff_t
            i = MIN(motorCount, DEBUG16_VALUE_COUNT);
            i--;
            i[debug] = lrintf(i[mix])
        ) NOOP
        break;

        case DEBUG_PIDSUMS:
        THROTTLE[debug] = lrintf(throttle * PWM_RANGE);
        FALLTHROUGH;

        default:
        for (
            register ptrdiff_t
            i = motorCount;
            i--;
            pwmWriteMotor(i, i[mix])
        ) NOOP

        pwmCompleteMotorUpdate(motorCount);
        break;
    }
}

static FAST_CODE FAST_CODE_NOINLINE void calculateRollAndPitchSetpoints(void) {
#ifdef USE_ACC
    register sig_atomic_t const
    flags = flightModeFlags ^ NFE_RACE_MODE;

    if (flags & (GPS_RESCUE_MODE | GPS_HOLD_MODE | NFE_RACE_MODE)) {
        register struct {
            float          z;
            float _Complex w;
        } input = {
            // See <https://deutsch.physics.ucsc.edu/6A/book/notes/node59.html>
            flags & GPS_HOLD_MODE ?
                input.z = true,
                imuGetBrakingCommands()
            : (
                // See <https://arxiv.org/abs/1509.06344>
                input.z = fmaxf(
                    fabsf(__real__ input.w = gpsRescueDeflection.x)
                ,
                    fabsf(__imag__ input.w = gpsRescueDeflection.y)
                ),
                flags & (GPS_RESCUE_MODE | HEADFREE_MODE) && (
                    input.w/= attitude.ratios.tc
                ),
                input.w/= cabsf(input.w)
            )
        };

        if (isnormal(input.z)) {
            if (flags & (HORIZON_MODE | GPS_RESCUE_MODE))
                input.z = (
                    (   isThrustReversed =
                            isFlipOverAfterCrashMode() || (
                                feature(FEATURE_3D) &&
                                ISRVALUE({
                                    register struct {
                                       uint16_t cmd, min;
                                    } const thr = {
                                        lrintf(rcCommand[THROTTLE])
                                    ,
                                        flight3DConfig()->deadband3d_throttle
                                    };

                                    isThrustReversed ?
                                        thr.cmd - thr.min : thr.cmd + thr.min;
                                }) < rxConfig()->midrc
                            )
                    ) ?-throttle : throttle
                ),
                input.w*= opposf(true, sq(throttle));
            else {
                register float _Complex const
                input_r = cexpf(DEGREES_TO_RADIANS(
                    flags== NFE_RACE_MODE ?
                        hypotf(
                            applyRates(
                                AI_PITCH
                            ,
                                copysignf(input.z,__real__ input.w)
                            ,
                                input.z
                            ) *__real__ input.w
                        ,
                            applyRates(
                                AI_ROLL
                            ,
                                copysignf(input.z,__imag__ input.w)
                            ,
                                input.z
                            ) *__imag__ input.w
                        ) * dT
                    :
                        (   currentPidProfile->angleExpo ?
                                ldexpf(
                                    fdimf(
                                        (   (uint16_t)
                                            currentPidProfile->levelAngleLimit *
                                            currentPidProfile->angleExpo
                                        ) * input.z * input.z * input.z + 1e2f
                                    ,
                                        currentPidProfile->angleExpo
                                    ) / 25.f
                                ,
                                   -2
                                )
                            :
                                currentPidProfile->levelAngleLimit
                        ) * input.z
                ) * I);

                input.z =__real__ input_r;
                input.w*=__imag__ input_r;
            }

          __real__ input.w = ISRVALUE({
                register float const
                input_r = opposf(true, sq(__imag__ input.w));

                isnormal(input_r) && (
                    gpsRescueAngle[AI_PITCH] = RADIANS_TO_CENTIDEGREES(
                        asin_approx((__real__ input.w)/= input_r)
                    ),
                    isnormal(
                        (__real__ input.w) = opposf(true, sq(__real__ input.w))
                    )
                ) ? input.z /__real__ input.w : false;
            });
            gpsRescueAngle[AI_ROLL] = RADIANS_TO_CENTIDEGREES(cargf(input.w));

            if (flags== NFE_RACE_MODE)
                for (
                    register ptrdiff_t
                    axis = 2;
                    axis--;
                    setSetpointRate(axis[gpsRescueAngle] * pidFrequency, axis)
                ) NOOP
        } else
            bzero(gpsRescueAngle, sizeof*gpsRescueAngle);
    } else
#endif // USE_ACC
    for (
        register ptrdiff_t
        axis = 2;
        axis--;
        axis[gpsRescueAngle] = axis[rcCommand]
    ) NOOP

#ifdef USE_ACC
    if ((   rxConfig()->fpvCamAngleDegrees || (
                rxConfig()->cinematicYaw &&
                accelerometerConfig()->acc_hardware
            )
        ) && IS_RC_MODE_ACTIVE(BOXFPVANGLEMIX)
    )
        scaleRcCommandToFpvCamAngle();
#endif
}

static FAST_CODE_NOINVOKE float getThrottleDeflection(
    register float deflection
) {
    if (FLIGHT_MODE(
#ifdef USE_ALT_HOLD
            BARO_MODE |
#endif
#ifdef USE_GPS_RESCUE
            GPS_RESCUE_MODE |
#endif
            HORIZON_MODE
        )
    )
        deflection = (
            isnormal(attitude.ratios.w.w) ?
                fminf(deflection / attitude.ratios.w.w, true) : true
        );

    return
    currentControlRateProfile->throttle_limit_type== THROTTLE_LIMIT_TYPE_SCALE ?
        throttleMax * deflection : deflection;
}

#ifdef USE_GYRO_DATA_ANALYSE
inline void mixerSetTargetLooptime(register timeUs_t const looptime) {
    targetMixerLooptimef = looptime;
}
#endif

FAST_CODE_NOINVOKE void calculateThrottleAndCurrentMotorEndpoints(
    register timeUs_t const currentTimeUs
) {
#ifdef USE_THROTTLE_BOOST
    register float const
    throttleLast = throttle;
#endif

    register float
    currentThrottleInputRange =
        currentControlRateProfile->throttle_limit_percent;
    currentThrottleInputRange = (
        currentThrottleInputRange ?
            ldexpf(currentThrottleInputRange / 25.f,-2) : true
    );

    throttleMax = (
        redlineIsActive() ?
            fmaxf(
                ldexpf(fminf(controllerMixRange, throttleMax) * 15.f,-4)
            ,
#if defined(USE_ACC) || defined(USE_ALT_HOLD) || defined(USE_GPS_RESCUE)
                rescueThrottle
#else
                failsafeConfig()->failsafe_throttle
#endif
            )
        :
            fminf(currentThrottleInputRange, throttleMax) * 1.0625f
    );

    currentThrottleInputRange =
#if defined(USE_ALT_HOLD) || defined(USE_GPS_RESCUE)
        FLIGHT_MODE(
 #ifdef USE_ALT_HOLD
  #ifdef USE_GPS_RESCUE
            GPS_RESCUE_MODE |
  #endif
            BARO_MODE
 #else
            GPS_RESCUE_MODE
 #endif
        ) ?
            rescueThrottle
        :
#endif
        rcCommand[THROTTLE];

    if (feature(FEATURE_3D)) {
        float _Complex rcCommand3dDeadband;
        #define rcCommand3dDeadBandLow (__real__ rcCommand3dDeadband)
        #define rcCommand3dDeadBandHigh (__imag__ rcCommand3dDeadband)
        if (!ARMING_FLAG(ARMED)) {
            rcThrottlePrevious = rxConfig()->midrc; // When disarmed set to mid_rc. It always results in positive direction after arming.
        }
        if (IS_RC_MODE_ACTIVE(BOX3D) || flight3DConfig()->switched_mode3d) {
             ENABLE_FLIGHT_MODE(HELI_MODE);
            // The min_check range is halved because the output throttle is scaled to 500us.
            // So by using half of min_check we maintain the same low-throttle deadband
            // stick travel as normal non-3D mode.
            const int mincheckOffset = (rxConfig()->mincheck - PWM_RANGE_MIN) / 2;
            rcCommand3dDeadBandLow = rxConfig()->midrc - mincheckOffset;
            rcCommand3dDeadBandHigh = rxConfig()->midrc + mincheckOffset;
        } else {
            DISABLE_FLIGHT_MODE(HELI_MODE);
            rcCommand3dDeadBandLow = rxConfig()->midrc - flight3DConfig()->deadband3d_throttle;
            rcCommand3dDeadBandHigh = rxConfig()->midrc + flight3DConfig()->deadband3d_throttle;
        }
        const float rcCommandThrottleRange3dLow = rcCommand3dDeadBandLow - PWM_RANGE_MIN;
        const float rcCommandThrottleRange3dHigh = PWM_RANGE_MAX - rcCommand3dDeadBandHigh;
        if (currentThrottleInputRange<= rcCommand3dDeadBandLow) {
            // INVERTED
            motorRangeMin = motorOutputLow;
            motorRangeMax = deadbandMotor3dLow;
            if (isMotorProtocolDshot()) {
                motorOutputMin = motorOutputLow;
                motorOutputRange = deadbandMotor3dLow - motorOutputLow;
            } else {
                motorOutputMin = deadbandMotor3dLow;
                motorOutputRange = motorOutputLow - deadbandMotor3dLow;
            }
            if (controllerMix3DModeSign)
                NOOP
            else
                controllerMix3DModeSign = true,
                reversalTimeUs = currentTimeUs;
            rcThrottlePrevious = currentThrottleInputRange;
            throttle = rcCommand3dDeadBandLow - currentThrottleInputRange;
            currentThrottleInputRange = rcCommandThrottleRange3dLow;
        } else if (currentThrottleInputRange >= rcCommand3dDeadBandHigh) {
            // NORMAL
            motorRangeMin = deadbandMotor3dHigh;
            motorRangeMax = motorOutputHigh;
            motorOutputMin = deadbandMotor3dHigh;
            motorOutputRange = motorOutputHigh - deadbandMotor3dHigh;
            if (controllerMix3DModeSign)
                controllerMix3DModeSign = false,
                reversalTimeUs = currentTimeUs;
            rcThrottlePrevious = currentThrottleInputRange;
            throttle = currentThrottleInputRange - rcCommand3dDeadBandHigh;
            currentThrottleInputRange = rcCommandThrottleRange3dHigh;
        } else if ((rcThrottlePrevious <= rcCommand3dDeadBandLow &&
                    !flight3DConfigMutable()->switched_mode3d) ||
                   isMotorsReversed()) {
            // INVERTED_TO_DEADBAND
            motorRangeMin = motorOutputLow;
            motorRangeMax = deadbandMotor3dLow;
            if (isMotorProtocolDshot()) {
                motorOutputMin = motorOutputLow;
                motorOutputRange = deadbandMotor3dLow - motorOutputLow;
            } else {
                motorOutputMin = deadbandMotor3dLow;
                motorOutputRange = motorOutputLow - deadbandMotor3dLow;
            }
            if (controllerMix3DModeSign)
                NOOP
            else
                controllerMix3DModeSign = true,
                reversalTimeUs = currentTimeUs;
            throttle = 0;
            currentThrottleInputRange = rcCommandThrottleRange3dLow;
        } else {
            // NORMAL_TO_DEADBAND
            motorRangeMin = deadbandMotor3dHigh;
            motorRangeMax = motorOutputHigh;
            motorOutputMin = deadbandMotor3dHigh;
            motorOutputRange = motorOutputHigh - deadbandMotor3dHigh;
            if (controllerMix3DModeSign)
                controllerMix3DModeSign = false,
                reversalTimeUs = currentTimeUs;
            throttle = 0;
            currentThrottleInputRange = rcCommandThrottleRange3dHigh;
        }
        #undef rcCommand3dDeadBandHigh
        #undef rcCommand3dDeadBandLow
    } else {
        throttle = currentThrottleInputRange - PWM_RANGE_MIN;
        currentThrottleInputRange = rcCommandThrottleRange;
        motorRangeMin = motorOutputLow;
        motorRangeMax = motorOutputHigh;
        motorOutputMin = motorOutputLow;
        motorOutputRange = motorOutputHigh - motorOutputLow;
        controllerMix3DModeSign = getBoxIdState(BOXUSER4);
    }

#ifdef USE_THROTTLE_BOOST
    throttle+= pt1FilterReuse(throttleLpf,
#endif
    throttle = getThrottleDeflection(throttle / currentThrottleInputRange)
#ifdef USE_THROTTLE_BOOST
    ) * throttleBoost
#endif
    ;

    calculateRollAndPitchSetpoints();
}

void mixerInitProfile(void) {
    throttleGain = REINTERPRET_CAST(sig_atomic_t, 1.f);
    throttleMax =
    throttleMin = rxConfig()->airModeActivateThreshold / (float)PWM_RANGE;
}

#define CRASH_FLIP_DEADBAND 20
#define CRASH_FLIP_STICK_MINF 0.15f

/*
static FAST_CODE_NOINVOKE void mixWithThrottleLegacy(
    register float            motorMix[restrict const],
    register float       controllerMix[restrict const],
    register float const controllerMixMin,
    register float const controllerMixMax
) {
    register float const
    throttleThrust = (
            motorToThrust(throttle, true) : throttle
    ),
    normFactor = (
        hardwareMotorType== MOTOR_BRUSHED ?
            true : fmaxf(controllerMixRange, true)
    );

    if (mixerImpl == MIXER_IMPL_LEGACY) {
        // legacy clipping handling
        if (normFactor > true) {
            for (
                register ptrdiff_t
                i = motorCount;
                i;
                (--i)[controllerMix]/= normFactor
            ) NOOP
            // Get the maximum correction by setting offset to center when airmode enabled
            if (currentPidProfile->linear_throttle)
                throttleThrust = 0.5f;
        } else {
            if (isAirmodeActive() || throttleThrust > 0.5f) {  // Only automatically adjust throttle when airmode enabled. Airmode logic is always active on high throttle
                throttleThrust = constrainf(throttleThrust, -controllerMixMin, 1.0f - controllerMixMax);
            }
        }
    } else {
        float throttleMotor = currentPidProfile->linear_throttle ? thrustToMotor(throttle, true) : throttle; // used to make authority grow faster
        float authority = isAirmodeActive() ? 1.0f : SCALE_UNITARY_RANGE(throttleMotor, 0.5f, 1.0f);
        normFactor *= authority;
    }

    for (
        register ptrdiff_t
        i = motorCount;
        i--;
        motorMix[i] = thrustToMotor(
            i[currentMixer].throttle * throttleThrust + i[controllerMix], true
        )
    )
        if (mixerImpl== MIXER_IMPL_SMOOTH)
            // clipping handling
            i[controllerMix] =
                (   (    mixerLaziness ?
                             SCALE_UNITARY_RANGE(throttleThrust, true,-true)
                                 * fabsf(i[controllerMix])
                         : SCALE_UNITARY_RANGE(
                             throttleThrust,-controllerMixMin,-controllerMixMax
                         )
                    ) + i[controllerMix]
                ) / normFactor;
}
*/

FAST_CODE_NOINVOKE void mixTable(void) {
    float motorMix[motorCount];

    // mix controller output with throttle
{   register float
  * thrMax = motorMix;
{   register float
  * thrMin = motorMix,
    thrSum = false;

{   register ptrdiff_t
    axis = FLIGHT_DYNAMICS_INDEX_COUNT;

    fpVector3_t thrPid;

{   register float const
    sgn125 = isFlipOverAfterCrashMode() ?-125.f : 125.f;

#ifdef USE_GYRO_DATA_ANALYSE
    register float
    (* const pidGetScaledSum)(ptrdiff_t) = ({
#endif
    // See <http://hyperphysics.phy-astr.gsu.edu/hbase/torq.html>
    // and <https://doi.org/10.15394/ijaaa.2019.1427>
    float pidGetScaledSum(register ptrdiff_t const axis) {
        return ldexpf(
            constrainf(axis[pidData].Sum,-PIDSUM_LIMIT_MAX, PIDSUM_LIMIT_MAX)
                / sgn125
        ,
           -3
        );
    }

#ifdef USE_GYRO_DATA_ANALYSE
    float pidGetFilteredSum(register ptrdiff_t const axis) {
        register float
        level = pt1FilterReuse(axis + pidSumFilter, axis[pidData].Sum);
        level = ldexpf(
            constrainf(
                // See <https://youtu.be/W1losp4DMoo?t=667>
                level+= pt1FilterLevel(axis + pidSumFilter)
            ,
               -PIDSUM_LIMIT_MAX
            ,
                PIDSUM_LIMIT_MAX
            ) / sgn125
        ,
           -3
        );

        switch (debugMode) {
            case DEBUG_FFT_FREQ:
            if (axis)
                break;
            (DEBUG16_VALUE_COUNT - 1)[debug] = lrintf(
                pt1FilterFreq(pidSumFilter, targetMixerLooptimef)
            );
            FALLTHROUGH;

            case DEBUG_FFT:
            (axis<< true | true)[debug] = lrintf(level);
            FALLTHROUGH;

            default:
            break;
        }

        return level;
    }

    isDynamicFilterActive() ? pidGetFilteredSum : pidGetScaledSum;
    });
#endif // USE_GYRO_DATA_ANALYSE

    while (axis--)
        axis[thrPid.raw] = pidGetScaledSum(axis);
}
    axis = motorCount;

    while (axis--) {
        register float
        thr =
            (   thrSum+= (
                    thr = axis[currentMixer].roll  * thrPid.x,
                    thr+= axis[currentMixer].pitch * thrPid.y
                ),

                thr
            ) + (
                thr = axis[currentMixer].yaw * thrPid.z,

                thrSum+= controllerMix3DModeSign ? (thr =-thr) : thr,

                thr
            );

        if (controllerMix3DModeSign)
            thr =-thr;

       *ISRVALUE({
            register float * restrict const
            thrPtr = motorMix + axis;

              thr <*thrMin ?
                thrMin = thrPtr
            : thr >*thrMax ?
                thrMax = thrPtr
            :
                thrPtr;
        }) = thr;
    }
}
    pidUpdateEmuGravityThrottleFilter(
        throttle = constrainf(
            REINTERPRET_CAST(float,
                loggingThrottle = REINTERPRET_CAST(sig_atomic_t,
                    (   throttle = fminf(
                            isAirmodeActive() ?
                                fmaxf(throttle, throttleMin) : throttle
                        ,
                            throttleMax
                        )
                    ) < (
                        controllerMixRange = fdimf(*thrMax,*thrMin)
                    ) ?
                        controllerMixRange = ISRVALUE({
                            for (
                                register ptrdiff_t
                                i = motorCount;
                                i--;
                                i[motorMix] =
                                i[motorMix] * throttle / controllerMixRange
                            ) NOOP

                            thrSum = thrSum * throttle / controllerMixRange;

                            throttle;
                        })
                    :
                        throttle
                )
            ) - thrSum / motorCountf
        ,
          -*thrMin
        ,
            controllerMixRange -*thrMax
        )
    );
}
    throttleGain = REINTERPRET_CAST(sig_atomic_t, ISRVALUE({
        register float
        (* const thrFn)(float) = ({
            float thrFn0(register float const thr) {
                return fmaxf(thr, false);
            }
                
            register float const
            thr0 = feature(FEATURE_3D) ? rxConfig()->midrc : false;

            float thrFn1(register float const thr) {
                return fabsf(thr - thr0);
            }

            thr0 ? thrFn1 : thrFn0;
        });

        ({  register float
            powSum = false;

            for (
                register ptrdiff_t
                i = motorCount;
                i;
                powSum+= thrFn((--i)[motorMix])
            ) NOOP

            powSum;
        }) / thrFn(*thrMax);
    }));
}
    for (
        register ptrdiff_t
        i = motorCount;
        i--;
        i[motor] = fminf(ISRVALUE({
            register float
            motorOutput = i[motorMix] * motorOutputRange + motorOutputMin;

            // See <http://physics.bu.edu/~duffy/sc545_notes04/back_emf.html>
            if (batteryVoltage.peak)
                motorOutput = ldexpf(
                    (   sq_approx(
                            sqrtf(
                                motorOutput *
                                REINTERPRET_CAST(float, batteryVoltage.thrK)
                            ) * REINTERPRET_CAST(float, batteryVoltage.rpmK)
                              + true
                        ) - true
                    ) / REINTERPRET_CAST(float, batteryVoltage.peak)
                ,
                   -2
                );

            (   mixerIsTricopter() ?
                    mixerTricopterMotorCorrection(i) + motorOutput
                :
                    motorOutput
            ) < motorRangeMin ?
                failsafePhase()!= FAILSAFE_RX_LOSS_DETECTED ?
                    motorRangeMin
                : isMotorProtocolDshot() ?
                    disarmMotorOutput
                :
                    fmaxf(disarmMotorOutput, motorOutput)
            :
                motorOutput;
        }),
            motorRangeMax
        )
    ) NOOP
}

float convertExternalToMotor(uint16_t externalValue) {
    float motorValue;
    switch ((int)isMotorProtocolDshot()) {
#ifdef USE_DSHOT
    case true:
        externalValue = constrain(externalValue, PWM_RANGE_MIN, PWM_RANGE_MAX);
        if (feature(FEATURE_3D)) {
            if (externalValue == PWM_RANGE_MID) {
                motorValue = DSHOT_DISARM_COMMAND;
            } else if (externalValue < PWM_RANGE_MID) {
                motorValue = scaleRangef(externalValue, PWM_RANGE_MIN, PWM_RANGE_MID - 1, DSHOT_3D_DEADBAND_LOW, DSHOT_MIN_THROTTLE);
            } else {
                motorValue = scaleRangef(externalValue, PWM_RANGE_MID + 1, PWM_RANGE_MAX, DSHOT_3D_DEADBAND_HIGH, DSHOT_MAX_THROTTLE);
            }
        } else {
            motorValue = (externalValue == PWM_RANGE_MIN) ? DSHOT_DISARM_COMMAND : scaleRangef(externalValue, PWM_RANGE_MIN + 1, PWM_RANGE_MAX, DSHOT_MIN_THROTTLE, DSHOT_MAX_THROTTLE);
        }
        break;
    case false:
#endif
    default:
        motorValue = externalValue;
        break;
    }
    return motorValue;
}

uint16_t convertMotorToExternal(float motorValue) {
    uint16_t externalValue;
    switch ((int)isMotorProtocolDshot()) {
#ifdef USE_DSHOT
    case true:
        if (feature(FEATURE_3D)) {
            if (motorValue == DSHOT_DISARM_COMMAND || motorValue < DSHOT_MIN_THROTTLE) {
                externalValue = PWM_RANGE_MID;
            } else if (motorValue <= DSHOT_3D_DEADBAND_LOW) {
                externalValue = scaleRangef(motorValue, DSHOT_MIN_THROTTLE, DSHOT_3D_DEADBAND_LOW, PWM_RANGE_MID - 1, PWM_RANGE_MIN);
            } else {
                externalValue = scaleRangef(motorValue, DSHOT_3D_DEADBAND_HIGH, DSHOT_MAX_THROTTLE, PWM_RANGE_MID + 1, PWM_RANGE_MAX);
            }
        } else {
            externalValue = (motorValue < DSHOT_MIN_THROTTLE) ? PWM_RANGE_MIN : scaleRangef(motorValue, DSHOT_MIN_THROTTLE, DSHOT_MAX_THROTTLE, PWM_RANGE_MIN + 1, PWM_RANGE_MAX);
        }
        break;
    case false:
#endif
    default:
        externalValue = motorValue;
        break;
    }
    return externalValue;
}

int16_t mixerGetLoggingThrottle(void) {
    return lrintf(REINTERPRET_CAST(float, loggingThrottle) * PWM_RANGE);
}

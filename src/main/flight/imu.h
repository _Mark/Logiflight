/*
 * This file is part of Cleanflight and Betaflight.
 *
 * Cleanflight and Betaflight are free software. You can redistribute
 * this software and/or modify this software under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Cleanflight and Betaflight are distributed in the hope that they
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.
 *
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "common/axis.h"
#include "common/time.h"
#include "common/maths.h"
#include "common/utils.h"
#include "pg/pg.h"

#if defined(USE_ACC) &&!(\
    defined(SIMULATOR_BUILD) && defined(SIMULATOR_IMU_SYNC)\
)
#define USE_ACC_SENSOR

typedef struct {
#ifdef USE_ACC_SENSOR
    sig_atomic_t TAS, EAS, Vario;
#else
    sig_atomic_t TAS, EAS;
#endif
} imuASI_t;

extern imuASI_t imuASI;
#define imuGetTAS() (imuASI.TAS)
#define imuGetEAS() (imuASI.EAS)
#endif

// Exported symbols
typedef struct {
    struct {                                // (dimensionless)
        struct {
           _Atomic float x, y, z;
        } x, y, z;

        struct {
                   float x;
           _Atomic float s, w;
        } w;

        float _Complex tc;
    } ratios;

    union {                                 // decidegrees
        int16_t raw[6];

      __PACKED_STRUCT {
            sig_atomic_t
            roll:16, pitch:16, yaw:16, slant:16, tilt:16, ahead:16;
        } values;
    };
} attitudeEulerAngles_t;
#define EULER_INITIALIZE NOOP

extern attitudeEulerAngles_t attitude;

typedef struct imuConfig_s {
    int16_t att_ctrofmass[XYZ_AXIS_COUNT];  // centi-cm
    uint16_t dcm_kp;                        // DCM filter proportional gain ( x 10000)
    uint16_t dcm_ki;                        // DCM filter integral gain ( x 10000)
    uint8_t small_angle;
    uint8_t acc_deadband;                   // cm/s/s
    uint8_t acc_unarmedcal;                 // turn automatic acc compensation on/off
} imuConfig_t;

PG_DECLARE(imuConfig_t, imuConfig);

typedef struct imuRuntimeConfig_s {
#ifdef USE_ESC_SENSOR
   uint64_t motor_poles;

    struct {
        struct {
            sig_atomic_t mach, cmss;
        } limit;

        float length;
    } erpm;
#endif

    float
#ifdef USE_ACC
    acc_deadband,
#endif
    gyro_deadband;                          // cdeg/s

    float _Complex dcm;

#ifdef USE_ACC
    bool acc_unarmedcal;
#endif
} imuRuntimeConfig_t;

extern imuRuntimeConfig_t imuRuntimeConfig;

enum {
    DEBUG_IMU0,
    DEBUG_IMU1,
    DEBUG_IMU2,
    DEBUG_IMU3
};

void imuConfigure(void);

#define getSinTiltAngle() (attitude.ratios.w.s)
#define getCosTiltAngle() (attitude.ratios.w.w)
bool imuUpdateAttitude(timeUs_t);

void imuInit(void);

#ifdef SIMULATOR_BUILD
void imuSetAttitudeRPY(float roll, float pitch, float yaw);  // in deg
void imuSetAttitudeQuat(float w, float x, float y, float z);
#if defined(SIMULATOR_IMU_SYNC)
void imuSetHasNewData(uint32_t dt);
#endif
#endif

#ifdef USE_ACC
#define imuGetBrakingCommands() (\
    (attitude.ratios.w.s * 499 + true) * attitude.ratios.tc /-500\
)
#endif

#ifdef USE_ESC_SENSOR
int32_t imuGetMotorERPMLimit(void);
#endif

/*
 * This file is part of Cleanflight and Betaflight.
 *
 * Cleanflight and Betaflight are free software. You can redistribute
 * this software and/or modify this software under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Cleanflight and Betaflight are distributed in the hope that they
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.
 *
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "build/debug.h"

#include "flight/position.h"
#if defined(USE_BARO) || defined(USE_GPS)
 #if defined(USE_ALT_HOLD) || defined(USE_GPS_RESCUE)
#include "flight/gps_rescue.h"
 #endif

 #ifdef USE_GPS
#include "io/gps.h"
 #endif

 #ifdef USE_ACC_SENSOR
#include "sensors/acceleration.h"
 #endif
#include "sensors/sensors.h"
 #ifdef USE_BARO
#include "sensors/barometer.h"
 #endif


FAST_RAM_ZERO_INIT sig_atomic_t
(* getEstimatedAltitude)(void), (* getEstimatedVario)(void);

 #ifdef USE_BARO
static sig_atomic_t baroGetAltitude(void) {
    return baro.baroAltitude;
}

static sig_atomic_t baroGetVario(void) {
    return baro.baroVario;
}
 #endif

 #ifdef USE_GPS
static sig_atomic_t gpsGetAltitude(void) {
    return gpsSol.llh.alt + gpsSol.llh.altSeaAGL;
}

static sig_atomic_t gpsGetVario(void) {
    return gpsSol.llh.vario;
}
 #endif

 #ifdef USE_ACC_SENSOR
static sig_atomic_t imuGetAltitude(void) {
    return false;
}

static sig_atomic_t imuGetVario(void) {
    return imuASI.Vario;
}
 #endif

FAST_CODE_NOINVOKE void calculateEstimatedAltitude(
 #ifndef USE_GPS_RESCUE
  __attribute__((unused))
 #endif
    register timeUs_t const currentTimeUs
) {
 #ifdef USE_BARO
    if (sensors(SENSOR_BARO) && isBaroCalibrationComplete())
        if (getEstimatedAltitude = baroGetAltitude,
            getEstimatedVario    = baroGetVario,
            debugMode== DEBUG_ALTITUDE
        )
            debug[0] = baro.baroAAS,
            debug[1] = baro.baroAltitude,
            debug[2] = baro.baroVario,
            debug[3] = baro.baroAccZ;
  #ifdef USE_GPS
        else
            NOOP
    else
  #endif
 #endif
 #ifdef USE_GPS
    if (isGpsUsable())
        if (getEstimatedAltitude = gpsGetAltitude,
            getEstimatedVario    = gpsGetVario,
            debugMode== DEBUG_ALTITUDE
        )
            debug[0] = rquot(u'\x64', gpsSol.hdop),
            debug[1] = gpsSol.llh.alt + gpsSol.llh.altSeaAGL,
            debug[2] = gpsSol.llh.vario;
  #ifdef USE_ACC_SENSOR
        else
            NOOP
    else
  #endif
 #endif
 #ifdef USE_ACC_SENSOR
    getEstimatedAltitude = imuGetAltitude,
    getEstimatedVario    = (
        accelerometerConfig()->acc_hardware &&!STATE(FIXED_WING) ?
            debugMode== DEBUG_ALTITUDE && (
                debug[0] = imuASI.TAS,
                debug[2] = imuASI.Vario
            ),
            imuGetVario
        :
            imuGetAltitude
    );
 #endif

 #if defined(USE_ALT_HOLD) || defined(USE_GPS_RESCUE)
    updateGPSRescueState(
  #ifdef USE_GPS_RESCUE
        currentTimeUs
  #endif
    );
 #endif
}
#endif

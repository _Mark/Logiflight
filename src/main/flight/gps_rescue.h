/*
 * This file is part of Betaflight.
 *
 * Betaflight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Betaflight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Betaflight. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "common/axis.h"

#include "drivers/time.h"

#include "pg/pg.h"

#include "fc/rc_modes.h"

#include "flight/failsafe.h"

#ifndef USE_GPS_RESCUE
#define gpsRescueAngle rcCommand
#endif

typedef enum {
    RESCUE_SANITY_OFF = 0,
    RESCUE_SANITY_ON,
    RESCUE_SANITY_FS_ONLY
} gpsRescueSanity_e;

typedef struct gpsRescue_s {
    uint16_t angle; //degrees
    uint16_t initialAltitude; //meters
    uint16_t descentDistance; //meters
    uint16_t rescueGroundspeed; // centimeters per second
    uint16_t throttleP, throttleI, throttleD;
    uint16_t yawP;
    uint16_t throttleMin;
    uint16_t throttleMax;
    uint16_t throttleHover;
    uint16_t velP, velI, velD;
    uint8_t minSats;
    gpsRescueSanity_e sanityChecks;
} gpsRescueConfig_t;

PG_DECLARE(gpsRescueConfig_t, gpsRescueConfig);

typedef enum {
    RESCUE_IDLE,
    RESCUE_INITIALIZE,
    RESCUE_ATTAIN_ALT,
    RESCUE_CROSSTRACK,
    RESCUE_LANDING_APPROACH,
    RESCUE_LANDING,
    RESCUE_ABORT,
    RESCUE_COMPLETE
} rescuePhase_e;

typedef enum {
    RESCUE_HEALTHY,
    RESCUE_FLYAWAY,
    RESCUE_CRASH_DETECTED,
} rescueFailureState_e;

typedef struct {
    int32_t targetAltitude;
    bool crosstrack;
} rescueIntent_s;

typedef struct {
    int32_t currentAltitude;
    int32_t currentVario;       // cm/s
    int32_t distanceToHome;     // cm
    int16_t directionToHome;
    uint8_t numSat;
    int16_t groundCourse;       // decidegrees
    int32_t groundSpeed;        // cm/s
    int32_t maxDistanceToHome;  // cm
} rescueSensorData_s;

typedef struct {
    bool bumpDetection;
    bool convergenceDetection;
} rescueSanityFlags;

typedef struct {
    rescuePhase_e stage;        // i.e., the next step in returning to home
    rescuePhase_e phase;
    rescueFailureState_e failure;
    rescueSensorData_s sensor;
    rescueIntent_s intent;
    bool isFailsafe;
} rescueState_s;

extern fpDeflection2_t gpsRescueDeflection;
#if defined(USE_ACC) || defined(USE_ALT_HOLD) || defined(USE_GPS_RESCUE)
extern _Atomic float   gpsRescueAngle[ANGLE_INDEX_COUNT], // centidegrees
                       rescueThrottle;                    // ms
extern  rescueState_s  rescueState;

 #ifdef USE_GPS_RESCUE
#define rescueStop()  (rescueState.phase = RESCUE_IDLE)
#define rescueStart() (rescueState.phase = RESCUE_INITIALIZE)
#define gpsRescueIsConfigured() (\
    failsafeConfig()->failsafe_procedure== FAILSAFE_PROCEDURE_GPS_RESCUE ||\
    isModeActivationConditionPresent(BOXGPSRESCUE)\
)
void updateGPSRescueState(timeUs_t);
 #else
void updateGPSRescueState(void);
 #endif
 #ifdef USE_GPS
void rescueNewGpsData(void);
 #endif
float climbRateController(int64_t);
#else
extern _Atomic float   gpsRescueAngle[ANGLE_INDEX_COUNT]; // centidegrees
#endif

/*
 * This file is part of Cleanflight and Betaflight.
 *
 * Cleanflight and Betaflight are free software. You can redistribute
 * this software and/or modify this software under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Cleanflight and Betaflight are distributed in the hope that they
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.
 *
 * If not, see <http://www.gnu.org/licenses/>.
 */

// Inertial Measurement Unit (IMU)

#include <stdbool.h>
#include <stdint.h>
#include <math.h>

#include "platform.h"

#ifdef USE_OSD
#include <string.h>
#endif

#include "build/build_config.h"
#include "build/debug.h"

#include "cms/cms_menu_osd.h"

#include "common/axis.h"
#include "common/maths.h"
#ifdef USE_ACC_SENSOR
#include "common/filter.h"
#endif

#include "config/feature.h"

#include "pg/pg.h"
#include "pg/pg_ids.h"

#include "fc/fc_core.h"
#include "fc/rc_modes.h"
#include "fc/runtime_config.h"
#include "fc/config.h"

#include "scheduler/scheduler.h"

#include "flight/redline.h"
#include "flight/position.h"
#include "flight/imu.h"
#include "flight/mixer.h"
#include "flight/pid.h"
#include "flight/gps_rescue.h"

#include "interface/cli.h"
#include "interface/settings.h"

#include "io/gps.h"
#include "io/beeper.h"

#include "sensors/acceleration.h"
#include "sensors/barometer.h"
#include "sensors/compass.h"
#ifdef USE_ACC
#include "sensors/battery.h"
#endif
#include "sensors/gyro.h"
#include "sensors/sensors.h"

#include <complex.h>
#if defined(SIMULATOR_BUILD) && defined(SIMULATOR_MULTITHREAD)
#include <pthread.h>

#include "common/printf.h"

#define printf tfp_printf

static pthread_mutex_t imuUpdateLock;

#define IMU_LOCK pthread_mutex_lock(&imuUpdateLock)
#define IMU_UNLOCK pthread_mutex_unlock(&imuUpdateLock)

#else

#define IMU_LOCK
#define IMU_UNLOCK

#endif

#ifdef USE_MAG
#define MAG_TARGET_LOOPTIME 100000u // µs
#endif
#ifdef USE_GPS
#define GPS_TARGET_LOOPTIME  10000u // µs
#endif

#define imuUseFastGains() (\
    ARMING_FLAG(ARMED) ? isBeeperOn() ? 17e-2f : 1.f : 17.f\
)

typedef struct {
    float const xx, xy, xz;
    float       xw;
    float const yy, yz;
    float       yw;
    float const zz;
    float       zw;
} quaternionProducts_t;

static FAST_RAM_ZERO_INIT timeUs_t previousTimeUs, imuDeltaT, imuCalcCountdown;

static FAST_RAM_ZERO_INIT union {
    timeUs_t
    raw[2
#ifdef USE_GPS
        + 1
#endif
#ifdef USE_MAG
        + 1
#endif
    ][2];

  __PACKED_STRUCT {
        struct {
            timeUs_t cycle, target;
        } gyro, acc
#ifdef USE_GPS
        , gps
#endif
#ifdef USE_MAG
        , mag
#endif
        ;
    };
} imuTimeUs;

#ifdef SIMULATOR_IMU_SYNC
static FAST_RAM_ZERO_INIT atomic_bool
#else
static FAST_RAM_ZERO_INIT bool
#endif
imuUpdated;

static FAST_RAM_ZERO_INIT struct {
     float
     gyro[XYZ_AXIS_COUNT], acc[XYZ_AXIS_COUNT],
      att[XYZ_AXIS_COUNT], yaw[XYZ_AXIS_COUNT];
} imuADC;

static FAST_RAM_ZERO_INIT fpVector3_t vGyro, vError, vAccAverage;

// quaternion of sensor frame relative to earth frame
static FAST_RAM_ZERO_INIT quaternion qAttitude;

#ifdef USE_ACC_SENSOR
static FAST_RAM_ZERO_INIT pt1Filter_t   imuAccLPF[2];
static FAST_RAM_ZERO_INIT float complex imuAccCis;

 #ifdef USE_ESC_SENSOR
static FAST_RAM_ZERO_INIT struct {
   _Atomic float x, y, z;
           float w;
} vAccPush;
 #endif
#endif // USE_ACC_SENSOR

FAST_RAM_ZERO_INIT imuASI_t              imuASI;
FAST_RAM_ZERO_INIT attitudeEulerAngles_t attitude;
FAST_RAM_ZERO_INIT imuRuntimeConfig_t    imuRuntimeConfig;

PG_REGISTER_WITH_RESET_TEMPLATE(imuConfig_t, imuConfig, PG_IMU_CONFIG, 0);

PG_RESET_TEMPLATE(imuConfig_t, imuConfig,
                  .dcm_kp = 2500,
                  .dcm_ki = 7,
                  .small_angle  = '\x32',   // i.e., 5 standard deviations
                  .acc_deadband = '\x28',   // cm/s/s
                  .acc_unarmedcal = 1
                 );


FAST_CODE_NOINVOKE void imuConfigure(void) {
    register imuConfig_t const * restrict const
    PTR = imuConfig();

    imuRuntimeConfig.dcm = CMPLX(PTR->dcm_kp * 1000000ull, PTR->dcm_ki);
    imuRuntimeConfig.acc_unarmedcal = PTR->acc_unarmedcal;
}

void imuInit(void) {
    attitude.ratios.w.w = qAttitude.w = true;

#ifdef USE_OSD
    if (STATE(FIXED_WING))
 #if defined(USE_CMS) && defined(USE_EXTENDED_CMS_MENUS)
        strcpy(cms_menu_pit_ang,"PIT ANG"        ),
        strcpy(cms_menu_rol_ang,"ROL ANG"        ),
 #endif
        strcpy(osd_pit_ang_pos, "osd_pit_ang_pos"),
        strcpy(osd_rol_ang_pos, "osd_rol_ang_pos");
#endif

#ifdef USE_ACC
    if (accelerometerConfig()->acc_hardware && (
 #if!(defined(SIMULATOR_BUILD) && defined(SKIP_IMU_CALC))
            imuAccLPF[1].k = imuAccLPF[0].k = pt1FilterGain(
                accelerometerConfig()->acc_lpf_hz, acc.targetLooptimef
            ),
 #endif
            imuRuntimeConfig.acc_deadband = imuConfig()->acc_deadband,
            armingConfig()->gyro_cal_on_first_arm
        )
    )
        setArmingDisabled(ARMING_DISABLED_BST);
#endif

#ifdef USE_ESC_SENSOR
    imuRuntimeConfig.erpm.limit.mach =
        (uint64_t)(
            imuRuntimeConfig.motor_poles = motorConfig()->motorPoleCount
        ) * redlineConfig()->propRedlineTipSpeed * 1000ull;

    imuRuntimeConfig.erpm.length =
        (redlineConfig()->propDiameter * 127ul) * M_PIf / 15.f;
#endif

#if defined(SIMULATOR_BUILD) && defined(SIMULATOR_MULTITHREAD)
    if (pthread_mutex_init(&imuUpdateLock, NULL) != 0) {
        printf("Create imuUpdateLock error!\n");
    }
#endif
}

#ifdef USE_ACC
static bool isAccelerating(void) {
    return
    ISRVALUE({
        register float
        accSqSum = false;

        for (
            register ptrdiff_t
            axis = XYZ_AXIS_COUNT;
            axis;
            accSqSum+= ({
                register float const
                accADC = (--axis)[acc.accADC];

                if (fabsf(accADC) > acc.accStdDev)
                    return true;

                accADC * accADC;
            })
        ) NOOP

        accSqSum;
    }) > acc.accVariance;
}

_Static_assert(
    XYZ_AXIS_COUNT > 2
,
    "XYZ_AXIS_COUNT is less than 3"
);

static void imuCalculateCenterOfMass(void) {
    if (STATE(SMALL_ANGLE) || isAccelerating())
        imuCalcCountdown = 249999ul / imuDeltaT + 1ul; // ca., 250 ms
    else if (imuCalcCountdown--)
        NOOP
    else {
    {   float
        matrix[3][4] = {
            acc.accADC[0]
        ,
            DEGREES_TO_RADIANS(gyro.gyroAccf[1])
        ,
            DEGREES_TO_RADIANS(gyro.gyroAccf[2])
        , ISRVALUE({
            for (
                register ptrdiff_t
                i = 3;
              --i;
                i[(i ^ 3)[matrix]] = sq_approx(DEGREES_TO_RADIANS(
                    i[gyro.gyroADCf]
                ))
            ) NOOP

            matrix[1][2] + matrix[2][1];
        }),
            acc.accADC[1]
        ,
            DEGREES_TO_RADIANS(gyro.gyroAccf[0])
        ,
            (matrix[2][2] = sq_approx(DEGREES_TO_RADIANS(gyro.gyroADCf[0])))
                + matrix[1][2]
        ,
            matrix[0][2]
        ,
            acc.accADC[2]
        ,
            matrix[2][2] + matrix[2][1]
        ,
            matrix[1][1]
        ,
            matrix[0][1]
        };

        for (register ptrdiff_t axis = 3; axis;--axis)
            for (
                register struct {
                    ptrdiff_t                  col;
                    float     * restrict const row;
                } lcv = {.row = ((lcv.col = axis) - 1)[matrix] };
                lcv.col;
                (--lcv.col)[lcv.row]/= axis[lcv.row]
            )
                for (
                    register typeof(matrix[0]) * restrict
                    row = (void *)matrix[3];
                  --row>= matrix;
                )
                    if (row!= (void const *)lcv.row)
                        ((float *)row)[lcv.col]-=
                            ((float const *)row)[axis] * lcv.row[lcv.col]
                                / lcv.row[axis];

        for (
            register struct {
                int16_t   * restrict const pos;
                ptrdiff_t                  axis;
            } lcv = { imuConfigMutable()->att_ctrofmass, XYZ_AXIS_COUNT };
            lcv.axis--;
            lcv.axis[lcv.pos] = lrintf(lcv.axis[matrix][0])
        ) NOOP
    }
        writeEEPROM();
         readEEPROM();

        beeper(BEEPER_ACC_CALIBRATION);

        unsetArmingDisabled(ARMING_DISABLED_BST);
    }
}

static FAST_CODE_NOINVOKE void imuCalculateAccelNet(void) {
{   struct eqn_s {
        float sqW, acc, disp;
    } eqn[XYZ_AXIS_COUNT];

    for (
        register struct {
            int16_t const * restrict const pos;
            ptrdiff_t                      axis;
        } config = { imuConfig()->att_ctrofmass, XYZ_AXIS_COUNT - 1 };

        config.axis[eqn] = (struct eqn_s){ sq_approx(
            DEGREES_TO_RADIANS(config.axis[gyro.gyroADCf])
        ),
            DEGREES_TO_RADIANS(config.axis[gyro.gyroAccf])
        ,
            config.axis[config.pos]
        },
        config.axis--;
    ) NOOP

    acc.accNormSq = false;

    for (
        register union {
            sig_atomic_t int32;

          __PACKED_STRUCT {
                char const x, y, z;
                char       w;
            };
        } axis = { 0x020100 };

        axis.x[vAccAverage.raw] = ldexpf(ISRVALUE({
            register float
            accSum = axis.x[imuADC.acc];
            // See <https://www.ux1.eiu.edu/~cfadd/1150/03Vct2D/accel.html>
            accSum+= axis.x[imuADC.acc] = axis.x[acc.accADC]-=
                (eqn[axis.y].sqW + eqn[axis.z].sqW) * eqn[axis.x].disp +
                 eqn[axis.y].acc                    * eqn[axis.z].disp +
                 eqn[axis.z].acc                    * eqn[axis.y].disp;
        }),
           -true
        ),

        acc.accNormSq+= sq_approx((axis.w = axis.x)[imuADC.acc]),

        (axis.int32>>= CHAR_BIT) & 0xFF;
    ) NOOP
}
    if ((acc.accNorm = sqrtf(acc.accNormSq)) > acc.accNormMax)
        acc.accNormMax = acc.accNorm;
}

 #ifdef USE_ACC_SENSOR
// rotate acc into Earth frame and calculate acceleration in it
static FAST_CODE_NOINLINE void imuCalculateAccelPush(void) {
    quaternion
    accel =*(quaternion const *)imuADC.acc;

    quaternionTransformVectorBodyToEarth(
        (void *)accel.raw, (void *)qAttitude.raw
    );

    acc.accClimb = accel.z;

{   register struct {
        float complex xy;
        float         r;
    } const accel_fru = {
        CMPLX(
            pt1FilterApply(imuAccLPF + X, accel.x)
        ,
            pt1FilterApply(imuAccLPF + Y, accel.y)
        )
    ,
        cabsf(accel_fru.xy)
    };

    if (accel_fru.r > imuRuntimeConfig.acc_deadband)
        imuAccCis = accel_fru.xy / accel_fru.r;
}
    quaternion
    ratio = {.z = true };

    quaternionTransformVectorEarthToBody(
        (void *)ratio.raw, (void *)qAttitude.raw
    );

    // See <https://mathworld.wolfram.com/TrigonometricAdditionFormulas.html>
    if (isnormal(ratio.z*= ratio.w = hypotf(ratio.x, ratio.y)) &&
        hypotf(
            accel.z =
                (imuADC.acc[X] * ratio.x + imuADC.acc[Y] * ratio.y) / ratio.z
        ,
            accel.w =
                (imuADC.acc[Y] * ratio.x - imuADC.acc[X] * ratio.y) / ratio.w
        ) > imuRuntimeConfig.acc_deadband && (
            quaternionTransformVectorBodyToEarth(
                (void *)(ratio = (quaternion){.z = true }).raw
            ,
                (void *)qAttitude.raw
            ),
            isnormal(ratio.z = hypotf(ratio.x, ratio.y))
        )
    )
        // See <https://youtu.be/NblR01hHK6U?t=566>
        vAccPush.z =
            hypotf(
                (accel.z * ratio.x - accel.w * ratio.y) / ratio.z + accel.x
            ,
                (accel.w * ratio.x + accel.z * ratio.y) / ratio.z + accel.y
            ) / ratio.z - imuADC.acc[Z],
        vAccPush.w =
            constrainf(sqrtf(ISRVALUE({
                register float const
                NORM =
                    fabsf(vAccPush.x =-imuADC.acc[X]) +
                    fabsf(vAccPush.y =-imuADC.acc[Y]);

                NORM / (fabsf(vAccPush.z) + NORM);
            })),
                FLT_MIN
            ,
                FLT_MAX
            );
}
 #endif // USE_ACC_SENSOR
#endif // USE_ACC

#if defined(USE_ACC) || defined(USE_MAG) || defined(USE_GPS)
 #ifdef USE_ACC
static FAST_CODE_NOINVOKE void applyAccCorrection(void) {
    struct {
        fpVector3_t est, dev;
        float       w;
    } accel = {
        {.z =
  #ifdef USE_BARO
            sensors(SENSOR_BARO) ?
                accel.est.z = opposf(
                    acc.accNormSq
                ,
                    sq_approx(accel.w = baro.baroAccZ + acc.accNormGnd)
                ),
                quaternionTransformVectorBodyToEarth(
                    (void *)accel.est.raw, (void *)qAttitude.raw
                ),
                accel.w
            :
  #endif
            acc.accNorm
        }
    , (
        quaternionTransformVectorEarthToBody(
            (void *)accel.est.raw, (void *)qAttitude.raw
        ),
        vAccAverage
    ) };

    quaternionTransformVectorBodyToEarth(
        (void *)accel.dev.raw, (void *)qAttitude.raw
    );

    for (
        register union {
            sig_atomic_t int32;

          __PACKED_STRUCT {
                char const z, y, x;
                char       w;
            };
        } axis = { 0x010200 };

        (axis.w = axis.z)[vError.raw] =
            (   axis.y[accel.est.raw] * axis.x[accel.dev.raw] -
                axis.x[accel.est.raw] * axis.y[accel.dev.raw]
            ) / acc.accNormSq,

        (axis.int32>>= CHAR_BIT) & 0xFF;
    ) NOOP
}
 #endif // USE_ACC

 #if defined(USE_MAG) || defined(USE_GPS)
static FAST_CODE_NOINVOKE bool applySensorCorrection(void) {
    quaternion vMagAverage;

  #ifdef USE_GPS
   #ifdef USE_MAG
    register bool const
    isMagModeActive = FLIGHT_MODE(MAG_MODE);

    // See <https://youtu.be/FlEm0-pXNf0?t=394>
    if (isMagModeActive)
        goto DO_CORR_FROM_MAG_CHECK;
    else
DO_CORR_FROM_GPS_CHECK:
   #endif
    if (isGpsCourseUsable()) {
        // William Premerlani and Paul Bizard, Direction Cosine Matrix IMU - Eqn. 22-23
        // (Rxx; Ryx) - measured (estimated) heading vector (EF)
        // (cos(COG), sin(COG)) - reference heading vector (EF)
        // error is cross product between reference heading and estimated heading (calculated in EF)
        register float complex const
        north_fru = (
   #ifdef USE_ACC_SENSOR
            accelerometerConfig()->acc_hardware &&!STATE(FIXED_WING) ?
                gpsSol.cis.acc / imuAccCis
            :
   #endif
            gpsSol.cis.gnd / CMPLX(attitude.ratios.x.x, attitude.ratios.y.x)
        );

        vMagAverage.x =__real__ north_fru;
        vMagAverage.y =__imag__ north_fru;
    }
  #endif // USE_GPS
  #ifdef USE_MAG
   #ifdef USE_GPS
    else if (isMagModeActive)
        return false;
    else
DO_CORR_FROM_MAG_CHECK:
   #endif // USE_GPS
    if (
        // For magnetometer correction we make an assumption that magnetic field is perpendicular to gravity (ignore Z-component in EF).
        // This way magnetic field will only affect heading and wont mess roll/pitch angles
        sensors(SENSOR_MAG) && (
            // (hx; hy; 0) - measured mag field vector in EF (assuming Z-component is zero)
            // (bx; 0; 0) - reference mag field vector heading due North in EF (assuming Z-component is zero)
            compassGetAverage(vMagAverage.raw),
            compassIsHealthy (vMagAverage.raw)
        )
    ) NOOP
   #ifdef USE_GPS
    else if (isMagModeActive)
        goto DO_CORR_FROM_GPS_CHECK;
   #endif
  #endif // USE_MAG
    else
        return false;

    quaternionTransformVectorBodyToEarth(
        (void *)vMagAverage.raw, (void *)qAttitude.raw
    );

    vMagAverage.z = false;
    vMagAverage.w = hypotf(vMagAverage.x, vMagAverage.y);

{   quaternion
    north_fru = { true };

    quaternionTransformVectorEarthToBody(
        (void *)north_fru.raw, (void *)qAttitude.raw
    );

    register struct {
        ptrdiff_t z, y, x;
    } AXIS = { 2, 1 };

    do
        // magnetometer error is cross product between estimated magnetic north and measured magnetic north (calculated in EF)
        vError.raw[AXIS.z] =
            (   north_fru.raw[AXIS.y] * vMagAverage.raw[AXIS.x] -
                north_fru.raw[AXIS.x] * vMagAverage.raw[AXIS.y]
            ) / vMagAverage.w;
    while (ISRVALUE({
        register ptrdiff_t const
        AXIS_z = AXIS.z;

        AXIS.z = AXIS.y;
        AXIS.y = AXIS.x;
        AXIS.x = AXIS_z;
    }));
}
    return true;
}
 #endif // USE_MAG || USE_GPS
#endif // USE_ACC || USE_MAG || USE_GPS

static FAST_CODE_NOINVOKE void imuMahonyAHRSupdate(register timeUs_t const dT) {
    // vGyro integration
    // PCDM Acta Mech 224, 3091–3109 (2013)
    stateFlags = (
        ISRVALUE({
            register float
            vGyroNorm = false;

            for (
                register ptrdiff_t
                axis = XYZ_AXIS_COUNT;
                axis--;
                // See <http://mathworld.wolfram.com/TrapezoidalRule.html>
                vGyroNorm+= ({
                    register float
                    axisSpin = axis[imuADC.gyro];

                    axisSpin*= axis[ vGyro.raw ] =
                    axisSpin+= axis[imuADC.gyro] = axis[gyro.gyroADCf];
                })
            ) NOOP

            if (isnormal(vGyroNorm = sqrtf(vGyroNorm))) {
                quaternion qBuf;

                sincosf(
                    ldexpf(dT * vGyroNorm / 703125.f,-7)
                ,
                    qBuf.raw
                ,
                    qBuf.raw + W
                );

                for (
                    register ptrdiff_t
                    axis = XYZ_AXIS_COUNT;
                    axis;
                    axis[qBuf.raw] = (--axis)[vGyro.raw] * qBuf.x / vGyroNorm
                ) NOOP

                quaternionMultiply(
                    (void *)qAttitude.raw
                ,
                    (void *)qBuf.raw
                ,
                    (void *)qAttitude.raw
                );
            }

            if (debugMode== DEBUG_IMU)
                debug[DEBUG_IMU0] = lrintf(ldexpf(sqrtf(vGyroNorm), 3) * 125.f);

            vGyroNorm;
        }) > imuRuntimeConfig.gyro_deadband ?
            stateFlags &~SMALL_ANGLE : stateFlags | SMALL_ANGLE
    );

    // vKpKi integration
    // Euler integration (q(n+1) is determined by a first-order Taylor expansion) (old bf method adapted)
#ifdef USE_ACC_SENSOR
    if (ISRVALUE({
#endif
    register float complex const
    attGainPI = imuUseFastGains() * imuRuntimeConfig.dcm;

    void applyFn(register float buf[restrict const]) {
        quaternion vKpKi;

        for (
            register ptrdiff_t
            axis = XYZ_AXIS_COUNT;
            axis--;
            vKpKi.w+= ({
                register float
                DELTA = axis[vError.raw] *__real__ attGainPI;

                axis[vKpKi.raw] =
                    ldexpf(
                        ( __imag__ attGainPI ?
                                DELTA+= axis[buf]+=
                                  __imag__ attGainPI * dT * axis[vError.raw]
                            :
                                DELTA
                        ) * dT / 390625.f / 390625.f
                    ,
                       -17
                    );

                DELTA * DELTA;
            })
        ) NOOP

        if (debugMode== DEBUG_IMU)
            ((buf== imuADC.yaw) + DEBUG_IMU1)[debug] = lrintf(
                ldexpf(sqrtf(vKpKi.w) / 390625.f,-8)
            );

        if (vKpKi.w > 9e14f)
            vKpKi.w = false,
            quaternionMultiply(
                (void *)vKpKi.raw, (void *)qAttitude.raw, (void *)vKpKi.raw
            ),
            quaternionAdd(
                (void *)vKpKi.raw, (void *)qAttitude.raw, (void *)qAttitude.raw
            ),
            quaternionNormalize((void *)qAttitude.raw);
    }

#if defined(USE_MAG) || defined(USE_GPS)
    if (applySensorCorrection())
        applyFn(imuADC.yaw);
#endif

#ifdef USE_ACC
 #if!(defined(SIMULATOR_BUILD) && defined(SIMULATOR_IMU_SYNC))
    accelerometerConfig()->acc_hardware &&!({
 #endif
    if (isnormal(acc.accNormSq))
        applyAccCorrection(),
        applyFn(imuADC.att);

 #if!(defined(SIMULATOR_BUILD) && defined(SIMULATOR_IMU_SYNC))
    STATE(FIXED_WING);
    });
    }) && imuUpdated)
        imuUpdated = false,
        imuCalculateAccelPush();
 #endif
}

static FAST_CODE_NOINLINE void imuUpdateAacceleration(void) {
    accUpdate();

    if ((
 #if!(defined(SIMULATOR_BUILD) && defined(SIMULATOR_IMU_SYNC))
        imuUpdated =
 #endif
        (getArmingDisableFlags() ^ ARMING_DISABLED_BST)
            & (ARMING_DISABLED_CALIBRATING | ARMING_DISABLED_BST)
    )) NOOP else
        imuCalculateCenterOfMass();
#endif // USE_ACC
}

static FAST_CODE_NOINVOKE void imuUpdateEulerAngles(void) {
    {   register quaternionProducts_t
        buf = {
            (buf.xw = ldexpf(qAttitude.x, 1)) * qAttitude.x
        ,
             buf.xw                           * qAttitude.y
        ,
             buf.xw                           * qAttitude.z
        ,
             buf.xw                           * qAttitude.w
        ,
            (buf.yw = ldexpf(qAttitude.y, 1)) * qAttitude.y
        ,
             buf.yw                           * qAttitude.z
        ,
             buf.yw                           * qAttitude.w
        ,
            (buf.zw = ldexpf(qAttitude.z, 1)) * qAttitude.z
        ,
             buf.zw                           * qAttitude.w
        };

        attitude.ratios.x.y = buf.xy - buf.zw;
        attitude.ratios.x.z = buf.xz + buf.yw;
        attitude.ratios.y.y = true - buf.xx - buf.zz;
        attitude.ratios.y.z = buf.yz - buf.xw;

        // See <https://en.wikipedia.org/wiki/Euler_angles#Conventions>
        attitude.values.roll  = lrintf(RADIANS_TO_DECIDEGREES(
            atan2_approx(
                attitude.ratios.z.y = buf.xw + buf.yz
            ,
                attitude.ratios.z.z = true - buf.xx - buf.yy
            )
        ));
        attitude.values.pitch = lrintf(RADS_CW_TO_DECIDEGREES(asin_approx(
            attitude.ratios.z.x = buf.xz - buf.yw
        )));
        attitude.values.yaw   =
            (   (int16_t)lrintf(RADS_CW_TO_DECIDEGREES(
                    atan2_approx(
                        attitude.ratios.y.x = buf.xy + buf.zw
                    ,
                        attitude.ratios.x.x = true - buf.yy - buf.zz
                    )
                )) + u'\xE10'
            ) % u'\xE10';
    }

    if (isnormal(
        attitude.ratios.w.s =
            hypotf(
                attitude.ratios.w.x =
                    fabsf(attitude.ratios.z.z) * attitude.ratios.z.x
            ,
                attitude.ratios.z.y
            )
    ))
        attitude.values.slant =
            (   (int16_t)lrintf(RADS_CW_TO_DECIDEGREES(
                    atan2_approx(attitude.ratios.z.y, attitude.ratios.w.x)
                )) + u'\xE10'
            ) % u'\xE10';

    attitude.values.tilt = lrintf(RADIANS_TO_DECIDEGREES(acos_approx(
        attitude.ratios.w.w =
            opposf(true, sq_approx((float)attitude.ratios.z.x)))
                * attitude.ratios.z.z
    ));

#ifdef USE_ACC_SENSOR
    if (accelerometerConfig()->acc_hardware &&!STATE(FIXED_WING)) {
        quaternion
        accel = { vAccPush.x, vAccPush.y, vAccPush.z };

        quaternionTransformVectorBodyToEarth(
            (void *)accel.raw, (void *)qAttitude.raw
        );

        imuASI.Vario = lrint(({
            register double const
            vario_u =
                copysign(
                    isnormal(accel.w = fabsf(accel.x) + fabsf(accel.y)) ?
                        sqrt(fabsf(accel.z / (fabsf(accel.z) + accel.w)))
                            * gyro.speed.air
                    :
                        gyro.speed.air
                ,
                    accel.z
                );

            isRedline.speed.air = ({
                register redlineConfig_t * restrict const
                PTR = redlineConfigMutable();

                (   imuASI.EAS = lrint(
                        sqrt(
                            ({  register double const
                                DELTA =
                                    fdim(sq(gyro.speed.air), vario_u * vario_u);

                                {   register uint16_t const
                                    NORM = imuASI.TAS = lrint(sqrt(DELTA));

                                    if (NORM > PTR->imuHighestTAS)
                                        PTR->imuHighestTAS = NORM;
                                }

                                DELTA;
                            }) * (
 #ifdef USE_BARO
                                sensors(SENSOR_BARO) ?
                                    baro.baroPressureScaled
                                :
 #endif
                                isnormal(gyro.speed.sagT) ?
                                    ldexp(sq(gyro.temp.here), 1)
                                        * 334901.l
                                        / (gyro.speed.sagT * 193.l)
                                :
                                    false
                            )
                        ) * 34027.l / gyro.speed.snd
                    )
                )>= PTR->imuRedlineEAS;
            });

            vario_u;
        }));

        if (isnormal(accel.w = sqrtf(accel.w))) {
            register int16_t const
            ahead_n = attitude.values.ahead =
                (   (int16_t)lrintf(RADIANS_TO_DECIDEGREES(cargf(
                        attitude.ratios.tc =
                            CMPLX(attitude.ratios.x.x, attitude.ratios.y.x) *
                            accel.w /
                            CMPLX(
                                copysignf(sqrtf(fabsf(accel.x)), accel.x)
                            ,
                                copysignf(sqrtf(fabsf(accel.y)), accel.y)
                            )
                    ))) + u'\xE10'
                ) % u'\xE10';

            if (debugMode== DEBUG_IMU)
                debug[DEBUG_IMU3] = ahead_n;
        }
    }
#endif // USE_ACC_SENSOR
}

FAST_CODE FAST_CODE_NOINLINE bool imuUpdateAttitude(
    register timeUs_t currentTimeUs
) {
    IMU_LOCK;

    imuTimeUs.gyro.target =
        (   imuTimeUs.gyro.cycle =
                gyro.targetLooptime -
                ISRVALUE({
                    register timeUs_t const
                    deltaT = currentTimeUs - imuTimeUs.gyro.target;

                    if (debugMode== DEBUG_CYCLETIME)
                        debug[0] = deltaT + imuTimeUs.gyro.cycle;

                    deltaT;
                }) % gyro.targetLooptime
        ) + currentTimeUs;

    gyroUpdate(currentTimeUs);

    register timeUs_t const
    offsetTimeUs =
        (   imuDeltaT = ISRVALUE({
                register timeUs_t const
                TIME = previousTimeUs;

                (previousTimeUs = currentTimeUs) - TIME;
            })
        ) * 3ul>> true;

#ifndef imuScheduleTask
    #define imuScheduleTask(x)\
    for (register ptrdiff_t i = ARRAYLEN(imuTimeUs.raw);--i;)\
        if (i!= (x)) {\
            register timeUs_t const\
            deltaT = imuTimeUs.raw[i][1];\
\
            if ((   deltaT < nextTimeUs ?\
                        nextTimeUs - deltaT : deltaT - nextTimeUs\
                ) < offsetTimeUs\
            )\
                nextTimeUs = deltaT + offsetTimeUs;\
        }
#endif

    register bool const
    isGyroDataUsable = isGyroDataNew();

#ifdef USE_ACC
    if (isGyroDataUsable && (
            gyroReadTemperature(),
            accelerometerConfig()->acc_hardware
        ) &&
        currentTimeUs>= imuTimeUs.acc.target
    )
        imuTimeUs.acc.target = ISRVALUE({
            register timeUs_t
            nextTimeUs =
                (   imuTimeUs.acc.cycle =
                        acc.targetLooptime -
                        ISRVALUE({
                            register timeUs_t const
                            deltaT = currentTimeUs - imuTimeUs.acc.target;

                            if (debugMode== DEBUG_CYCLETIME)
                                debug[1] = deltaT + imuTimeUs.acc.cycle;

                            deltaT;
                        }) % acc.targetLooptime
                ) + currentTimeUs;

            imuScheduleTask(1);

            nextTimeUs;
        }),
        imuUpdateAacceleration(),
        imuCalculateAccelNet();
#endif // USE_ACC

#ifdef USE_GPS
    if (currentTimeUs>= imuTimeUs.gps.target && feature(FEATURE_GPS))
        imuTimeUs.gps.target = ISRVALUE({
            register timeUs_t
            nextTimeUs =
                (   imuTimeUs.gps.cycle =
                        GPS_TARGET_LOOPTIME - (
                                currentTimeUs - imuTimeUs.gps.target
                            ) % GPS_TARGET_LOOPTIME
                ) + currentTimeUs;

            imuScheduleTask(2);

            nextTimeUs;
        }),
        gpsUpdate(currentTimeUs);
#endif // USE_GPS

#ifdef USE_MAG
    if (currentTimeUs>= imuTimeUs.mag.target && sensors(SENSOR_MAG))
        imuTimeUs.mag.target = ISRVALUE({
            register timeUs_t
            nextTimeUs =
                (   imuTimeUs.mag.cycle =
                        MAG_TARGET_LOOPTIME - (
                                currentTimeUs - imuTimeUs.mag.target
                            ) % MAG_TARGET_LOOPTIME
                ) + currentTimeUs;

            imuScheduleTask(
 #ifdef USE_GPS
                3
 #else
                2
 #endif
            );

            nextTimeUs;
        }),
        compassUpdate(currentTimeUs);
#endif // USE_MAG

#ifdef imuScheduleTask
    #undef imuScheduleTask
#endif

    if (isGyroDataUsable)
        imuMahonyAHRSupdate(
            imuDeltaT / gyro.targetLooptime * gyro.targetLooptime
        ),
        imuUpdateEulerAngles();

    IMU_UNLOCK;

    return isGyroDataUsable;
}

#ifdef SIMULATOR_BUILD
void imuSetAttitudeRPY(float roll, float pitch, float yaw) {
    IMU_LOCK;
    attitude.values.roll = roll * 10;
    attitude.values.pitch = pitch * 10;
    attitude.values.yaw = yaw * 10;
    IMU_UNLOCK;
}

void imuSetAttitudeQuat(float w, float x, float y, float z) {
    IMU_LOCK;
    qAttitude.x = x;
    qAttitude.y = y;
    qAttitude.z = z;
    qAttitude.w = w;
    imuUpdateEulerAngles();
    IMU_UNLOCK;
}
#endif
#if defined(SIMULATOR_BUILD) && defined(SIMULATOR_IMU_SYNC)
void imuSetHasNewData(uint32_t dt) {
    IMU_LOCK;
    imuUpdated = true;
    imuDeltaT = dt;
    IMU_UNLOCK;
}
#endif

#ifdef USE_ESC_SENSOR
FAST_CODE_NOINVOKE int32_t imuGetMotorERPMLimit(void) {
return lrintf(
 #ifdef USE_ACC_SENSOR
    (   accelerometerConfig()->acc_hardware ? ({
            register quaternion
            ACC = {.w =
                (ACC.x = vAccPush.x) +
                (ACC.y = vAccPush.y) +
                (ACC.z = vAccPush.z)
            };

            isnormal(ACC.w) ?
                sqrtf(({
                    register float const
                    TMP = sq_approx(imuASI.TAS * imuRuntimeConfig.motor_poles);

                    fdimf(sq_approx(
                        llrint(sqrt((TMP * ACC.x + TMP * ACC.y) / ACC.w))
                            - imuRuntimeConfig.erpm.limit.cmss
                    ),
                        TMP * ACC.z / ACC.w
                    );
                })) * 15.f
            :
                false;
        }) :
            (imuASI.TAS - imuRuntimeConfig.erpm.limit.cmss) *-15l
    ) / imuRuntimeConfig.erpm.length
 #else
    ((imuASI.TAS - imuRuntimeConfig.erpm.limit.cmss) *-15l)
        / imuRuntimeConfig.erpm.length
 #endif
); }
#endif // USE_ESC_SENSOR

/*
 * This file is part of Logiflight.
 *
 * Logiflight is free software. You can redistribute
 * this software and/or modify this software under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Logiflight is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.
 *
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "common/maths.h"

#include "config/feature.h"

#include "pg/pg_ids.h"

#include "sensors/barometer.h"
#include "sensors/sensors.h"

#include "fc/runtime_config.h"

#include "flight/redline.h"
#include "flight/imu.h"
#include "flight/mixer.h"

FAST_RAM_ZERO_INIT redlineFlag_t isRedline;

PG_REGISTER_WITH_RESET_TEMPLATE(redlineConfig_t, redlineConfig, PG_REDLINE_CONFIG, 0);

PG_RESET_TEMPLATE(redlineConfig_t, redlineConfig,
    // See <https://www.law.cornell.edu/cfr/text/14/91.117>
   .imuRedlineEAS = u'\x2D37',              // ca. 225 kn
   .imuHighestTAS = u'\0',                  // cm/s

   .motorRedlineAmperage   = u'\xDAC',      // i.e., 35 A
   .vbatRedlineCellVoltage = u'\x14A',      // i.e., 3.3 V

#ifdef USE_GPS
   .gpsRedlineGndSpeed = u'\x1176',         // ca. 100 mph
   .gpsHighestGndSpeed = u'\0',
#endif

#ifdef USE_ALT
   .varioRedlineAltitude = u'\x6E',         // i.e., 110 m AGL
#endif
#ifdef USE_ALT_HOLD
   .varioSetpointLimit = {
        u'\x12C'                            // i.e., 300 cm/s down
    ,
        u'\x258'                            // i.e., 600 cm/s up
    },
#endif

#ifdef USE_ESC_SENSOR
    // See <https://rcplanes.online/calc_thrust.htm>
   .propDiameter        = u'\x7F',          // ca. 5"
   .propRedlineTipSpeed =  '\x5C',          // i.e., Mach 0.92

    // See <https://raw.githubusercontent.com/bitdump/BLHeli/master/SiLabs/BLHeli%20manual%20SiLabs%20Rev14.x.pdf>
   .escRedlineTemperature = u'\x87'         // °C
#endif
);

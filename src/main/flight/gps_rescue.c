/*
 * This file is part of Betaflight.
 *
 * Betaflight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Betaflight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Betaflight. If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <math.h>

#include "platform.h"

#include "flight/gps_rescue.h"

FAST_RAM_ZERO_INIT fpDeflection2_t gpsRescueDeflection;
#if defined(USE_ACC) || defined(USE_ALT_HOLD) || defined(USE_GPS_RESCUE)
FAST_RAM_ZERO_INIT _Atomic float   gpsRescueAngle[ANGLE_INDEX_COUNT],
                                   rescueThrottle;

#if defined(USE_ALT_HOLD) || defined(USE_GPS_RESCUE)
#include "build/debug.h"

#include "common/axis.h"
#include "common/maths.h"
#include "common/filter.h"

#include "io/gps.h"

#include "fc/config.h"
#include "fc/fc_core.h"
#include "fc/rc_controls.h"
#include "fc/rc_modes.h"
#include "fc/runtime_config.h"

#include "flight/redline.h"
#include "flight/failsafe.h"
#include "flight/imu.h"
#include "flight/mixer.h"
#include "flight/pid.h"
#include "flight/position.h"

#include "config/feature.h"

#include "pg/pg_ids.h"

#include "rx/rx.h"

#include "sensors/acceleration.h"
#include "sensors/battery.h"

#define GPS_RESCUE_MAX_YAW_RATE       360  // deg/sec max yaw rate
#define GPS_RESCUE_RATE_SCALE_DEGREES 45   // Scale the commanded yaw rate when the error is less then this angle

PG_REGISTER_WITH_RESET_TEMPLATE(gpsRescueConfig_t, gpsRescueConfig, PG_GPS_RESCUE, 0);

PG_RESET_TEMPLATE(gpsRescueConfig_t, gpsRescueConfig,
                  .angle = 32,
                  .initialAltitude = 10, // i.e., 10 m
                  .descentDistance =  0, // note: GPS auto-config resets this to ca. 400'
                  .rescueGroundspeed = 100,
                  .throttleP = 150,
                  .throttleI = 20,
                  .throttleD = 50,
                  .velP = 80,
                  .velI = 20,
                  .velD = 15,
                  .yawP = 40,
                  .throttleMin   = 0,
                  .throttleMax   = PWM_RANGE,
                  .throttleHover = PWM_RANGE_2,
                  .sanityChecks = RESCUE_SANITY_ON,
                  .minSats = 5
                 );

FAST_RAM_ZERO_INIT rescueState_s rescueState;

 #ifdef USE_GPS_RESCUE
static FAST_RAM_ZERO_INIT timeUs_t overallTimeUs, previousTimeUs, targetTimeUs;
static FAST_RAM_ZERO_INIT uint8_t  msI, gsI;
 #endif

static FAST_RAM_ZERO_INIT struct {
    struct {
        int64_t P, I;
 #ifdef USE_GPS_RESCUE
    } gnd, alt;
 #else
    } alt;
 #endif
    int32_t thr; // centi-ms
} rescueCtrl;

 #ifdef USE_GPS
static FAST_RAM_ZERO_INIT bool newGPSData;

/*
 If we have new GPS data, update home heading
 if possible and applicable.
*/
void rescueNewGpsData(void) {
    newGPSData = true;
}
 #endif // USE_GPS

static FAST_CODE_NOINVOKE void sensorUpdate() {
    rescueState.sensor.currentAltitude = getEstimatedAltitude();
    rescueState.sensor.currentVario    = getEstimatedVario();

 #ifdef USE_GPS
    rescueState.sensor.maxDistanceToHome =
        lrintf(ldexpf(
            (   sq_approx(
                    newGPSData ? (
                        rescueState.sensor.numSat = gpsSol.numSat,
                        rescueState.sensor.groundCourse = gpsSol.groundCourse,
                        rescueState.sensor.groundSpeed  = lrintf(
                            cosf(DECIDEGREES_TO_RADIANS((
                                rescueState.sensor.distanceToHome  =
                                    GPS_distanceToHome,
                                rescueState.sensor.directionToHome =
                                    GPS_directionToHome * u'\xA'
                            ))) * gpsSol.groundSpeed
                        )
                    ) :
                        rescueState.sensor.groundSpeed
                ) *-3l
            ) / acc.accNormMax
        ,
           -1
        )) + ISRVALUE({
            register uint16_t const
            NORM = gpsRescueConfig()->descentDistance; // m

            NORM ?
                rescueState.phase== RESCUE_IDLE ?
                     NORM           * 100l
                : NORM > u'\xA' ?
                    (NORM - u'\xA') * 100l
                :
                    false
            :
                false;
        });
 #endif // USE_GPS
}

 #ifdef USE_GPS_RESCUE
static FAST_CODE_NOINVOKE void performSanityChecks(
    register timeUs_t const currentTimeUs
) {
    if (rescueState.phase == RESCUE_IDLE) {
        rescueState.failure = RESCUE_HEALTHY;
        return;
    }
    // Do not abort until each of these items is fully tested
    if (rescueState.failure != RESCUE_HEALTHY) {
        rescueState.failure = RESCUE_HEALTHY;
        if (ISRVALUE({
            register gpsRescueSanity_e const
            STATE = gpsRescueConfig()->sanityChecks;

            STATE== RESCUE_SANITY_ON || (
                STATE== RESCUE_SANITY_FS_ONLY && rescueState.isFailsafe
            );
        }))
            rescueState.stage = rescueState.phase,
            rescueState.phase = RESCUE_ABORT;
    }
    // Check if crash recovery mode is active, disarm if so.
    if (crashRecoveryModeActive()) {
        rescueState.failure = RESCUE_CRASH_DETECTED;
    }
    //  Things that should run at a low refresh rate (such as flyaway detection, etc)
    //  This runs at ~1hz
    if (currentTimeUs>= targetTimeUs) {
        targetTimeUs+= 1000000ul - (currentTimeUs - targetTimeUs) % 1000000ul;

        if (gpsRescueConfig()->minSats > rescueState.sensor.numSat)
            if (msI < '\xA')
              ++msI;
            else
                rescueState.failure = RESCUE_FLYAWAY;
        else if (msI)
          --msI;

        // Stalled movement detection
        if (rescueState.phase== RESCUE_CROSSTRACK)
            if (rescueState.sensor.groundSpeed < 150)
                if (gsI < '\x14')
                  ++gsI;
                else
                    rescueState.failure = RESCUE_CRASH_DETECTED;
            else if (gsI)
              --gsI;
        else NOOP
    }
}

static FAST_CODE_NOINVOKE double gndCourseController(register int64_t error) {
return
    gpsSol.groundSpeed > u'\x12C' ?
        (double)constrain(ISRVALUE({
            register int64_t const
            rescueCtrl_gnd_D = ({
                register int64_t const
                POS = rescueCtrl.gnd.P;

                (   rescueCtrl.gnd.P = (
                        (   gpsSol.groundSpeed < u'\x258' ?
                                error =
                                    rquot(
                                        (gpsSol.groundSpeed - u'\x12C')
                                            * error
                                    ,
                                        300ll
                                    )
                            :
                                error
                        ) < 1800ll ? error : 1800ll - error
                    )
                ) - POS;
            });

            rescueCtrl.gnd.I = (
                llabs(rescueCtrl.gnd.P) < 250ll ?
                    constrain(
                        rescueCtrl.gnd.P + rescueCtrl.gnd.I
                    ,
                       -2500ll
                    ,
                        2500ll
                    )
                :
                    0ll
            );

            register gpsRescueConfig_t const * restrict const
            PTR = gpsRescueConfig();

            PTR->throttleP * rescueCtrl.gnd.P * 20ll +
            PTR->throttleI * rescueCtrl.gnd.I *  2ll +
            PTR->throttleD * rescueCtrl_gnd_D;
        }),
            PWM_RANGE *-100ll
        ,
            PWM_RANGE * 100ll
        ) / (PWM_RANGE * 100)
    :
        0.l;
}

static FAST_CODE_NOINVOKE void rescueAttainPosition(void) {
    switch (rescueState.phase) {
        case RESCUE_IDLE:
        return;

        case RESCUE_LANDING_APPROACH:
        rescueThrottle = PWM_RANGE_MIN;
        break;

        case RESCUE_LANDING:
        rescueThrottle = PWM_RANGE_MAX;
        break;

        default:
        rescueThrottle =
            fminf(climbRateController(
                (   rescueState.phase > RESCUE_LANDING ? ISRVALUE({
                        register int32_t const
                        DELTA =
                            rescueState.intent.targetAltitude -
                            rescueState.sensor.currentAltitude;

                        rquot(
                             (DELTA>= 0l)[redlineConfig()->varioSetpointLimit]
                                 * DELTA
                        , ({
                            register int32_t const
                            NORM = labs(DELTA);

                            NORM > 100l ? NORM : 100l;
                        }));
                    }) :
                       -(int32_t)gpsRescueConfig()->rescueGroundspeed
                ) - rescueState.sensor.currentVario
            ),
                gpsRescueConfig()->throttleMax
            );
        break;
    }

  #ifdef USE_GPS
    if (newGPSData) {
        newGPSData = false;

        if (rescueState.intent.crosstrack)
            setBearing(rescueState.sensor.directionToHome);

        if (FLIGHT_MODE(GPS_RESCUE_MODE | GPS_HOLD_MODE)== GPS_RESCUE_MODE) {
            register struct {
                bool         FLAG;
                double const VEC;
            } OBJ = {
                false
            , gndCourseController(ISRVALUE({
                register int16_t const
                DELTA =
                    rescueState.sensor.directionToHome -
                    rescueState.sensor.groundCourse;

                  DELTA > u'\x707' ?
                    DELTA - u'\xE10'
                : DELTA >-u'\x709' ?
                    OBJ.FLAG = true,
                    DELTA
                :
                    DELTA + u'\xE10';
            })) };

            gpsRescueDeflection.y = OBJ.VEC;
            gpsRescueDeflection.x = (
                rescueState.stage > RESCUE_ATTAIN_ALT ? ISRVALUE({
                    register float const
                    RATIO = (
                        OBJ.FLAG &&
                        rescueState.sensor.distanceToHome >
                        rescueState.sensor.maxDistanceToHome ? ({
                            register int32_t const
                            TIME = gpsRescueConfig()->throttleHover;

                              rescueState.sensor.distanceToHome > 399l ?
                                (float)TIME / PWM_RANGE_2
                            : rescueState.sensor.distanceToHome > 200l ?
                                ldexpf(
                                    (float)(
                                        (   rescueState.sensor.distanceToHome
                                                - 200l
                                        ) * TIME
                                    ) / (PWM_RANGE_2 * 25)
                                ,
                                   -3
                                )
                            :
                                false;
                        })
                        : rescueState.sensor.groundSpeed > 599l ?
                           -true
                        : rescueState.sensor.groundSpeed > 300l ?
                            ldexpf(
                                (rescueState.sensor.groundSpeed - 300l) /-75.f
                            ,
                               -2
                            )
                        :
                            false
                    );

                    copysignf(
                        fminf(oppos(true, OBJ.VEC * OBJ.VEC), fabsf(RATIO))
                    ,
                        RATIO
                    );
                }) :
                    false
            );
        }
    }
  #endif
}

/*
    Determine what phase we are in, determine if all criteria are met to move to the next phase
*/
FAST_CODE_NOINVOKE void updateGPSRescueState(
    register timeUs_t const currentTimeUs
) {
    performSanityChecks(currentTimeUs);

    if (FLIGHT_MODE(GPS_RESCUE_MODE))
        if (rescueState.phase== RESCUE_IDLE)
            rescueStart();
        else NOOP
    else
        rescueStop();

 #else
FAST_CODE_NOINVOKE void updateGPSRescueState(void) {
 #endif // USE_GPS_RESCUE
    if ((rescueState.isFailsafe = failsafeIsActive()) &&
        FLIGHT_MODE(GPS_HOLD_MODE)
    )
        DISABLE_FLIGHT_MODE(GPS_HOLD_MODE);

    sensorUpdate();

 #ifdef USE_GPS
    if (getBatteryState() > BATTERY_WARNING)
        GPS_reset_home_position();
 #endif

DO_PHASE:
    switch (rescueState.phase) {
    case RESCUE_IDLE:
        break;
 #ifdef USE_GPS_RESCUE
DO_RESCUE_INITIALIZE:
    case RESCUE_INITIALIZE:
        rescueState.phase = RESCUE_ATTAIN_ALT;
        rescueState.intent.targetAltitude = ISRVALUE({
            register int32_t
            POS = gpsRescueConfig()->initialAltitude;

            if (POS < rescueState.sensor.currentAltitude)
                POS = rescueState.sensor.currentAltitude;

            register int32_t
            ALT = redlineConfig()->varioRedlineAltitude;

            ALT && (ALT*= 100l)<= POS ? ALT : POS;
        });
        rescueState.intent.crosstrack = false;
        FALLTHROUGH;
    case RESCUE_ATTAIN_ALT:
        if (getVoltageState() > BATTERY_WARNING)
            goto DO_RESCUE_LANDING_APPROACH;
        // Get to a safe altitude at a low velocity ASAP
        if (rescueState.sensor.groundSpeed <-600)
            break;
        rescueState.phase = RESCUE_CROSSTRACK;
        rescueState.intent.crosstrack = true;
        FALLTHROUGH;
    case RESCUE_CROSSTRACK:
        // We can assume at this point that we are at or above our RTH height, so we need to try and point to home and tilt while maintaining alt
        if (getVoltageState() < BATTERY_CRITICAL)
            break;
DO_RESCUE_LANDING_APPROACH:
        // Is our altitude way off?  We should probably kick back to phase RESCUE_ATTAIN_ALT
        rescueState.phase = RESCUE_LANDING_APPROACH;
        rescueState.intent.targetAltitude =
            MAX(lrintf(
                rescueState.sensor.currentAltitude / acc.accNormMax
            ),
                gpsRescueConfig()->initialAltitude + 500l
            );
        rescueState.intent.crosstrack = false;
        FALLTHROUGH;
    case RESCUE_LANDING_APPROACH:
        // We are getting close to home in the XY plane, get Z where it needs to be to move to landing phase
        if (rescueState.intent.targetAltitude <
            rescueState.sensor.currentAltitude
        )
            break;
        rescueState.phase = RESCUE_LANDING;
        FALLTHROUGH;
    case RESCUE_LANDING:
        // We have reached the XYZ envelope to be considered at "home".  We need to land gently and check our accelerometer for abnormal data.
        if (rescueState.sensor.currentVario <-300l)
            break;
        overallTimeUs = 0;
        FALLTHROUGH;
    case RESCUE_COMPLETE:
        // At this point, do not let the target altitude go up anymore, so if we overshoot, we dont' move in a parabolic trajectory
        rescueState.phase = RESCUE_COMPLETE;
        if (rescueState.sensor.currentVario < 0l ||
            (overallTimeUs+= currentTimeUs - previousTimeUs) > 1250000ul
        ) {
            previousTimeUs = currentTimeUs;
            break;
        }
        disarm();
        rescueStop();
        break;
    case RESCUE_ABORT:
        switch (rescueState.failure) {
            case RESCUE_FLYAWAY:
            if (rescueState.stage < RESCUE_LANDING_APPROACH)
                break;
            FALLTHROUGH;

            case RESCUE_HEALTHY:
            if ((rescueState.phase = rescueState.stage)== RESCUE_CROSSTRACK)
                rescueState.intent.crosstrack = true;
            goto DO_PHASE;

            default:
            {   register bool const
                FLAG = ARMING_FLAG(ARMED);

                if (rescueState.sensor.currentVario <-490) {
                    if (FLAG) NOOP else
                        ENABLE_ARMING_FLAG(ARMED);
                    goto DO_RESCUE_INITIALIZE;
                } else if (FLAG)
                    DISABLE_ARMING_FLAG(ARMED),
                    stopMotors();
            }
            break;
        }
        FALLTHROUGH;
    default:
        rescueState.intent.crosstrack = false;
        break;
    }

    rescueAttainPosition();

    if (debugMode== DEBUG_RTH)
  #ifdef USE_GPS
        debug[0] = rescueState.sensor.groundCourse,
  #endif
        debug[1] = lrintf(gpsRescueDeflection.y * PWM_RANGE_2),
        debug[2] = lrintf(gpsRescueDeflection.x * PWM_RANGE_2),
        debug[3] = rescueState.failure;
 #endif // USE_GPS_RESCUE
}

float climbRateController(register int64_t const error /* cm/s */) {
    return constrainf(
        ldexpf(
            (   rescueCtrl.thr+=
                    rquot(ISRVALUE({
                        register int64_t const
                        rescueCtrl_alt_D = ({
                            register int64_t const
                            POS = rescueCtrl.alt.P;

                            (   rescueCtrl.alt.P = (
 #ifdef USE_ACC
                                accelerometerConfig()->acc_hardware ?
                // Note: this assumes that vertical jerk equals +/-1700 cm/s/s/s
                                    llrint(ldexp(
                                        sq_approx(
                                            ldexpf(acc.accClimb, 3) * 25.f
                                                - 196133.f
                                        ) * 3.f / 10625.f
                                    ,
                                       -6
                                    )) + error
                                :
 #endif
                                    error
                                )
                            ) - POS;
                        });

                        rescueCtrl.alt.I = (
                            llabs(rescueCtrl.alt.P) < 2500ll ?
                                constrain(
                                    rescueCtrl.alt.P + rescueCtrl.alt.I
                                ,
                                   -25000ll
                                ,
                                    25000ll
                                )
                            :
                                0ll
                        );

                        register gpsRescueConfig_t const * restrict const
                        PTR = gpsRescueConfig();

                        PTR->velP * rescueCtrl.alt.P * 100ll +
                        PTR->velD * rescueCtrl_alt_D * 100ll +
                        PTR->velI * rescueCtrl.alt.I;
                    }),
                        signbit(attitude.ratios.w.w) ?-90000ll : 90000ll
                    )
            ) / 25.f
        ,
           -2
        ) + PWM_RANGE_MIN
    ,
        gpsRescueConfig()->throttleMin
    ,
        gpsRescueConfig()->throttleMax
    );
}
#endif
#endif // USE_ACC || USE_ALT_HOLD || USE_GPS_RESCUE

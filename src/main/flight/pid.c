/*
 * This file is part of Cleanflight and Betaflight.
 *
 * Cleanflight and Betaflight are free software. You can redistribute
 * this software and/or modify this software under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Cleanflight and Betaflight are distributed in the hope that they
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.
 *
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <math.h>

#include "platform.h"

#include "build/build_config.h"
#include "build/debug.h"

#include "common/axis.h"
#include "common/maths.h"
#include "common/filter.h"

#include "config/config_reset.h"
#include "pg/pg_ids.h"
#include "pg/rx.h"
#include "rx/rx.h"

#include "drivers/sound_beeper.h"
#include "drivers/time.h"

#include "fc/controlrate_profile.h"

#include "fc/fc_core.h"
#include "fc/fc_rc.h"

#include "fc/runtime_config.h"

#include "flight/pid.h"
#include "flight/imu.h"
#include "flight/gps_rescue.h"
#include "flight/mixer.h"
#include "flight/mixer_tricopter.h"

#include "io/gps.h"

#include "sensors/gyro.h"
#include "sensors/acceleration.h"
#include "sensors/battery.h"


FAST_RAM_ZERO_INIT timeUs_t targetPidLooptime;
FAST_RAM_ZERO_INIT float    targetPidLooptimef, dT, pidFrequency;

FAST_RAM_ZERO_INIT pidAxisData_t pidData[XYZ_AXIS_COUNT];

static FAST_RAM_ZERO_INIT float pidDenom;

static FAST_RAM_ZERO_INIT bool inCrashRecoveryMode = false;

PG_REGISTER_WITH_RESET_TEMPLATE(pidConfig_t, pidConfig, PG_PID_CONFIG, 2);

#ifndef DEFAULT_PIDS_ROLL
#define DEFAULT_PIDS_ROLL {50, 70, 28, 0}
#endif //DEFAULT_PIDS_ROLL

#ifndef DEFAULT_PIDS_PITCH
#define DEFAULT_PIDS_PITCH {58, 70, 30, 0}
#endif //DEFAULT_PIDS_PITCH

#ifndef DEFAULT_PIDS_YAW
#define DEFAULT_PIDS_YAW {60, 70, 5, 0}
#endif //DEFAULT_PIDS_YAW

#ifdef USE_RUNAWAY_TAKEOFF
PG_RESET_TEMPLATE(pidConfig_t, pidConfig,
                  .pid_process_denom = PID_PROCESS_DENOM_DEFAULT,
                  .runaway_takeoff_prevention = true,
                  .runaway_takeoff_deactivate_throttle = 20, // throttle level % needed to accumulate deactivation time
                  .runaway_takeoff_deactivate_delay = 500    // Accumulated time (in milliseconds) before deactivation in successful takeoff
                 );
#else
PG_RESET_TEMPLATE(pidConfig_t, pidConfig,
                  .pid_process_denom = PID_PROCESS_DENOM_DEFAULT);
#endif

PG_REGISTER_ARRAY_WITH_RESET_FN(pidProfile_t, PID_PROFILE_COUNT, pidProfiles, PG_PID_PROFILE, 8);

void resetPidProfile(register pidProfile_t * restrict const pidProfile) {
    RESET_CONFIG(pidProfile_t, pidProfile,
    .pid = {
        [PID_ROLL] = DEFAULT_PIDS_ROLL,
        [PID_PITCH] = DEFAULT_PIDS_PITCH,
        [PID_YAW] = DEFAULT_PIDS_YAW,
        [PID_LEVEL_LOW] = {100, 70, 10, 40},
        [PID_LEVEL_HIGH] = {35, 0, 1, 0},
        [PID_MAG] = {40, 0, 0, 0},
    },
    .dFilter = {
        // See <https://youtu.be/WiWr0gP3AT0?t=552>
#ifndef USE_GYRO_DATA_ANALYSE
        [PID_ROLL] = {90, 200},  // dtermlpf, dtermlpf2
        [PID_PITCH] = {90, 200}, // dtermlpf, dtermlpf2
        [PID_YAW] = {90, 200},   // dtermlpf, dtermlpf2
#endif
    },
    .dterm_filter_type = FILTER_PT1,
    .dterm_filter2_type = FILTER_PT1,
    .itermWindupPointPercent = 70,
    .levelAngleLimit = 45,
    .angleExpo = 10,
    .setPointPTransition[ROLL] = 110,
    .setPointPTransition[PITCH] = 110,
    .setPointPTransition[YAW] = 130,
    .setPointITransition[ROLL] = 85,
    .setPointITransition[PITCH] = 85,
    .setPointITransition[YAW] = 70,
    .setPointDTransition[ROLL] = 110,
    .setPointDTransition[PITCH] = 110,
    .setPointDTransition[YAW] = 130,
    .feathered_pids = 100,
    .i_decay = 4,
    .i_decay_cutoff = 200,                   // value of 0 mimicks old i_decay behaviour
    .errorBoost = 15,
    .errorBoostYaw = 40,
    .errorBoostLimit = 20,
    .errorBoostLimitYaw = 40,
    .dtermBoost = 0,
    .dtermBoostLimit = 0,
    .yawRateAccelLimit = 0,
    .rateAccelLimit = 0,
    .crash_time = 500,                        // ms
    .crash_delay = 0,                         // ms
    .crash_recovery_angle = 10,               // degrees
    .crash_recovery_rate = 100,               // degrees/second
    .crash_dthreshold = 50,                   // degrees/second/second
    .crash_gthreshold = 400,                  // degrees/second
    .crash_setpoint_threshold = 350,          // degrees/second
    .crash_recovery = PID_CRASH_RECOVERY_OFF, // off by default
    .crash_limit_yaw = 200,
    .itermLimit = 400,
    .throttle_boost = 50,
    .iterm_rotation = true,
    .iterm_relax_cutoff = 11,
    .iterm_relax_cutoff_yaw = 25,
    .iterm_relax_threshold = 35,
    .iterm_relax_threshold_yaw = 35,
    .motor_output_limit = 20,
    .auto_profile_cell_count = AUTO_PROFILE_CELL_COUNT_STAY,
    .axis_lock_hz = 2,
    .axis_lock_multiplier = 0,
    .directFF_yaw = 15,
#ifdef USE_SMITH_PREDICTOR
    .dterm_ABG_alpha = 0,
    .dterm_ABG_boost = 275,
    .dterm_ABG_half_life = 50,
#endif
    .emuGravityGain = 50,
    .angle_filter = 100,
                );
}

void pgResetFn_pidProfiles(register pidProfile_t pidProfiles[const]) {
    for (
        register pidProfile_t * restrict
        profile = pidProfiles + PID_PROFILE_COUNT;
        profile!= pidProfiles;
        resetPidProfile(--profile)
    ) NOOP
}


typedef union dtermLowpass_u {
    pt1Filter_t    brown;
    biquadFilter_t biquad;
    ptnFilter_t    multipass;
} dtermLowpass_t;

typedef union {
    dtermLowpass_t raw[4];

  __PACKED_STRUCT {
        dtermLowpass_t lowpass[2], notch[2];
    };
} ptermFilter_t;

static FAST_RAM_ZERO_INIT float previousSetpoint[XYZ_AXIS_COUNT];
static FAST_RAM_ZERO_INIT float previousPidSetpoint[XYZ_AXIS_COUNT];

static FAST_RAM_ZERO_INIT filterApplyFnPtr
ptermFilterApplyFn[2][XYZ_AXIS_COUNT + 1], dtermFilterApplyFn[2];

static FAST_RAM_ZERO_INIT ptermFilter_t ptermFilter[XYZ_AXIS_COUNT];
#ifdef USE_SMITH_PREDICTOR
static FAST_RAM_ZERO_INIT alphaBetaGammaFilter_t ptermABG[XYZ_AXIS_COUNT];
#endif
static FAST_RAM_ZERO_INIT pt1Filter_t angleSetpointFilter[2];
static FAST_RAM_ZERO_INIT dtermLowpass_t dtermFilter[XYZ_AXIS_COUNT][2];
#ifdef USE_SMITH_PREDICTOR
static FAST_RAM_ZERO_INIT alphaBetaGammaFilter_t dtermABG[XYZ_AXIS_COUNT];
#endif

#if defined(USE_ITERM_RELAX)
static FAST_RAM_ZERO_INIT pt1Filter_t windupLpf[XYZ_AXIS_COUNT];
static FAST_RAM_ZERO_INIT uint8_t itermRelaxCutoff;
static FAST_RAM_ZERO_INIT uint8_t itermRelaxCutoffYaw;
#endif

static FAST_RAM_ZERO_INIT pt1Filter_t emuGravityThrottleLpf[1];
static FAST_RAM_ZERO_INIT pt1Filter_t axisLockLpf[XYZ_AXIS_COUNT];

#ifdef USE_RC_SMOOTHING_FILTER
static FAST_RAM_ZERO_INIT dtermLowpass_t setpointDerivativeLPF[XYZ_AXIS_COUNT];
static FAST_RAM_ZERO_INIT bool setpointDerivativeLpfInitialized;
static FAST_RAM_ZERO_INIT uint8_t rcSmoothingDebugAxis;
static FAST_RAM_ZERO_INIT uint8_t rcSmoothingFilterType;
#endif

static FAST_CODE_NOINVOKE uint16_t calculateNyquistAdjustedNotchHz(
    register uint16_t const notchHz,
    register uint16_t const notchCutoffHz
) { return
    notchCutoffHz ? ISRVALUE({
        register uint16_t const
        notchNyquistHz = 500000ul / gyro.targetLooptime;

        notchNyquistHz < notchHz ?
            notchNyquistHz > notchCutoffHz ?
                notchNyquistHz
            :
                false
        :
            notchHz;
    }) :
        false;
}

void pidInitFilters(register pidProfile_t const pidProfile[restrict const]) {
{   struct {
        float P, D;
    } filterCutoff[2] = NOOP;

    register gyroConfig_t const * restrict const
    config = gyroConfig();

    for (register ptrdiff_t i = 2; i--;) {
        register struct {
           uint16_t center, cutoff;
        } notchHz = {
            i[config->gyro_soft_notch_hz], i[config->gyro_soft_notch_cutoff]
        };

        if ((
            i[filterCutoff].P =
                calculateNyquistAdjustedNotchHz(notchHz.center, notchHz.cutoff)
        ))
            i[filterCutoff].D =
                filterGetNotchQ(notchHz.center, notchHz.cutoff);
    }

#if defined(USE_THROTTLE_BOOST)
    throttleLpf->k = pt1FilterMaxK(targetPidLooptimef);
#endif

    for (
        register ptrdiff_t
        axis = XYZ_AXIS_COUNT;
        axis--;
#ifdef USE_ITERM_RELAX
        axis[windupLpf].k =
            pt1FilterGain(
                axis== FD_YAW ? itermRelaxCutoffYaw : itermRelaxCutoff
            ,
                targetPidLooptimef
            )
#endif
    ) {
#ifdef USE_SMITH_PREDICTOR
        if (pidProfile->dterm_ABG_alpha)
            ABGInit(
                axis + dtermABG
            ,
                pidProfile->dterm_ABG_alpha
            ,
                pidProfile->dterm_ABG_boost
            ,
                pidProfile->dterm_ABG_half_life
            ,
                dT
            );

        if (config->gyro_ABG_alpha)
            ABGInit(
                axis + ptermABG
            ,
                config->gyro_ABG_alpha
            ,
                config->gyro_ABG_boost
            ,
                config->gyro_ABG_half_life
            ,
                dT
            );
#endif

        void * initFn0(
            register float const cutoff,
            register void        * restrict const filter
        ) {
            pt1FilterInit(
                filter, pt1FilterGain(cutoff, targetPidLooptimef)
            );

            return pt1FilterApply;
        }

        void * initFn1(
            register float const cutoff,
            register void        * restrict const filter
        ) {
            biquadFilterInitLPF(filter, cutoff, targetPidLooptimef);

            return biquadFilterApply;
        }

        void * initFnN(
            register float const cutoff,
            register void        * restrict const filter,
            register char  const order
        ) {
            ptnFilterInit(filter, order, cutoff, targetPidLooptimef);

            return ptnFilterApply;
        }

        void * initFn2(
            register float const cutoff,
            register void        * restrict const filter
        ) {
            return initFnN(cutoff, filter, 2);
        }

        void * initFn3(
            register float const cutoff,
            register void        * restrict const filter
        ) {
            return initFnN(cutoff, filter, 3);
        }

        void * initFn4(
            register float const cutoff,
            register void        * restrict const filter
        ) {
            return initFnN(cutoff, filter, 4);
        }

        typeof(initFn0) * initFn(register lowpassFilterType_e const type) {
            switch (type) {
                case FILTER_BIQUAD:
                return initFn1;

                case FILTER_PT2:
                return initFn2;

                case FILTER_PT3:
                return initFn3;

                case FILTER_PT4:
                return initFn4;

                default:
                return initFn0;
            }
        }

        for (register ptrdiff_t i = 2; i--;) {
            if (filterCutoff[i].P)
                ptermFilterApplyFn[i][XYZ_AXIS_COUNT] =
                    (void *)biquadFilterApply,
                biquadFilterInit((void *)(
                    ptermFilter[axis].notch + i
                ),
                    filterCutoff[i].P
                ,
                    targetPidLooptimef
                ,
                    filterCutoff[i].D
                ,
                    FILTER_NOTCH
                );

            if (config->gyro_lowpassn_hz[i][axis])
                ptermFilterApplyFn[i][axis] =
                    initFn(config->gyro_lowpassn_type[i])(
                        config->gyro_lowpassn_hz[i][axis]
                    ,
                        ptermFilter[axis].lowpass + i
                    );

            if (pidProfile->dFilter[axis].dLpfs[i] &&
                pidProfile->dFilter[axis].dLpfs[i]<< true < pidFrequency
            )
                dtermFilterApplyFn[i] = initFn(pidProfile->dterm_filter_type)(
                    pidProfile->dFilter[axis].dLpfs[i], dtermFilter[axis] + i
                );
        }
    }
}
    for (
        register ptrdiff_t
        axis = XYZ_AXIS_COUNT;
        axis--;
        pt1FilterInit(
            axis + angleSetpointFilter
        ,
            pt1FilterGain(pidProfile->angle_filter, targetPidLooptimef)
        )
    ) NOOP

    pt1FilterInit(
        emuGravityThrottleLpf
    ,
        pt1FilterGain(EMU_GRAVITY_THROTTLE_FILTER_CUTOFF, targetPidLooptimef)
    );
}



#ifdef USE_RC_SMOOTHING_FILTER
void pidInitSetpointDerivativeLpf(
    register uint16_t const filterCutoff,
    register uint8_t  const debugAxis,
    register uint8_t  const filterType
) {
    if ((   rcSmoothingDebugAxis  = debugAxis,
            rcSmoothingFilterType = filterType
        ) && filterCutoff
    )
        for (
            register ptrdiff_t
            axis = (
                setpointDerivativeLpfInitialized = true,
                XYZ_AXIS_COUNT
            );
            axis--;
        )
            if (rcSmoothingFilterType== RC_SMOOTHING_DERIVATIVE_PT1)
                pt1FilterInit((void *)(
                    axis + setpointDerivativeLPF
                ),
                    pt1FilterGain(filterCutoff, targetPidLooptimef)
                );
            else
                biquadFilterInitLPF((void *)(
                    axis + setpointDerivativeLPF
                ),
                    filterCutoff
                ,
                    targetPidLooptimef
                );
}

void pidUpdateSetpointDerivativeLpf(register uint16_t const filterCutoff) {
    if (filterCutoff && rcSmoothingFilterType)
        for (register ptrdiff_t axis = XYZ_AXIS_COUNT; axis--;)
            if (rcSmoothingFilterType== RC_SMOOTHING_DERIVATIVE_PT1)
                pt1FilterUpdateCutoff((void *)(
                    axis + setpointDerivativeLPF
                ),
                    pt1FilterGain(filterCutoff, targetPidLooptimef)
                );
            else
                biquadFilterUpdateLPF((void *)(
                    axis + setpointDerivativeLPF
                ),
                    filterCutoff
                ,
                    targetPidLooptimef
                );
}
#endif

typedef struct pidCoefficient_s {
    float Kp;
    float Ki;
    float Kd;
    float Kf;
} pidCoefficient_t;

static FAST_RAM_ZERO_INIT pidCoefficient_t pidCoefficient[XYZ_AXIS_COUNT];
static FAST_RAM_ZERO_INIT float directFF[3];
static FAST_RAM_ZERO_INIT float maxVelocity[XYZ_AXIS_COUNT];
static FAST_RAM_ZERO_INIT float feathered_pids;
static FAST_RAM_ZERO_INIT float setPointTransition[XYZ_AXIS_COUNT][3];
static FAST_RAM_ZERO_INIT float dtermBoostMultiplier, dtermBoostLimitPercent;
static FAST_RAM_ZERO_INIT float P_angle_low, D_angle_low, P_angle_high, D_angle_high, F_angle, DF_angle_low, DF_angle_high;
static FAST_RAM_ZERO_INIT float ITermWindupPointInv;
static FAST_RAM_ZERO_INIT timeUs_t crashDetectedAtUs;
static FAST_RAM_ZERO_INIT timeDelta_t crashTimeLimitUs;
static FAST_RAM_ZERO_INIT timeDelta_t crashTimeDelayUs;
static FAST_RAM_ZERO_INIT int32_t crashRecoveryAngleDeciDegrees;
static FAST_RAM_ZERO_INIT float crashRecoveryRate;
static FAST_RAM_ZERO_INIT float crashDtermThreshold;
static FAST_RAM_ZERO_INIT float crashGyroThreshold;
static FAST_RAM_ZERO_INIT float crashSetpointThreshold;
static FAST_RAM_ZERO_INIT float crashLimitYaw;
static FAST_RAM_ZERO_INIT float itermLimit;
static FAST_RAM_ZERO_INIT float axisLockMultiplier;
#if defined(USE_THROTTLE_BOOST)
       FAST_RAM_ZERO_INIT pt1Filter_t throttleLpf[1];
       FAST_RAM_ZERO_INIT float       throttleBoost;
#endif
static FAST_RAM_ZERO_INIT bool itermRotation;
static FAST_RAM_ZERO_INIT float temporaryIterm[XYZ_AXIS_COUNT];
static FAST_RAM_ZERO_INIT float previousEuler[2], previousAngle[2]; // ddeg
static FAST_RAM_ZERO_INIT float emuGravityThrottleHpf;

void pidUpdateEmuGravityThrottleFilter(register float const throttle) {
    emuGravityThrottleHpf = fabsf(
        pt1FilterApply(emuGravityThrottleLpf, throttle) - throttle
    );
}

void pidInitConfig(register pidProfile_t const pidProfile[restrict const]) {
    for (register ptrdiff_t axis = XYZ_AXIS_COUNT; axis--;) {
        for (
            register ptrdiff_t
            i = 2;
            i[setPointTransition[axis]] = ldexpf(
                (i[pidProfile->setPointTransition][axis] - 100l) / 25.f,-2
            ),
            i--;
        ) NOOP
        pidCoefficient[axis].Kp = PTERM_SCALE * pidProfile->pid[axis].P;
        pidCoefficient[axis].Ki = ITERM_SCALE * pidProfile->pid[axis].I;
        pidCoefficient[axis].Kd = DTERM_SCALE * pidProfile->pid[axis].D;
    }
    directFF[0] = DIRECT_FF_SCALE * pidProfile->directFF_yaw;
    DF_angle_low = DIRECT_FF_SCALE * pidProfile->pid[PID_LEVEL_LOW].I;
    DF_angle_high = DIRECT_FF_SCALE * pidProfile->pid[PID_LEVEL_HIGH].I;
    feathered_pids = pidProfile->feathered_pids;
    dtermBoostMultiplier = (pidProfile->dtermBoost * pidProfile->dtermBoost / 1000000) * 0.003;
    dtermBoostLimitPercent = pidProfile->dtermBoostLimit / 100.0f;
    P_angle_low = pidProfile->pid[PID_LEVEL_LOW].P * 0.1f;
    D_angle_low = pidProfile->pid[PID_LEVEL_LOW].D * 0.00002428571f;
    P_angle_high = pidProfile->pid[PID_LEVEL_HIGH].P * 0.1f;
    D_angle_high = pidProfile->pid[PID_LEVEL_HIGH].D * 0.00002428571f;
    F_angle = pidProfile->pid[PID_LEVEL_LOW].F * 0.00000125f;
    maxVelocity[FD_ROLL] = maxVelocity[FD_PITCH] = pidProfile->rateAccelLimit * 100 * dT;
    maxVelocity[FD_YAW] = pidProfile->yawRateAccelLimit * 100 * dT;
    ITermWindupPointInv = 0.0f;
    if (pidProfile->itermWindupPointPercent != 0) {
        const float itermWindupPoint = pidProfile->itermWindupPointPercent / 100.0f;
        ITermWindupPointInv = 1.0f / itermWindupPoint;
    }
    crashTimeLimitUs = pidProfile->crash_time * 1000;
    crashTimeDelayUs = pidProfile->crash_delay * 1000;
    crashRecoveryAngleDeciDegrees = pidProfile->crash_recovery_angle * 10;
    crashRecoveryRate = pidProfile->crash_recovery_rate;
    crashGyroThreshold = pidProfile->crash_gthreshold;
    crashDtermThreshold = pidProfile->crash_dthreshold;
    crashSetpointThreshold = pidProfile->crash_setpoint_threshold;
    crashLimitYaw = pidProfile->crash_limit_yaw;
    itermLimit = pidProfile->itermLimit;
#if defined(USE_THROTTLE_BOOST)
    throttleBoost = pidProfile->throttle_boost * DIRECT_FF_SCALE;
#endif
    itermRotation = pidProfile->iterm_rotation;
#if defined(USE_ITERM_RELAX)
    itermRelaxCutoff = pidProfile->iterm_relax_cutoff;
    itermRelaxCutoffYaw = pidProfile->iterm_relax_cutoff_yaw;
#endif
    axisLockMultiplier = pidProfile->axis_lock_multiplier / 100.0f;
    mixerInitProfile();
}

void pidInit(register pidProfile_t const * restrict const pidProfile) {
    dT = ldexpf(
        (   targetPidLooptimef =
            targetPidLooptime  =
                ISRVALUE({
                    register uint8_t const
                    denom = pidConfig()->pid_process_denom;

                    pidDenom = denom;

                    denom;
                }) * gyro.targetLooptime
        ) / 15625.f
    ,
       -6
    );
    pidFrequency = 1e6l / targetPidLooptime;

    pidInitFilters(pidProfile);
    pidInitConfig(pidProfile);
}

void pidCopyProfile(uint8_t dstPidProfileIndex, uint8_t srcPidProfileIndex) {
    if ((dstPidProfileIndex < PID_PROFILE_COUNT - 1 && srcPidProfileIndex < PID_PROFILE_COUNT - 1) && dstPidProfileIndex != srcPidProfileIndex) {
        memcpy(pidProfilesMutable(dstPidProfileIndex), pidProfilesMutable(srcPidProfileIndex), sizeof(pidProfile_t));
    }
}

#define pidLevel(axis, pidProfile, angleTrim)\
    ldexpf(\
        ISRVALUE({\
            register float\
            TMP0 =\
                constrainf(\
                    ldexpf(\
                        constrainf(\
                            axis[gpsRescueAngle]\
                        ,\
                           -pidProfile->levelAngleLimit\
                        ,\
                            pidProfile->levelAngleLimit\
                        )\
                    ,\
                        true\
                    ) * 5.f + axis[angleTrim] - axis[attitude.raw]\
                ,\
                   -9e2\
                ,\
                    9e2\
                );\
\
            ({  register float\
                TMP1 = fabsf(TMP0);\
\
                axis[directFF] =\
                    ldexpf(\
                        ({  register float const\
                            TMP2 = TMP1 * DF_angle_high;\
\
                            (TMP1 = fdimf(9e2, TMP1)) * DF_angle_low + TMP2;\
                        }) / 225.f\
                    ,\
                       -2\
                    );\
\
                TMP0 =\
                    ({  register float\
                        TMP2 =\
                            (   TMP2 =\
                                axis[previousEuler],\
                                axis[previousEuler] = axis[attitude.raw]\
                            ) - TMP2;\
\
                        (D_angle_low * TMP1 + D_angle_high * TMP0) * TMP2;\
                    })+ (P_angle_low * TMP1 + P_angle_high * TMP0) * TMP0;\
                TMP0+=\
                    (   TMP1 =\
                            (   TMP1 =\
                                axis[previousAngle],\
                                axis[previousAngle] = axis[gpsRescueAngle]\
                            ) - TMP1\
                    ) * pidFrequency * F_angle;\
\
                fabsf(TMP1) > 18e2f ? TMP1 - copysignf(36e2f, TMP1) : TMP1;\
            }) + TMP0;\
        }) / 225.f\
    ,\
       -2\
    )

static FAST_CODE_NOINLINE float handleCrashRecovery(
    register pidCrashRecovery_e const crash_recovery,
    register int16_t            const angleTrim         [restrict const],
    register ptrdiff_t          const axis,
    register timeUs_t           const currentTimeUs,
    register float                    currentPidSetpoint[restrict const],
    register float                    gyroRate
) {
    if (inCrashRecoveryMode && cmpTimeUs(currentTimeUs, crashDetectedAtUs) > crashTimeDelayUs) {
        if (crash_recovery == PID_CRASH_RECOVERY_BEEP) {
            BEEP_ON;
        }
        if (axis== FD_YAW)
            gyroRate = constrainf(gyroRate,-crashLimitYaw, crashLimitYaw);
        else if (accelerometerConfig()->acc_hardware)
            // on roll and pitch axes calculate currentPidSetpoint and errorRate to level the aircraft to recover from crash
            gyroRate =
                // errorAngle is deviation from horizontal
                (   currentPidSetpoint[0] =
                        ldexpf(
                            (axis[angleTrim] - axis[attitude.raw]) / 5.f,-1
                        ) * P_angle_low
                ) - gyroRate;
        // reset ITerm, since accumulated error before crash is now meaningless
        // and ITerm windup during crash recovery can be extreme, especially on yaw axis
        temporaryIterm[axis] = 0.0f;
        if (
            cmpTimeUs(currentTimeUs, crashDetectedAtUs) > crashTimeLimitUs ||
            (getControllerMixRange() < 1.0f &&
             ABS(gyro.gyroADCf[FD_ROLL]) < crashRecoveryRate &&
             ABS(gyro.gyroADCf[FD_PITCH]) < crashRecoveryRate &&
             ABS(gyro.gyroADCf[FD_YAW]) < crashRecoveryRate
            )
        ) {
            if (sensors(SENSOR_ACC)) {
                // check aircraft nearly level
                if (ABS(FD_ROLL [angleTrim] - FD_ROLL [attitude.raw])
                        < crashRecoveryAngleDeciDegrees &&
                    ABS(FD_PITCH[angleTrim] - FD_PITCH[attitude.raw])
                        < crashRecoveryAngleDeciDegrees
                ) {
                    inCrashRecoveryMode = false;
                    BEEP_OFF;
                }
            } else {
                inCrashRecoveryMode = false;
                BEEP_OFF;
            }
        }
    }

    return gyroRate;
}

static void detectAndSetCrashRecovery(
    const pidCrashRecovery_e crash_recovery, const int axis,
    const timeUs_t currentTimeUs, const float delta, const float errorRate) {
    // if crash recovery is on and accelerometer enabled and there is no gyro overflow, then check for a crash
    // no point in trying to recover if the crash is so severe that the gyro overflows
    if ((crash_recovery || FLIGHT_MODE(GPS_RESCUE_MODE)) && !gyroOverflowDetected()) {
        if (ARMING_FLAG(ARMED)) {
            if (getControllerMixRange() >= 1.0f && !inCrashRecoveryMode && ABS(delta) > crashDtermThreshold && ABS(errorRate) > crashGyroThreshold && ABS(getSetpointRate(axis)) < crashSetpointThreshold) {
                inCrashRecoveryMode = true;
                crashDetectedAtUs = currentTimeUs;
            }
            if (inCrashRecoveryMode && cmpTimeUs(currentTimeUs, crashDetectedAtUs) < crashTimeDelayUs && (ABS(errorRate) < crashGyroThreshold || ABS(getSetpointRate(axis)) > crashSetpointThreshold)) {
                inCrashRecoveryMode = false;
                BEEP_OFF;
            }
        } else if (inCrashRecoveryMode) {
            inCrashRecoveryMode = false;
            BEEP_OFF;
        }
    }
}

static void rotateVector(
    register float       v       [restrict const],
    register float const rotation[restrict const]
) {
    // rotate v around rotation vector rotation
    // rotation in radians, all elements must be small
    for (int i = 0; i < XYZ_AXIS_COUNT; i++) {
        int i_1 = (i + 1) % 3;
        int i_2 = (i + 2) % 3;
        float newV = v[i_1] + v[i_2] * rotation[i];
        v[i_2] -= v[i_1] * rotation[i];
        v[i_1] = newV;
    }
}

static void rotateITermAndAxisError() {
    if (itermRotation) {
        fpVector3_t
        v = 0[(fpVector3_t const *)temporaryIterm];
        rotateVector(
            v.raw
        ,
            ISRVALUE({
                fpVector3_t rotationRads;

                for (
                    register struct {
                        float     const gyroToAngle;
                        ptrdiff_t       axis;
                    } lcv = { dT * RAD, XYZ_AXIS_COUNT };
                    lcv.axis--;
                    lcv.axis[rotationRads.raw] =
                        lcv.axis[gyro.gyroADCf] * lcv.gyroToAngle
                ) NOOP

                rotationRads;
            }).raw
        );
        memcpy(temporaryIterm, v.raw, sizeof temporaryIterm);
    }
}

static FAST_RAM_ZERO_INIT float scaledAxisPid[XYZ_AXIS_COUNT];
static FAST_RAM_ZERO_INIT float stickMovement[XYZ_AXIS_COUNT];
static FAST_RAM_ZERO_INIT float lastRcDeflectionAbs[XYZ_AXIS_COUNT];
static FAST_RAM_ZERO_INIT float previousError[XYZ_AXIS_COUNT];
static FAST_RAM_ZERO_INIT float previousMeasurement[XYZ_AXIS_COUNT];
static FAST_RAM_ZERO_INIT timeUs_t crashDetectedAtUs;

FAST_CODE FAST_CODE_NOINLINE void pidController(
    register pidProfile_t const pidProfile[restrict const],
    register int16_t      const angleTrim [restrict const],
    register timeUs_t     const currentTimeUs
) {
    float axisLock[XYZ_AXIS_COUNT];

    for (
        register ptrdiff_t
        axis = XYZ_AXIS_COUNT - 1;
        axis[axisLock] =
            pt1FilterApply(axis + axisLockLpf, axis[stickMovement])
                * axisLockMultiplier,
        axis--;
    ) NOOP

    for (
        register ptrdiff_t
        axis = XYZ_AXIS_COUNT - 1;
        axis[scaledAxisPid] =
            fminf(
                fdimf(
                    axis[axisLock] + true
                ,
                    ((axis + 1) % XYZ_AXIS_COUNT)[axisLock] +
                    ((axis + 2) % XYZ_AXIS_COUNT)[axisLock]
                )
            ,
                true
            ),
        axis--;
    ) NOOP

    // gradually scale back integration when above windup point
    register float const
    dynCi = (
        ITermWindupPointInv ?
            fminf(
                fdimf(true, getControllerMixRange()) * ITermWindupPointInv * dT
            ,
                dT
            )
        :
            dT
    );

#ifdef USE_GYRO_DATA_ANALYSE
    register bool const
    isPidSumFilterActive = isDynamicFilterActive();
#endif

    // ----------PID controller----------
    for (register ptrdiff_t axis = XYZ_AXIS_COUNT; axis--;) {
        axis[stickMovement] =
            ISRVALUE({
                register float const
                deflection = axis[lastRcDeflectionAbs];

                (   axis[lastRcDeflectionAbs] =
                        REINTERPRET_CAST(float, axis[setptRate].curr) /
                        REINTERPRET_CAST(
                            float
                        ,
                            axis[setptRate].curr < false ?
                                axis[setptRate].min : axis[setptRate].max
                        )
                ) - deflection;
            }) * pidFrequency;

        // Yaw control is GYRO based, direct sticks control is applied to rate PID
        // NFE racermode applies angle only to the roll axis
        // disable directFF for pitch and roll while not in angle
        // Handle yaw spin recovery - zero the setpoint on yaw to aid in recovery
        // It's not necessary to zero the set points for R/P because the PIDs will be zeroed below

        float currentPidSetpoint = getSetpointRate(axis);

        register float
#ifndef USE_RC_SMOOTHING_FILTER
        directFeedForward = getSetpointRateRate(axis) * dT;
#else
        directFeedForward = currentPidSetpoint - getSetpointRatePrev(axis);

        if (setpointDerivativeLpfInitialized && rcSmoothingFilterType)
            directFeedForward =
                (   rcSmoothingFilterType== RC_SMOOTHING_DERIVATIVE_PT1 ?
                        (filterApplyFnPtr)pt1FilterApply
                    :
                        (filterApplyFnPtr)biquadFilterApplyDF1
                )(axis + setpointDerivativeLPF, directFeedForward);
#endif

        if (axis[maxVelocity])
            axis[previousSetpoint] = (
                fabsf(directFeedForward) > axis[maxVelocity] ?
                    currentPidSetpoint =
                        (   directFeedForward =
                                copysignf(axis[maxVelocity], directFeedForward)
                        ) + axis[previousSetpoint]
                :
                    currentPidSetpoint
            );

        register float
        gyroRate = axis[gyro.gyroADCf] + directFeedForward;

#ifdef USE_SMITH_PREDICTOR
        if (axis[ptermABG].a)
            gyroRate = alphaBetaGammaApply(axis + ptermABG, gyroRate);
#endif

        for (register ptrdiff_t i = 4; i--;) {
            register filterApplyFnPtr const
            applyFn =
                ptermFilterApplyFn
                    [i & true][i>> true ? XYZ_AXIS_COUNT : axis];

            if (applyFn)
                gyroRate = applyFn(axis[ptermFilter].raw + i, gyroRate);
        }

        if (debugMode== DEBUG_GYRO_FILTERED)
            axis[debug] = lrintf(gyroRate);

        // -----calculate error rate
        register float
        errorRate =
            (   axis[previousPidSetpoint] = (
                    axis== FD_YAW ?
#ifdef USE_YAW_SPIN_RECOVERY
                        gyroYawSpinDetected() ?
                            currentPidSetpoint = false,
                            false
                        :
#endif
                        currentPidSetpoint
                    : (   (   axis[directFF] = false,
                              axis
                          ) ? ACC_MODE ^ NFE_RACE_MODE : ACC_MODE
                      ) & flightModeFlags && (
                          currentPidSetpoint+=
                              pidLevel(axis, pidProfile, angleTrim),
                          angleSetpointFilter
                      ) ?
                        currentPidSetpoint = pt1FilterApply(
                            angleSetpointFilter, currentPidSetpoint
                        )
                    :
                        currentPidSetpoint
                )
            ) - gyroRate;

        if (axis!= FD_YAW && pidProfile->emuGravityGain)
            errorRate = ldexpf(
                (pidProfile->emuGravityGain * emuGravityThrottleHpf + 1e1f)
                    * errorRate
                    / 5.f
            ,
               -true
            );

        // EmuFlight pid controller, which will be maintained in the future with additional features specialised for current (mini) multirotor usage.
        // Based on 2DOF reference design (matlab)
        register float errorBoostAxis, errorLimitAxis;
        if (axis!= FD_YAW) {
            errorBoostAxis = pidProfile->errorBoost;
            errorLimitAxis = pidProfile->errorBoostLimit;
        } else {
            errorBoostAxis = pidProfile->errorBoostYaw;
            errorLimitAxis = pidProfile->errorBoostLimitYaw;
        }
        errorLimitAxis = errorLimitAxis / 100;
        float errorMultiplier = (errorBoostAxis * errorBoostAxis / 1000000) * 0.003;
        float boostedErrorRate;
        boostedErrorRate = (errorRate * fabsf(errorRate)) * errorMultiplier;
        if (fabsf(errorRate * errorLimitAxis) < fabsf(boostedErrorRate)) {
            boostedErrorRate = errorRate * errorLimitAxis;
        }
        rotateITermAndAxisError();
        // --------low-level gyro-based PID based on 2DOF PID controller. ----------
        // 2-DOF PID controller with optional filter on derivative term.
        // derivative term can be based on measurement or error using a sliding value from 0-100
        float itermErrorRate = boostedErrorRate + errorRate;
        float iterm          = temporaryIterm[axis];
#if defined(USE_ITERM_RELAX)
        register float setpointLpf, setpointHpf, itermRelaxFactor;
        if (axis== FD_YAW ? itermRelaxCutoffYaw : itermRelaxCutoff)
            if (itermRelaxFactor =
                    fdimf(
                        true
                    ,
                        (   setpointHpf = fabsf(
                                (   setpointLpf =
                                        pt1FilterApply(
                                            axis + windupLpf, currentPidSetpoint
                                        )
                                ) - currentPidSetpoint
                            )
                        ) / (
                            axis== FD_YAW ?
                                pidProfile->iterm_relax_threshold_yaw
                            :
                                pidProfile->iterm_relax_threshold
                        )
                    ),
                signbit(iterm * itermErrorRate)
            )
                NOOP
            else
                itermErrorRate *= itermRelaxFactor;
        else
            itermRelaxFactor = setpointHpf = setpointLpf = false;
#endif // USE_ITERM_RELAX

        // -----calculate P component
        axis[pidData].P =
            (boostedErrorRate + errorRate) * axis[pidCoefficient].Kp;

        // -----calculate I component
        if (isItermActivated() &&
            fabsf(
                iterm = constrainf(ISRVALUE({
                    register float const
                    ITermNew = axis[pidCoefficient].Ki * dynCi * itermErrorRate;

                    ITermNew ?
                        ({  register float
                            newVal = ITermNew * iterm;

                            signbit(newVal) &&
                            fabsf(
                                newVal =
                                    (pidProfile->i_decay - true)
                                        * newVal
                                        / pidProfile->i_decay_cutoff
                                    + ITermNew
                            ) < fabsf(iterm) ? newVal : ITermNew;
                        }) + iterm
                    :
                        iterm;
                }),
                   -itermLimit
                ,
                    itermLimit
                )
            )<= fabsf(axis[temporaryIterm]) || (
                axis== FD_YAW && mixerIsTricopter() ?
                   !mixerTricopterIsServoSaturated(errorRate)
                :
                    getControllerMixRange() < true
            )
        )
            // Only increase ITerm if output is not saturated
            axis[temporaryIterm] = iterm;

        // -----calculate D component
        register float
        dDeltaMultiplier = axis[gyro.gyroAccf] * dT;

        axis[pidData].D =
        axis[pidCoefficient].Kd ? ISRVALUE({
            //filter Kd properly, no setpoint filtering
            register float
            dDelta = axis[previousError];
            dDeltaMultiplier =
                ((dDelta-= axis[previousError] = errorRate) + dDeltaMultiplier)
                    * feathered_pids
                    /-25.f;
            dDelta+= ldexpf(ldexpf(directFeedForward, 3) + dDeltaMultiplier,-2);

            //filter the dterm
#ifdef USE_SMITH_PREDICTOR
            if (axis[dtermABG].a)
                dDelta = alphaBetaGammaApply(axis + dtermABG, dDelta);
#endif

        {   register ptrdiff_t
            i = true;

            do {
                register filterApplyFnPtr const
                applyFn = i[dtermFilterApplyFn];

                if (applyFn)
                    dDelta = applyFn(axis[dtermFilter] + i, dDelta);
            } while (i--);
        }
            // Divide rate change by dT to get differential (ie dr/dt).
            // dT is fixed and calculated from the target PID loop time
            // This is done to avoid DTerm spikes that occur with dynamically
            // calculated deltaT whenever another task causes the PID
            // loop execution to be delayed.
            dDelta*=
                (   fminf(fabsf(
                         dtermBoostMultiplier * dDelta
                    ),
                         dtermBoostLimitPercent
                    ) + true
                ) * axis[pidCoefficient].Kd;
        }) :
            false;

        switch (debugMode) {
#ifdef USE_ITERM_RELAX
            case DEBUG_ITERM_RELAX:
            if (axis) NOOP else
                debug[0] = lrintf(setpointHpf),
                debug[1] = lrintf(ldexpf(itermRelaxFactor, 2) * 25.f),
                debug[2] = lrintf(itermErrorRate);
            break;
#endif

            case DEBUG_ANGLERATE:
            debug[axis] = lrintf(currentPidSetpoint);
            break;

            case DEBUG_SMART_SMOOTHING:
            debug[axis] = lrintf(ldexpf(dDeltaMultiplier, 3) * 125.f);
            //debug[axis] = lrintf(ldexpf(pidData[axis].D, 6) * 15625.f);
            FALLTHROUGH;

            default:
            break;
        }

        detectAndSetCrashRecovery(
            pidProfile->crash_recovery
        ,
            axis
        ,
            currentTimeUs
        ,
            axis[pidData].D
        ,
            handleCrashRecovery(
                pidProfile->crash_recovery
            ,
                angleTrim
            ,
                axis
            ,
                currentTimeUs
            ,
               &currentPidSetpoint
            ,
                errorRate
            )
        );

#ifdef USE_YAW_SPIN_RECOVERY
        if (gyroYawSpinDetected()) {
            temporaryIterm[axis] = 0; // in yaw spin always disable I
            if (axis!= FD_YAW) {
                // zero PIDs on pitch and roll leaving yaw P to correct spin
                pidData[axis].P = 0;
                pidData[axis].D = 0;
            }
        }
#endif // USE_YAW_SPIN_RECOVERY

        axis[pidData].I   = axis[temporaryIterm];
        axis[pidData].Sum = axis[directFF] * directFeedForward;

        // applying SetPointAttenuation
        // SPA boost if SPA > 100 SPA cut if SPA < 100
        for (
            register ptrdiff_t
            i = 2;
            axis[pidData].Sum   +=
            axis[pidData].raw[i]+=
                axis[lastRcDeflectionAbs]
                    * axis[setPointTransition][i]
                    * axis[pidData].raw[i],
            i--;
        ) NOOP

        axis[pidData].Sum*= axis[scaledAxisPid];

#ifndef USE_GYRO_DATA_ANALYSE
        if (debugMode== DEBUG_PIDSUMS)
#else
        switch (debugMode) {
            case DEBUG_PIDSUMS:
#endif
            axis[debug] = lrintf(axis[pidData].Sum);
#ifdef USE_GYRO_DATA_ANALYSE
            break;

            case DEBUG_FFT_FREQ:
            if (axis)
                break;
            FALLTHROUGH;

            case DEBUG_FFT:
            (axis<< true)[debug] = lrintf(axis[pidData].Sum);
            FALLTHROUGH;

            default:
            break;
        }
#endif
    }
}

bool crashRecoveryModeActive(void) {
    return inCrashRecoveryMode;
}

float pidGetPreviousSetpoint(int axis) {
    return previousPidSetpoint[axis];
}

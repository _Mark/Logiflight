/*
 * This file is part of Cleanflight and Betaflight.
 *
 * Cleanflight and Betaflight are free software. You can redistribute
 * this software and/or modify this software under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Cleanflight and Betaflight are distributed in the hope that they
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.
 *
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include "platform.h"

#include "build/debug.h"

#include "common/utils.h"

#include "drivers/stack_check.h"

#define STACK_FILL_CHAR 0xa5

extern char _estack; // end of stack, declared in .LD file
extern char _Min_Stack_Size; // declared in .LD file

/*
 * The ARM processor uses a full descending stack. This means the stack pointer holds the address
 * of the last stacked item in memory. When the processor pushes a new item onto the stack,
 * it decrements the stack pointer and then writes the item to the new memory location.
 *
 *
 * RAM layout is generally as below, although some targets vary
 *
 * F1 Boards
 * RAM is origin 0x20000000 length 20K that is:
 * 0x20000000 to 0x20005000
 *
 * F3 Boards
 * RAM is origin 0x20000000 length 40K that is:
 * 0x20000000 to 0x2000a000
 *
 * F4 Boards
 * RAM is origin 0x20000000 length 128K that is:
 * 0x20000000 to 0x20020000
 *
 */

#ifdef STACK_CHECK

static uint32_t usedStackSize;

void taskStackCheck(
    register timeUs_t __attribute__((unused)) const currentTimeUs
) {
    register char
    * const stackHighMem =&_estack,
    * const stackLowMem  = stackHighMem - (size_t)&_Min_Stack_Size;
    register char const
    * const stackCurrent = (void const *)&stackLowMem;

    register char
    * p = stackLowMem;

    while (p < stackCurrent && (++p)[0]!= STACK_FILL_CHAR)
        NOOP

    usedStackSize = (uint32_t)stackHighMem - (uint32_t)p;

    if (debugMode== DEBUG_STACK)
        debug[0] = stackHighMem & u'\x7FFF',
        debug[1] = stackLowMem  & u'\x7FFF',
        debug[2] = stackCurrent & u'\x7FFF',
        debug[3] = p            & u'\x7FFF';
}

uint32_t stackUsedSize(void) {
    return usedStackSize;
}
#endif

uint32_t stackTotalSize(void) {
    return (uint32_t)(intptr_t)&_Min_Stack_Size;
}

uint32_t stackHighMem(void) {
    return (uint32_t)(intptr_t)&_estack;
}

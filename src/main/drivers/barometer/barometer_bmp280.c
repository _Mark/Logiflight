/*
 * This file is part of Cleanflight and Betaflight.
 *
 * Cleanflight and Betaflight are free software. You can redistribute
 * this software and/or modify this software under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Cleanflight and Betaflight are distributed in the hope that they
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.
 *
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stdint.h>
#include <math.h>

#include "platform.h"

#include "build/build_config.h"
#include "build/debug.h"

#include "barometer.h"

#include "drivers/bus.h"
#include "drivers/bus_i2c.h"
#include "drivers/bus_i2c_busdev.h"
#include "drivers/bus_spi.h"
#include "drivers/io.h"
#include "drivers/time.h"

#include "barometer_bmp280.h"

#if defined(USE_BARO) && (defined(USE_BARO_BMP280) || defined(USE_BARO_SPI_BMP280))

static FAST_RAM_ZERO_INIT union {
    double dig[0xC];

  __PACKED_STRUCT {
        double
        dig_T1, dig_T2, dig_T3,
        dig_P1, dig_P2, dig_P3, dig_P4, dig_P5, dig_P6, dig_P7, dig_P8, dig_P9;
    };
} bmp280_cal;

// uncompensated pressure and temperature
static FAST_RAM_ZERO_INIT double _Complex bmp280_u;
#define bmp280_up __real__(bmp280_u)
#define bmp280_ut __imag__(bmp280_u)

static void bmp280_get_up(baroDev_t *baro);

static void bmp280_calculate(uint32_t *, int32_t *);

#ifdef USE_BARO_SPI_BMP280
static void bmp280BusInit(register busDevice_t * restrict const busdev) {
        IOHi(busdev->busdev_u.spi.csnPin); // Disable
        IOInit(busdev->busdev_u.spi.csnPin, OWNER_BARO_CS, 0);
        IOConfigGPIO(busdev->busdev_u.spi.csnPin, IOCFG_OUT_PP);
        spiSetDivisor(busdev->busdev_u.spi.instance, SPI_CLOCK_STANDARD); // XXX
}

static void bmp280BusDeinit(register busDevice_t * restrict const busdev) {
    if (busdev->bustype == BUSTYPE_SPI) {
        spiPreinitCsByIO(busdev->busdev_u.spi.csnPin);
    }
}
#endif

bool bmp280Detect(register baroDev_t * restrict const baro) {
    delay(20);
{   register
    bool defaultAddressApplied = false;
    #define busdev ((busDevice_t *)baro)
#ifndef USE_BARO_SPI_BMP280
    if ((busdev->bustype == BUSTYPE_I2C) && (busdev->busdev_u.i2c.address == 0)) {
#else
    switch (busdev->bustype) {
        case BUSTYPE_SPI:
        bmp280BusInit(busdev);
        break;

        case BUSTYPE_I2C:
#endif
        // Default address for BMP280
        busdev->busdev_u.i2c.address = BMP280_I2C_ADDR;
        defaultAddressApplied = true;
#ifdef USE_BARO_SPI_BMP280
        FALLTHROUGH;

        default:
        break;
#endif
    }
    if (ISRVALUE({
            union {
               uint8_t raw[1], chip_id;
            } data;

            busReadRegisterBuffer(busdev, BMP280_CHIP_ID_REG, data.raw, true);

            data;
        }).chip_id!= BMP280_DEFAULT_CHIP_ID
    ) {
#ifdef USE_BARO_SPI_BMP280
        bmp280BusDeinit(busdev);
#endif
        if (defaultAddressApplied) {
            busdev->busdev_u.i2c.address = 0;
        }
        return false;
    }
}
#ifdef USE_BARO_SPI_BMP280
    busWriteRegister(busdev, BMP280_CONFIG_REG, busdev->bustype== BUSTYPE_SPI);
#endif
    // read calibration
{   int16_t data[0x18 * sizeof(int8_t) / sizeof(int16_t)];
    busReadRegisterBuffer(
        busdev
    ,
        BMP280_TEMPERATURE_CALIB_DIG_T1_LSB_REG
    , (void *)
        data
    ,
        '\x18'
    );
    for (
        register ptrdiff_t
        i = ARRAYLEN(bmp280_cal.dig);
      --i;
        i[bmp280_cal.dig] = i[data]
    ) NOOP
    bmp280_cal.dig_T1 = 0[(uint16_t const *)data];
    bmp280_cal.dig_P1 = 3[(uint16_t const *)data];
}
    bmp280_cal.dig_P2 = ldexp(bmp280_cal.dig_P2, 20);
    bmp280_cal.dig_P4 = ldexp(bmp280_cal.dig_P4, 35);
    bmp280_cal.dig_P7 = ldexp(bmp280_cal.dig_P7, 12);
    // set oversampling + power mode (forced), and start sampling
    busWriteRegister(busdev, BMP280_CTRL_MEAS_REG, BMP280_MODE);
    #undef busdev
    // set Pa to LSB conversion factor
    baro->baro_4053Pa = (baro->baro_1Pa = 0x400) * 4053ull;
    // these are dummy as temperature is measured as part of pressure
    baro->ut_delay = 0;
    baro->get_ut = NULL;
    // only _up part is executed, and gets both temperature and pressure
    baro->get_up = bmp280_get_up;
    baro->up_delay = ((T_INIT_MAX + T_MEASURE_PER_OSRS_MAX * (((1 << BMP280_TEMPERATURE_OSR) >> 1) + ((1 << BMP280_PRESSURE_OSR) >> 1)) + (BMP280_PRESSURE_OSR ? T_SETUP_PRESSURE_MAX : 0) + 15) / 16) * 1000;
    baro->calculate = bmp280_calculate;
    return true;
}

static void bmp280_get_up(register baroDev_t * restrict const baro) {
    union {
       uint8_t raw[BMP280_DATA_FRAME_SIZE];

      __PACKED_STRUCT {
            unsigned
            p:8 * BMP280_DATA_FRAME_SIZE / 2,
            t:8 * BMP280_DATA_FRAME_SIZE / 2;
        };
    } data;

    // read data from sensor
    busReadRegisterBuffer(
        (void *)baro, BMP280_PRESSURE_MSB_REG, data.raw, BMP280_DATA_FRAME_SIZE
    );

    bmp280_up =__builtin_bswap32(data.p)>> '\xC';
    bmp280_ut =__builtin_bswap32(data.t)>> '\xC';
}

static void bmp280_calculate(
    register uint32_t * restrict const pressure,
    register  int32_t * restrict const temperature
) {
    // calculate
    register double
    t = ldexp(
        (   ldexp(
                (ldexp(bmp280_ut,-4) - bmp280_cal.dig_T1) * bmp280_ut,-17
            ) - bmp280_cal.dig_T1
        ) * bmp280_cal.dig_T3
    ,
       -3
    );
    t+= (ldexp(bmp280_ut,-4) - bmp280_cal.dig_T1) * bmp280_cal.dig_T2;

    if (temperature)
       *temperature = lrint(ldexp(t / 5.l,-20));

    if (pressure)
       *pressure = ISRVALUE({
            register double
            p = ldexp(t = ldexp(t + -0x7Dp20l,-10), 17) * bmp280_cal.dig_P5;
            p+= bmp280_cal.dig_P6 * t * t;
            p+= bmp280_cal.dig_P4;

            isnormal(
                t = ldexp(
                    ((bmp280_cal.dig_P3 * t + bmp280_cal.dig_P2) * t + 0x1p55l)
                        * bmp280_cal.dig_P1
                ,
                   -41
                )
            ) ?
                t = ldexp(
                    (p = (ldexp(bmp280_up + -0x1p20l, 31) + p) * 3125.l / t)
                        * p
                        * bmp280_cal.dig_P9
                ,
                   -38
                ),
                (uint32_t)rintf(ldexp(
                    fdim(
                        bmp280_cal.dig_P7 + t
                    ,
                        ldexp(bmp280_cal.dig_P8 * p,-19) + p
                    )
                ,
                   -8
                ))
            :
                false;
        });
}

#endif

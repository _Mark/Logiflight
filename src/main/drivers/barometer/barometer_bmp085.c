/*
 * This file is part of Cleanflight and Betaflight.
 *
 * Cleanflight and Betaflight are free software. You can redistribute
 * this software and/or modify this software under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Cleanflight and Betaflight are distributed in the hope that they
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.
 *
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stdint.h>

#include "platform.h"

#include "build/build_config.h"

#include "barometer.h"

#include "drivers/bus.h"
#include "drivers/bus_i2c.h"
#include "drivers/bus_i2c_busdev.h"
#include "drivers/exti.h"
#include "drivers/io.h"
#include "drivers/nvic.h"
#include "drivers/time.h"

#include "barometer_bmp085.h"

#ifdef USE_BARO

#if defined(BARO_EOC_GPIO)

static IO_t eocIO;

static bool isConversionComplete = false;
static bool isEOCConnected = true;

// EXTI14 for BMP085 End of Conversion Interrupt
void bmp085_extiHandler(extiCallbackRec_t* cb) {
    UNUSED(cb);
    isConversionComplete = true;
}

bool bmp085TestEOCConnected(const bmp085Config_t *config);
# endif

typedef struct {
    int16_t ac1;
    int16_t ac2;
    int16_t ac3;
    uint16_t ac4;
    uint16_t ac5;
    uint16_t ac6;
    int16_t b1;
    int16_t b2;
    int16_t mb;
    int16_t mc;
    int16_t md;
} bmp085_smd500_calibration_param_t;

typedef struct {
    bmp085_smd500_calibration_param_t cal_param;
    uint8_t mode;
    uint8_t chip_id, ml_version, al_version;
    uint8_t dev_addr;
    int32_t param_b5;
    int16_t oversampling_setting;
} bmp085_t;

#define BMP085_I2C_ADDR         0x77
#define BMP085_CHIP_ID          0x55
#define BOSCH_PRESSURE_BMP085   85
#define BMP085_CHIP_ID_REG      0xD0
#define BMP085_VERSION_REG      0xD1
#define E_SENSOR_NOT_DETECTED   (char) 0
#define BMP085_PROM_START__ADDR 0xaa
#define BMP085_PROM_DATA__LEN   22
#define BMP085_T_MEASURE        0x2E                // temperature measurent
#define BMP085_P_MEASURE        0x34                // pressure measurement
#define BMP085_CTRL_MEAS_REG    0xF4
#define BMP085_ADC_OUT_MSB_REG  0xF6
#define BMP085_ADC_OUT_LSB_REG  0xF7
#define BMP085_CHIP_ID__POS     0
#define BMP085_CHIP_ID__MSK     0xFF
#define BMP085_CHIP_ID__LEN     8
#define BMP085_CHIP_ID__REG     BMP085_CHIP_ID_REG

#define BMP085_ML_VERSION__POS      0
#define BMP085_ML_VERSION__LEN      4
#define BMP085_ML_VERSION__MSK      0x0F
#define BMP085_ML_VERSION__REG      BMP085_VERSION_REG

#define BMP085_AL_VERSION__POS      4
#define BMP085_AL_VERSION__LEN      4
#define BMP085_AL_VERSION__MSK      0xF0
#define BMP085_AL_VERSION__REG      BMP085_VERSION_REG

#define BMP085_GET_BITSLICE(regvar, bitname) (regvar & bitname##__MSK) >> bitname##__POS
#define BMP085_SET_BITSLICE(regvar, bitname, val) (regvar & ~bitname##__MSK) | ((val<<bitname##__POS)&bitname##__MSK)

#define SMD500_PARAM_MG      3038        //calibration parameter
#define SMD500_PARAM_MH     -7357        //calibration parameter
#define SMD500_PARAM_MI      3791        //calibration parameter

STATIC_UNIT_TESTED bmp085_t bmp085;

#define UT_DELAY    6000        // 1.5ms margin according to the spec (4.5ms T conversion time)
#define UP_DELAY    27000       // 6000+21000=27000 1.5ms margin according to the spec (25.5ms P conversion time with OSS=3)

static bool bmp085InitDone = false;
STATIC_UNIT_TESTED uint16_t bmp085_ut;  // static result of temperature measurement
STATIC_UNIT_TESTED uint32_t bmp085_up;  // static result of pressure measurement

static void bmp085_get_cal_param(busDevice_t *busdev);
static void bmp085_get_ut(baroDev_t *baro);
static void bmp085_start_up(baroDev_t *baro);
static void bmp085_get_up(baroDev_t *baro);
static int32_t bmp085_get_temperature(uint32_t ut);
static uint32_t bmp085_get_pressure(uint32_t);
static void bmp085_calculate(uint32_t *, int32_t *);

static IO_t xclrIO;

#ifdef BARO_XCLR_PIN
#define BMP085_OFF  IOLo(xclrIO);
#define BMP085_ON   IOHi(xclrIO);
#else
#define BMP085_OFF
#define BMP085_ON
#endif


void bmp085InitXclrIO(const bmp085Config_t *config) {
    if (!xclrIO && config && config->xclrIO) {
        xclrIO = IOGetByTag(config->xclrIO);
        IOInit(xclrIO, OWNER_BARO_CS, 0);
        IOConfigGPIO(xclrIO, IOCFG_OUT_PP);
    }
}

void bmp085Disable(const bmp085Config_t *config) {
    bmp085InitXclrIO(config);
    BMP085_OFF;
}

bool bmp085Detect(const bmp085Config_t *config, baroDev_t *baro) {
    uint8_t data;
    bool ack;
    bool defaultAddressApplied = false;
#if defined(BARO_EOC_GPIO)
    IO_t eocIO = IO_NONE;
#endif
    if (bmp085InitDone)
        return true;
    bmp085InitXclrIO(config);
    BMP085_ON;   // enable baro
#if defined(BARO_EOC_GPIO) && defined(USE_EXTI)
    if (config && config->eocIO) {
        eocIO = IOGetByTag(config->eocIO);
        // EXTI interrupt for barometer EOC
        IOInit(eocIO, OWNER_BARO_EXTI, 0);
        IOConfigGPIO(eocIO, Mode_IN_FLOATING);
        EXTIHandlerInit(&bmp085_extiCallbackRec, bmp085_extiHandler);
        EXTIConfig(eocIO, &bmp085_extiCallbackRec, NVIC_PRIO_BARO_EXTI, EXTI_Trigger_Rising);
        EXTIEnable(eocIO, true);
    }
#else
    UNUSED(config);
#endif
    delay(20); // datasheet says 10ms, we'll be careful and do 20.
    busDevice_t *busdev = &baro->busdev;
    if ((busdev->bustype == BUSTYPE_I2C) && (busdev->busdev_u.i2c.address == 0)) {
        // Default address for BMP085
        busdev->busdev_u.i2c.address = BMP085_I2C_ADDR;
        defaultAddressApplied = true;
    }
    ack = busReadRegisterBuffer(busdev, BMP085_CHIP_ID__REG, &data, 1); /* read Chip Id */
    if (ack) {
        bmp085.chip_id = BMP085_GET_BITSLICE(data, BMP085_CHIP_ID);
        bmp085.oversampling_setting = 3;
        if (bmp085.chip_id == BMP085_CHIP_ID) { /* get bitslice */
            busReadRegisterBuffer(busdev, BMP085_VERSION_REG, &data, 1); /* read Version reg */
            bmp085.ml_version = BMP085_GET_BITSLICE(data, BMP085_ML_VERSION); /* get ML Version */
            bmp085.al_version = BMP085_GET_BITSLICE(data, BMP085_AL_VERSION); /* get AL Version */
            bmp085_get_cal_param(busdev); /* readout bmp085 calibparam structure */
            baro->ut_delay = UT_DELAY;
            baro->up_delay = UP_DELAY;
            baro->get_ut = bmp085_get_ut;
            baro->get_up = bmp085_get_up;
            baro->calculate = bmp085_calculate;
#if defined(BARO_EOC_GPIO)
            isEOCConnected = bmp085TestEOCConnected(config);
#endif
            bmp085InitDone = true;
            return true;
        }
    }
#if defined(BARO_EOC_GPIO)
    if (eocIO)
        EXTIRelease(eocIO);
#endif
    BMP085_OFF;
    if (defaultAddressApplied) {
        busdev->busdev_u.i2c.address = 0;
    }
    return false;
}

static int32_t bmp085_get_temperature(uint32_t ut) {
    int32_t temperature;
    int32_t x1, x2;
    x1 = (((int32_t) ut - (int32_t) bmp085.cal_param.ac6) * (int32_t) bmp085.cal_param.ac5) >> 15;
    x2 = ((int32_t) bmp085.cal_param.mc << 11) / (x1 + bmp085.cal_param.md);
    bmp085.param_b5 = x1 + x2;
    temperature = ((bmp085.param_b5 * 10 + 8) >> 4);  // temperature in 0.01 C (make same as MS5611)
    return temperature;
}

static uint32_t bmp085_get_pressure(register uint32_t up) {
{   register uint32_t b4, b7;
{   register  int32_t x1, x2, x3, b3, b6;
    b6 = bmp085.param_b5 - 4000;
    // *****calculate B3************
    x1 = (b6 * b6) >> 12;
    x1 *= bmp085.cal_param.b2;
    x1 >>= 11;
    x2 = (bmp085.cal_param.ac2 * b6);
    x2 >>= 11;
    x3 = x1 + x2;
    b3 = (((((int32_t) bmp085.cal_param.ac1) * 4 + x3) << bmp085.oversampling_setting) + 2) >> 2;
    // *****calculate B4************
    x1 = (bmp085.cal_param.ac3 * b6) >> 13;
    x2 = (bmp085.cal_param.b1 * ((b6 * b6) >> 12)) >> 16;
    x3 = ((x1 + x2) + 2) >> 2;
    b4 = (bmp085.cal_param.ac4 * (uint32_t)(x3 + 32768)) >> 15;
    b7 = ((uint32_t)(up - b3) * (50000 >> bmp085.oversampling_setting));
}   up = b7 < 0x80000000ul ? (b7<< 1) / b4 : b7 / b4<< 1;
}
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wshift-count-overflow"
    return // Pa
    (   ((up>> 16) * SMD500_PARAM_MG + SMD500_PARAM_MH) * up>> 16
            + SMD500_PARAM_MI
       >> 4
    ) + up;
#pragma GCC diagnostic pop
}

static void bmp085_start_ut(baroDev_t *baro) {
#if defined(BARO_EOC_GPIO)
    isConversionComplete = false;
#endif
    busWriteRegister(&baro->busdev, BMP085_CTRL_MEAS_REG, BMP085_T_MEASURE);
}

static void bmp085_get_ut(baroDev_t *baro) {
#if defined(BARO_EOC_GPIO)
    // return old baro value if conversion time exceeds datasheet max when EOC is connected
    if (isConversionComplete ||!isEOCConnected)
#endif
{   uint8_t data[2];
    busReadRegisterBuffer(&baro->busdev, BMP085_ADC_OUT_MSB_REG, data, 2);
    bmp085_ut = (data[0] << 8) | data[1];
}
    bmp085_start_up(baro);
}

static void bmp085_start_up(baroDev_t *baro) {
    uint8_t ctrl_reg_data;
    ctrl_reg_data = BMP085_P_MEASURE + (bmp085.oversampling_setting << 6);
#if defined(BARO_EOC_GPIO)
    isConversionComplete = false;
#endif
    busWriteRegister(&baro->busdev, BMP085_CTRL_MEAS_REG, ctrl_reg_data);
}

/** read out up for pressure conversion
 depending on the oversampling ratio setting up can be 16 to 19 bit
 \return up parameter that represents the uncompensated pressure value
 */
static void bmp085_get_up(baroDev_t *baro) {
#if defined(BARO_EOC_GPIO)
    // return old baro value if conversion time exceeds datasheet max when EOC is connected
    if (isConversionComplete ||!isEOCConnected)
#endif
{   uint8_t data[3];
    busReadRegisterBuffer(&baro->busdev, BMP085_ADC_OUT_MSB_REG, data, 3);
    bmp085_up = (((uint32_t) data[0] << 16) | ((uint32_t) data[1] << 8) | (uint32_t) data[2])
                >> (8 - bmp085.oversampling_setting);
}
    bmp085_start_ut(baro);
}

static void bmp085_calculate(
    register uint32_t * restrict const pressure,
    register  int32_t * restrict const temperature
) {
{   register  int32_t const
    temp = bmp085_get_temperature(bmp085_ut);

    if (temperature)
       *temperature = temp;
}
    register uint32_t const
    press = bmp085_get_pressure(bmp085_up);

    if (pressure)
       *pressure = press;
}

static void bmp085_get_cal_param(busDevice_t *busdev) {
    uint8_t data[22];
    busReadRegisterBuffer(busdev, BMP085_PROM_START__ADDR, data, BMP085_PROM_DATA__LEN);
    /*parameters AC1-AC6*/
    bmp085.cal_param.ac1 = (data[0] << 8) | data[1];
    bmp085.cal_param.ac2 = (data[2] << 8) | data[3];
    bmp085.cal_param.ac3 = (data[4] << 8) | data[5];
    bmp085.cal_param.ac4 = (data[6] << 8) | data[7];
    bmp085.cal_param.ac5 = (data[8] << 8) | data[9];
    bmp085.cal_param.ac6 = (data[10] << 8) | data[11];
    /*parameters B1,B2*/
    bmp085.cal_param.b1 = (data[12] << 8) | data[13];
    bmp085.cal_param.b2 = (data[14] << 8) | data[15];
    /*parameters MB,MC,MD*/
    bmp085.cal_param.mb = (data[16] << 8) | data[17];
    bmp085.cal_param.mc = (data[18] << 8) | data[19];
    bmp085.cal_param.md = (data[20] << 8) | data[21];
}

#if defined(BARO_EOC_GPIO)
bool bmp085TestEOCConnected(const bmp085Config_t *config) {
    UNUSED(config);
    if (!bmp085InitDone && eocIO) {
        bmp085_start_ut();
        delayMicroseconds(UT_DELAY * 2); // wait twice as long as normal, just to be sure
        // conversion should have finished now so check if EOC is high
        uint8_t status = IORead(eocIO);
        if (status) {
            return true;
        }
    }
    return false; // assume EOC is not connected
}
#endif

#endif /* BARO */

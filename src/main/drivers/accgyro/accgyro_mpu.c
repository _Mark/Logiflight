/*
 * This file is part of Cleanflight and Betaflight.
 *
 * Cleanflight and Betaflight are free software. You can redistribute
 * this software and/or modify this software under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Cleanflight and Betaflight are distributed in the hope that they
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.
 *
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "platform.h"

#include "build/atomic.h"
#include "build/build_config.h"
#include "build/debug.h"

#include "common/maths.h"
#include "common/utils.h"

#include "drivers/bus.h"
#include "drivers/bus_i2c.h"
#include "drivers/bus_spi.h"
#include "drivers/exti.h"
#include "drivers/io.h"
#include "drivers/nvic.h"
#include "drivers/sensor.h"
#include "drivers/system.h"
#include "drivers/time.h"

#ifdef USE_DMA_SPI_DEVICE
#include "drivers/dma_spi.h"
#include "sensors/gyro.h"
#endif //USE_DMA_SPI_DEVICE

#include "drivers/accgyro/accgyro.h"
#include "drivers/accgyro/accgyro_mpu3050.h"
#include "drivers/accgyro/accgyro_mpu6050.h"
#include "drivers/accgyro/accgyro_mpu6500.h"
#include "drivers/accgyro/accgyro_spi_bmi160.h"
#include "drivers/accgyro/accgyro_spi_icm20649.h"
#include "drivers/accgyro/accgyro_spi_icm20689.h"
#include "drivers/accgyro/accgyro_spi_mpu6000.h"
#include "drivers/accgyro/accgyro_spi_mpu6500.h"
#include "drivers/accgyro/accgyro_spi_mpu9250.h"
#include "drivers/accgyro/accgyro_mpu.h"
#ifdef USE_GYRO_IMUF9001
#include "drivers/accgyro/accgyro_imuf9001.h"
#include "rx/rx.h"
#include "fc/fc_rc.h"
#include "fc/runtime_config.h"
#endif //USE_GYRO_IMUF9001

mpuResetFnPtr mpuResetFn;

#ifdef USE_GYRO_IMUF9001
imufData_t imufData;
#endif
#ifndef MPU_I2C_INSTANCE
#define MPU_I2C_INSTANCE I2C_DEVICE
#endif

#ifndef MPU_ADDRESS
#define MPU_ADDRESS             0x68
#endif

#define MPU_INQUIRY_MASK   0x7E

#if defined(USE_I2C) && !defined(USE_DMA_SPI_DEVICE)
static void mpu6050FindRevision(register gyroDev_t * restrict const gyro) {
    // There is a map of revision contained in the android source tree which is quite comprehensive and may help to understand this code
    // See https://android.googlesource.com/kernel/msm.git/+/eaf36994a3992b8f918c18e4f7411e8b2320a35f/drivers/misc/mpu6050/mldl_cfg.c
    // determine product ID and revision
    uint8_t
    revision = ISRVALUE({
      __PACKED_STRUCT {
            uint8_t  head[1];
            unsigned tail:32;
        } buf;

        busReadRegisterBuffer((void *)gyro, MPU_RA_XA_OFFS_H, buf.head, 6) ?
            (   union {
                  __PACKED_STRUCT {
                       uint8_t low:1, mid:1, high:1;
                    };

                   uint8_t uint;
                }
            ){
                buf.tail & 0x1u, buf.tail & 0x10000u, buf.tail & 0x100000000u
            }.uint
        :
            false;
    });
    if (revision)
        switch (revision) {
            case 1:
            gyro->mpuDetectionResult.resolution = MPU_HALF_RESOLUTION;
            break;

            case 2:
            case 3:
            case 7:
            gyro->mpuDetectionResult.resolution = MPU_FULL_RESOLUTION;
            break;

            default:
            failureMode(FAILURE_ACC_INCOMPATIBLE);
            break;
        }
    else if (
        busReadRegisterBuffer(
            (void *)gyro, MPU_RA_PRODUCT_ID,&revision, true
        ) && (revision&= '\xF')
    )
        gyro->mpuDetectionResult.resolution = revision== '\4';
    else
        failureMode(FAILURE_ACC_INCOMPATIBLE);
}
#endif

/*
 * Gyro interrupt service routine
 */
#if defined(MPU_INT_EXTI)
static FAST_CODE_NOINVOKE void mpuIntExtiHandler(
    register extiCallbackRec_t * restrict const cb
) {
#ifdef USE_DMA_SPI_DEVICE
    //start dma read
    (void)(cb);
    gyroDmaSpiStartRead();
#else
#ifdef DEBUG_MPU_DATA_READY_INTERRUPT
    static uint32_t lastCalledAtUs = 0;
    const uint32_t nowUs = micros();
    debug[0] = (uint16_t)(nowUs - lastCalledAtUs);
    lastCalledAtUs = nowUs;
#endif
    gyroDev_t *gyro = container_of(cb, gyroDev_t, exti);
    gyro->dataReady = true;
#ifdef DEBUG_MPU_DATA_READY_INTERRUPT
    const uint32_t now2Us = micros();
    debug[1] = (uint16_t)(now2Us - nowUs);
#endif // DEBUG_MPU_DATA_READY_INTERRUPT
#endif // USE_DMA_SPI_DEVICE
}

void mpuGyroInit(register gyroDev_t * restrict const gyro) {
    if (gyro->mpuIntExtiTag) {
    register
    const IO_t mpuIntIO = IOGetByTag(gyro->mpuIntExtiTag);
#ifdef ENSURE_MPU_DATA_READY_IS_LOW
    if (IORead(mpuIntIO)) NOOP else {
#endif
    IOInit(mpuIntIO, OWNER_MPU_EXTI, 0);
#if defined(STM32F0) || defined(STM32F1) || defined(STM32F3) || defined(STM32F4)
    IOConfigGPIO(mpuIntIO, IOCFG_IN_FLOATING);
#endif
    EXTIHandlerInit(&gyro->exti, mpuIntExtiHandler);
    EXTIConfig(
        mpuIntIO
    ,
       &gyro->exti
    ,
        NVIC_PRIO_MPU_INT_EXTI
    ,
#if defined(STM32F0) || defined(STM32F1) || defined(STM32F3) || defined(STM32F4)
        EXTI_Trigger_Rising
#else
        IO_CONFIG(GPIO_MODE_INPUT, false, GPIO_NOPULL)
#endif
    );
    EXTIEnable(mpuIntIO, true);
#ifdef ENSURE_MPU_DATA_READY_IS_LOW
    }
#endif
    }
}
#endif // MPU_INT_EXTI

#ifdef USE_DMA_SPI_DEVICE
FAST_CODE bool mpuGyroDmaSpiReadStart(gyroDev_t * gyro) {
    (void)(gyro); ///not used at this time
    //no reason not to get acc and gyro data at the same time
#ifdef USE_GYRO_IMUF9001
    if (isImufCalibrating == IMUF_IS_CALIBRATING) { //calibrating
        //two steps
        //step 1 is isImufCalibrating=1, this starts the calibration command and sends it to the IMU-f
        //step 2 is isImufCalibrating=2, this sets the tx buffer back to 0 so we don't keep sending the calibration command over and over
        memset(dmaTxBuffer, 0, sizeof(imufCommand_t)); //clear buffer
        //set calibration command with CRC, typecast the dmaTxBuffer as imufCommand_t
        (*(imufCommand_t *)(dmaTxBuffer)).command = IMUF_COMMAND_CALIBRATE;
        (*(imufCommand_t *)(dmaTxBuffer)).crc     = getCrcImuf9001((uint32_t *)dmaTxBuffer, 11); //typecast the dmaTxBuffer as a uint32_t array which is what the crc command needs
        //set isImufCalibrating to step 2, which is just used so the memset to 0 runs after the calibration commmand is sent
        isImufCalibrating = IMUF_DONE_CALIBRATING; //go to step two
    } else if (isImufCalibrating == IMUF_DONE_CALIBRATING) {
        // step 2, memset of the tx buffer has run, set isImufCalibrating to 0.
        (*(imufCommand_t *)(dmaTxBuffer)).command = 0;
        (*(imufCommand_t *)(dmaTxBuffer)).crc     = 0; //typecast the dmaTxBuffer as a uint32_t array which is what the crc command needs
        imufEndCalibration();
    } else {
        if (isSetpointNew) {
            //send setpoint and arm status
            (*(imufCommand_t *)(dmaTxBuffer)).command = IMUF_COMMAND_SETPOINT;
            (*(imufCommand_t *)(dmaTxBuffer)).param1  = getSetpointRateInt(0);
            (*(imufCommand_t *)(dmaTxBuffer)).param2  = getSetpointRateInt(1);
            (*(imufCommand_t *)(dmaTxBuffer)).param3  = getSetpointRateInt(2);
            (*(imufCommand_t *)(dmaTxBuffer)).crc     = getCrcImuf9001((uint32_t *)dmaTxBuffer, 11); //typecast the dmaTxBuffer as a uint32_t array which is what the crc command needs
            isSetpointNew = 0;
        }
    }
    memset(dmaRxBuffer, 0, gyroConfig()->imuf_mode); //clear buffer
    //send and receive data using SPI and DMA
    dmaSpiTransmitReceive(dmaTxBuffer, dmaRxBuffer, gyroConfig()->imuf_mode, 0);
#else
    dmaTxBuffer[0] = MPU_RA_ACCEL_XOUT_H | 0x80;
    dmaSpiTransmitReceive(dmaTxBuffer, dmaRxBuffer, 15, 0);
#endif // USE_GYRO_IMUF9001
    return true;
}

FAST_CODE void mpuGyroDmaSpiReadFinish(gyroDev_t * gyro) {
    //spi rx dma callback
#ifdef USE_GYRO_IMUF9001
    memcpy(&imufData, dmaRxBuffer, sizeof(imufData_t));
 #ifdef USE_ACC
    gyro-> accADCRaw[X]  = lrintf(acc.dev.acc_1G * imufData.accX);
    gyro-> accADCRaw[Y]  = lrintf(acc.dev.acc_1G * imufData.accY);
    gyro-> accADCRaw[Z]  = lrintf(acc.dev.acc_1G * imufData.accZ);
 #endif
    gyro->gyroADCf[X]    = imufData.gyroX;
    gyro->gyroADCf[Y]    = imufData.gyroY;
    gyro->gyroADCf[Z]    = imufData.gyroZ;
    gyro->gyroADCRaw[X]  = (int16_t)(imufData.gyroX * 16.4f);
    gyro->gyroADCRaw[Y]  = (int16_t)(imufData.gyroY * 16.4f);
    gyro->gyroADCRaw[Z]  = (int16_t)(imufData.gyroZ * 16.4f);
#else
    for (
        register struct {
            int16_t const * restrict const src;
            int16_t       * restrict const dst;
 #ifndef USE_ACC
            ptrdiff_t                      axis;
 #endif
        } buf = { (void const *)(
 #ifdef USE_ACC
            (gyro->isAccDataUsed<< true ^ 7) + dmaRxBuffer
 #else
            dmaRxBuffer + 7
 #endif
        ),
            gyro->tempADCRaw
 #ifdef USE_ACC
                - gyro->isAccDataUsed
 #endif
        ,
 #ifdef USE_ACC
            gyro->isAccDataUsed+=
 #endif
            XYZ_AXIS_COUNT + 1
        };
 #ifdef USE_ACC
        gyro->isAccDataUsed--;
        gyro->isAccDataUsed[buf.dst] =__builtin_bswap16(
            gyro->isAccDataUsed[buf.src]
        )
 #else
        buf.axis--;
        buf.axis[buf.dst] =__builtin_bswap16(buf.axis[buf.src])
 #endif
    ) NOOP
#endif // USE_GYRO_IMUF9001
}
#endif

#ifdef GYRO_USES_SPI
 #ifdef USE_ACC
_Static_assert(
    MPU_RA_ACCEL_XOUT_H | 0x80== 0xBB
,
    "MPU_RA_ACCEL_XOUT_H | 0x80 does not equal 59"
);
 #endif
_Static_assert(
    MPU_RA_TEMP_OUT_H | 0x80== 0xC1
,
    "MPU_RA_TEMP_OUT_H | 0x80 does not equal 65"
);

FAST_CODE FAST_CODE_NOINLINE bool mpuGyroReadSPI(
#else
FAST_CODE FAST_CODE_NOINLINE bool mpuGyroRead(
#endif // GYRO_USES_SPI
    register gyroDev_t * restrict const gyro
) {
#ifdef GYRO_USES_SPI
  __PACKED_STRUCT {
   uint8_t
    ptr[1];
#endif
    int16_t
    buf[
#ifdef USE_ACC
        gyro->isAccDataUsed +
#endif
        (XYZ_AXIS_COUNT + 1)
    ];
#ifdef GYRO_USES_SPI
    } arr;

    return
    spiBusTransfer((void *)
#else
    busReadRegisterBuffer((void *)
#endif
       gyro
    ,
#ifdef USE_ACC
        gyro->isAccDataUsed ?
 #ifdef GYRO_USES_SPI
            u8"\xBB\xFF\xFF\xFF\xFF\xFF\xFF"
 #else
            MPU_RA_ACCEL_XOUT_H
 #endif
        :
#endif // USE_ACC
#ifdef GYRO_USES_SPI
        u8"\xC1\xFF\xFF\xFF\xFF\xFF\xFF"
#else
        MPU_RA_TEMP_OUT_H
#endif
    ,
#ifdef GYRO_USES_SPI
        arr.ptr
#else
        (void *)buf
#endif
    ,
#ifdef GYRO_USES_SPI
        sizeof arr
#else
        sizeof buf
#endif
    ) && ISRVALUE({
#ifndef USE_ACC
        for (
            register ptrdiff_t
            axis = XYZ_AXIS_COUNT + 1;
            axis--;
            axis[gyro->tempADCRaw] =__builtin_bswap16(axis[
#else
    {   register int16_t * restrict const
        adc = gyro->tempADCRaw - gyro->isAccDataUsed;

        gyro->isAccDataUsed+= XYZ_AXIS_COUNT + 1;

        do
            gyro->isAccDataUsed[adc] =__builtin_bswap16((--gyro->isAccDataUsed)[
#endif
#ifdef GYRO_USES_SPI
                arr.
#endif
                buf
#ifdef USE_ACC
            ]);
        while (gyro->isAccDataUsed);
    }
#else
            ])
        ) NOOP
#endif
        true;
    });
}

#ifdef USE_SPI
static bool detectSPISensorsAndUpdateDetectionResult(
    register gyroDev_t * restrict const gyro
) {
    register uint8_t sensor;
    UNUSED(gyro); // since there are FCs which have gyro on I2C but other devices on SPI
    UNUSED(sensor);
    // note, when USE_DUAL_GYRO is enabled the gyro->bus must already be initialised.
#ifdef USE_GYRO_SPI_MPU6000
#ifndef USE_DUAL_GYRO
    spiBusSetInstance((void *)gyro, MPU6000_SPI_INSTANCE);
#endif
#ifdef MPU6000_CS_PIN
    gyro->bus.busdev_u.spi.csnPin = gyro->bus.busdev_u.spi.csnPin == IO_NONE ? IOGetByTag(IO_TAG(MPU6000_CS_PIN)) : gyro->bus.busdev_u.spi.csnPin;
#endif
    if ((sensor = mpu6000SpiDetect((void *)gyro))) {
        gyro->mpuDetectionResult.sensor = sensor;
        return true;
    }
#endif
#ifdef USE_GYRO_SPI_MPU6500
#ifndef USE_DUAL_GYRO
    spiBusSetInstance((void *)gyro, MPU6500_SPI_INSTANCE);
#endif
#ifdef MPU6500_CS_PIN
    gyro->bus.busdev_u.spi.csnPin = gyro->bus.busdev_u.spi.csnPin == IO_NONE ? IOGetByTag(IO_TAG(MPU6500_CS_PIN)) : gyro->bus.busdev_u.spi.csnPin;
#endif
    // some targets using MPU_9250_SPI, ICM_20608_SPI or ICM_20602_SPI state sensor is MPU_65xx_SPI
    if ((sensor = mpu6500SpiDetect((void *)gyro))) {
        gyro->mpuDetectionResult.sensor = sensor;
        return true;
    }
#endif
#ifdef USE_GYRO_IMUF9001
#ifdef IMUF9001_SPI_INSTANCE
    spiBusSetInstance((void *)gyro, IMUF9001_SPI_INSTANCE);
#else
#error IMUF9001 is SPI only
#endif
#ifdef IMUF9001_CS_PIN
    gyro->bus.busdev_u.spi.csnPin = gyro->bus.busdev_u.spi.csnPin == IO_NONE ? IOGetByTag(IO_TAG(IMUF9001_CS_PIN)) : gyro->bus.busdev_u.spi.csnPin;
#else
#error IMUF9001 must use a CS pin (IMUF9001_CS_PIN)
#endif
#ifdef IMUF9001_RST_PIN
    gyro->bus.busdev_u.spi.rstPin = IOGetByTag(IO_TAG(IMUF9001_RST_PIN));
#else
#error IMUF9001 must use a RST pin (IMUF9001_RST_PIN)
#endif
    // some targets using MPU_9250_SPI, ICM_20608_SPI or ICM_20602_SPI state sensor is MPU_65xx_SPI
    if ((sensor = imuf9001SpiDetect(gyro))) {
        gyro->mpuDetectionResult.sensor = sensor;
        return true;
    }
#endif
#ifdef  USE_GYRO_SPI_MPU9250
#ifndef USE_DUAL_GYRO
    spiBusSetInstance((void *)gyro, MPU9250_SPI_INSTANCE);
#endif
#ifdef MPU9250_CS_PIN
    gyro->bus.busdev_u.spi.csnPin = gyro->bus.busdev_u.spi.csnPin == IO_NONE ? IOGetByTag(IO_TAG(MPU9250_CS_PIN)) : gyro->bus.busdev_u.spi.csnPin;
#endif
    if ((sensor = mpu9250SpiDetect((void *)gyro))) {
        gyro->mpuDetectionResult.sensor = sensor;
        gyro->mpuConfiguration.resetFn = mpu9250SpiResetGyro;
        return true;
    }
#endif
#ifdef USE_GYRO_SPI_ICM20649
#ifdef ICM20649_SPI_INSTANCE
    spiBusSetInstance((void *)gyro, ICM20649_SPI_INSTANCE);
#endif
#ifdef ICM20649_CS_PIN
    gyro->bus.busdev_u.spi.csnPin = gyro->bus.busdev_u.spi.csnPin == IO_NONE ? IOGetByTag(IO_TAG(ICM20649_CS_PIN)) : gyro->bus.busdev_u.spi.csnPin;
#endif
    if ((sensor = icm20649SpiDetect((void *)gyro))) {
        gyro->mpuDetectionResult.sensor = sensor;
        return true;
    }
#endif
#ifdef USE_GYRO_SPI_ICM20689
#ifndef USE_DUAL_GYRO
    spiBusSetInstance((void *)gyro, ICM20689_SPI_INSTANCE);
#endif
#ifdef ICM20689_CS_PIN
    gyro->bus.busdev_u.spi.csnPin = gyro->bus.busdev_u.spi.csnPin == IO_NONE ? IOGetByTag(IO_TAG(ICM20689_CS_PIN)) : gyro->bus.busdev_u.spi.csnPin;
#endif
    // icm20689SpiDetect detects ICM20602 and ICM20689
    if ((sensor = icm20689SpiDetect((void *)gyro))) {
        gyro->mpuDetectionResult.sensor = sensor;
        return true;
    }
#endif
#ifdef USE_ACCGYRO_BMI160
#ifndef USE_DUAL_GYRO
    spiBusSetInstance((void *)gyro, BMI160_SPI_INSTANCE);
#endif
#ifdef BMI160_CS_PIN
    gyro->bus.busdev_u.spi.csnPin = gyro->bus.busdev_u.spi.csnPin == IO_NONE ? IOGetByTag(IO_TAG(BMI160_CS_PIN)) : gyro->bus.busdev_u.spi.csnPin;
#endif
    if ((sensor = bmi160Detect((void *)gyro))) {
        gyro->mpuDetectionResult.sensor = sensor;
        return true;
    }
#endif
    return false;
}
#endif

void mpuDetect(
    register gyroDev_t * restrict const gyro
) {
    // MPU datasheet specifies 30ms.
    delay(35);
#if defined(USE_I2C) && !defined(USE_DMA_SPI_DEVICE)
    if (gyro->bus.bustype ?
            gyro->bus.bustype== BUSTYPE_I2C
        // if no bustype is selected try I2C first.
        : (
            gyro->bus.bustype = BUSTYPE_I2C,
            true
        )
    ) {
        gyro->bus.busdev_u.i2c.device = MPU_I2C_INSTANCE;
        gyro->bus.busdev_u.i2c.address = MPU_ADDRESS;
        uint8_t sig;
        register bool
        ack = busReadRegisterBuffer((void *)gyro, MPU_RA_WHO_AM_I,&sig, 1);
        if (ack) {
            // If an MPU3050 is connected sig will contain 0.
            uint8_t inquiryResult;
            if ((   ack =
                        busReadRegisterBuffer((void *)
                            gyro
                        ,
                            MPU_RA_WHO_AM_I_LEGACY
                        ,
                           &inquiryResult
                        ,
                            1
                        )
                ) && (inquiryResult&= MPU_INQUIRY_MASK)== MPUx0x0_WHO_AM_I_CONST
            ) {
                gyro->mpuDetectionResult.sensor = MPU_3050;
                return;
            }
            sig &= MPU_INQUIRY_MASK;
            if (sig == MPUx0x0_WHO_AM_I_CONST) {
                gyro->mpuDetectionResult.sensor = MPU_60x0;
                mpu6050FindRevision(gyro);
            } else if (sig == MPU6500_WHO_AM_I_CONST) {
                gyro->mpuDetectionResult.sensor = MPU_65xx_I2C;
            }
            return;
        }
    }
#endif
#ifdef USE_SPI
    gyro->bus.bustype = BUSTYPE_SPI;
    detectSPISensorsAndUpdateDetectionResult(gyro);
#endif
}

uint8_t mpuGyroDLPF(register gyroDev_t const * restrict const gyro) {
    return gyro->gyroRateKHz > GYRO_RATE_8_kHz ? false : gyro->hardware_lpf;
}

uint8_t mpuGyroFCHOICE(register gyroDev_t const * restrict const gyro) {
    return
    gyro->gyroRateKHz > GYRO_RATE_8_kHz ?
        gyro->hardware_32khz_lpf== GYRO_32KHZ_HARDWARE_LPF_EXPERIMENTAL ?
            FCB_8800_32
        :
            FCB_3600_32
    :
        FCB_DISABLED;
}

#ifdef USE_GYRO_REGISTER_DUMP
uint8_t mpuGyroReadRegister(const busDevice_t *bus, uint8_t reg) {
    uint8_t data;
    const bool ack = busReadRegisterBuffer(bus, reg, &data, 1);
    if (ack) {
        return data;
    } else {
        return 0;
    }
}
#endif

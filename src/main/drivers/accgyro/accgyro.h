/*
 * This file is part of Cleanflight and Betaflight.
 *
 * Cleanflight and Betaflight are free software. You can redistribute
 * this software and/or modify this software under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Cleanflight and Betaflight are distributed in the hope that they
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.
 *
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <signal.h>

#include "platform.h"

#include "common/axis.h"
#include "common/filter.h"
#include "drivers/exti.h"
#include "drivers/bus.h"
#include "drivers/sensor.h"
#include "drivers/accgyro/accgyro_mpu.h"
#include "sensors/gyro.h"
#pragma GCC diagnostic push
#if defined(SIMULATOR_BUILD) && defined(SIMULATOR_MULTITHREAD)
#include <pthread.h>
#endif

#ifndef MPU_I2C_INSTANCE
#define MPU_I2C_INSTANCE I2C_DEVICE
#endif

#define GYRO_32KHZ_HARDWARE_LPF_NORMAL       0
#define GYRO_32KHZ_HARDWARE_LPF_EXPERIMENTAL 1

#define GYRO_LPF_256HZ      0
#define GYRO_LPF_188HZ      1
#define GYRO_LPF_98HZ       2
#define GYRO_LPF_42HZ       3
#define GYRO_LPF_20HZ       4
#define GYRO_LPF_10HZ       5
#define GYRO_LPF_5HZ        6
#define GYRO_LPF_NONE       7

//This optimizes the frequencies instead of calculating them
//in the case of 1100 and 9000, they would divide as irrational numbers.
#define GYRO_RATE_1_kHz     1000.0f
#define GYRO_RATE_1100_Hz   909.09f
#define GYRO_RATE_3200_Hz   312.5f
#define GYRO_RATE_8_kHz     125.0f
#define GYRO_RATE_9_kHz     111.11f
#define GYRO_RATE_16_kHz    64.0f
#define GYRO_RATE_32_kHz    32.0f

typedef struct gyroDev_s {
    busDevice_t bus;
#if defined(SIMULATOR_BUILD) && defined(SIMULATOR_MULTITHREAD)
    pthread_mutex_t lock;
#endif
    sensorGyroInitFuncPtr initFn;                             // initialize function
    sensorGyroReadFuncPtr readFn;                             // read 3 axis data function
    extiCallbackRec_t exti;
#ifdef USE_ACC
  __PACKED_STRUCT {
    int16_t accADCRaw[XYZ_AXIS_COUNT];
#endif
  __PACKED_STRUCT {
      __PACKED_UNION {
            int16_t tempRel, tempADCRaw[1];
        };
        int16_t gyroADCRaw[XYZ_AXIS_COUNT];
    };
#ifdef USE_ACC
    };
    int16_t gyro_1G;
#endif
    int32_t gyroADCRawPrevious[XYZ_AXIS_COUNT];
#ifdef USE_ACC
    ptrdiff_t isAccDataUsed;
    float  accADCf[XYZ_AXIS_COUNT];
#endif
    float gyroADCf[XYZ_AXIS_COUNT];
    float tempAbs,                                            // K
          scale,                                              // 16*deg/s
          gyro_1K, gyro_27315cK;
    double gyro_200G;
    mpuConfiguration_t mpuConfiguration;
    mpuDetectionResult_t mpuDetectionResult;
    sensor_align_e gyroAlign;
#ifdef USE_ACC
    sensor_align_e  accAlign;
#endif
    float gyroRateKHz;
    bool dataReady;
    bool gyro_high_fsr;
#ifdef USE_ACC
    bool  acc_high_fsr;
#endif
    uint8_t hardware_lpf;
    uint8_t hardware_32khz_lpf;
    uint8_t mpuDividerDrops;
    ioTag_t mpuIntExtiTag;
    uint8_t gyroHasOverflowProtection;
    gyroSensor_e gyroHardware;
} gyroDev_t;

static inline void gyroDevLock(gyroDev_t *gyro) {
#if defined(SIMULATOR_BUILD) && defined(SIMULATOR_MULTITHREAD)
    pthread_mutex_lock(&gyro->lock);
#else
    (void)gyro;
#endif
}

static inline void gyroDevUnLock(gyroDev_t *gyro) {
#if defined(SIMULATOR_BUILD) && defined(SIMULATOR_MULTITHREAD)
    pthread_mutex_unlock(&gyro->lock);
#else
    (void)gyro;
#endif
}
#pragma GCC diagnostic pop

/*
 * This file is part of Cleanflight and Betaflight.
 *
 * Cleanflight and Betaflight are free software. You can redistribute
 * this software and/or modify this software under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Cleanflight and Betaflight are distributed in the hope that they
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.
 *
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include <signal.h>

#include "platform.h"

#pragma once

// FIXME some of these are flight modes, some of these are general status indicators
typedef enum {
    ARMED                       = (1 << 0),
    WAS_EVER_ARMED              = (1 << 1),
    WAS_ARMED_WITH_PREARM       = (1 << 2)
} armingFlag_e;

extern sig_atomic_t armingFlags;

#define DISABLE_ARMING_FLAG(mask) (armingFlags &= ~(mask))
#define ENABLE_ARMING_FLAG(mask) (armingFlags |= (mask))
#define ARMING_FLAG(mask) (armingFlags & (mask))

/*
 * Arming disable flags are listed in the order of criticalness.
 * (Beeper code can notify the most critical reason.)
 */
typedef enum {
    ARMING_DISABLED_NO_GYRO         = (1 << 0),
    ARMING_DISABLED_FAILSAFE        = (1 << 1),
    ARMING_DISABLED_RX_FAILSAFE     = (1 << 2),
    ARMING_DISABLED_BAD_RX_RECOVERY = (1 << 3),
    ARMING_DISABLED_BOXFAILSAFE     = (1 << 4),
    ARMING_DISABLED_RUNAWAY_TAKEOFF = (1 << 5),
    ARMING_DISABLED_THROTTLE        = (1 << 6),
    ARMING_DISABLED_ANGLE           = (1 << 7),
    ARMING_DISABLED_BOOT_GRACE_TIME = (1 << 8),
    ARMING_DISABLED_NOPREARM        = (1 << 9),
    ARMING_DISABLED_LOAD            = (1 << 10),
    ARMING_DISABLED_CALIBRATING     = (1 << 11),
    ARMING_DISABLED_CLI             = (1 << 12),
    ARMING_DISABLED_CMS_MENU        = (1 << 13),
    ARMING_DISABLED_OSD_MENU        = (1 << 14),
    ARMING_DISABLED_BST             = (1 << 15),
    ARMING_DISABLED_PARALYZE        = (1 << 17),
    ARMING_DISABLED_GPS             = (1 << 18),
    ARMING_DISABLED_ARM_SWITCH      = (1 << 19), // Needs to be the last element, since it's always activated if one of the others is active when arming
} armingDisableFlags_e;

#define ARMING_DISABLE_FLAGS_COUNT 19

extern char const * const armingDisableFlagNames[ARMING_DISABLE_FLAGS_COUNT];

void setArmingDisabled(armingDisableFlags_e flag);
void unsetArmingDisabled(armingDisableFlags_e flag);
bool isArmingDisabled(void);
sig_atomic_t getArmingDisableFlags(void);

typedef enum {
    MAG_MODE         = 1 <<  0,
    BARO_MODE        = 1 <<  1,
    ANGLE_MODE       = 1 <<  2,
    HORIZON_MODE     = 1 <<  3,
    HEADFREE_MODE    = 1 <<  4,
    RANGEFINDER_MODE = 1 <<  5,
    LOOKDOWN_MODE    = RANGEFINDER_MODE,
    NFE_RACE_MODE    = 1 <<  6,
    GPS_HOLD_MODE    = 1 <<  7,
    GPS_HOME_MODE    = 1 <<  8,
#ifdef USE_GPS_RESCUE
    GPS_RESCUE_MODE  = GPS_HOME_MODE,
#endif
    FAILSAFE_MODE    = 1 <<  9,
    PASSTHRU_MODE    = 1 << 10,
    HELI_MODE        = 1 << 14,
    ACC_MODE         =
#ifdef USE_GPS_RESCUE
        GPS_RESCUE_MODE |
#endif
        ANGLE_MODE | HORIZON_MODE | NFE_RACE_MODE | GPS_HOLD_MODE,
} flightModeFlags_e;

extern sig_atomic_t flightModeFlags;

#define DISABLE_FLIGHT_MODE(mask) disableFlightMode(mask)
#define ENABLE_FLIGHT_MODE(mask) enableFlightMode(mask)
#define FLIGHT_MODE(mask) (flightModeFlags & (mask))

// macro to initialize map from boxId_e to log2(flightModeFlags). Keep it in sync with flightModeFlags_e enum.
// [BOXARM] is left unpopulated
#define BOXID_TO_FLIGHT_MODE_MAP_INITIALIZER {           \
   [BOXANGLE]       = LOG2(ANGLE_MODE),                  \
   [BOXHORIZON]     = LOG2(HORIZON_MODE),                \
   [BOXMAG]         = LOG2(MAG_MODE),                    \
   [BOXBARO]        = LOG2(BARO_MODE),                   \
   [BOXGPSHOME]     = LOG2(GPS_HOME_MODE),               \
   [BOXGPSHOLD]     = LOG2( GPS_HOLD_MODE),              \
   [BOXHEADFREE]    = LOG2(HEADFREE_MODE),               \
   [BOXPASSTHRU]    = LOG2(PASSTHRU_MODE),               \
   [BOXFAILSAFE]    = LOG2(FAILSAFE_MODE),               \
   [BOXNFEMODE]     = LOG2(NFE_RACE_MODE),               \
}                                                        \
/**/

typedef enum {
    GPS_FIX_HOME   = (1 << 0),
    GPS_FIX_AHEAD  = (1 << 5),
    GPS_USABLE_FIX = (1 << 6),
    GPS_FIX        = (1 << 1),
    CALIBRATE_MAG  = (1 << 2),
    SMALL_ANGLE    = (1 << 3),
    FIXED_WING     = (1 << 4)                    // set when in flying_wing or airplane mode. currently used by althold selection code
} stateFlags_t;

#define DISABLE_STATE(mask) (stateFlags &= ~(mask))
#define ENABLE_STATE(mask) (stateFlags |= (mask))
#define STATE(mask) (stateFlags & (mask))

extern sig_atomic_t stateFlags;

uint16_t enableFlightMode(flightModeFlags_e mask);
uint16_t disableFlightMode(flightModeFlags_e mask);

bool sensors(uint32_t mask);
void sensorsSet(uint32_t mask);
void sensorsClear(uint32_t mask);
uint32_t sensorsMask(void);

void mwDisarm(void);

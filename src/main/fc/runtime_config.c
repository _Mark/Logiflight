/*
 * This file is part of Cleanflight and Betaflight.
 *
 * Cleanflight and Betaflight are free software. You can redistribute
 * this software and/or modify this software under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Cleanflight and Betaflight are distributed in the hope that they
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.
 *
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stdint.h>

#include "fc/runtime_config.h"
#include "io/beeper.h"

char const * const
armingDisableFlagNames[ARMING_DISABLE_FLAGS_COUNT] = {
    "NOGYRO",
    "FAILSAFE",
    "RXLOSS",
    "BADRX",
    "BOXFAILSAFE",
    "RUNAWAY",
    "THROTTLE",
    "ANGLE",
    "BOOTGRACE",
    "NOPREARM",
    "LOAD",
    "CALIB",
    "CLI",
    "CMS",
    "OSD",
    "NOTOSS",
    "PARALYZE",
    "GPS",
    "ARMSWITCH"
};

static FAST_RAM_ZERO_INIT sig_atomic_t enabledSensors, armingDisableFlags;
       FAST_RAM_ZERO_INIT sig_atomic_t armingFlags, stateFlags, flightModeFlags;

FAST_CODE_NOINVOKE void setArmingDisabled(
    register armingDisableFlags_e const flag
) {
    armingDisableFlags|= flag;
}

FAST_CODE_NOINVOKE void unsetArmingDisabled(
    register armingDisableFlags_e const flag
) {
    armingDisableFlags&=~flag;
}

FAST_CODE_NOINVOKE bool isArmingDisabled(void) {
    return armingDisableFlags;
}

FAST_CODE_NOINVOKE sig_atomic_t getArmingDisableFlags(void) {
    return armingDisableFlags;
}

/**
 * Enables the given flight mode.  A beep is sounded if the flight mode
 * has changed.  Returns the new 'flightModeFlags' value.
 */
uint16_t enableFlightMode(register flightModeFlags_e const mask) {
    if (ISRVALUE({
        register typeof(flightModeFlags | mask) const
        oldVal = flightModeFlags;

        (flightModeFlags|= mask)!= oldVal;
    }))
        beeperConfirmationBeeps(1);
    return flightModeFlags;
}

/**
 * Disables the given flight mode.  A beep is sounded if the flight mode
 * has changed.  Returns the new 'flightModeFlags' value.
 */
uint16_t disableFlightMode(register flightModeFlags_e const mask) {
    if (ISRVALUE({
        register typeof(flightModeFlags &~mask) const
        oldVal = flightModeFlags;

        (flightModeFlags&=~mask)!= oldVal;
    }))
        beeperConfirmationBeeps(1);
    return flightModeFlags;
}

FAST_CODE_NOINVOKE bool sensors(register uint32_t const mask) {
    return enabledSensors & mask;
}

FAST_CODE_NOINVOKE void sensorsSet(register uint32_t const mask) {
    enabledSensors |= mask;
}

FAST_CODE_NOINVOKE void sensorsClear(register uint32_t const mask) {
    enabledSensors &= ~(mask);
}

FAST_CODE_NOINVOKE uint32_t sensorsMask(void) {
    return enabledSensors;
}

/*
 * This file is part of Cleanflight and Betaflight.
 *
 * Cleanflight and Betaflight are free software. You can redistribute
 * this software and/or modify this software under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Cleanflight and Betaflight are distributed in the hope that they
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.
 *
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <math.h>

#include "platform.h"

#include "build/debug.h"

#include "blackbox/blackbox.h"

#include "common/axis.h"
#include "common/maths.h"
#include "common/utils.h"

#include "config/feature.h"
#include "pg/pg.h"
#include "pg/pg_ids.h"
#include "pg/rx.h"

#include "drivers/dma_spi.h"
#include "drivers/light_led.h"
#include "drivers/sound_beeper.h"
#include "drivers/system.h"
#include "drivers/time.h"
#include "drivers/transponder_ir.h"

#include "sensors/acceleration.h"
#include "sensors/barometer.h"
#include "sensors/battery.h"
#include "sensors/boardalignment.h"
#include "sensors/gyro.h"
#include "sensors/sensors.h"

#include "fc/config.h"
#include "fc/controlrate_profile.h"
#include "fc/fc_core.h"
#include "fc/fc_rc.h"
#include "fc/rc_adjustments.h"
#include "fc/rc_controls.h"
#include "fc/runtime_config.h"

#include "msp/msp_serial.h"

#include "interface/cli.h"

#include "io/beeper.h"
#include "io/gps.h"
#include "io/motors.h"
#include "io/pidaudio.h"
#include "io/servos.h"
#include "io/osd.h"
#include "io/serial.h"
#include "io/statusindicator.h"
#include "io/transponder_ir.h"
#include "io/vtx_control.h"
#include "io/vtx_rtc6705.h"

#include "rx/rx.h"

#include "scheduler/scheduler.h"

#include "telemetry/telemetry.h"

#include "flight/redline.h"
#include "flight/position.h"
#include "flight/failsafe.h"
#include "flight/imu.h"
#include "flight/mixer.h"
#include "flight/pid.h"
#include "flight/servos.h"
#include "flight/gps_rescue.h"


// June 2013     V2.2-dev

enum {
    ALIGN_GYRO = 0,
    ALIGN_ACCEL = 1,
    ALIGN_MAG = 2
};

enum {
    ARMING_DELAYED_DISARMED = 0,
    ARMING_DELAYED_NORMAL = 1,
    ARMING_DELAYED_CRASHFLIP = 2
};

#define GYRO_WATCHDOG_DELAY 80 //  delay for gyro sync

#ifdef USE_RUNAWAY_TAKEOFF
#define RUNAWAY_TAKEOFF_PIDSUM_THRESHOLD         600   // The pidSum threshold required to trigger - corresponds to a pidSum value of 60% (raw 600) in the blackbox viewer
#define RUNAWAY_TAKEOFF_ACTIVATE_DELAY           75000 // (75ms) Time in microseconds where pidSum is above threshold to trigger
#define RUNAWAY_TAKEOFF_DEACTIVATE_STICK_PERCENT 15    // 15% - minimum stick deflection during deactivation phase
#define RUNAWAY_TAKEOFF_DEACTIVATE_PIDSUM_LIMIT  100   // 10.0% - pidSum limit during deactivation phase
#define RUNAWAY_TAKEOFF_GYRO_LIMIT_RP            15    // Roll/pitch 15 deg/sec threshold to prevent triggering during bench testing without props
#define RUNAWAY_TAKEOFF_GYRO_LIMIT_YAW           50    // Yaw 50 deg/sec threshold to prevent triggering during bench testing without props
#define RUNAWAY_TAKEOFF_HIGH_THROTTLE_PERCENT    75    // High throttle limit to accelerate deactivation (halves the deactivation delay)

#define DEBUG_RUNAWAY_TAKEOFF_ENABLED_STATE      0
#define DEBUG_RUNAWAY_TAKEOFF_ACTIVATING_DELAY   1
#define DEBUG_RUNAWAY_TAKEOFF_DEACTIVATING_DELAY 2
#define DEBUG_RUNAWAY_TAKEOFF_DEACTIVATING_TIME  3

#define DEBUG_RUNAWAY_TAKEOFF_TRUE  1
#define DEBUG_RUNAWAY_TAKEOFF_FALSE 0
#endif

#if defined(USE_GPS) || defined(USE_MAG)
FAST_RAM_ZERO_INIT sig_atomic_t magHold;
#endif

#ifdef USE_GYRO_SAMPLING_32KHZ
static FAST_RAM_ZERO_INIT uint8_t  rcUpdateCountdown;
#endif
#ifdef USE_BLACKBOX
static FAST_RAM_ZERO_INIT uint8_t  bblUpdateCountdown;
#endif
static FAST_RAM_ZERO_INIT uint8_t  pidUpdateCountdown;
static FAST_RAM_ZERO_INIT timeUs_t debugTime, disarmAt;
static FAST_RAM_ZERO_INIT int      lastArmingDisabledReason;

static FAST_RAM_ZERO_INIT bool
flipOverAfterCrashMode, airmodeIsActivated, itermIsActivated,
#ifdef USE_RUNAWAY_TAKEOFF
preflightIsActivated,
#endif
motorsAreActivated, armedBeeperOn
#ifdef USE_TELEMETRY
, sharedPortTelemetryEnabled;
#else
;
#endif

#ifdef USE_RUNAWAY_TAKEOFF
static FAST_RAM_ZERO_INIT timeUs_t
runawayTakeoffDeactivateUs, runawayTakeoffAccumulatedUs,
runawayTakeoffTriggerUs;

static FAST_RAM_ZERO_INIT bool         runawayTakeoffCheckDisabled;
static FAST_RAM_ZERO_INIT sig_atomic_t runawayTakeoffCheckInactive,
                                       tryingToArm, lastDisarmTimeUs;
#else
static FAST_RAM_ZERO_INIT sig_atomic_t tryingToArm, lastDisarmTimeUs;
#endif

void applyAndSaveAccelerometerTrimsDelta(rollAndPitchTrims_t *rollAndPitchTrimsDelta) {
    accelerometerConfigMutable()->accelerometerTrims.values.roll += rollAndPitchTrimsDelta->values.roll;
    accelerometerConfigMutable()->accelerometerTrims.values.pitch += rollAndPitchTrimsDelta->values.pitch;
    saveConfigAndNotify();
}

static bool isMotorReverseActive(void) {
    return
#ifdef USE_ACC
    isModeActivationConditionPresent(BOXFLIPOVERAFTERCRASH) ?
#else
        isModeActivationConditionPresent(BOXFLIPOVERAFTERCRASH) &&
#endif
        IS_RC_MODE_ACTIVE(BOXFLIPOVERAFTERCRASH)
#ifdef USE_ACC
    :
        accelerometerConfig()->acc_hardware &&
        signbit(getCosTiltAngle())
#endif
    ;
}

static bool isCalibrating(void) {
return
    // Note: compass calibration is handled completely differently, outside of the main loop, see f.CALIBRATE_MAG
#ifdef USE_BARO
    (sensors(SENSOR_BARO) &&!isBaroCalibrationComplete()) ||
#endif
#ifdef USE_ACC
    (accelerometerConfig()->acc_hardware && isAccCalibrating()) ||
#endif
    isGyroCalibrating();
}

void resetArmingDisabled(void) {
    lastArmingDisabledReason = 0;
}

void updateArmingStatus(void) {
    if (ARMING_FLAG(ARMED)) {
        LED0_ON;
    } else {
        // Check if the power on arming grace time has elapsed
        if ((getArmingDisableFlags() & ARMING_DISABLED_BOOT_GRACE_TIME) && (millis() >= systemConfig()->powerOnArmingGraceTime * 1000)) {
            // If so, unset the grace time arming disable flag
            unsetArmingDisabled(ARMING_DISABLED_BOOT_GRACE_TIME);
        }
        // Clear the crash flip active status
        flipOverAfterCrashMode = false;
        // If switch is used for arming then check it is not defaulting to on when the RX link recovers from a fault
        if (!isUsingSticksForArming()) {
            static bool hadRx = false;
            const bool haveRx = rxIsReceivingSignal();
            const bool justGotRxBack = !hadRx && haveRx;
            if (justGotRxBack && IS_RC_MODE_ACTIVE(BOXARM)) {
                // If the RX has just started to receive a signal again and the arm switch is on, apply arming restriction
                setArmingDisabled(ARMING_DISABLED_BAD_RX_RECOVERY);
            } else if (haveRx && !IS_RC_MODE_ACTIVE(BOXARM)) {
                // If RX signal is OK and the arm switch is off, remove arming restriction
                unsetArmingDisabled(ARMING_DISABLED_BAD_RX_RECOVERY);
            }
            hadRx = haveRx;
        }
        (   IS_RC_MODE_ACTIVE(BOXFAILSAFE) ?
                setArmingDisabled : unsetArmingDisabled
        )(ARMING_DISABLED_BOXFAILSAFE);
        (calculateThrottleStatus() ? setArmingDisabled : unsetArmingDisabled)(
            ARMING_DISABLED_THROTTLE
        );
        (   STATE(SMALL_ANGLE) || IS_RC_MODE_ACTIVE(BOXFLIPOVERAFTERCRASH) ?
                unsetArmingDisabled : setArmingDisabled
        )(ARMING_DISABLED_ANGLE);
        (   averageSystemLoadPercent < LOAD_PERCENTAGE_ONE ?
                unsetArmingDisabled : setArmingDisabled
        )(ARMING_DISABLED_LOAD);
        (isCalibrating() ? setArmingDisabled : unsetArmingDisabled)(
            ARMING_DISABLED_CALIBRATING
        );
#ifdef USE_GPS_RESCUE
        (  !isModeActivationConditionPresent(BOXGPSRESCUE   )  ||
            isModeActivationConditionPresent(BOXBEEPGPSCOUNT)  ||
            osdConfig()->item_pos[OSD_GPS_SATS] & VISIBLE_FLAG ||
            isGpsUsable()
            ? unsetArmingDisabled : setArmingDisabled
        )(ARMING_DISABLED_GPS);
#endif
        if (isModeActivationConditionPresent(BOXPREARM))
            (   ARMING_FLAG(WAS_ARMED_WITH_PREARM) ||
               !IS_RC_MODE_ACTIVE(BOXPREARM) ?
                    setArmingDisabled : unsetArmingDisabled
            )(ARMING_DISABLED_NOPREARM);
        if (!isUsingSticksForArming()) {
            /* Ignore ARMING_DISABLED_CALIBRATING if we are going to calibrate gyro on first arm */
            bool ignoreGyro = armingConfig()->gyro_cal_on_first_arm
                              && !(getArmingDisableFlags() & ~(ARMING_DISABLED_ARM_SWITCH | ARMING_DISABLED_CALIBRATING));
            /* Ignore ARMING_DISABLED_THROTTLE (once arm switch is on) if we are in 3D mode */
            bool ignoreThrottle = feature(FEATURE_3D)
                                  && !IS_RC_MODE_ACTIVE(BOX3D)
                                  && !flight3DConfig()->switched_mode3d
                                  && !(getArmingDisableFlags() & ~(ARMING_DISABLED_ARM_SWITCH | ARMING_DISABLED_THROTTLE));
#ifdef USE_RUNAWAY_TAKEOFF
            if (!IS_RC_MODE_ACTIVE(BOXARM)) {
                unsetArmingDisabled(ARMING_DISABLED_RUNAWAY_TAKEOFF);
            }
#endif
            // If arming is disabled and the ARM switch is on
            if (isArmingDisabled()
                    && !ignoreGyro
                    && !ignoreThrottle
                    && IS_RC_MODE_ACTIVE(BOXARM)) {
                setArmingDisabled(ARMING_DISABLED_ARM_SWITCH);
            } else if (!IS_RC_MODE_ACTIVE(BOXARM)) {
                unsetArmingDisabled(ARMING_DISABLED_ARM_SWITCH);
            }
        }
        if (isArmingDisabled()) {
            warningLedFlash();
        } else {
            warningLedDisable();
        }
        warningLedUpdate();
    }
}

void disarm(void) {
    if (ARMING_FLAG(ARMED)) {
        DISABLE_ARMING_FLAG(ARMED);
        lastDisarmTimeUs = micros();
#ifdef USE_BLACKBOX
        if (blackboxConfig()->device && blackboxConfig()->mode != BLACKBOX_MODE_ALWAYS_ON) { // Close the log upon disarm except when logging mode is ALWAYS ON
            blackboxFinish();
        }
#endif
        BEEP_OFF;
#ifdef USE_DSHOT
        if (isMotorProtocolDshot() && flipOverAfterCrashMode && !feature(FEATURE_3D)) {
            pwmWriteDshotCommand(ALL_MOTORS, getMotorCount(), DSHOT_CMD_SPIN_DIRECTION_NORMAL, false);
        }
#endif
        flipOverAfterCrashMode = false;
        // if ARMING_DISABLED_RUNAWAY_TAKEOFF is set then we want to play it's beep pattern instead
        if (!(getArmingDisableFlags() & ARMING_DISABLED_RUNAWAY_TAKEOFF)) {
            beeper(BEEPER_DISARMING);      // emit disarm tone
        }
    }
}

void tryArm(void) {
    if (armingConfig()->gyro_cal_on_first_arm) {
        gyroStartCalibration(true);
    }
    updateArmingStatus();
    if (!isArmingDisabled()) {
        if (ARMING_FLAG(ARMED)) {
            return;
        }
        if (isBatteryVoltageConfigured())
            batteryReInit();
#ifdef USE_BARO
        if (sensors(SENSOR_BARO))
            performBaroCalibrationCycle();
#endif
#ifdef USE_ACC
        acc.accNormMax = acc.accNormGnd = acc.accNorm;
#endif
        register
        const timeUs_t currentTimeUs = micros();
#ifdef USE_DSHOT
        if (currentTimeUs - getLastDshotBeaconCommandTimeUs() < DSHOT_BEACON_GUARD_DELAY_US) {
            if (tryingToArm== ARMING_DELAYED_DISARMED)
                tryingToArm = (
                    isMotorReverseActive() ?
                        ARMING_DELAYED_CRASHFLIP : ARMING_DELAYED_NORMAL
                );
            return;
        }
        if (isMotorProtocolDshot())
            pwmWriteDshotCommand(
                ALL_MOTORS
            ,
                getMotorCount()
            ,
                (   flipOverAfterCrashMode =
                        tryingToArm== ARMING_DELAYED_CRASHFLIP ||
                        isMotorReverseActive()
                ) ?
#ifdef USE_RUNAWAY_TAKEOFF
                    runawayTakeoffCheckDisabled = false,
#endif
                    DSHOT_CMD_SPIN_DIRECTION_REVERSED
                :
                    DSHOT_CMD_SPIN_DIRECTION_NORMAL
            ,
                false
            );
#endif
        ENABLE_ARMING_FLAG(ARMED | WAS_EVER_ARMED);
        resetTryingToArm();
        if (isModeActivationConditionPresent(BOXPREARM)) {
            ENABLE_ARMING_FLAG(WAS_ARMED_WITH_PREARM);
        }
        disarmAt =
            armingConfig()->auto_disarm_delay * 1000000ul + currentTimeUs;
        lastArmingDisabledReason = 0;
        //beep to indicate arming
        beeper(
#ifdef USE_GPS
            isGpsCourseUsable() ? BEEPER_ARMING_GPS_FIX : BEEPER_ARMING
#else
            BEEPER_ARMING
#endif
        );
#ifdef USE_RUNAWAY_TAKEOFF
        runawayTakeoffDeactivateUs = 0;
        runawayTakeoffAccumulatedUs = 0;
        runawayTakeoffTriggerUs = 0;
#endif
    } else {
        resetTryingToArm();
        if (!isFirstArmingGyroCalibrationRunning()) {
            int armingDisabledReason = ffs(getArmingDisableFlags());
            if (lastArmingDisabledReason != armingDisabledReason) {
                lastArmingDisabledReason = armingDisabledReason;
                beeperWarningBeeps(armingDisabledReason);
            }
        }
    }
}

// Automatic ACC Offset Calibration
bool AccInflightCalibrationArmed = false;
bool AccInflightCalibrationMeasurementDone = false;
bool AccInflightCalibrationSavetoEEProm = false;
bool AccInflightCalibrationActive = false;
uint16_t InflightcalibratingA = 0;

void handleInflightCalibrationStickPosition(void) {
    if (AccInflightCalibrationMeasurementDone) {
        // trigger saving into eeprom after landing
        AccInflightCalibrationMeasurementDone = false;
        AccInflightCalibrationSavetoEEProm = true;
    } else {
        AccInflightCalibrationArmed = !AccInflightCalibrationArmed;
        if (AccInflightCalibrationArmed) {
            beeper(BEEPER_ACC_CALIBRATION);
        } else {
            beeper(BEEPER_ACC_CALIBRATION_FAIL);
        }
    }
}

static void updateInflightCalibrationState(void) {
    if (AccInflightCalibrationArmed && ARMING_FLAG(ARMED) && rcData[THROTTLE] > rxConfig()->mincheck && !IS_RC_MODE_ACTIVE(BOXARM)) {   // Copter is airborne and you are turning it off via boxarm : start measurement
        InflightcalibratingA = 50;
        AccInflightCalibrationArmed = false;
    }
    if (IS_RC_MODE_ACTIVE(BOXCALIB)) {      // Use the Calib Option to activate : Calib = TRUE measurement started, Land and Calib = 0 measurement stored
        if (!AccInflightCalibrationActive && !AccInflightCalibrationMeasurementDone)
            InflightcalibratingA = 50;
        AccInflightCalibrationActive = true;
    } else if (AccInflightCalibrationMeasurementDone && !ARMING_FLAG(ARMED)) {
        AccInflightCalibrationMeasurementDone = false;
        AccInflightCalibrationSavetoEEProm = true;
    }
}

#if defined(USE_GPS) || defined(USE_MAG)
static FAST_CODE_NOINVOKE void updateMagHold(void) {
    {   register uint16_t const
        STR = flightModeFlags;

 #ifdef USE_GPS_RESCUE
        if (STR & GPS_RESCUE_MODE) NOOP else
 #endif
 #ifdef USE_ACC
        if (STR & RANGEFINDER_MODE)
            setBearing(
                (   (int16_t)lrintf(
                        ldexpf(getRcDeflection(YAW), 3) * 225.f
                    ) + attitude.values.slant + u'\xE10'
                ) % u'\xE10'
            );
        else
 #endif
        if (!STR || rcCommand[YAW]) {
            setBearing(attitude.values.yaw);
            return;
        }
    }

    rcCommand[YAW] =
        constrainf(
            ldexpf(
                (   (int32_t)ISRVALUE({
                        register int16_t const
                        DELTA = (
                            rcControlsConfig()->yaw_control_reversed ?
                                magHold - attitude.values.yaw
                            :
                                attitude.values.yaw - magHold
                        );

                        DELTA < u'\x708' ? DELTA : u'\x708' - DELTA;
                    }) * currentPidProfile->pid[PID_MAG].P
                ) / 15.f
            ,
               -1
            )
        ,
           -PWM_RANGE_2
        ,
            PWM_RANGE_2
        );
}
#endif // USE_GPS || USE_MAG

#if defined(USE_ACC) || defined(USE_ALT_HOLD) || defined(USE_GPS_RESCUE)
static FAST_CODE_NOINVOKE void updateClimbHold(void) {
 #ifdef USE_GPS_RESCUE
    if (FLIGHT_MODE(GPS_RESCUE_MODE)) NOOP else
 #endif
    rescueThrottle = powf(
 #ifdef USE_ALT_HOLD
        FLIGHT_MODE(BARO_MODE) ? climbRateController(
            rquot(ISRVALUE({
                register int32_t
                TMP =
                    (   (TMP = redlineConfig()->varioRedlineAltitude) &&
                        (TMP = TMP * 100l - rescueState.sensor.currentAltitude)
                            < 1500l
                        ?
                            rquot(TMP * PWM_RANGE_2, 1500l) + -PWM_RANGE_MAX
                        :
                           -PWM_RANGE_MIDDLE
                    ) + (long)rcCommand[THROTTLE];

                (TMP>= false)[redlineConfig()->varioSetpointLimit] * TMP;
            }),
                PWM_RANGE_2
            ) - rescueState.sensor.currentVario
        ) :
 #endif // USE_ALT_HOLD
    ISRVALUE({
        register float const
        TIME = rcCommand[THROTTLE];

        FLIGHT_MODE(HORIZON_MODE) ? TIME : attitude.ratios.w.w * TIME;
    }),
        // Note: T varies with w^2, w varies with P^(2/3), and P varies with V
        .75
    );

    //DEBUG_SET(DEBUG_ALTITUDE, 3, lrintf(rescueThrottle));
}
#endif // USE_ALT_HOLD || USE_GPS_RESCUE

#ifdef USE_VTX_CONTROL
static bool canUpdateVTX(void) {
#ifdef USE_VTX_RTC6705
    return vtxRTC6705CanUpdate();
#endif
    return true;
}
#endif

#ifdef USE_RUNAWAY_TAKEOFF
// determine if the R/P/Y stick deflection exceeds the specified limit - integer math is good enough here.
bool areSticksActive(uint8_t stickPercentLimit) {
    for (int axis = FD_ROLL; axis <= FD_YAW; axis ++) {
        const uint8_t deadband = axis == FD_YAW ? rcControlsConfig()->yaw_deadband : rcControlsConfig()->deadband;
        uint8_t stickPercent = 0;
        if ((rcData[axis] >= PWM_RANGE_MAX) || (rcData[axis] <= PWM_RANGE_MIN)) {
            stickPercent = 100;
        } else {
            if (rcData[axis] > (rxConfig()->midrc + deadband)) {
                stickPercent = ((rcData[axis] - rxConfig()->midrc - deadband) * 100) / (PWM_RANGE_MAX - rxConfig()->midrc - deadband);
            } else if (rcData[axis] < (rxConfig()->midrc - deadband)) {
                stickPercent = ((rxConfig()->midrc - deadband - rcData[axis]) * 100) / (rxConfig()->midrc - deadband - PWM_RANGE_MIN);
            }
        }
        if (stickPercent >= stickPercentLimit) {
            return true;
        }
    }
    return false;
}


// allow temporarily disabling runaway takeoff prevention if we are connected
// to the configurator and the ARMING_DISABLED_MSP flag is cleared.
void runawayTakeoffTemporaryDisable(register bool const disableFlag) {
    runawayTakeoffCheckInactive = disableFlag || runawayTakeoffCheckDisabled;
}
#endif


// calculate the throttle stick percent - integer math is good enough here.
int8_t calculateThrottlePercent(void) {
    uint8_t ret = 0;
    if (feature(FEATURE_3D)
            && !IS_RC_MODE_ACTIVE(BOX3D)
            && !flight3DConfig()->switched_mode3d) {
        if ((rcData[THROTTLE] >= PWM_RANGE_MAX) || (rcData[THROTTLE] <= PWM_RANGE_MIN)) {
            ret = 100;
        } else {
            if (rcData[THROTTLE] > (rxConfig()->midrc + flight3DConfig()->deadband3d_throttle)) {
                ret = ((rcData[THROTTLE] - rxConfig()->midrc - flight3DConfig()->deadband3d_throttle) * 100) / (PWM_RANGE_MAX - rxConfig()->midrc - flight3DConfig()->deadband3d_throttle);
            } else if (rcData[THROTTLE] < (rxConfig()->midrc - flight3DConfig()->deadband3d_throttle)) {
                ret = ((rxConfig()->midrc - flight3DConfig()->deadband3d_throttle - rcData[THROTTLE]) * 100) / (rxConfig()->midrc - flight3DConfig()->deadband3d_throttle - PWM_RANGE_MIN);
            }
        }
    } else {
        ret = constrain(((rcData[THROTTLE] - rxConfig()->mincheck) * 100) / (PWM_RANGE_MAX - rxConfig()->mincheck), 0, 100);
    }
    return ret;
}

uint8_t calculateThrottlePercentAbs(void) {
    return ABS(calculateThrottlePercent());
}

bool isAirmodeActivated() {
    return airmodeIsActivated;
}

bool isItermActivated() {
    return itermIsActivated;
}

bool areMotorsActivated() {
    return motorsAreActivated;
}

#define toggleMode(mode, condition)\
    switch ((bool)(condition) - (bool)FLIGHT_MODE((mode))) {\
        case true:\
         ENABLE_FLIGHT_MODE((mode));\
        break;\
\
        case-true:\
        DISABLE_FLIGHT_MODE((mode));\
        FALLTHROUGH;\
\
        default:\
        break;\
    }

/*
 * processRx called from taskUpdateRxMain
 */
FAST_CODE_NOINVOKE bool processRx(
    register timeUs_t const currentTimeUs
) {
    if (!calculateRxChannelsAndUpdateFailsafe(currentTimeUs)) {
        return false;
    }
    updateRcRefreshRate(currentTimeUs);
    updateRSSI(currentTimeUs);
    if (currentTimeUs > FAILSAFE_POWER_ON_DELAY_US && !failsafeIsMonitoring()) {
        failsafeStartMonitoring();
    }
    if (failsafeIsMonitoring())
        failsafeUpdateState();
#ifdef USE_RUNAWAY_TAKEOFF
    preflightIsActivated = (
#endif
    itermIsActivated = (
        ARMING_FLAG(ARMED) ?
           (airmodeIsActivated = isAirmodeActive()) ||
            currentPidProfile->pidAtMinThrottle ||
            calculateThrottleStatus()
        : (
            airmodeIsActivated = false,
            false
        )
#ifdef USE_RUNAWAY_TAKEOFF
    )) &&
    pidConfig()->runaway_takeoff_prevention &&!(
        runawayTakeoffCheckInactive ||
        STATE(FIXED_WING)
#endif
    );
    register
    const throttleStatus_e throttleStatus = calculateThrottleStatus();
    register
    const uint8_t throttlePercent = calculateThrottlePercentAbs();
#ifdef USE_RUNAWAY_TAKEOFF
    // If runaway_takeoff_prevention is enabled, accumulate the amount of time that throttle
    // is above runaway_takeoff_deactivate_throttle with the any of the R/P/Y sticks deflected
    // to at least runaway_takeoff_stick_percent percent while the pidSum on all axis is kept low.
    // Once the amount of accumulated time exceeds runaway_takeoff_deactivate_delay then disable
    // prevention for the remainder of the battery.
    if (preflightIsActivated) {
        // Determine if we're in "flight"
        //   - motors running
        //   - throttle over runaway_takeoff_deactivate_throttle_percent
        //   - sticks are active and have deflection greater than runaway_takeoff_deactivate_stick_percent
        //   - pidSum on all axis is less then runaway_takeoff_deactivate_pidlimit
        bool inStableFlight = false;
        if (!feature(FEATURE_MOTOR_STOP) || isAirmodeActive() || (throttleStatus != THROTTLE_LOW)) { // are motors running?
            const uint8_t lowThrottleLimit = pidConfig()->runaway_takeoff_deactivate_throttle;
            const uint8_t midThrottleLimit = constrain(lowThrottleLimit * 2, lowThrottleLimit * 2, RUNAWAY_TAKEOFF_HIGH_THROTTLE_PERCENT);
            if ((((throttlePercent >= lowThrottleLimit) && areSticksActive(RUNAWAY_TAKEOFF_DEACTIVATE_STICK_PERCENT)) || (throttlePercent >= midThrottleLimit))
                    && (fabsf(pidData[FD_PITCH].Sum) < RUNAWAY_TAKEOFF_DEACTIVATE_PIDSUM_LIMIT)
                    && (fabsf(pidData[FD_ROLL].Sum) < RUNAWAY_TAKEOFF_DEACTIVATE_PIDSUM_LIMIT)
                    && (fabsf(pidData[FD_YAW].Sum) < RUNAWAY_TAKEOFF_DEACTIVATE_PIDSUM_LIMIT)) {
                inStableFlight = true;
                if (runawayTakeoffDeactivateUs == 0) {
                    runawayTakeoffDeactivateUs = currentTimeUs;
                }
            }
        }
        // If we're in flight, then accumulate the time and deactivate once it exceeds runaway_takeoff_deactivate_delay milliseconds
        if (inStableFlight) {
            if (runawayTakeoffDeactivateUs == 0) {
                runawayTakeoffDeactivateUs = currentTimeUs;
            }
            uint16_t deactivateDelay = pidConfig()->runaway_takeoff_deactivate_delay;
            // at high throttle levels reduce deactivation delay by 50%
            if (throttlePercent >= RUNAWAY_TAKEOFF_HIGH_THROTTLE_PERCENT) {
                deactivateDelay = deactivateDelay / 2;
            }
            if ((cmpTimeUs(currentTimeUs, runawayTakeoffDeactivateUs) + runawayTakeoffAccumulatedUs) > deactivateDelay * 1000) {
                runawayTakeoffCheckInactive =
                runawayTakeoffCheckDisabled = true;
            }
        } else {
            if (runawayTakeoffDeactivateUs != 0) {
                runawayTakeoffAccumulatedUs += cmpTimeUs(currentTimeUs, runawayTakeoffDeactivateUs);
            }
            runawayTakeoffDeactivateUs = 0;
        }
        if (runawayTakeoffDeactivateUs == 0) {
            DEBUG_SET(DEBUG_RUNAWAY_TAKEOFF, DEBUG_RUNAWAY_TAKEOFF_DEACTIVATING_DELAY, DEBUG_RUNAWAY_TAKEOFF_FALSE);
            DEBUG_SET(DEBUG_RUNAWAY_TAKEOFF, DEBUG_RUNAWAY_TAKEOFF_DEACTIVATING_TIME, runawayTakeoffAccumulatedUs / 1000);
        } else {
            DEBUG_SET(DEBUG_RUNAWAY_TAKEOFF, DEBUG_RUNAWAY_TAKEOFF_DEACTIVATING_DELAY, DEBUG_RUNAWAY_TAKEOFF_TRUE);
            DEBUG_SET(DEBUG_RUNAWAY_TAKEOFF, DEBUG_RUNAWAY_TAKEOFF_DEACTIVATING_TIME, (cmpTimeUs(currentTimeUs, runawayTakeoffDeactivateUs) + runawayTakeoffAccumulatedUs) / 1000);
        }
    } else {
        DEBUG_SET(DEBUG_RUNAWAY_TAKEOFF, DEBUG_RUNAWAY_TAKEOFF_DEACTIVATING_DELAY, DEBUG_RUNAWAY_TAKEOFF_FALSE);
        DEBUG_SET(DEBUG_RUNAWAY_TAKEOFF, DEBUG_RUNAWAY_TAKEOFF_DEACTIVATING_TIME, DEBUG_RUNAWAY_TAKEOFF_FALSE);
    }
#endif
    // When armed and motors aren't spinning, do beeps and then disarm
    // board after delay so users without buzzer won't lose fingers.
    // mixTable constrains motor commands, so checking  throttleStatus is enough
    register
    const timeUs_t autoDisarmDelayUs = armingConfig()->auto_disarm_delay * 1e6;
    if ((
        motorsAreActivated =
            (   itermIsActivated ||
               !flipOverAfterCrashMode ||
                feature(FEATURE_MOTOR_STOP | FEATURE_3D) ^ FEATURE_MOTOR_STOP ||
                FLIGHT_MODE(GPS_RESCUE_MODE)
            ) && pwmAreMotorsEnabled()
    ))
        disarmAt = currentTimeUs + autoDisarmDelayUs;
    else if (isUsingSticksForArming())
        if (throttleStatus && (
                disarmAt = currentTimeUs + autoDisarmDelayUs,
                armedBeeperOn
            )
        )
            beeperSilence(),
            armedBeeperOn = false;
        else
                if ((autoDisarmDelayUs > 0) && (currentTimeUs > disarmAt)) {
                    // auto-disarm configured and delay is over
                    disarm();
                    armedBeeperOn = false;
                } else {
                    // still armed; do warning beeps while armed
                    beeper(BEEPER_ARMED);
                    armedBeeperOn = true;
                }
    else
            // arming is via AUX switch; beep while throttle low
            if (!throttleStatus) {
                beeper(BEEPER_ARMED);
                armedBeeperOn = true;
            } else if (armedBeeperOn) {
                beeperSilence();
                armedBeeperOn = false;
            }
    processRcStickPositions();
    if (feature(FEATURE_INFLIGHT_ACC_CAL)) {
        updateInflightCalibrationState();
    }
    updateActivatedModes();
    if (IS_RC_MODE_ACTIVE(BOXPARALYZE)) {
        setArmingDisabled(ARMING_DISABLED_BOOT_GRACE_TIME);

        if (ARMING_FLAG(ARMED))
            disarm();

        mixerResetDisarmedMotors();
        stopPwmAllMotors();
        systemReset();
    }
#ifdef USE_DSHOT
    /* Enable beep warning when the crash flip mode is active */
    if (isMotorProtocolDshot() && isMotorReverseActive())
        beeper(BEEPER_CRASH_FLIP_MODE);
#endif
    if (!cliMode) {
        updateAdjustmentStates();
        processRcAdjustments(currentControlRateProfile);
    }

#ifdef USE_ACC
    if (sensors(SENSOR_ACC)) {
        #define resetFlightMode(x, y)\
        if (ISRVALUE({\
                register sig_atomic_t const\
                flags = flightModeFlags ^ (y);\
\
                if (flags & (y))\
                    ENABLE_FLIGHT_MODE(y);\
\
                flags;\
            }) & (x)\
        )\
            DISABLE_FLIGHT_MODE(x);\
        else NOOP

 #ifdef USE_GPS_RESCUE
        if (IS_RC_MODE_ACTIVE(BOXGPSRESCUE) || failsafeRthIsActive())
            resetFlightMode(
                ANGLE_MODE | HORIZON_MODE | NFE_RACE_MODE, GPS_RESCUE_MODE
            )
        else
 #endif
        if (failsafeIsActive())
            resetFlightMode(
                ANGLE_MODE | HORIZON_MODE | GPS_RESCUE_MODE | NFE_RACE_MODE
            ,
                GPS_HOLD_MODE
            )
        else if (IS_RC_MODE_ACTIVE(BOXHORIZON))
            resetFlightMode(
                ANGLE_MODE | GPS_RESCUE_MODE | GPS_HOLD_MODE | NFE_RACE_MODE
            ,
                HORIZON_MODE
            )
        else if (IS_RC_MODE_ACTIVE(BOXANGLE))
            resetFlightMode(
                HORIZON_MODE | GPS_RESCUE_MODE | GPS_HOLD_MODE | NFE_RACE_MODE
            ,
                ANGLE_MODE
            )
        else if (IS_RC_MODE_ACTIVE(BOXNFEMODE))
            resetFlightMode(
                ANGLE_MODE | HORIZON_MODE | GPS_RESCUE_MODE | GPS_HOLD_MODE
            ,
                NFE_RACE_MODE
            )
        else
            goto DO_DISABLE_ACC_MODE;

        if (IS_RC_MODE_ACTIVE(BOXHEADFREE))
            if (IS_RC_MODE_ACTIVE(BOXHEADADJ))
                if (~flightModeFlags & (HEADFREE_MODE | LOOKDOWN_MODE))
                    ENABLE_FLIGHT_MODE(HEADFREE_MODE | LOOKDOWN_MODE);
            else
                resetFlightMode(LOOKDOWN_MODE, HEADFREE_MODE)
        else if (FLIGHT_MODE(HEADFREE_MODE | LOOKDOWN_MODE))
            DISABLE_FLIGHT_MODE(HEADFREE_MODE | LOOKDOWN_MODE);

        #undef resetFlightMode

        LED1_ON;
    } else
DO_DISABLE_ACC_MODE:
        if (FLIGHT_MODE(ACC_MODE | HEADFREE_MODE | LOOKDOWN_MODE))
            DISABLE_FLIGHT_MODE(ACC_MODE | HEADFREE_MODE | LOOKDOWN_MODE),
            LED1_OFF;
#endif // USE_ACC

    if (!IS_RC_MODE_ACTIVE(BOXPREARM) && ARMING_FLAG(WAS_ARMED_WITH_PREARM)) {
        DISABLE_ARMING_FLAG(WAS_ARMED_WITH_PREARM);
    }

#ifdef USE_GPS
    if (IS_RC_MODE_ACTIVE(BOXBEEPGPSCOUNT) && isGpsUsable())
        GPS_reset_home_position(),
        beeper(BEEPER_RX_SET);
#endif

#if defined(USE_ALT_HOLD) && (defined(USE_BARO) || defined(USE_GPS))
    toggleMode(
        BARO_MODE
    ,
        IS_RC_MODE_ACTIVE(BOXFPVANGLEMIX) &&
 #if   defined(USE_BARO) && defined(USE_GPS)
        (   sensors(SENSOR_BARO) ||
            isGpsUsable()
        )
 #elif defined(USE_BARO)
        sensors(SENSOR_BARO)
 #else
        isGpsUsable()
 #endif
    )
#endif // USE_ALT_HOLD && (USE_BARO || USE_GPS)

#ifdef USE_MAG
    toggleMode(MAG_MODE, IS_RC_MODE_ACTIVE(BOXMAG) && sensors(SENSOR_MAG))
#endif

    toggleMode(PASSTHRU_MODE, IS_RC_MODE_ACTIVE(BOXPASSTHRU))

    if (mixerConfig()->mixerMode == MIXER_FLYING_WING || mixerConfig()->mixerMode == MIXER_AIRPLANE) {
        DISABLE_FLIGHT_MODE(HEADFREE_MODE);
    }
#ifdef USE_TELEMETRY
    if (feature(FEATURE_TELEMETRY)) {
        bool enableSharedPortTelemetry = (!isModeActivationConditionPresent(BOXTELEMETRY) && ARMING_FLAG(ARMED)) || (isModeActivationConditionPresent(BOXTELEMETRY) && IS_RC_MODE_ACTIVE(BOXTELEMETRY));
        if (enableSharedPortTelemetry && !sharedPortTelemetryEnabled) {
            mspSerialReleaseSharedTelemetryPorts();
            telemetryCheckState();
            sharedPortTelemetryEnabled = true;
        } else if (!enableSharedPortTelemetry && sharedPortTelemetryEnabled) {
            // the telemetry state must be checked immediately so that shared serial ports are released.
            telemetryCheckState();
            mspSerialAllocatePorts();
            sharedPortTelemetryEnabled = false;
        }
    }
#endif
#ifdef USE_VTX_CONTROL
    vtxUpdateActivatedChannel();
    if (canUpdateVTX()) {
        handleVTXControlButton();
    }
#endif
#ifdef USE_RC_SMOOTHING_FILTER
    if (ARMING_FLAG(ARMED) && !rcSmoothingInitializationComplete()) {
        beeper(BEEPER_RC_SMOOTHING_INIT_FAIL);
    }
#endif
    return true;
}

static FAST_CODE_NOINVOKE void subTaskPidController(
    register timeUs_t const currentTimeUs
) {
    // PID - note this is function pointer set by setPIDController()
    {   register timeUs_t const
        startTime = (debugMode== DEBUG_PIDLOOP ? micros() : currentTimeUs);

        pidController(
            currentPidProfile
        ,
            accelerometerConfig()->accelerometerTrims.raw
        ,
            startTime
        );

        DEBUG_SET(DEBUG_PIDLOOP, 1, micros() - startTime);
    }
#ifdef USE_RUNAWAY_TAKEOFF
    // Check to see if runaway takeoff detection is active (anti-taz), the pidSum is over the threshold,
    // and gyro rate for any axis is above the limit for at least the activate delay period.
    // If so, disarm for safety
    if (preflightIsActivated) {
        if (((fabsf(pidData[FD_PITCH].Sum) >= RUNAWAY_TAKEOFF_PIDSUM_THRESHOLD)
                || (fabsf(pidData[FD_ROLL].Sum) >= RUNAWAY_TAKEOFF_PIDSUM_THRESHOLD)
                || (fabsf(pidData[FD_YAW].Sum) >= RUNAWAY_TAKEOFF_PIDSUM_THRESHOLD))
            && (
                gyroAbsRateDps(FD_ROLL ) > RUNAWAY_TAKEOFF_GYRO_LIMIT_RP ||
                gyroAbsRateDps(FD_PITCH) > RUNAWAY_TAKEOFF_GYRO_LIMIT_RP ||
                gyroAbsRateDps(FD_YAW  ) > RUNAWAY_TAKEOFF_GYRO_LIMIT_YAW
            )
        ) {
            if (runawayTakeoffTriggerUs == 0) {
                runawayTakeoffTriggerUs = currentTimeUs + RUNAWAY_TAKEOFF_ACTIVATE_DELAY;
            } else if (currentTimeUs > runawayTakeoffTriggerUs) {
                setArmingDisabled(ARMING_DISABLED_RUNAWAY_TAKEOFF);
                disarm();
            }
        } else {
            runawayTakeoffTriggerUs = 0;
        }
        DEBUG_SET(DEBUG_RUNAWAY_TAKEOFF, DEBUG_RUNAWAY_TAKEOFF_ENABLED_STATE, DEBUG_RUNAWAY_TAKEOFF_TRUE);
        DEBUG_SET(DEBUG_RUNAWAY_TAKEOFF, DEBUG_RUNAWAY_TAKEOFF_ACTIVATING_DELAY, runawayTakeoffTriggerUs == 0 ? DEBUG_RUNAWAY_TAKEOFF_FALSE : DEBUG_RUNAWAY_TAKEOFF_TRUE);
    } else {
        runawayTakeoffTriggerUs = 0;
        DEBUG_SET(DEBUG_RUNAWAY_TAKEOFF, DEBUG_RUNAWAY_TAKEOFF_ENABLED_STATE, DEBUG_RUNAWAY_TAKEOFF_FALSE);
        DEBUG_SET(DEBUG_RUNAWAY_TAKEOFF, DEBUG_RUNAWAY_TAKEOFF_ACTIVATING_DELAY, DEBUG_RUNAWAY_TAKEOFF_FALSE);
    }
#endif
#ifdef USE_PID_AUDIO
    if (isModeActivationConditionPresent(BOXPIDAUDIO)) {
        pidAudioUpdate();
    }
#endif
}

static FAST_CODE_NOINVOKE void subTaskPidSubprocesses(void) {
    register uint32_t const
    startTime = (debugMode== DEBUG_PIDLOOP ? micros() : startTime);

#ifdef USE_GPS_RESCUE
    if ((   FLIGHT_MODE(ANGLE_MODE | HORIZON_MODE | GPS_RESCUE_MODE)
                ^ GPS_RESCUE_MODE
        ) > GPS_RESCUE_MODE
    )
#else
    if (FLIGHT_MODE(ANGLE_MODE | HORIZON_MODE))
#endif
        gpsRescueDeflection.y = getRcDeflection(ROLL ),
        gpsRescueDeflection.x = getRcDeflection(PITCH);

#if defined(USE_MAG) || defined(USE_GPS)
    updateMagHold();
#endif

#if defined(USE_ACC) || defined(USE_ALT_HOLD) || defined(USE_GPS_RESCUE)
 #ifdef USE_ACC
    if (attitude.ratios.w.w)
 #endif
    updateClimbHold();
#endif

    DEBUG_SET(DEBUG_PIDLOOP, 3, micros() - startTime);
}

void taskUpdateMotors(
    register timeUs_t const currentTimeUs
) {
    switch (debugMode) {
        case DEBUG_PIDLOOP:
        debugTime = currentTimeUs;
        break;

        case DEBUG_CYCLETIME:
    {   register timeUs_t
        deltaT = debugTime;

        debug[2] = deltaT = (debugTime = currentTimeUs) - deltaT;
        debug[3] = rquot(1000000ul, deltaT);
    }
        FALLTHROUGH;

        default:
        break;
    }
    mixTable();
#ifdef USE_SERVOS
    // motor outputs are used as sources for servo mixing, so motors must be calculated using mixTable() before servos.
    if (isMixerUsingServos()) {
        writeServos();
    }
#endif
    writeMotors();
    DEBUG_SET(DEBUG_PIDLOOP, 2, micros() - debugTime);
}

static FAST_CODE_NOINVOKE void subTaskRcCommand(void) {
    // If we're armed, at minimum throttle, and we do arming via the
    // sticks, do not process yaw input from the rx.  We do this so the
    // motors do not spin up while we are trying to arm or disarm.
    // Allow yaw control for tricopters if the user wants the servo to move even when unarmed.
#ifndef USE_QUAD_MIXER_ONLY
    switch (mixerConfig()->mixerMode) {
#ifdef USE_SERVOS
        case MIXER_TRI:
        case MIXER_CUSTOM_TRI:
        if (servoConfig()->tri_unarmed_servo)
            break;
        FALLTHROUGH;
#endif

        default:
#endif
    if (isUsingSticksForArming() &&!calculateThrottleStatus())
        resetYawAxis();
#ifndef USE_QUAD_MIXER_ONLY
        FALLTHROUGH;

        case MIXER_FLYING_WING:
        case MIXER_AIRPLANE:
        break;
    }
#endif
}

// Function for loop trigger
FAST_CODE_NOINVOKE void taskMainPidLoop(register timeUs_t const currentTimeUs) {
#ifdef USE_DMA_SPI_DEVICE
    dmaSpiDeviceDataReady = false;
#endif
#if defined(SIMULATOR_BUILD) && defined(SIMULATOR_GYROPID_SYNC)
    if (lockMainPID())
        return;
#endif

    // DEBUG_PIDLOOP, timings for:
    // 0 - imuUpdateAttitude()
    // 1 - pidController()
    // 2 - subTaskMotorUpdate()
    // 3 - subTaskMainSubprocesses()
    //when using dma, this shouldn't run until the dma spi transfer flag is complete
    //gyroUpdateSensor in gyro.c is called by gyroUpdate

    register bool const
    isGyroDataUsable = imuUpdateAttitude(currentTimeUs);

   if (debugMode== DEBUG_PIDLOOP)
        debug[0] = micros() - currentTimeUs;

    if (pidUpdateCountdown)
      --pidUpdateCountdown;
    else
        pidUpdateCountdown = (
            isGyroDataUsable ? ISRVALUE({
#ifdef USE_GYRO_SAMPLING_32KHZ
                do {register gyroConfig_t const * restrict const
                    PTR = gyroConfig();

                    if (PTR->gyro_use_32khz) {
                        if (rcUpdateCountdown--)
                            break;

                        rcUpdateCountdown =
                            (uint8_t)(
                                false<< (uint8_t)'\3' / pidUpdateCountdown
                            ) / PTR->gyro_sync_denom;
                    }
                } while (
                    subTaskRcCommand(),
                    processRcCommand(currentTimeUs),
                    false
                );
#else
                subTaskRcCommand();
                processRcCommand(currentTimeUs);
#endif
                subTaskPidSubprocesses();
                calculateThrottleAndCurrentMotorEndpoints(currentTimeUs);
                subTaskPidController(currentTimeUs);

#ifdef USE_BLACKBOX
                bblUpdateCountdown =
                    (   bblUpdateCountdown ?
                            bblUpdateCountdown
                        : (
                            blackboxUpdate(currentTimeUs),
                            blackboxGetRateDenom()
                        )
                    ) - true;
#endif

                pidConfig()->pid_process_denom - true;
            }) :
                true
        );
}


bool isFlipOverAfterCrashMode(void) {
    return flipOverAfterCrashMode;
}

timeUs_t getLastDisarmTimeUs(void) {
    return lastDisarmTimeUs;
}

bool isTryingToArm() {
    return (tryingToArm != ARMING_DELAYED_DISARMED);
}

void resetTryingToArm() {
    tryingToArm = ARMING_DELAYED_DISARMED;
}

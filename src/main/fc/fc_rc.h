/*
 * This file is part of Cleanflight and Betaflight.
 *
 * Cleanflight and Betaflight are free software. You can redistribute
 * this software and/or modify this software under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Cleanflight and Betaflight are distributed in the hope that they
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.
 *
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <signal.h>

#include "common/time.h"

#define SETPOINT_RATE_LIMIT 1998.f

typedef enum {
    INTERPOLATION_CHANNELS_RP,
    INTERPOLATION_CHANNELS_RPY,
    INTERPOLATION_CHANNELS_RPYT,
    INTERPOLATION_CHANNELS_T,
    INTERPOLATION_CHANNELS_RPT,
} interpolationChannels_e;

typedef float (applyRatesFn)(ptrdiff_t, float, float);

typedef struct {
    sig_atomic_t prev, curr, min, max;
} setptRate_t;

extern applyRatesFn *applyRates;
extern sig_atomic_t  currentRxRefreshRate,
                     isSetpointNew,
                     rcDeflection[], rcDeflectionAbs[];
extern setptRate_t   setptRate[];

#define getRcDeflectionVel(x) (rcDeflectionVel[(x)])
#define getRcDeflection(x) REINTERPRET_CAST(float, rcDeflection[(x)])
#define getRcDeflectionAbs(x) REINTERPRET_CAST(float, rcDeflectionAbs[(x)])
#ifdef USE_RC_SMOOTHING_FILTER
#define getSetpointRatePrev(x) REINTERPRET_CAST(float, setptRate[(x)].prev)
#else
#define getSetpointRateRate(x) REINTERPRET_CAST(float, setptRate[(x)].prev)
#endif
#define getSetpointRate(x) REINTERPRET_CAST(float, setptRate[(x)].curr)
#define getSetpointRateInt(x) REINTERPRET_CAST(uint32_t, setptRate[(x)].curr)
#ifdef USE_RC_SMOOTHING_FILTER
#define setSetpointRatePrev(x, y) (\
    (y)[setptRate].prev = REINTERPRET_CAST(\
        sig_atomic_t, constrainf(x,-SETPOINT_RATE_LIMIT, SETPOINT_RATE_LIMIT)\
    )\
)
#endif
#define setSetpointRate(x, y) (\
    (y)[setptRate].curr = REINTERPRET_CAST(\
        sig_atomic_t, constrainf(x,-SETPOINT_RATE_LIMIT, SETPOINT_RATE_LIMIT)\
    )\
)

void processRcCommand(timeUs_t);
void updateRcCommands(void);
void resetYawAxis(void);
void initRcProcessing(void);
bool isMotorsReversed(void);
bool rcSmoothingIsEnabled(void);
#ifdef USE_RC_SMOOTHING_FILTER
int rcSmoothingGetValue(int whichValue);
bool rcSmoothingAutoCalculate(void);
bool rcSmoothingInitializationComplete(void);
#endif
void updateRcRefreshRate(timeUs_t currentTimeUs);

void scaleRcCommandToFpvCamAngle(void);

/*
 * This file is part of Cleanflight and Betaflight.
 *
 * Cleanflight and Betaflight are free software. You can redistribute
 * this software and/or modify this software under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Cleanflight and Betaflight are distributed in the hope that they
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.
 *
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stdlib.h>
#include <stdint.h>

#include "platform.h"

#include "build/debug.h"

#include "cms/cms.h"

#include "common/color.h"
#include "common/utils.h"

#include "config/feature.h"

#include "drivers/accgyro/accgyro.h"
#include "drivers/camera_control.h"
#include "drivers/compass/compass.h"
#include "drivers/dma_spi.h"
#include "drivers/sensor.h"
#include "drivers/serial.h"
#include "drivers/serial_usb_vcp.h"
#include "drivers/stack_check.h"
#include "drivers/transponder_ir.h"
#include "drivers/usb_io.h"
#include "drivers/vtx_common.h"
#include "drivers/beesign.h"
#ifdef USB_CDC_HID
//TODO: Make it platform independent in the future
#include "vcpf4/usbd_cdc_vcp.h"
#include "usbd_hid_core.h"
//TODO: Nicer way to handle this...
#undef MIN
#endif

#include "fc/config.h"
#include "fc/fc_core.h"
#include "fc/fc_rc.h"
#include "fc/fc_dispatch.h"
#include "fc/rc_controls.h"
#include "fc/runtime_config.h"

#include "flight/position.h"
#include "flight/mixer.h"
#include "flight/pid.h"

#include "interface/cli.h"
#include "interface/msp.h"

#include "io/asyncfatfs/asyncfatfs.h"
#include "io/beeper.h"
#include "io/dashboard.h"
#include "io/gps.h"
#include "io/ledstrip.h"
#include "io/osd.h"
#include "io/piniobox.h"
#include "io/serial.h"
#include "io/transponder_ir.h"
#include "io/vtx_tramp.h" // Will be gone
#include "io/rcdevice_cam.h"
#include "io/usb_cdc_hid.h"
#include "io/vtx.h"

#include "msp/msp_serial.h"

#include "pg/rx.h"

#include "rx/rx.h"

#include "sensors/acceleration.h"
#include "sensors/adcinternal.h"
#include "sensors/barometer.h"
#include "sensors/battery.h"
#include "sensors/compass.h"
#include "sensors/esc_sensor.h"
#include "sensors/gyro.h"
#include "sensors/sensors.h"
#include "sensors/rangefinder.h"

#include "scheduler/scheduler.h"

#include "telemetry/telemetry.h"

#ifdef USE_USB_CDC_HID
//TODO: Make it platform independent in the future
#include "vcpf4/usbd_cdc_vcp.h"
#include "usbd_hid_core.h"
#endif

#ifdef USE_BST
#include "i2c_bst.h"
#endif

#ifdef USE_USB_CDC_HID
//TODO: Make it platform independent in the future
#ifdef STM32F4
#include "vcpf4/usbd_cdc_vcp.h"
#include "usbd_hid_core.h"
#elif defined(STM32F7)
#include "usbd_cdc_interface.h"
#include "usbd_hid.h"
#endif
#endif

#include "fc_tasks.h"

void taskHandleSerial(
  __attribute__((unused))
    register timeUs_t const currentTimeUs
) {
#ifdef USE_SDCARD
    afatfs_poll();
#endif

#if defined(USE_VCP)
    if (debugMode== DEBUG_USB)
        debug[0] = usbCableIsInserted(),
        debug[1] = usbVcpIsConnected ();
#endif

#ifdef USE_CLI
    if (cliMode)
        cliProcess();
    else
#endif
#ifdef OSD_SLAVE
    if (mspSerialWaiting())
#endif
    mspSerialProcess(
#ifdef OSD_SLAVE
        osdSlaveIsLocked
#else
        ARMING_FLAG(ARMED)
#endif
    ,
        mspFcProcessCommand
    ,
        mspFcProcessReply
    );
}

#ifndef USE_OSD_SLAVE
void taskUpdateRxMain(register timeUs_t const currentTimeUs) {
    if (!processRx(currentTimeUs)) {
        return;
    }
#ifdef USE_USB_CDC_HID
    if (!ARMING_FLAG(ARMED)) {
        int8_t report[8];
        for (
            register ptrdiff_t
            i = 7;
            i[report] = rquot(
                i[rcData] - PWM_RANGE_MIDDLE<< CHAR_BIT - true, PWM_RANGE_2
            ),
            i--;
        ) NOOP
        USBD_HID_SendReport(&USB_OTG_dev, (uint8_t*)report, sizeof(report));
    }
#endif
#ifdef USE_USB_CDC_HID
    if (!ARMING_FLAG(ARMED)) {
        sendRcDataToHid();
    }
#endif
    // updateRcCommands sets rcCommand, which is needed by updateAltHoldState and updateSonarAltHoldState
    updateRcCommands();
    updateArmingStatus();
}
#endif

#ifdef USE_BARO
void taskUpdateBaro(register timeUs_t currentTimeUs) {
    if (sensors(SENSOR_BARO) && (currentTimeUs = baroUpdate(currentTimeUs)))
        rescheduleTask(TASK_SELF, currentTimeUs);
}
#endif

#ifdef USE_CAMERA_CONTROL
void taskCameraControl(register timeUs_t const currentTime) {
    if (ARMING_FLAG(ARMED)) {
        return;
    }
    cameraControlProcess(currentTime);
}
#endif

void fcTasksInit(void) {
    schedulerInit();
    setTaskEnabled(TASK_SERIAL, true);
    rescheduleTask(TASK_SERIAL, TASK_PERIOD_HZ(serialConfig()->serial_update_rate_hz));
    const bool useBatteryVoltage = batteryConfig()->voltageMeterSource != VOLTAGE_METER_NONE;
    const bool useBatteryCurrent = batteryConfig()->currentMeterSource != CURRENT_METER_NONE;
#ifdef USE_OSD_SLAVE
    const bool useBatteryAlerts = batteryConfig()->useVBatAlerts || batteryConfig()->useConsumptionAlerts;
#else
    const bool useBatteryAlerts = batteryConfig()->useVBatAlerts || batteryConfig()->useConsumptionAlerts || feature(FEATURE_OSD);
#endif
    setTaskEnabled(TASK_BATTERY_ALERTS, (useBatteryVoltage || useBatteryCurrent) && useBatteryAlerts);
#ifdef USE_TRANSPONDER
    setTaskEnabled(TASK_TRANSPONDER, feature(FEATURE_TRANSPONDER));
#endif
#ifdef STACK_CHECK
    setTaskEnabled(TASK_STACK_CHECK, true);
#endif
#ifdef USE_OSD_SLAVE
    setTaskEnabled(TASK_OSD_SLAVE, osdSlaveInitialized());
#else
    if (sensors(SENSOR_GYRO)) {
#ifdef USE_GYRO_DATA_ANALYSE
        rescheduleTask(
            TASK_ATTITUDE, TASK_PERIOD_HZ(gyroConfig()->dyn_notch_min_hz)
        );
#endif
        rescheduleTask(TASK_GYROPID, gyro.targetLooptime);
        setTaskEnabled(TASK_GYROPID, true);
        setTaskEnabled(TASK_ATTITUDE, true);
    }
    setTaskEnabled(TASK_RX, true);
    setTaskEnabled(TASK_DISPATCH, dispatchIsEnabled());
#ifdef USE_BEEPER
    setTaskEnabled(TASK_BEEPER, true);
#endif
#ifdef USE_BARO
    setTaskEnabled(TASK_BARO, sensors(SENSOR_BARO));
#endif
#if defined(USE_ACC) || defined(USE_BARO) || defined(USE_GPS)
    setTaskEnabled(
        TASK_ALTITUDE
    ,
 #if defined(USE_BARO) || defined(USE_GPS)
  #ifdef USE_ACC
        accelerometerConfig()->acc_hardware ||
  #endif
  #ifdef USE_GPS
   #ifdef USE_BARO
        sensors(SENSOR_BARO) ||
   #endif
        feature(FEATURE_GPS)
  #else
        sensors(SENSOR_BARO)
  #endif // USE_GPS
 #else
        accelerometerConfig()->acc_hardware
 #endif // USE_BARO || USE_GPS
    );
#endif // USE_ACC || USE_BARO || USE_GPS
#ifdef USE_DASHBOARD
    setTaskEnabled(TASK_DASHBOARD, feature(FEATURE_DASHBOARD));
#endif
#ifdef USE_TELEMETRY
    if (feature(FEATURE_TELEMETRY)) {
        setTaskEnabled(TASK_TELEMETRY, true);
        if (rxConfig()->serialrx_provider == SERIALRX_JETIEXBUS) {
            // Reschedule telemetry to 500hz for Jeti Exbus
            rescheduleTask(TASK_TELEMETRY, TASK_PERIOD_HZ(500));
        } else if (rxConfig()->serialrx_provider == SERIALRX_CRSF) {
            // Reschedule telemetry to 500hz, 2ms for CRSF
            rescheduleTask(TASK_TELEMETRY, TASK_PERIOD_HZ(500));
        }
    }
#endif
#ifdef USE_LED_STRIP
    setTaskEnabled(TASK_LEDSTRIP, feature(FEATURE_LED_STRIP));
#endif
#ifdef USE_TRANSPONDER
    setTaskEnabled(TASK_TRANSPONDER, feature(FEATURE_TRANSPONDER));
#endif
#ifdef USE_OSD
    rescheduleTask(TASK_OSD, TASK_PERIOD_HZ(osdConfig()->task_frequency));
    setTaskEnabled(TASK_OSD, feature(FEATURE_OSD) && osdInitialized());
#endif
#ifdef USE_ESC_SENSOR
    setTaskEnabled(TASK_ESC_SENSOR, feature(FEATURE_ESC_SENSOR));
#endif
#ifdef USE_ADC_INTERNAL
    setTaskEnabled(TASK_ADC_INTERNAL, true);
#endif
#ifdef USE_PINIOBOX
    pinioBoxTaskControl();
#endif
#ifdef USE_BEESIGN
    setTaskEnabled(TASK_BEESIGN, true);
#endif
#ifdef USE_CMS
#ifdef USE_MSP_DISPLAYPORT
    setTaskEnabled(TASK_CMS, true);
#else
    setTaskEnabled(TASK_CMS, feature(FEATURE_OSD) || feature(FEATURE_DASHBOARD));
#endif
#endif
#ifdef USE_VTX_CONTROL
#if defined(USE_VTX_RTC6705) || defined(USE_VTX_SMARTAUDIO) || defined(USE_VTX_TRAMP)  || defined(USE_VTX_BEESIGN)
    setTaskEnabled(TASK_VTXCTRL, true);
#endif
#endif
#ifdef USE_CAMERA_CONTROL
    setTaskEnabled(TASK_CAMCTRL, true);
#endif
#ifdef USE_RCDEVICE
    setTaskEnabled(TASK_RCDEVICE, rcdeviceIsEnabled());
#endif
#endif
}

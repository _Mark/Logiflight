/*
 * This file is part of Cleanflight and Betaflight.
 *
 * Cleanflight and Betaflight are free software. You can redistribute
 * this software and/or modify this software under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Cleanflight and Betaflight are distributed in the hope that they
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.
 *
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <string.h>
#include <stdint.h>
#include <math.h>

#include "platform.h"

#include "build/debug.h"

#include "common/axis.h"
#include "common/maths.h"
#include "common/utils.h"
#include "common/time.h"

#include "config/feature.h"

#include "fc/config.h"
#include "fc/controlrate_profile.h"
#include "drivers/time.h"
#include "fc/fc_core.h"
#include "fc/fc_rc.h"
#include "fc/rc_controls.h"
#include "fc/rc_modes.h"
#include "fc/runtime_config.h"

#include "flight/failsafe.h"
#include "flight/imu.h"
#include "flight/gps_rescue.h"
#include "flight/pid.h"
#include "scheduler/scheduler.h"
#include "pg/rx.h"
#include "rx/rx.h"


#include "sensors/battery.h"
#include "sensors/acceleration.h"

#include <complex.h>

enum {
    ROLL_FLAG = 1 << ROLL,
    PITCH_FLAG = 1 << PITCH,
    YAW_FLAG = 1 << YAW,
    THROTTLE_FLAG = 1 << THROTTLE,
};

FAST_RAM_ZERO_INIT applyRatesFn *applyRates;

FAST_RAM_ZERO_INIT sig_atomic_t
currentRxRefreshRate,
isSetpointNew,
rcDeflection   [FLIGHT_DYNAMICS_INDEX_COUNT],
rcDeflectionAbs[FLIGHT_DYNAMICS_INDEX_COUNT];

static FAST_RAM_ZERO_INIT pt1Filter_t
#ifdef USE_RC_SMOOTHING_FILTER
rcCommandFilter[FLIGHT_DYNAMICS_INDEX_COUNT];
#else
rcCommandFilter[FLIGHT_DYNAMICS_INDEX_COUNT],
setptRateFilter[FLIGHT_DYNAMICS_INDEX_COUNT];
#endif

static FAST_RAM_ZERO_INIT float
lastRcCommandData[FLIGHT_DYNAMICS_INDEX_COUNT],
iterm            [FLIGHT_DYNAMICS_INDEX_COUNT], currentRxRefreshRatef;

static FAST_RAM_ZERO_INIT timeUs_t lastRxTimeUs;
static FAST_RAM_ZERO_INIT bool  reverseMotors;

// static float rcCommandInterp[4] = { 0, 0, 0, 0 };
// static float rcStepSize[4] = { 0, 0, 0, 0 };
// static float inverseRcInt;

static FAST_RAM_ZERO_INIT uint8_t interpolationChannels;
static FAST_RAM_ZERO_INIT uint8_t lastFpvCamAngleDegrees;
static FAST_RAM_ZERO_INIT float complex fpvCamAngleCis;

FAST_RAM_ZERO_INIT setptRate_t setptRate[FLIGHT_DYNAMICS_INDEX_COUNT];

static FAST_RAM_ZERO_INIT bool isRXDataNew;
#ifdef USE_RC_SMOOTHING_FILTER
static FAST_RAM_ZERO_INIT uint8_t skipInterpolate;
static FAST_RAM_ZERO_INIT int16_t rcInterpolationStepCount;
#endif


#ifdef USE_RC_SMOOTHING_FILTER
#define RC_SMOOTHING_IDENTITY_FREQUENCY         80    // Used in the formula to convert a BIQUAD cutoff frequency to PT1
#define RC_SMOOTHING_FILTER_STARTUP_DELAY_MS    5000  // Time to wait after power to let the PID loop stabilize before starting average frame rate calculation
#define RC_SMOOTHING_FILTER_TRAINING_SAMPLES    50    // Number of rx frame rate samples to average
#define RC_SMOOTHING_FILTER_TRAINING_DELAY_MS   1000  // Additional time to wait after receiving first valid rx frame before initial training starts
#define RC_SMOOTHING_FILTER_RETRAINING_DELAY_MS 2000  // Guard time to wait after retraining to prevent retraining again too quickly
#define RC_SMOOTHING_RX_RATE_CHANGE_PERCENT     20    // Look for samples varying this much from the current detected frame rate to initiate retraining
#define RC_SMOOTHING_RX_RATE_MIN_US             1000  // 1ms
#define RC_SMOOTHING_RX_RATE_MAX_US             50000 // 50ms or 20hz

static FAST_RAM_ZERO_INIT rcSmoothingFilter_t rcSmoothingData;
#endif // USE_RC_SMOOTHING_FILTER

#define THROTTLE_LOOKUP_LENGTH 12
static int16_t lookupThrottleRC[THROTTLE_LOOKUP_LENGTH];    // lookup table for expo & mid THROTTLE

static int16_t rcLookupThrottle(int32_t tmp) {
    const int32_t tmp2 = tmp / 100;
    // [0;1000] -> expo -> [MINTHROTTLE;MAXTHROTTLE]
    return lookupThrottleRC[tmp2] + (tmp - tmp2 * 100) * (lookupThrottleRC[tmp2 + 1] - lookupThrottleRC[tmp2]) / 100;
}

float applyBetaflightRates(
    register ptrdiff_t const axis,
    register float           rcCommandf,
    register float     const rcCommandfAbs
) {
    register float
    rcRate = axis[currentControlRateProfile->rcExpo];

    if (rcRate)
        rcCommandf-= ldexpf(
            fdimf(true, power3(rcCommandfAbs)) * rcRate * rcCommandf,-2
        );

    rcRate = fdimf(ldexpf(
        axis[currentControlRateProfile->rcRates] * 14.54f / 25.f,-2
    ),
        29.08
    );
    rcRate*= ldexpf(rcCommandf, 3) * 25.f;

    return
    axis[currentControlRateProfile->rates] ?
        rcRate / fmaxf(
            ldexpf(
                axis[currentControlRateProfile->rates] * rcCommandfAbs /-25.f,-2
            ) + true
        ,
            FLT_EPSILON
        )
    :
        rcRate;
}

float applyRaceFlightRates(
    register ptrdiff_t const axis,
    register float           rcCommandf,
    register float     const rcCommandfAbs
) {
    // -1.0 to 1.0 ranged and curved
    rcCommandf = ((1.0f + 0.01f * currentControlRateProfile->rcExpo[axis] * (rcCommandf * rcCommandf - 1.0f)) * rcCommandf);
    // convert to -2000 to 2000 range using acro+ modifier
    float angleRate = 10.0f * currentControlRateProfile->rcRates[axis] * rcCommandf;
    angleRate = angleRate * (1 + rcCommandfAbs * (float)currentControlRateProfile->rates[axis] * 0.01f);
    return angleRate;
}

float applyKissRates(
    register ptrdiff_t const axis,
    register float     const rcCommandf,
    register float     const rcCommandfAbs
) {
    const float rcCurvef = currentControlRateProfile->rcExpo[axis] / 100.0f;

    float kissRpyUseRates = 1.0f / (constrainf(1.0f - (rcCommandfAbs * (currentControlRateProfile->rates[axis] / 100.0f)), 0.01f, 1.00f));
    float kissRcCommandf = (power3(rcCommandf) * rcCurvef + rcCommandf * (1 - rcCurvef)) * (currentControlRateProfile->rcRates[axis] / 1000.0f);
    float kissAngle = constrainf(((2000.0f * kissRpyUseRates) * kissRcCommandf), -SETPOINT_RATE_LIMIT, SETPOINT_RATE_LIMIT);

    return kissAngle;
}

float applyActualRates(
    register ptrdiff_t const axis,
    register float     const rcCommandf,
    register float           rcCommandfAbs
) {
    rcCommandfAbs = ldexpf(
        axis[currentControlRateProfile->rcExpo] * rcCommandfAbs / 25.f,-2
    );
    rcCommandfAbs*= fdimf(
        ldexpf(axis[currentControlRateProfile->rates], 1) * 5.f
    , rcCommandfAbs =
        (__builtin_powif(rcCommandf, 5) - rcCommandf) * rcCommandfAbs
            + rcCommandf
    );
    return
    rcCommandfAbs+=
        ldexpf(axis[currentControlRateProfile->rcRates], 1) * 5.f * rcCommandf;
}

FAST_CODE_NOINVOKE void scaleRcCommandToFpvCamAngle(void) {
    register float _Complex
    input = getSetpointRate(ROLL);
  __imag__
    input = getSetpointRate(YAW );

    //recalculate sin/cos only when rxConfig()->fpvCamAngleDegrees changed
    setSetpointRate(__real__(
        input*= ISRVALUE({
            register float const
            camAngle = rxConfig()->fpvCamAngleDegrees;

            camAngle!= lastFpvCamAngleDegrees ?
                fpvCamAngleCis = cexpf(
                    DEGREES_TO_RADIANS(
                        lastFpvCamAngleDegrees = (
                            rxConfig()->cinematicYaw ?
                                fminf(
                                    ldexpf(attitude.values.pitch / 5.f,-true)
                                ,
                                    camAngle
                                )
                            :
                                camAngle
                        )
                    ) * I
                )
            :
                fpvCamAngleCis;
        })
    ),
        ROLL
    );
    setSetpointRate(__imag__ input, PITCH);
}

void updateRcRefreshRate(register timeUs_t currentTimeUs) {
    register float const
    filterGain = pt1FilterMaxK(
        currentRxRefreshRatef = currentRxRefreshRate =
            constrain(currentTimeUs - lastRxTimeUs, u'\x3E8', u'\x7530')
    );

     lastRxTimeUs = currentTimeUs;
    currentTimeUs = FLIGHT_DYNAMICS_INDEX_COUNT - 1;

    do
#ifndef USE_RC_SMOOTHING_FILTER
        currentTimeUs[setptRateFilter].k =
#endif
        currentTimeUs[rcCommandFilter].k = filterGain;
    while (currentTimeUs--);
}

#ifdef USE_RC_SMOOTHING_FILTER
#define THROTTLE_BUFFER_MAX 20
#define THROTTLE_DELTA_MS 100

static uint16_t getRxRefreshRate(void) {
    switch (rxConfig()->rcInterpolation) {
        case RC_SMOOTHING_MANUAL:
        return rxConfig()->rcInterpolationInterval * u'\x3E8';

        case RC_SMOOTHING_AUTO:
        return currentRxRefreshRate + u'\x3E8';

        default:
        return rxGetRefreshRate();
    }
}

static FAST_RAM_ZERO_INIT float
rcInterpolationStepCount, rcCommandInterp[4], rcStepSize[4];

static uint8_t processRcInterpolation(void) {
    register uint8_t
    channel = PRIMARY_CHANNEL_COUNT;

    if (rxConfig()->rcInterpolation) {
        register uint16_t const
        // Set RC refresh rate for sampling and channels to filter
        rxRefreshRate = getRxRefreshRate();

        if (rxRefreshRate && isRXDataNew) {
            rcInterpolationStepCount =
                rxRefreshRate / fmaxf(gyro.targetLooptimef, targetPidLooptimef);

            while (channel--)
                if ((true<< channel) & interpolationChannels)
                    channel[rcStepSize] =
                        (channel[rcCommand] - channel[rcCommandInterp])
                            / rcInterpolationStepCount;

            if (debugMode== DEBUG_RC_INTERPOLATION)
                debug[0] = lrintf(rcCommand[ROLL]),
                debug[1] = rquot(1000000ul, currentRxRefreshRate);
        } else
          --rcInterpolationStepCount;

        // Interpolate steps of rcCommand
        if (signbit(rcInterpolationStepCount))
            while (channel--)
                if ((true<< channel) & interpolationChannels)
                    channel[rcCommand] = channel[rcCommandInterp]+=
                        channel[rcStepSize];
    } else
        rcInterpolationStepCount = 0; // reset factor in case of level modes flip flopping

    if (debugMode== DEBUG_RC_INTERPOLATION)
        debug[2] = rcInterpolationStepCount;

    return (uint8_t)PRIMARY_CHANNEL_COUNT - channel;
}

// Determine a cutoff frequency based on filter type and the calculated
// average rx frame time
FAST_CODE_NOINLINE int calcRcSmoothingCutoff(int avgRxFrameTimeUs, bool pt1) {
    if (avgRxFrameTimeUs > 0) {
        float cutoff = (1 / (avgRxFrameTimeUs * 1e-6f)) / 2;  // calculate the nyquist frequency
        cutoff = cutoff * 0.90f;  // Use 90% of the calculated nyquist frequency
        if (pt1) {
            cutoff = sq(cutoff) / RC_SMOOTHING_IDENTITY_FREQUENCY; // convert to a cutoff for pt1 that has similar characteristics
        }
        return lrintf(cutoff);
    } else {
        return 0;
    }
}

// Preforms a reasonableness check on the rx frame time to avoid bad data
// skewing the average.
#define rcSmoothingRxRateValid(x) (\
    (x)>= RC_SMOOTHING_RX_RATE_MIN_US &&\
    (x)<= RC_SMOOTHING_RX_RATE_MAX_US\
)

// Initialize or update the filters base on either the manually selected cutoff, or
// the auto-calculated cutoff frequency based on detected rx frame rate.
FAST_CODE_NOINLINE void rcSmoothingSetFilterCutoffs(
    register rcSmoothingFilter_t * restrict const smoothingData
) {
    // initialize or update the input filter
    if ((   rxConfig()->rc_smoothing_input_cutoff ?
                smoothingData->inputCutoffFrequency
            : (
                smoothingData->inputCutoffFrequency = calcRcSmoothingCutoff(
                    smoothingData->averageFrameTimeUs
                ,
                    rxConfig()->rc_smoothing_input_type== RC_SMOOTHING_INPUT_PT1
                )
            )
       )!= smoothingData->inputCutoffFrequency ||
      !smoothingData->filterInitialized
    )
        for (
            register struct {
                void      (* const updateFn)(void);
                ptrdiff_t channel;
            } filter = { ISRVALUE({
                void    pt1UpdateFn(void) {
                    filter.channel[(pt1Filter_t *)smoothingData->filter].k =
                        pt1FilterGain(
                            smoothingData->inputCutoffFrequency
                        ,
                            targetPidLooptimef
                        );
                }

                void biquadUpdateFn(void) {
                    biquadFilterUpdateLPF((void *)(
                        channel + smoothingData->filter
                    ),
                        smoothingData->inputCutoffFrequency
                    ,
                        targetPidLooptimef
                    );
                }

                rxConfig()->rc_smoothing_input_type== RC_SMOOTHING_INPUT_PT1 ?
                    pt1UpdateFn : biquadUpdateFn;
            }),
                PRIMARY_CHANNEL_COUNT
            };
            channel--;
        )
            if ((true<< channel) & interpolationChannels)
                filter.updateFn();

    // update or initialize the derivative filter
    register uint16_t const
    oldCutoff = smoothingData->derivativeCutoffFrequency;

    if (rxConfig()->rc_smoothing_derivative_type   &&
       !rxConfig()->rc_smoothing_derivative_cutoff && (
            smoothingData->derivativeCutoffFrequency = calcRcSmoothingCutoff(
                smoothingData->averageFrameTimeUs
            ,
                rxConfig()->rc_smoothing_derivative_type==
                    RC_SMOOTHING_DERIVATIVE_PT1
            ),
            smoothingData->filterInitialized
        )
    )
        if (smoothingData->derivativeCutoffFrequency!= oldCutoff)
            pidUpdateSetpointDerivativeLpf(
                smoothingData->derivativeCutoffFrequency
            );
        else
            NOOP
    else
        pidInitSetpointDerivativeLpf(
            smoothingData->derivativeCutoffFrequency
        ,
            rxConfig()->rc_smoothing_debug_axis
        ,
            rxConfig()->rc_smoothing_derivative_type
        );
}

FAST_CODE_NOINLINE void rcSmoothingResetAccumulation(rcSmoothingFilter_t *smoothingData) {
    smoothingData->training.sum = 0;
    smoothingData->training.count = 0;
    smoothingData->training.min = UINT16_MAX;
    smoothingData->training.max = 0;
}

// Accumulate the rx frame time samples. Once we've collected enough samples calculate the
// average and return true.
static FAST_CODE_NOINVOKE bool rcSmoothingAccumulateSample(
    register rcSmoothingFilter_t * restrict const smoothingData,
    register int                            const rxFrameTimeUs
) {
    smoothingData->training.sum += rxFrameTimeUs;
    smoothingData->training.count++;
    smoothingData->training.max = MAX(smoothingData->training.max, rxFrameTimeUs);
    smoothingData->training.min = MIN(smoothingData->training.min, rxFrameTimeUs);
    // if we've collected enough samples then calculate the average and reset the accumulation
    if (smoothingData->training.count >= RC_SMOOTHING_FILTER_TRAINING_SAMPLES) {
        smoothingData->training.sum = smoothingData->training.sum - smoothingData->training.min - smoothingData->training.max; // Throw out high and low samples
        smoothingData->averageFrameTimeUs = lrintf(smoothingData->training.sum / (smoothingData->training.count - 2));
        rcSmoothingResetAccumulation(smoothingData);
        return true;
    }
    return false;
}

// Determine if we need to caclulate filter cutoffs. If not then we can avoid
// examining the rx frame times completely
FAST_CODE_NOINLINE bool rcSmoothingAutoCalculate(void) {
    bool ret = false;
    // if the input cutoff is 0 (auto) then we need to calculate cutoffs
    if (rxConfig()->rc_smoothing_input_cutoff == 0) {
        ret = true;
    }
    // if the derivative type isn't OFF and the cutoff is 0 then we need to calculate
    if (rxConfig()->rc_smoothing_derivative_type != RC_SMOOTHING_DERIVATIVE_OFF) {
        if (rxConfig()->rc_smoothing_derivative_cutoff == 0) {
            ret = true;
        }
    }
    return ret;
}

static uint8_t processRcSmoothingFilter(void) {
    uint8_t updatedChannel = 0;
    static FAST_RAM_ZERO_INIT float lastRxData[4];
    static FAST_RAM_ZERO_INIT bool initialized;
    static FAST_RAM_ZERO_INIT timeMs_t validRxFrameTimeMs;
    static FAST_RAM_ZERO_INIT bool calculateCutoffs;
    // first call initialization
    if (!initialized) {
        initialized = true;
        rcSmoothingData.filterInitialized = false;
        rcSmoothingData.averageFrameTimeUs = 0;
        rcSmoothingResetAccumulation(&rcSmoothingData);
        rcSmoothingData.inputCutoffFrequency = rxConfig()->rc_smoothing_input_cutoff;
        if (rxConfig()->rc_smoothing_derivative_type != RC_SMOOTHING_DERIVATIVE_OFF) {
            rcSmoothingData.derivativeCutoffFrequency = rxConfig()->rc_smoothing_derivative_cutoff;
        }
        calculateCutoffs = rcSmoothingAutoCalculate();
        // if we don't need to calculate cutoffs dynamically then the filters can be initialized now
        if (!calculateCutoffs) {
            rcSmoothingSetFilterCutoffs(&rcSmoothingData);
            rcSmoothingData.filterInitialized = true;
        }
    }
    if (isRXDataNew) {
        // store the new raw channel values
        for (int i = 0; i < PRIMARY_CHANNEL_COUNT; i++) {
            if ((1 << i) & interpolationChannels) {
                lastRxData[i] = rcCommand[i];
            }
        }
        // for dynamically calculated filters we need to examine each rx frame interval
        if (calculateCutoffs) {
            const timeMs_t currentTimeMs = millis();
            int sampleState = 0;
            // If the filter cutoffs are set to auto and we have good rx data, then determine the average rx frame rate
            // and use that to calculate the filter cutoff frequencies
            if ((currentTimeMs > RC_SMOOTHING_FILTER_STARTUP_DELAY_MS) && (targetPidLooptime > 0)) { // skip during FC initialization
                if (rxIsReceivingSignal()  && rcSmoothingRxRateValid(currentRxRefreshRate)) {
                    // set the guard time expiration if it's not set
                    if (validRxFrameTimeMs == 0) {
                        validRxFrameTimeMs = currentTimeMs + (rcSmoothingData.filterInitialized ? RC_SMOOTHING_FILTER_RETRAINING_DELAY_MS : RC_SMOOTHING_FILTER_TRAINING_DELAY_MS);
                    } else {
                        sampleState = 1;
                    }
                    // if the guard time has expired then process the rx frame time
                    if (currentTimeMs > validRxFrameTimeMs) {
                        sampleState = 2;
                        bool accumulateSample = true;
                        // During initial training process all samples.
                        // During retraining check samples to determine if they vary by more than the limit percentage.
                        if (rcSmoothingData.filterInitialized) {
                            const float percentChange = (ABS(currentRxRefreshRate - rcSmoothingData.averageFrameTimeUs) / (float)rcSmoothingData.averageFrameTimeUs) * 100;
                            if (percentChange < RC_SMOOTHING_RX_RATE_CHANGE_PERCENT) {
                                // We received a sample that wasn't more than the limit percent so reset the accumulation
                                // During retraining we need a contiguous block of samples that are all significantly different than the current average
                                rcSmoothingResetAccumulation(&rcSmoothingData);
                                accumulateSample = false;
                            }
                        }
                        // accumlate the sample into the average
                        if (accumulateSample) {
                            if (rcSmoothingAccumulateSample(&rcSmoothingData, currentRxRefreshRate)) {
                                // the required number of samples were collected so set the filter cutoffs
                                rcSmoothingSetFilterCutoffs(&rcSmoothingData);
                                rcSmoothingData.filterInitialized = true;
                                validRxFrameTimeMs = 0;
                            }
                        }
                    }
                } else {
                    // we have either stopped receiving rx samples (failsafe?) or the sample time is unreasonable so reset the accumulation
                    rcSmoothingResetAccumulation(&rcSmoothingData);
                }
            }
            // rx frame rate training blackbox debugging
            if (debugMode == DEBUG_RC_SMOOTHING_RATE) {
                DEBUG_SET(DEBUG_RC_SMOOTHING_RATE, 0, currentRxRefreshRate);              // log each rx frame interval
                DEBUG_SET(DEBUG_RC_SMOOTHING_RATE, 1, rcSmoothingData.training.count);    // log the training step count
                DEBUG_SET(DEBUG_RC_SMOOTHING_RATE, 2, rcSmoothingData.averageFrameTimeUs);// the current calculated average
                DEBUG_SET(DEBUG_RC_SMOOTHING_RATE, 3, sampleState);                       // indicates whether guard time is active
            }
        }
    }
    if (rcSmoothingData.filterInitialized && (debugMode == DEBUG_RC_SMOOTHING)) {
        // after training has completed then log the raw rc channel and the calculated
        // average rx frame rate that was used to calculate the automatic filter cutoffs
        DEBUG_SET(DEBUG_RC_SMOOTHING, 0, lrintf(lastRxData[rxConfig()->rc_smoothing_debug_axis]));
        DEBUG_SET(DEBUG_RC_SMOOTHING, 3, rcSmoothingData.averageFrameTimeUs);
    }
    // each pid loop continue to apply the last received channel value to the filter
    for (updatedChannel = 0; updatedChannel < PRIMARY_CHANNEL_COUNT; updatedChannel++) {
        if ((1 << updatedChannel) & interpolationChannels) {  // only smooth selected channels base on the rc_interp_ch value
            if (rcSmoothingData.filterInitialized) {
                switch (rxConfig()->rc_smoothing_input_type) {
                case RC_SMOOTHING_INPUT_PT1:
                    rcCommand[updatedChannel] = pt1FilterApply((pt1Filter_t*) &rcSmoothingData.filter[updatedChannel], lastRxData[updatedChannel]);
                    break;
                case RC_SMOOTHING_INPUT_BIQUAD:
                default:
                    rcCommand[updatedChannel] = biquadFilterApplyDF1((biquadFilter_t*) &rcSmoothingData.filter[updatedChannel], lastRxData[updatedChannel]);
                    break;
                }
            } else {
                // If filter isn't initialized yet then use the actual unsmoothed rx channel data
                rcCommand[updatedChannel] = lastRxData[updatedChannel];
            }
        }
    }
    return interpolationChannels;
}
#endif // USE_RC_SMOOTHING_FILTER

FAST_CODE FAST_CODE_NOINLINE void processRcCommand(
    register timeUs_t const currentTimeUs
) {
#ifdef USE_RC_SMOOTHING_FILTER
    register uint8_t const
    updatedChannel =
        (   rxConfig()->rc_smoothing_type ?
                processRcSmoothingFilter : processRcInterpolation
        )();

    if (isRXDataNew ? (isRXDataNew = false), true : updatedChannel) {
#else
    if (isRXDataNew) {
        isRXDataNew = false;
#endif // USE_RC_SMOOTHING_FILTER
        for (
            register ptrdiff_t
            axis = FLIGHT_DYNAMICS_INDEX_COUNT;
            axis--;
#ifdef USE_RC_SMOOTHING_FILTER
            axis[setptRate].prev = axis[setptRate].curr,
#else
            axis[setptRate].prev = REINTERPRET_CAST(sig_atomic_t, ldexpf(
                pt1FilterReuse(
                    axis + setptRateFilter
                ,
#endif
            setSetpointRate(ISRVALUE({
                register float const
                       deflection  =
                axis[rcDeflection] = axis[rcCommand] / PWM_RANGE_2;

                applyRates(
                    axis, deflection, axis[rcDeflectionAbs] = fabsf(deflection)
                );
            }),
                axis
            )
#ifndef USE_RC_SMOOTHING_FILTER
                ) * currentRxRefreshRatef / 15625.f
            ,
               -6
            )),
            axis[setptRate].curr = pt1FilterLevel(axis + setptRateFilter)
#endif
        ) NOOP

        isSetpointNew = true;

#ifndef USE_RC_SMOOTHING_FILTER
        if (debugMode== DEBUG_ANGLERATE)
#else
        switch (debugMode) {
            case DEBUG_RC_INTERPOLATION:
            debug[2] = rcInterpolationStepCount;
            debug[3] = lrintf(REINTERPRET_CAST(float, setptRate->curr));
            break;

            case DEBUG_ANGLERATE:
#endif
            for (
                register ptrdiff_t
                axis = XYZ_AXIS_COUNT;
                axis--;
                axis[debug] = lrintf(
                    REINTERPRET_CAST(float, axis[setptRate].curr)
                )
            ) NOOP
#ifdef USE_RC_SMOOTHING_FILTER
            FALLTHROUGH;

            default:
            break;
        }
#endif
    }
}

static void applyRollYawMix(void) {
    register struct {
        float RATIO, CMD;
    } const OBJ = {
        ldexp(
            currentControlRateProfile->addYawToRollRc / (
                rcControlsConfig()->yaw_control_reversed ? 25.f :-25.f
            )
        ,
           -2
        )
    ,
        rcCommand[FD_ROLL]
    };

    rcCommand[FD_ROLL] =
        constrainf(rcCommand[FD_YAW] * OBJ.RATIO + OBJ.CMD,-5e2f, 5e2f);
    rcCommand[FD_YAW ] =
        constrainf(OBJ.RATIO * OBJ.CMD + rcCommand[FD_YAW],-5e2f, 5e2f);
}

static void applyPolarExpo(void) {
    const float roll_pitch_mag = fast_fsqrtf((rcCommand[FD_ROLL] * rcCommand[FD_ROLL] / 250000.0f) + (rcCommand[FD_PITCH] * rcCommand[FD_PITCH] / 250000.0f));

    float roll_pitch_scale;
    const float rollPitchMagExpo = currentControlRateProfile->rollPitchMagExpo / 100.0f;
    if (roll_pitch_mag > 1.0f) {
        roll_pitch_scale = (1.0f / roll_pitch_mag);
        roll_pitch_scale = ((roll_pitch_scale - 1.0f) * rollPitchMagExpo) + 1.0f;
    } else {
        roll_pitch_scale = 1.0f;
    }

    rcCommand[FD_ROLL] *= roll_pitch_scale;
    rcCommand[FD_PITCH] *= roll_pitch_scale;
}

#define calculateK(x) (\
    (x) ?\
        currentRxRefreshRatef / (625e1f / (x) + currentRxRefreshRatef + -62.5f)\
    :\
        false\
)

static FAST_CODE_NOINVOKE float rateDynamics(
    register float           rcCommand,
    register ptrdiff_t const axis
) {
    return
    ISRVALUE({
        register float const
        deflection = fminf(fdimf(PWM_RANGE_2, rcCommand) / PWM_RANGE_2, true);

        float FUNC(
            register uint8_t const midpt,
            register uint8_t const endpt,
            register float   const error
        ) {
            return ldexpf(
                ({  register float const
                    k = calculateK(midpt);

                    (calculateK(endpt) - k) * deflection + k;
                }) * error / 25.f
            ,
               -2
            );
        }

        register float const
        DELTA = axis[lastRcCommandData] - rcCommand;

        axis[iterm]+= FUNC(
            currentControlRateProfile->rateDynamics.rateCorrectionCenter
        , 
            currentControlRateProfile->rateDynamics.rateCorrectionEnd
        ,
            rcCommand-=
                ldexpf(
                    (   (   currentControlRateProfile
                              ->rateDynamics.rateSensEnd -
                            currentControlRateProfile
                              ->rateDynamics.rateSensCenter
                        ) * deflection
                          + currentControlRateProfile
                              ->rateDynamics.rateSensCenter
                    ) * (axis[lastRcCommandData] = rcCommand) / 25.f
                ,
                   -2
                ) + axis[iterm]
        );

        FUNC(
            currentControlRateProfile->rateDynamics.rateCorrectionCenter
        ,
            currentControlRateProfile->rateDynamics.rateCorrectionEnd
        ,
            DELTA
        );
    }) + rcCommand + axis[iterm];
}

FAST_CODE_NOINVOKE void updateRcCommands(void) {
    // PITCH & ROLL only dynamic PID adjustment,  depending on throttle value
    for (
        register ptrdiff_t
        axis = XYZ_AXIS_COUNT;
        axis--;
        axis[rcCommand] = ISRVALUE({
            register float const
            command = pt1FilterReuse(
                axis + rcCommandFilter
            ,
                rateDynamics(({
                    register struct {
                        rcControlsConfig_t const * restrict const cfg;
                        int16_t                  mid, abs;
                        bool               const sgn;
                    } cmd = {
                        rcControlsConfig()
                    ,.sgn =
                        (cmd.abs = (cmd.mid = rxConfig()->midrc) - axis[rcData])
                            & INT_MIN
                    };

                    cmd.mid = (
                        cmd.sgn ? (
                            cmd.abs =-cmd.abs,
                            PWM_RANGE_MAX - cmd.mid
                        ) :
                            cmd.mid - PWM_RANGE_MIN
                    );

                    axis== YAW ? ({
                        register int16_t const
                        deadband = cmd.cfg->yaw_deadband;

                        deadband < cmd.abs ?
                            (float)(
                                (   cmd.cfg->yaw_control_reversed== cmd.sgn ?
                                        cmd.abs - deadband : deadband - cmd.abs
                                ) * (int32_t)PWM_RANGE_2
                            ) / (deadband - cmd.mid)
                        :
                            false;
                    }) : ({
                        register int16_t const
                        deadband =
#ifdef USE_ALT_HOLD
                            axis== THROTTLE && FLIGHT_MODE(BARO_MODE) ?
                                cmd.cfg->alt_hold_deadband
                            :
#endif
                            cmd.cfg->deadband;

                        deadband < cmd.abs ?
                            (float)(
                                (   cmd.sgn ?
                                        cmd.abs - deadband : deadband - cmd.abs
                                ) * (int32_t)PWM_RANGE_2
                            ) / (deadband - cmd.mid)
                        :
                            false;
                    });
                }),
                    axis
                )
            );

            pt1FilterLevel(axis + rcCommandFilter) + command;
        })
    ) NOOP

      applyPolarExpo();

      applyRollYawMix();

    if (rxConfig()->showAlteredRc) for (
        register ptrdiff_t axis = XYZ_AXIS_COUNT; axis--;
    )
        rcData[axis] = ISRVALUE({
            register float const
            midrc = rxConfig()->midrc;

            axis!= YAW || rcControlsConfig()->yaw_control_reversed ?
                midrc + axis[rcCommand] : midrc - axis[rcCommand];
        });
    register int32_t
    tmp = constrain(
        feature(FEATURE_3D) ?
            rcData[THROTTLE] - PWM_RANGE_MIN
        :
            rquot(
                (rcData[THROTTLE] - (tmp = rxConfig()->mincheck)) * PWM_RANGE
            ,
                 PWM_RANGE_MAX    -  tmp
            )
    ,
        false
    ,
        PWM_RANGE
    );
    if (getLowVoltageCutoff()->enabled) {
        tmp = rquot(getLowVoltageCutoff()->percentage * tmp, 100l);
    }
    rcCommand[THROTTLE] = rcLookupThrottle(tmp);
    if (feature(FEATURE_3D) && !failsafeIsActive()) {
        if (!flight3DConfig()->switched_mode3d) {
            if (IS_RC_MODE_ACTIVE(BOX3D)) {
                fix12_t throttleScaler = qConstruct(rcCommand[THROTTLE] - 1000, 1000);
                rcCommand[THROTTLE] = rxConfig()->midrc + qMultiply(throttleScaler, PWM_RANGE_MAX - rxConfig()->midrc);
            }
        } else {
            if (IS_RC_MODE_ACTIVE(BOX3D)) {
                reverseMotors = true;
                fix12_t throttleScaler = qConstruct(rcCommand[THROTTLE] - 1000, 1000);
                rcCommand[THROTTLE] = rxConfig()->midrc + qMultiply(throttleScaler, PWM_RANGE_MIN - rxConfig()->midrc);
            } else {
                reverseMotors = false;
                fix12_t throttleScaler = qConstruct(rcCommand[THROTTLE] - 1000, 1000);
                rcCommand[THROTTLE] = rxConfig()->midrc + qMultiply(throttleScaler, PWM_RANGE_MAX - rxConfig()->midrc);
            }
        }
    }
    isRXDataNew = true;
}

void resetYawAxis(void) {
    rcCommand[YAW] = 0;
    setptRate[YAW].curr =
    setptRate[YAW].prev = false;
}

bool isMotorsReversed(void) {
    return reverseMotors;
}

void initRcProcessing(void) {
    for (int i = 0; i < THROTTLE_LOOKUP_LENGTH; i++) {
        const int16_t tmp = 10 * i - currentControlRateProfile->thrMid8;
        uint8_t y = 1;
        if (tmp > 0)
            y = 100 - currentControlRateProfile->thrMid8;
        if (tmp < 0)
            y = currentControlRateProfile->thrMid8;
        lookupThrottleRC[i] = 10 * currentControlRateProfile->thrMid8 + tmp * (100 - currentControlRateProfile->thrExpo8 + (int32_t) currentControlRateProfile->thrExpo8 * (tmp * tmp) / (y * y)) / 10;
        lookupThrottleRC[i] = PWM_RANGE_MIN + (PWM_RANGE_MAX - PWM_RANGE_MIN) * lookupThrottleRC[i] / 1000; // [MINTHROTTLE;MAXTHROTTLE]
    }
    switch (currentControlRateProfile->rates_type) {
    case RATES_TYPE_BETAFLIGHT:
        applyRates = applyBetaflightRates;
        break;
    case RATES_TYPE_RACEFLIGHT:
        applyRates = applyRaceFlightRates;
        break;
    case RATES_TYPE_KISS:
        applyRates = applyKissRates;
        break;
    default:
        applyRates = applyActualRates;
        break;
    }
    for (
        register ptrdiff_t
        axis = XYZ_AXIS_COUNT - 1;
        axis[setptRate].min = REINTERPRET_CAST(
            sig_atomic_t, applyRates(axis,-true, true)
        ),
        axis[setptRate].max = REINTERPRET_CAST(
            sig_atomic_t, applyRates(axis, true, true)
        ),
        axis--;
    ) NOOP
    interpolationChannels = 0;
    switch (rxConfig()->rcInterpolationChannels) {
    case INTERPOLATION_CHANNELS_RPYT:
        interpolationChannels |= THROTTLE_FLAG;
        FALLTHROUGH;
    case INTERPOLATION_CHANNELS_RPY:
        interpolationChannels |= YAW_FLAG;
        FALLTHROUGH;
    case INTERPOLATION_CHANNELS_RP:
        interpolationChannels |= ROLL_FLAG | PITCH_FLAG;
        break;
    case INTERPOLATION_CHANNELS_RPT:
        interpolationChannels |= ROLL_FLAG | PITCH_FLAG;
        FALLTHROUGH;
    case INTERPOLATION_CHANNELS_T:
        interpolationChannels |= THROTTLE_FLAG;
        break;
    }
}

bool rcSmoothingIsEnabled(void) {
    return
#if defined(USE_RC_SMOOTHING_FILTER)
        rxConfig()->rc_smoothing_type!= RC_SMOOTHING_TYPE_INTERPOLATION ||
#endif
        rxConfig()->rcInterpolation!= RC_SMOOTHING_OFF;
}

#ifdef USE_RC_SMOOTHING_FILTER
int rcSmoothingGetValue(int whichValue) {
    switch (whichValue) {
    case RC_SMOOTHING_VALUE_INPUT_ACTIVE:
        return rcSmoothingData.inputCutoffFrequency;
    case RC_SMOOTHING_VALUE_DERIVATIVE_ACTIVE:
        return rcSmoothingData.derivativeCutoffFrequency;
    case RC_SMOOTHING_VALUE_AVERAGE_FRAME:
        return rcSmoothingData.averageFrameTimeUs;
    default:
        return 0;
    }
}

bool rcSmoothingInitializationComplete(void) {
    return (rxConfig()->rc_smoothing_type != RC_SMOOTHING_TYPE_FILTER) || rcSmoothingData.filterInitialized;
}

#endif // USE_RC_SMOOTHING_FILTER

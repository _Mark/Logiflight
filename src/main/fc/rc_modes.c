/*
 * This file is part of Cleanflight and Betaflight.
 *
 * Cleanflight and Betaflight are free software. You can redistribute
 * this software and/or modify this software under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Cleanflight and Betaflight are distributed in the hope that they
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.
 *
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include "platform.h"

#include "common/bitarray.h"
#include "common/maths.h"

#include "drivers/time.h"

#include "config/feature.h"

#include "fc/config.h"
#include "fc/rc_controls.h"

#include "io/piniobox.h"

#include "pg/pg.h"
#include "pg/pg_ids.h"
#include "pg/rx.h"

#include "rx/rx.h"

#include "rc_modes.h"

#define STICKY_MODE_BOOT_DELAY_US 5e6

PG_REGISTER_ARRAY(modeActivationCondition_t, MAX_MODE_ACTIVATION_CONDITION_COUNT, modeActivationConditions,
                  PG_MODE_ACTIVATION_PROFILE, 1);

       FAST_RAM_ZERO_INIT boxBitmask_t rcModeActivationMask;
static FAST_RAM_ZERO_INIT boxBitmask_t stickyModesEverDisabled;

bool isRangeActive(uint8_t auxChannelIndex, const channelRange_t *range) {
    if (!IS_RANGE_USABLE(range)) {
        return false;
    }
    const uint16_t channelValue = constrain(rcData[auxChannelIndex + NON_AUX_CHANNEL_COUNT], CHANNEL_RANGE_MIN, CHANNEL_RANGE_MAX - 1);
    return (channelValue >= 900 + (range->startStep * 25) &&
            channelValue < 900 + (range->endStep * 25));
}

void updateMasksForMac(const modeActivationCondition_t *mac, boxBitmask_t *andMask, boxBitmask_t *newMask) {
    bool bAnd = (mac->modeLogic == MODELOGIC_AND) || bitArrayGet(andMask, mac->modeId);
    bool bAct = isRangeActive(mac->auxChannelIndex, &mac->range);
    if (bAnd)
        bitArraySet(andMask, mac->modeId);
    if (bAnd != bAct)
        bitArraySet(newMask, mac->modeId);
}

void updateMasksForStickyModes(const modeActivationCondition_t *mac, boxBitmask_t *andMask, boxBitmask_t *newMask) {
    if (IS_RC_MODE_ACTIVE(mac->modeId)) {
        bitArrayClr(andMask, mac->modeId);
        bitArraySet(newMask, mac->modeId);
    } else {
        if (bitArrayGet(&stickyModesEverDisabled, mac->modeId)) {
            updateMasksForMac(mac, andMask, newMask);
        } else {
            if (micros() >= STICKY_MODE_BOOT_DELAY_US && !isRangeActive(mac->auxChannelIndex, &mac->range)) {
                bitArraySet(&stickyModesEverDisabled, mac->modeId);
            }
        }
    }
}

void updateActivatedModes(void) {
    rcModeActivationMask = ISRVALUE({
        union {
            boxBitmask_t raw[3];

          __PACKED_STRUCT {
                boxBitmask_t sticky, and, new;
            };
        } modeMask;

        bzero(modeMask.raw, sizeof modeMask.raw);

        bitArraySet((void *)modeMask.sticky.bits, BOXPARALYZE);

        for (register ptrdiff_t i = MAX_MODE_ACTIVATION_CONDITION_COUNT; i--;) {
            register modeActivationCondition_t const * restrict const
            PTR = modeActivationConditions(i);

            if (PTR->linkedTo) NOOP
            else if (bitArrayGet(modeMask.sticky.bits, PTR->modeId))
                updateMasksForStickyModes(
                    PTR, (void *)modeMask.and.bits, (void *)modeMask.new.bits
                );
            else if (PTR->modeId < CHECKBOX_ITEM_COUNT)
                updateMasksForMac(
                    PTR, (void *)modeMask.and.bits, (void *)modeMask.new.bits
                );
        }

        bitArrayXor(
            (void *)modeMask.new.bits
        ,
            sizeof modeMask.new.bits
        ,
            (void *)modeMask.new.bits
        ,
            (void *)modeMask.and.bits
        );

        for (register ptrdiff_t i = MAX_MODE_ACTIVATION_CONDITION_COUNT; i--;) {
            register modeActivationCondition_t const * restrict const
            PTR = modeActivationConditions(i);

            if (PTR->linkedTo)
                bitArrayCopy(
                    (void *)modeMask.new.bits, PTR->linkedTo, PTR->modeId
                );
        }

        modeMask.new;
    });

#ifdef USE_PINIOBOX
    pinioBoxTaskControl();
#endif
}

bool isModeActivationConditionPresent(register boxId_e const modeId) {
    for (register ptrdiff_t i = MAX_MODE_ACTIVATION_CONDITION_COUNT; i--;) {
        register modeActivationCondition_t const * restrict const
        mac = modeActivationConditions(i);

        if (mac->modeId == modeId && (IS_RANGE_USABLE(&mac->range) || mac->linkedTo)) {
            return true;
        }
    }
    return false;
}

void removeModeActivationCondition(const boxId_e modeId) {
    unsigned offset = 0;
    for (unsigned i = 0; i < MAX_MODE_ACTIVATION_CONDITION_COUNT; i++) {
        modeActivationCondition_t *mac = modeActivationConditionsMutable(i);
        if (mac->modeId == modeId && !offset) {
            offset++;
        }
        if (offset) {
            while (i + offset < MAX_MODE_ACTIVATION_CONDITION_COUNT && modeActivationConditions(i + offset)->modeId == modeId) {
                offset++;
            }
            if (i + offset < MAX_MODE_ACTIVATION_CONDITION_COUNT) {
                memcpy(mac, modeActivationConditions(i + offset), sizeof(modeActivationCondition_t));
            } else {
                memset(mac, 0, sizeof(modeActivationCondition_t));
            }
        }
    }
}

/*
 * This file is part of Cleanflight and Betaflight.
 *
 * Cleanflight and Betaflight are free software. You can redistribute
 * this software and/or modify this software under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Cleanflight and Betaflight are distributed in the hope that they
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.
 *
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "common/time.h"

#define LOOPTIME_SUSPEND_TIME 3  // Prevent too long busy wait times

void taskHandleSerial(timeUs_t);
#ifndef USE_OSD_SLAVE
void taskUpdateRxMain(timeUs_t);
#endif
#ifdef USE_BARO
void taskUpdateBaro(timeUs_t);
#endif
#ifdef USE_CAMERA_CONTROL
void taskCameraControl(timeUs_t);
#endif
void fcTasksInit(void);

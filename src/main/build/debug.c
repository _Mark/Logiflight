/*
 * This file is part of Cleanflight and Betaflight.
 *
 * Cleanflight and Betaflight are free software. You can redistribute
 * this software and/or modify this software under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Cleanflight and Betaflight are distributed in the hope that they
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.
 *
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "build/debug.h"

#ifdef DEBUG_SECTION_TIMES
FAST_RAM_ZERO_INIT timeUs_t     sectionTimes[DEBUG16_VALUE_COUNT][2];
#endif
FAST_RAM_ZERO_INIT sig_atomic_t debugMode;
FAST_RAM_ZERO_INIT int16_t      debug[DEBUG16_VALUE_COUNT];

FAST_RAM char const * const debugModeNames[] = {
    "NONE",
    "SCHEDULER",
    "STACK",
    "CYCLETIME",
    "CORE_TEMP",
    "ESC_SENSOR",
    "ESC_SENSOR_RPM",
    "ESC_SENSOR_TEMP",
    "BATTERY",
    "CURRENT_SENSOR",
    "IMU",
#ifdef USE_DUAL_GYRO
    "DUAL_GYRO",
    "DUAL_GYRO_RAW",
    "DUAL_GYRO_COMBINE",
    "DUAL_GYRO_DIFF",
#else
    "GYRO_RAW",
    "GYRO_SCALED",
    "GYRO_TREND",
#endif
    "ACCELEROMETER",
    "AIRSPEED",
    "ALTITUDE",
#ifdef USE_RANGEFINDER
    "RANGEFINDER",
    "RANGEFINDER_QUALITY",
    "LIDAR_TF",
#endif
    "PIDLOOP",
    "GYRO_FILTERED",
    "ITERM_RELAX",
    "ANGLERATE",
    "SMART_SMOOTHING",
    "PIDSUMS",
#ifdef USE_GYRO_DATA_ANALYSE
    "FFT",
    "FFT_TIME",
    "FFT_FREQ",
#endif
    "MOTOR_SIGNALS",
    "RUNAWAY_TAKEOFF",
    "SBUS",
    "FPORT",
    "RX_FRSKY_SPI",
    "RX_SFHSS_SPI",
    "RX_SIGNAL_LOSS",
#ifdef USE_RC_SMOOTHING_FILTER
    "RC_INTERPOLATION",
    "RC_SMOOTHING",
    "RC_SMOOTHING_RATE",
#endif
    "MAX7456_SIGNAL",
    "MAX7456_SPICLOCK",
    "SDIO",
    "USB",
    "SMARTAUDIO",
#ifdef USE_GPS
    "GNSS",
#endif
    "RTH",
    "MODE_TILT",
    "MODE_THROTTLE"
};

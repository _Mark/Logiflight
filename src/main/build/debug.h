/*
 * This file is part of Cleanflight and Betaflight.
 *
 * Cleanflight and Betaflight are free software. You can redistribute
 * this software and/or modify this software under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Cleanflight and Betaflight are distributed in the hope that they
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.
 *
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#define DEBUG16_VALUE_COUNT 4

#include <signal.h>

#ifndef DEBUG_SECTION_TIMES
#include "platform.h"

#define TIME_SECTION_BEGIN(index)
#define TIME_SECTION_END(index)
#else
#include "common/time.h"

extern timeUs_t sectionTimes[DEBUG16_VALUE_COUNT][2];

#define TIME_SECTION_BEGIN(index) { \
   *(index)[sectionTimes] = micros(); \
}

#define TIME_SECTION_END(index) ISRVALUE({ \
    register __auto_type const\
   _index = (index);\
\
   _index[debug] = (_index[sectionTimes][1] = micros()) -*_index[sectionTimes];\
})
#endif

typedef enum {
    DEBUG_NONE = 0,
    DEBUG_SCHEDULER,
    DEBUG_STACK,
    DEBUG_CYCLETIME,
    DEBUG_CORE_TEMP,
    DEBUG_ESC_SENSOR,
    DEBUG_ESC_SENSOR_RPM,
    DEBUG_ESC_SENSOR_TMP,
    DEBUG_BATTERY,
    DEBUG_CURRENT,
    DEBUG_IMU,
#ifdef USE_DUAL_GYRO
    DEBUG_DUAL_GYRO,
    DEBUG_DUAL_GYRO_RAW,
    DEBUG_DUAL_GYRO_COMBINE,
    DEBUG_DUAL_GYRO_DIFF,
#else
    DEBUG_GYRO_RAW,
    DEBUG_GYRO_SCALED,
    DEBUG_GYRO_TREND,
#endif
    DEBUG_ACCELEROMETER,
    DEBUG_AIRSPEED,
    DEBUG_ALTITUDE,
#ifdef USE_RANGEFINDER
    DEBUG_RANGEFINDER,
    DEBUG_RANGEFINDER_QUALITY,
    DEBUG_LIDAR_TF,
#endif
    DEBUG_PIDLOOP,
    DEBUG_GYRO_FILTERED,
    DEBUG_ITERM_RELAX,
    DEBUG_ANGLERATE,
    DEBUG_SMART_SMOOTHING,
    DEBUG_PIDSUMS,
#ifdef USE_GYRO_DATA_ANALYSE
    DEBUG_FFT,
    DEBUG_FFT_TIME,
    DEBUG_FFT_FREQ,
#endif
    DEBUG_MOTOR_SIGNALS,
    DEBUG_RUNAWAY_TAKEOFF,
    DEBUG_SBUS,
    DEBUG_FPORT,
    DEBUG_RX_FRSKY_SPI,
    DEBUG_RX_SFHSS_SPI,
    DEBUG_RX_SIGNAL_LOSS,
#ifdef USE_RC_SMOOTHING_FILTER
    DEBUG_RC_INTERPOLATION,
    DEBUG_RC_SMOOTHING,
    DEBUG_RC_SMOOTHING_RATE,
#endif
    DEBUG_MAX7456_SIGNAL,
    DEBUG_MAX7456_SPICLOCK,
    DEBUG_SDIO,
    DEBUG_USB,
    DEBUG_SMARTAUDIO,
#ifdef USE_GPS
    DEBUG_GNSS,
#endif
    DEBUG_RTH,
    DEBUG_ANGLE,
    DEBUG_HORIZON,
    DEBUG_COUNT
} debugType_e;

extern sig_atomic_t debugMode;
extern int16_t      debug[DEBUG16_VALUE_COUNT];

#define DEBUG_SET(mode, index, value) (\
    (void)(debugMode== (mode) && ((index)[debug] = (value)))\
)

extern const char * const debugModeNames[DEBUG_COUNT];

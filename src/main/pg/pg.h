/*
 * This file is part of Cleanflight and Betaflight.
 *
 * Cleanflight and Betaflight are free software. You can redistribute
 * this software and/or modify this software under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Cleanflight and Betaflight are distributed in the hope that they
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.
 *
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "build/build_config.h"

typedef uint16_t pgn_t;

// parameter group registry flags
typedef enum {
    PGRF_NONE = 0,
    PGRF_CLASSIFICATON_BIT = (1 << 0)
} pgRegistryFlags_e;

typedef enum {
    PGR_PGN_MASK =          0x0fff,
    PGR_PGN_VERSION_MASK =  0xf000,
    PGR_SIZE_MASK =         0x0fff,
    PGR_SIZE_SYSTEM_FLAG =  0x0000 // documentary
} pgRegistryInternal_e;

typedef struct pgRegistry_s {
    pgn_t pgn;             // The parameter group number, the top 4 bits are reserved for version
    size_t size:16;        // Size of the group in RAM, the top 4 bits are reserved for flags
    uint8_t *address;      // Address of the group in RAM.
    uint8_t *copy;         // Address of the copy in RAM.
    uint8_t **ptr;         // The pointer to update after loading the record into ram.
    union {
        void const *ptr;  // Pointer to init template
        void (*fn)(void *); // Pointer to initializer
    } const reset;
} pgRegistry_t;

#define pgN(reg) (((pgRegistry_t const *)(reg))->pgn & PGR_PGN_MASK)
#define pgVersion(reg) (((pgRegistry_t const *)(reg))->pgn>> '\xC')
#define pgSize(reg) (((pgRegistry_t const *)(reg))->size & PGR_SIZE_MASK)

#define PG_PACKED __attribute__((packed))

#ifdef __APPLE__
extern const pgRegistry_t __pg_registry_start[] __asm("section$start$__DATA$__pg_registry");
extern const pgRegistry_t __pg_registry_end[] __asm("section$end$__DATA$__pg_registry");
#define PG_REGISTER_ATTRIBUTES __attribute__ ((section("__DATA,__pg_registry"), used, aligned(4)))

extern const uint8_t __pg_resetdata_start[] __asm("section$start$__DATA$__pg_resetdata");
extern const uint8_t __pg_resetdata_end[] __asm("section$end$__DATA$__pg_resetdata");
#define PG_RESETDATA_ATTRIBUTES __attribute__ ((section("__DATA,__pg_resetdata"), used, aligned(2)))
#else
extern const pgRegistry_t __pg_registry_start[];
extern const pgRegistry_t __pg_registry_end[];
#define PG_REGISTER_ATTRIBUTES __attribute__ ((section(".pg_registry"), used, aligned(4)))

extern const uint8_t __pg_resetdata_start[];
extern const uint8_t __pg_resetdata_end[];
#define PG_RESETDATA_ATTRIBUTES __attribute__ ((section(".pg_resetdata"), used, aligned(2)))
#endif

#define PG_REGISTRY_SIZE (__pg_registry_end - __pg_registry_start)

// Helper to iterate over the PG register.  Cheaper than a visitor style callback.
#define PG_FOREACH(_name) \
    for (const pgRegistry_t *(_name) = __pg_registry_start; (_name) < __pg_registry_end; _name++)

// Reset configuration to default (by name)
#define PG_RESET(_name)                                         \
{   extern pgRegistry_t const _name ##_Registry[1];\
    pgReset(_name ## _Registry);\
}

// Declare system config
#define PG_DECLARE(_type, _name)                                        \
    extern _type\
   _name ## _System[1],\
   _name ## _Copy;\
   _type * _name ## Mutable(void);\
   _type const * _name(void);

// Declare system config array
#define PG_DECLARE_ARRAY(_type, _size, _name)                           \
    extern _type\
   _name ## _SystemArray[_size],\
   _name ##   _CopyArray[_size];\
   _type * _name ## Mutable(ptrdiff_t);\
   _type const * _name(ptrdiff_t);\
   _type (* _name ## _array(void))[_size];

// Register system config
#define PG_REGISTER_I(_type, _name, _pgn, _version, _reset)             \
    /* Force external linkage for g++. Catch multi registration */      \
    PG_REGISTER_ATTRIBUTES pgRegistry_t const\
   _name ## _Registry[1] = { {\
        .pgn = _pgn | (_version << 12),                                 \
        .size = sizeof(_type) | PGR_SIZE_SYSTEM_FLAG,                   \
        .address = (void *)_name ## _System,\
        .copy = (void *)&_name ## _Copy,\
        _reset,                                                         \
    } };\
    FAST_RAM_ZERO_INIT _type\
   _name ## _System[1],\
   _name ## _Copy;\
    inline _type * _name ## Mutable(void) {\
        return _name ## _System;\
    }\
    inline _type const * _name(void) {\
        return _name ## Mutable();\
    }

#define PG_REGISTER(_type, _name, _pgn, _version)                       \
    PG_REGISTER_I(_type, _name, _pgn, _version, .reset = NOOP)

#define PG_REGISTER_WITH_RESET_FN(_type, _name, _pgn, _version)         \
    extern void pgResetFn_ ## _name(_type *);                           \
    PG_REGISTER_I(\
       _type\
    ,\
       _name\
    ,\
       _pgn\
    ,\
       _version\
    ,.reset =\
        {.fn = (void const *)pgResetFn_ ## _name }\
    )

#define PG_REGISTER_WITH_RESET_TEMPLATE(_type, _name, _pgn, _version)   \
    extern _type const\
    pgResetTemplate_ ## _name[1];\
    PG_REGISTER_I(\
       _type\
    ,\
       _name\
    ,\
       _pgn\
    ,\
       _version\
    ,.reset =\
        {.ptr = pgResetTemplate_ ## _name}\
    )

// Register system config array
#define PG_REGISTER_ARRAY_I(_type, _size, _name, _pgn, _version, _reset)  \
    PG_REGISTER_ATTRIBUTES pgRegistry_t const\
   _name ## _Registry[1] = { {\
        .pgn = _pgn | (_version << 12),                                 \
        .size = (sizeof(_type) * _size) | PGR_SIZE_SYSTEM_FLAG,         \
        .address = (void *)_name ## _SystemArray,\
        .copy = (void *)_name ## _CopyArray,\
        _reset,                                                         \
    } };\
    FAST_RAM_ZERO_INIT _type\
   _name ## _SystemArray[_size],\
   _name ##   _CopyArray[_size];\
    inline _type * _name ## Mutable(register ptrdiff_t const _index) {\
        return _index + _name ## _SystemArray;\
    }\
    inline _type const * _name(register ptrdiff_t const _index) {\
        return _name ## Mutable(_index);\
    }\
    inline _type (* _name ## _array(void))[_size] {\
        return (void *)_name ## _SystemArray;\
    }

#define PG_REGISTER_ARRAY(_type, _size, _name, _pgn, _version)            \
    PG_REGISTER_ARRAY_I(_type, _size, _name, _pgn, _version, .reset = NOOP)

#define PG_REGISTER_ARRAY_WITH_RESET_FN(_type, _size, _name, _pgn, _version) \
    extern void pgResetFn_ ## _name(_type *);    \
    PG_REGISTER_ARRAY_I(\
       _type\
    ,\
       _size\
    ,\
       _name\
    ,\
       _pgn\
    ,\
       _version\
    ,.reset =\
        {.fn = (void const *)pgResetFn_ ## _name}\
    )

// Emit reset defaults for config.
// Config must be registered with PG_REGISTER_<xxx>_WITH_RESET_TEMPLATE macro
#define PG_RESET_TEMPLATE(_type, _name, ...)                            \
    PG_RESETDATA_ATTRIBUTES _type const\
    pgResetTemplate_ ## _name[1] = { {__VA_ARGS__ } }

#define CONVERT_PARAMETER_TO_FLOAT(param) ((param) / 1e3f)
#define CONVERT_PARAMETER_TO_PERCENT(param) ((param) / 1e2f)

const pgRegistry_t* pgFind(pgn_t pgn);

bool pgLoad(const pgRegistry_t* reg, const void *from, int size, int version);
int pgStore(const pgRegistry_t* reg, void *to, int size);
void pgResetAll(void);
void pgResetInstance(pgRegistry_t const *, void *);
bool pgResetCopy(void *copy, pgn_t pgn);
void pgReset(const pgRegistry_t* reg);

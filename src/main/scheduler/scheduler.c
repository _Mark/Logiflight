/*
 * This file is part of Cleanflight and Betaflight.
 *
 * Cleanflight and Betaflight are free software. You can redistribute
 * this software and/or modify this software under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Cleanflight and Betaflight are distributed in the hope that they
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.
 *
 * If not, see <http://www.gnu.org/licenses/>.
 */

#define SRC_MAIN_SCHEDULER_C_

#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include "platform.h"

#include "build/build_config.h"
#include "build/debug.h"

#include "cms/cms.h"

#include "scheduler/scheduler.h"

#include "config/config_unittest.h"

#include "common/maths.h"
#include "common/time.h"
#include "common/utils.h"

#include "drivers/time.h"

#include "fc/fc_tasks.h"
#include "fc/fc_core.h"
#include "fc/fc_dispatch.h"

#include "flight/position.h"
#include "flight/mixer.h"

#include "io/beeper.h"
#include "io/dashboard.h"
#include "io/ledstrip.h"
#include "io/osd.h"
#include "io/piniobox.h"
#include "io/rcdevice_cam.h"
#include "io/vtx.h"

#include "rx/rx.h"

#include "sensors/adcinternal.h"
#include "sensors/barometer.h"
#include "sensors/battery.h"
#include "sensors/esc_sensor.h"

#include "telemetry/telemetry.h"

// DEBUG_SCHEDULER, timings for:
// 0 - gyroUpdate()
// 1 - pidController()
// 2 - time spent in scheduler
// 3 - time spent executing check function

FAST_RAM_ZERO_INIT sig_atomic_t
averageSystemLoadPercent, calculateTaskStatistics;

static FAST_RAM_ZERO_INIT cfTask_t *currentTask;

static FAST_RAM_ZERO_INIT int taskQueuePos = 0;
STATIC_UNIT_TESTED FAST_RAM_ZERO_INIT int taskQueueSize = 0;

// No need for a linked list for the queue, since items are only inserted at startup

STATIC_UNIT_TESTED FAST_RAM_ZERO_INIT cfTask_t* taskQueueArray[TASK_COUNT + 1]; // extra item for NULL pointer at end of queue

static FAST_RAM cfTask_t cfTasks[TASK_COUNT] = {
    [TASK_SERIAL] = {
        .taskName = "SERIAL",
        .taskFunc = taskHandleSerial,
#ifdef USE_OSD_SLAVE
        .checkFunc = taskSerialCheck,
        .desiredPeriod = TASK_PERIOD_HZ(30),
        .staticPriority = TASK_PRIORITY_REALTIME,
#else
        .desiredPeriod = TASK_PERIOD_HZ(30),
        .staticPriority = TASK_PRIORITY_LOW,
#endif
    },

    [TASK_BATTERY_ALERTS] = {
        .taskName = "POWER_SOURCE",
        .taskFunc = batteryUpdateStates,
        .desiredPeriod = TASK_PERIOD_HZ(50),
        .staticPriority = TASK_PRIORITY_IDLE,
    },

#ifdef USE_TRANSPONDER
    [TASK_TRANSPONDER] = {
        .taskName = "TRANSPONDER",
        .taskFunc = transponderUpdate,
        .desiredPeriod = TASK_PERIOD_HZ(250),       // 250 Hz, 4ms
        .staticPriority = TASK_PRIORITY_IDLE,
    },
#endif

#ifdef STACK_CHECK
    [TASK_STACK_CHECK] = {
        .taskName = "STACKCHECK",
        .taskFunc = taskStackCheck,
        .desiredPeriod = TASK_PERIOD_HZ(10),          // 10 Hz
        .staticPriority = TASK_PRIORITY_IDLE,
    },
#endif

#ifdef USE_OSD_SLAVE
    [TASK_OSD_SLAVE] = {
        .taskName = "OSD_SLAVE",
        .checkFunc = osdSlaveCheck,
        .taskFunc = osdSlaveUpdate,
        .desiredPeriod = TASK_PERIOD_HZ(30),
        .staticPriority = TASK_PRIORITY_HIGH,
    },

#else

    [TASK_GYROPID] = {
        .taskName = "PID",
#ifdef USE_GPS
        .subTaskName = "IMU/GPS/ATT",
#else
        .subTaskName = "IMU/ATT",
#endif
#ifdef USE_DMA_SPI_DEVICE
        .checkFunc = isDmaSpiDataReady,
#endif
        .staticPriority = TASK_PRIORITY_REALTIME,
        .taskFunc = taskMainPidLoop,
        .desiredPeriod = TASK_GYROPID_DESIRED_PERIOD,
    },

    [TASK_ATTITUDE] = {
        .taskName = "MIX/PWM",
        .taskFunc = taskUpdateMotors,
        // See <https://youtu.be/W1losp4DMoo?t=667>
        .desiredPeriod = TASK_PERIOD_HZ(10),
        .staticPriority = TASK_PRIORITY_MEDIUM_HIGH,
    },

    [TASK_RX] = {
        .taskName = "RX",
        .checkFunc = rxUpdateCheck,
        .taskFunc = taskUpdateRxMain,
        .desiredPeriod = TASK_PERIOD_HZ(160),        // If event-based scheduling doesn't work, fallback to periodic scheduling
        .staticPriority = TASK_PRIORITY_IDLE,
    },

    [TASK_DISPATCH] = {
        .taskName = "DISPATCH",
        .taskFunc = dispatchProcess,
        .desiredPeriod = TASK_PERIOD_HZ(1000),
        .staticPriority = TASK_PRIORITY_HIGH,
    },

#ifdef USE_BEEPER
    [TASK_BEEPER] = {
        .taskName = "BEEPER",
        .taskFunc = beeperUpdate,
        .desiredPeriod = TASK_PERIOD_HZ(5),
        .staticPriority = TASK_PRIORITY_LOW,
    },
#endif

#ifdef USE_BARO
    [TASK_BARO] = {
        .taskName = "BARO",
        .taskFunc = taskUpdateBaro,
        .desiredPeriod = TASK_BARO_DESIRED_PERIOD,
        .staticPriority = TASK_PRIORITY_IDLE,
    },
#endif

#if defined(USE_ACC_SENSOR) || defined(USE_BARO) || defined(USE_GPS)
    [TASK_ALTITUDE] = {
        .taskName = "ALT/VARIO/RTH",
        .taskFunc = calculateEstimatedAltitude,
        .desiredPeriod = TASK_BARO_DESIRED_PERIOD * 2,
        .staticPriority = TASK_PRIORITY_LOW,
    },
#endif

#ifdef USE_DASHBOARD
    [TASK_DASHBOARD] = {
        .taskName = "DASHBOARD",
        .taskFunc = dashboardUpdate,
        .desiredPeriod = TASK_PERIOD_HZ(10),
        .staticPriority = TASK_PRIORITY_LOW,
    },
#endif

#ifdef USE_OSD
    [TASK_OSD] = {
        .taskName = "OSD",
        .taskFunc = osdUpdate,
        .desiredPeriod = TASK_PERIOD_HZ(30),
        .staticPriority = TASK_PRIORITY_LOW,
    },
#endif

#ifdef USE_TELEMETRY
    [TASK_TELEMETRY] = {
        .taskName = "TELEMETRY",
        .taskFunc = telemetryProcess,
        .desiredPeriod = TASK_PERIOD_HZ(250),       // 250 Hz, 4ms
        .staticPriority = TASK_PRIORITY_LOW,
    },
#endif

#ifdef USE_LED_STRIP
    [TASK_LEDSTRIP] = {
        .taskName = "LEDSTRIP",
        .taskFunc = ledStripUpdate,
        .desiredPeriod = TASK_PERIOD_HZ(100),       // 100 Hz, 10ms
        .staticPriority = TASK_PRIORITY_IDLE,
    },
#endif

#ifdef USE_ESC_SENSOR
    [TASK_ESC_SENSOR] = {
        .taskName = "ESC_SENSOR",
        .taskFunc = escSensorProcess,
        .desiredPeriod = TASK_PERIOD_HZ(100),       // 100 Hz, 10ms
        .staticPriority = TASK_PRIORITY_IDLE,
    },
#endif

#ifdef USE_CMS
    [TASK_CMS] = {
        .taskName = "CMS",
        .taskFunc = cmsHandler,
        .desiredPeriod = TASK_PERIOD_HZ(30),
        .staticPriority = TASK_PRIORITY_LOW,
    },
#endif

#ifdef USE_VTX_CONTROL
    [TASK_VTXCTRL] = {
        .taskName = "VTXCTRL",
        .taskFunc = vtxUpdate,
        .desiredPeriod = TASK_PERIOD_HZ(5),          // 5 Hz, 200ms
        .staticPriority = TASK_PRIORITY_IDLE,
    },
#endif

#ifdef USE_RCDEVICE
    [TASK_RCDEVICE] = {
        .taskName = "RCDEVICE",
        .taskFunc = rcdeviceUpdate,
        .desiredPeriod = TASK_PERIOD_HZ(20),
        .staticPriority = TASK_PRIORITY_IDLE,
    },
#endif

#ifdef USE_CAMERA_CONTROL
    [TASK_CAMCTRL] = {
        .taskName = "CAMCTRL",
        .taskFunc = taskCameraControl,
        .desiredPeriod = TASK_PERIOD_HZ(5),
        .staticPriority = TASK_PRIORITY_IDLE
    },
#endif

#ifdef USE_ADC_INTERNAL
    [TASK_ADC_INTERNAL] = {
        .taskName = "ADCINTERNAL",
        .taskFunc = adcInternalProcess,
        .desiredPeriod = TASK_PERIOD_HZ(1),
        .staticPriority = TASK_PRIORITY_IDLE
    },
#endif

#ifdef USE_PINIOBOX
    [TASK_PINIOBOX] = {
        .taskName = "PINIOBOX",
        .taskFunc = pinioBoxUpdate,
        .desiredPeriod = TASK_PERIOD_HZ(20),
        .staticPriority = TASK_PRIORITY_IDLE
    },
#endif

#ifdef USE_BEESIGN
    [TASK_BEESIGN] = {
        .taskName = "BEESIGN",
        .taskFunc = beesignUpdate,
        .desiredPeriod = TASK_PERIOD_HZ(60),
        .staticPriority = TASK_PRIORITY_LOW
    },
#endif

#endif
};

#define getTask(x) ((x) < TASK_COUNT ? (x) + cfTasks : currentTask)

void queueClear(void) {
    bzero(taskQueueArray, sizeof(taskQueueArray));
    taskQueuePos = 0;
    taskQueueSize = 0;
}

bool queueContains(cfTask_t *task) {
    for (int ii = taskQueueSize; ii--;)
        if (taskQueueArray[ii] == task) {
            return true;
        }
    return false;
}

bool queueAdd(cfTask_t *task) {
    if ((taskQueueSize >= TASK_COUNT) || queueContains(task)) {
        return false;
    }
    for (int ii = 0; ii <= taskQueueSize; ++ii) {
        if (taskQueueArray[ii] == NULL || taskQueueArray[ii]->staticPriority < task->staticPriority) {
            memmove(&taskQueueArray[ii + 1], &taskQueueArray[ii], sizeof(task) * (taskQueueSize - ii));
            taskQueueArray[ii] = task;
            ++taskQueueSize;
            return true;
        }
    }
    return false;
}

bool queueRemove(cfTask_t *task) {
    for (int ii = 0; ii < taskQueueSize; ++ii) {
        if (taskQueueArray[ii] == task) {
            memmove(&taskQueueArray[ii], &taskQueueArray[ii + 1], sizeof(task) * (taskQueueSize - ii));
            --taskQueueSize;
            return true;
        }
    }
    return false;
}

/*
 * Returns first item queue or NULL if queue empty
 */
static FAST_CODE_NOINVOKE cfTask_t *queueFirst(void) {
    return (taskQueuePos = false, false)[taskQueueArray];
}

/*
 * Returns next item in queue or NULL if at end of queue
 */
static FAST_CODE_NOINVOKE cfTask_t *queueNext(void) {
    return (++taskQueuePos)[taskQueueArray];
}

#ifndef SKIP_TASK_STATISTICS
#define MOVING_SUM_COUNT 32
timeUs_t checkFuncMaxExecutionTime;
timeUs_t checkFuncTotalExecutionTime;
timeUs_t checkFuncMovingSumExecutionTime;

void getCheckFuncInfo(cfCheckFuncInfo_t *checkFuncInfo) {
    checkFuncInfo->maxExecutionTime = checkFuncMaxExecutionTime;
    checkFuncInfo->totalExecutionTime = checkFuncTotalExecutionTime;
    checkFuncInfo->averageExecutionTime = checkFuncMovingSumExecutionTime / MOVING_SUM_COUNT;
}

void getTaskInfo(cfTaskId_e taskId, cfTaskInfo_t * taskInfo) {
    taskInfo->taskName = cfTasks[taskId].taskName;
    taskInfo->subTaskName = cfTasks[taskId].subTaskName;
    taskInfo->isEnabled = queueContains(&cfTasks[taskId]);
    taskInfo->desiredPeriod = cfTasks[taskId].desiredPeriod;
    taskInfo->staticPriority = cfTasks[taskId].staticPriority;
    taskInfo->maxExecutionTime = cfTasks[taskId].maxExecutionTime;
    taskInfo->totalExecutionTime = cfTasks[taskId].totalExecutionTime;
    taskInfo->averageExecutionTime = cfTasks[taskId].movingSumExecutionTime / MOVING_SUM_COUNT;
    taskInfo->latestDeltaTime = cfTasks[taskId].taskLatestDeltaTime;
}
#endif

void rescheduleTask(cfTaskId_e taskId, timeUs_t newPeriodMicros) {
    getTask(taskId)->desiredPeriod =
        MAX(newPeriodMicros, SCHEDULER_DELAY_LIMIT);
}

void setTaskEnabled(
    register cfTaskId_e const taskId,
    register bool       const doEnable
) {
    register cfTask_t * restrict const
    task = getTask(taskId);

    (task->taskFunc && doEnable ? queueAdd : queueRemove)(task);
}

timeDelta_t getTaskDeltaTime(register cfTaskId_e const taskId) {
    return getTask(taskId)->taskLatestDeltaTime;
}

#ifndef SKIP_TASK_STATISTICS
void schedulerResetTaskStatistics(register cfTaskId_e const taskId) {
    register cfTask_t * restrict const
    task = getTask(taskId);

    task->totalExecutionTime     =
    task->maxExecutionTime       =
    task->movingSumExecutionTime = false;
}

void schedulerResetTaskMaxExecutionTime(register cfTaskId_e const taskId) {
    getTask(taskId)->maxExecutionTime = false;
}
#endif

void schedulerInit(void) {
    calculateTaskStatistics = true;
    queueClear();
    mixerSetTargetLooptime(cfTasks[TASK_ATTITUDE].desiredPeriod);
}

static inline uintmax_t schedulerCalc(
    register cfTask_t const * const task0,
    register cfTask_t const * const task1
) {
    return
    (uintmax_t)task0->waitingPeriod * task1->desiredPeriod - (
        task0->staticPriority < task1->staticPriority
    );
}

FAST_CODE FAST_CODE_NOINLINE void scheduler(void) {
    // Cache currentTime
{   register timeUs_t
#if defined(SKIP_TASK_STATISTICS) &&!defined(SCHEDULER_DEBUG)
    const
#endif
    currentTimeUs = micros();
{
    // The task to be invoked
    register cfTask_t
  * waitingTask = currentTask = queueFirst();

    // Update task dynamic priorities
    do
        // Task has checkFunc - event driven
        if ((   waitingTask->executeNow =
                    (   waitingTask->waitingPeriod =
                            waitingTask->averagePeriod
                                - waitingTask->lastExecutedAt
                                + currentTimeUs
                                + true
                           >> true,
                        waitingTask->waitingPeriod*=
                           !waitingTask->checkFunc ||
                            waitingTask->checkFunc(
                                currentTimeUs, waitingTask->waitingPeriod
                            )
                    )>= waitingTask->desiredPeriod
            ) &&
            (   currentTask->staticPriority ||
               !waitingTask->staticPriority
            ) && (
                (   currentTask->staticPriority &&
                   !waitingTask->staticPriority
                ) || (
                    schedulerCalc(waitingTask, currentTask) >
                    schedulerCalc(currentTask, waitingTask)
                )
            )
        )
            // Increase priority for event driven tasks
            currentTask = waitingTask;
    while ((waitingTask = queueNext()));
}
    if (currentTask->executeNow) {
        currentTask->averagePeriod = currentTask->waitingPeriod;

        // Found a task that should be run
        currentTask->taskLatestDeltaTime =
            currentTimeUs - currentTask->lastExecutedAt;

        // Execute task
        currentTask->lastExecutedAt = currentTimeUs;
        currentTask->taskFunc(
#if defined(SCHEDULER_DEBUG) ||!defined(SKIP_TASK_STATISTICS)
            currentTimeUs =
#endif
            micros()
        );

#if defined(SCHEDULER_DEBUG)
        if (debugMode== DEBUG_SCHEDULER)
            debug[2] = currentTimeUs - currentTask->lastExecutedAt;
#endif

#ifndef SKIP_TASK_STATISTICS
        register timeUs_t const
        startTimeUs = micros();

        // Forecast CPU utilization using Brown's simple exponential smoothing
        averageSystemLoadPercent =
            rquot(
                (startTimeUs - currentTimeUs)
                    * LOAD_PERCENTAGE_ONE
                    / currentTask->desiredPeriod
                + averageSystemLoadPercent * (MOVING_SUM_COUNT - 1)
            ,
                MOVING_SUM_COUNT
            );

        if (calculateTaskStatistics)
            currentTask->movingSumExecutionTime+=
                (   (currentTimeUs = startTimeUs - currentTimeUs)
                        > currentTask->maxExecutionTime
                    ?
                        currentTask->maxExecutionTime = currentTimeUs
                    :
                        currentTimeUs
                ) - currentTask->movingSumExecutionTime / MOVING_SUM_COUNT,
            currentTask->totalExecutionTime+= currentTimeUs;
#endif
    }

#ifdef SCHEDULER_DEBUG
    if (debugMode== DEBUG_SCHEDULER)
        debug[3] =
#ifndef SKIP_TASK_STATISTICS
            calculateTaskStatistics ?
                currentTimeUs
            :
#endif
            micros() - currentTimeUs;
#endif
}
    GET_SCHEDULER_LOCALS();
}

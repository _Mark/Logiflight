![Logiflight logo](docs/assets/images/logo.png)

Logiflight is an autopilot derived from [this open-source one](https://github.com/emuflight/EmuFlight), but tailored to [speedway drones](https://youtu.be/JhfOOCX5ts0). You can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.


## Disclaimer

Logiflight is distributed in the hope that it will be useful, but **without any warranty**; without even the implied warranty of **merchantability** or **fitness for a particular purpose**. See [this](http://www.gnu.org/licenses/) for more details.


## Building

See [this](docs/development) for more details.


## Installing & Configuring

Use [this configurator](https://github.com/emuflight/EmuConfigurator/releases) at your own risk.


## Operating

See [this](https://github.com/emuflight/EmuFlight/wiki) for more details. Use [this 22mm strap](https://www.aliexpress.com/item/4000662258520.html), [this battery](https://dxf-hobby.store/products/dxf-6s-22-2v-6500mah-100c-200c-lipo-battery-xt60-t-deans-xt90-ec5-for-fpv-drone-airplane-car-racing-truck-boat-rc-parts?variant=36745294086307), [this solder](https://www.mundorf.com/audio/en/shop/solders/MSolder-SUPREME-SilverGold/?card=3768), [this 3400KV motor](https://brotherhobby.com/speed-shield-22075-v2-motor-p00152p1.html), and [these propellers](https://www.hqprop.com/hq-durable-prop-3x5x3-2cw2ccw-poly-carbonate-p0099.html) at your own risk.


## Contributing

See [this](https://github.com/emuflight/EmuFlight/issues) for more details.
